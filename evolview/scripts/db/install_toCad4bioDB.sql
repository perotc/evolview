## run our evolview in Cad4Bio postgresql 


DROP SCHEMA eview;

CREATE SCHEMA eview AUTHORIZATION role_cad4bio_admin;
  
GRANT USAGE ON SCHEMA eview TO role_cad4bio_admin;
GRANT CREATE ON SCHEMA eview TO role_cad4bio_admin;
GRANT USAGE ON SCHEMA eview TO public;


-- Existing objects 
GRANT USAGE ON SCHEMA  eview TO role_cad4bio_readonly;
GRANT SELECT ON ALL TABLES IN SCHEMA  eview TO role_cad4bio_readonly;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA  eview TO role_cad4bio_readonly;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA  eview TO role_cad4bio_readonly;

-- default for New objects  <-- to be done under postgres and cad4bio_admin
ALTER DEFAULT PRIVILEGES IN SCHEMA eview GRANT SELECT ON TABLES TO
role_cad4bio_readonly;
ALTER DEFAULT PRIVILEGES IN SCHEMA eview GRANT SELECT ON SEQUENCES
TO role_cad4bio_readonly;
ALTER DEFAULT PRIVILEGES IN SCHEMA eview GRANT EXECUTE ON FUNCTIONS
TO role_cad4bio_readonly;


-- Existing objects 
GRANT USAGE ON SCHEMA  eview TO role_cad4bio_write;
GRANT ALL ON ALL TABLES IN SCHEMA  eview TO role_cad4bio_write;
GRANT ALL ON ALL SEQUENCES IN SCHEMA  eview TO role_cad4bio_write;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA  eview TO role_cad4bio_write;

-- default for New objects  <-- to be done under postgres and cad4bio_admin
ALTER DEFAULT PRIVILEGES IN SCHEMA eview GRANT ALL ON TABLES TO
role_cad4bio_write;
ALTER DEFAULT PRIVILEGES IN SCHEMA eview GRANT ALL ON SEQUENCES
TO role_cad4bio_write;
ALTER DEFAULT PRIVILEGES IN SCHEMA eview GRANT EXECUTE ON FUNCTIONS
TO role_cad4bio_write;



