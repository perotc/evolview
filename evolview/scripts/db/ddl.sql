   
--
-- Table structure for table catpanels
--

DROP TABLE IF EXISTS "eview"."catpanels";

CREATE SEQUENCE eview_catpanels_id_seq;
CREATE TABLE "eview"."catpanels" (
  ID bigint PRIMARY KEY NOT NULL DEFAULT NEXTVAL('eview_catpanels_id_seq'),
  TreeID bigint NOT NULL,
  DatasetType varchar(100) DEFAULT NULL,
  Active bool NOT NULL DEFAULT true,
  DateCreated timestamp NOT NULL,
  LastModified timestamp NOT NULL
) 
;

--
-- Table structure for table datasets
--

DROP TABLE IF EXISTS  "eview"."datasets";

CREATE SEQUENCE eview_datasets_id_seq;
CREATE TABLE "eview"."datasets" (
  ID bigint PRIMARY KEY NOT NULL DEFAULT NEXTVAL('eview_datasets_id_seq'),
  TreeID bigint NOT NULL,
  DatasetName varchar(255) NOT NULL,
  Description varchar(255) DEFAULT NULL,
  DateCreated timestamp NOT NULL,
  LastModified timestamp NOT NULL,
  Active bool NOT NULL DEFAULT TRUE,
  rank bigint NOT NULL,
  DatasetContent text NOT NULL,
  DatasetType varchar(100) DEFAULT NULL
) 
;


DROP TABLE IF EXISTS "eview"."events";

CREATE TABLE "eview"."events" (
  eventDate date NOT NULL ,
  eventTitle varchar(100) NOT NULL DEFAULT '',
  eventDescription text
) 
;
--
-- Table structure for table settings
--

DROP TABLE IF EXISTS "eview"."settings";

CREATE TABLE "eview"."settings" (
  UserID bigint PRIMARY KEY NOT NULL,
  key varchar(255) DEFAULT NULL,
  value varchar(255) DEFAULT NULL
) 
;
--
-- Table structure for table treecanvasinfo
--

DROP TABLE IF EXISTS "eview"."treecanvasinfo";

CREATE SEQUENCE eview_treecanvas_id_seq;
CREATE TABLE "eview"."treecanvasinfo" (
  ID bigint PRIMARY KEY NOT NULL DEFAULT NEXTVAL('eview_treecanvas_id_seq'),
  TreeID bigint NOT NULL,
  key varchar(50) NOT NULL,
  value float NOT NULL
) 
;

--
-- Table structure for table trees
--

DROP TABLE IF EXISTS "eview"."trees";

 
CREATE SEQUENCE eview_trees_id_seq; 
CREATE TABLE "eview"."trees"
(
   id bigint PRIMARY KEY NOT NULL DEFAULT NEXTVAL('eview_trees_id_seq'),
   userid bigint NOT NULL,
   projectid bigint NOT NULL,
   treename varchar(255) NOT NULL,
   description varchar(255) DEFAULT NULL,
   datecreated timestamp NOT NULL,
   lastmodified timestamp NOT NULL,
   color varchar(20) DEFAULT 'red' NOT NULL,
   active bool DEFAULT true NOT NULL,
   favorite bool DEFAULT false NOT NULL,
   rank bigint NOT NULL,
   treecontent text NOT NULL,
   treeformat varchar(20) NOT NULL,
   leaftype varchar(20) DEFAULT 'NCBI_GENE' NOT NULL,
   traitsType varchar(20) DEFAULT 'PROTEIN_SEQUENCE' NOT NULL,
   selectedleaves varchar(1000)
)
; 


GRANT ALL ON ALL SEQUENCES IN SCHEMA  eview TO cad4bio_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA  eview TO cad4bio_user;

GRANT ALL ON ALL SEQUENCES IN SCHEMA  public TO cad4bio_admin;
GRANT ALL ON ALL SEQUENCES IN SCHEMA  public TO cad4bio_user;
-- GRANT ALL ON ALL SEQUENCES IN SCHEMA  public TO uixaocsa;


