@echo off



CD /D D:\webdev\tomcat7\bin
Rem call D:\webdev\tomcat7\bin\shutdown.bat


REM  --> script repository
CD /D "%~dp0"

rmdir /s /q  D:\webdev\tomcat7\webapps\ev6
xcopy war D:\webdev\tomcat7\webapps\ev6\  /s /e /y

CD /D D:\webdev\tomcat7\bin
Rem call D:\webdev\tomcat7\bin\startup.bat


pause
