## -- add an extra column --
alter table events add column url varchar(255);
truncate events;
insert into events () values("2011-12-28", "Millstone 1", "First version came online", "http://code.google.com/p/evolgenius/wiki/EvolViewWhatisNew#Build#_200;_milestone_1;_Dec_28,_2012"), ("2012-05-28", "Manuscript accepted by NAR", "accepted", "http://code.google.com/p/evolgenius/wiki/EvolViewWhatisNew#Build#_526;_May_30,_2012");
