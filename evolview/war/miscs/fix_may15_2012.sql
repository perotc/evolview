## -- change the difinition of dataset types --
## -- May 15, 2012 --
ALTER TABLE eviewer_ver1.datasets MODIFY DatasetType enum('SavedViews','Pies','Branch Colors','Leaf Colors','Leaf BK Colors','Strips','Bars','Charts','Protein Domains','Node Aliases','Horizontal Gene Transfers');

