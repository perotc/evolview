package org.evolview.shared;


import com.google.gwt.user.client.rpc.IsSerializable;

public class UserData implements IsSerializable{
    
    private String name = "",
            content = "",
            desc = "",
            color = "",
            typeOrformat = "";
    
    private int db_serial = 0; // dbserial 
    
    private boolean active = false,
            favorite = false;
    
    public UserData(){
        // do nothing
    }
    
    //
    public void setName(String n){
        this.name = n;
    }
    
    public void setDesc(String d){
        this.desc = d;
    }
    
    public void setContent(String c){
        this.content = c;
    }
    
    public void setColor(String c){
        this.color = c;
    }
    
    public void setFavorite(boolean b){
        this.favorite = b;
    }
    
    public void setActive(boolean a){
        this.active = a;
    }
    
    public void setDBSerial(int newid){
        this.db_serial = newid;
    }
    
    public void setTypeOrFormat(String fm){
        this.typeOrformat = fm;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getDesc(){
        return this.desc;
    }
    
    public String getContent(){
        return this.content;
    }
    
    public String getColor(){
        return this.color;
    }
    
    public boolean getActive(){
        return this.active;
    }
    
    public boolean getFavorite(){
        return this.favorite;
    }
    
    public int getDBSerial(){
        return this.db_serial;
    }
    
    public String getTypeOrFormat(){
        return this.typeOrformat;
    }
}
