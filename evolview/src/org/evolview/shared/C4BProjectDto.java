package org.evolview.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

public class C4BProjectDto  implements IsSerializable {

	
	private long idProject;
	
	private String acronyme;
	
	private String name;

	
	
	public C4BProjectDto() {
		super();
	}



	public C4BProjectDto(long idProject, String acronyme, String name) {
		super();
		this.idProject = idProject;
		this.acronyme = acronyme;
		this.name = name;
	}



	public long getIdProject() {
		return idProject;
	}



	public String getAcronyme() {
		return acronyme;
	}



	public String getName() {
		return name;
	}



	public void setIdProject(long idProject) {
		this.idProject = idProject;
	}



	public void setAcronyme(String acronyme) {
		this.acronyme = acronyme;
	}



	public void setName(String name) {
		this.name = name;
	}
	
	
	
	
}
