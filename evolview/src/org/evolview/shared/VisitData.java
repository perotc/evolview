package org.evolview.shared;

import java.util.Date;
import com.google.gwt.user.client.rpc.IsSerializable;

public class VisitData implements IsSerializable {
    private Date date = new Date();
    private int visits = 0;
    private String event = null, url = null;
    public VisitData(){
        
    }
    
    public void setDate( Date d ){
        date = d;
    }
    public void setVisits (int i){
        visits = i;
    }
    public void setEvent( String s ){
        event = s;
    }
    
    public Date getDate(){
        return date;
    }
    public String getEvent(){
        return event;
    }
    public int getVisits(){
        return visits;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
