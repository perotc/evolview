package org.evolview.shared;


import com.google.gwt.user.client.rpc.IsSerializable;

public class UserDataProjectSkeleton implements IsSerializable {
    
    private String projectName = "",
            treeName = "",
            projectColor = "",
            treeFormat = ""; 

    private String leafType;
    
    private String traitsType;
	
    private int projectID = 0,
            treeID = 0;
    
    private boolean favorite = false, tree_active = false;
    
    public UserDataProjectSkeleton(){
    }
    
    public void setProjectName( String pn ){
        this.projectName = pn;
    }
    
    public void setTreeName (String tn){
        this.treeName = tn;
    }
    
    public void setProjectDBSerial( int ps){
        this.projectID = ps;
    }
    
    public void setTreeDBSerial( int ts ){
        this.treeID = ts;
    }
    
    public void setProjectColor (String col){
        this.projectColor = col;
    }
    
    public void setProjectFav( boolean f) {
        this.favorite = f;
    }
    
    public void setTreeActive( boolean b ){
        this.tree_active = b;
    }
    
    public String getProjectName(  ){
        return this.projectName;
    }
    
    public String getTreeName (){
        return this.treeName;
    }
    
    public int getProjectDBSerial (){
        return this.projectID;
    }
    
    public int getTreeDBSerial (){
        return this.treeID;
    }
    
    public String getProjectColor(){
        return this.projectColor;
        
    }
    
    public boolean getFavorite(){
        return this.favorite;
    }
    
    public boolean getTreeActive(){
        return this.tree_active;
    }
    
    public void setTreeFormat(String s){
        this.treeFormat = s;
    }
    
    public String getTreeFormat(){
        return this.treeFormat;
    }

	public String getLeafType() {
		return leafType;
	}

	public void setLeafType(String leafType) {
		this.leafType = leafType;
	}

	public String getTraitsType() {
		return traitsType;
	}

	public void setTraitsType(String traitsType) {
		this.traitsType = traitsType;
	}
    
    
    
}
