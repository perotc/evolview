package org.evolview.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/*
 * Feb 18, 2013 --
 * May 13, 2013 --
 */
public class SignInStatus implements IsSerializable {
	
	private String username = "", email = "", sessionid = "";
	private boolean isSignedIn = false, isTemporaryUser = true;
	
	/**
	 * -1 means no User is authenticated
	 * 
	 */
	private int userID = -1;
	
	public SignInStatus(){
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isSignedIn() {
		return isSignedIn;
	}

	public void setSignedIn(boolean isSignedIn) {
		this.isSignedIn = isSignedIn;
	}

	public boolean isTemporaryUser() {
		return isTemporaryUser;
	}

	public void setTemporaryUser(boolean isTemporaryUser) {
		this.isTemporaryUser = isTemporaryUser;
	} 
	
	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}


}
