package org.evolview.client;

import com.google.gwt.event.shared.EventHandler;

public interface ConfirmationNoHandler  extends EventHandler {
	void onConfirmationNoClicked( ConfirmationNoEvent noEvent );

}
