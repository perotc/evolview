package org.evolview.client;

import java.util.ArrayList;
import java.util.HashMap;

import org.evolview.shared.C4BProjectDto;
import org.evolview.shared.SignInStatus;
import org.evolview.shared.UserData;
import org.evolview.shared.UserDataProjectSkeleton; 

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface IGWTServiceAsync {
	public void isCurrentBrowserSupported( AsyncCallback<Boolean> callback);
//    public void  createAnAccount(Account newuser, AsyncCallback<Account> callback);
//    public void sendEmail(String subject, String content, String emailto, AsyncCallback<Boolean> callback);
//    public void checkAccountStatus(String email, String password, AsyncCallback<Integer> callback);
//    public void activateUserAccount(String email, String pass, String activatecode,  AsyncCallback<Boolean> callback);

    //public void checkSessionIsStillLegal(String sessionID, AsyncCallback<Account> asyncCallback);

    public void processSignIn(String login, String password, AsyncCallback<String> asyncCallback);

    public void updateTreeCanvasInfoOnserver(String sessionid, int dBserialID, String key, float value, AsyncCallback<Boolean> asyncCallback);

    public void changeProjectColor(String sessionID, String projectID, String hexcolor, AsyncCallback<Boolean> asyncCallback);

    public void getBuildNumber(AsyncCallback<String> asyncCallback);

    public void fetchCad4BioProjects( AsyncCallback<ArrayList<C4BProjectDto>> asyncCallback);
	
    public void getProjectSkeletons(String session_id, AsyncCallback<ArrayList<UserDataProjectSkeleton>> asyncCallback);

//    public void addProject(String sessionid, UserData userdata, AsyncCallback<Integer> asyncCallback);

    public void getTreeStringAndFormatByDBSerialID(String sessionid, int treeDBSerial, AsyncCallback<HashMap<String,String>> asyncCallback);

    public void getTreeCanvasInfor(String sessionid, int treeDBSerial, AsyncCallback<HashMap<String, Float>> asyncCallback);

    public void getCatPanelInfo(String sessionid, int treeDBSerial, AsyncCallback<HashMap<String, Boolean>> asyncCallback);

    public void getDatasets(String sessionid, int treeDBSerial, AsyncCallback<ArrayList<UserData>> asyncCallback);

    public void addTree(
			String SessionID, 
			int project_dbserial,
			String treeName,
			String treestring, 
			String treeformat,
			String leafType,
			String traitsType,
			boolean active,
			AsyncCallback<Integer> asyncCallback);

    public void deleteTree(String sessionid, int dBserialID, AsyncCallback<Integer> asyncCallback);

    public void swapDatasetOrdersOnServer(String sessionid, int treeDBSerial, int dataset1, int dataset2, String catpanalName, AsyncCallback<Boolean> asyncCallback);

    public void changeCatpanelActiveStatusOnServer(String sessionid, int treeDBSerial, String datasettype, boolean active, AsyncCallback<Boolean> asyncCallback);
    public void changeDatasetActiveStatusOnServer(String sessionid, int treeDBSerial, String datasetname, String datasettype, Boolean active, AsyncCallback<Boolean> asyncCallback);
    
    public void deleteDataset(String sessionid, int treeDBSerial, String datasetID, String datatype, AsyncCallback<Boolean> asyncCallback);

    public void exportTreeToSVG(String svgcontents, AsyncCallback<String> asyncCallback);

    public void exportTreeImage(String svgfileprefix, String outfileformat, float offsetWidth, float offsetHeight, AsyncCallback<String> asyncCallback);

    public void addDataset(String sessionID, int treeDBSerial, String dataSetName, String dataSetContent, String datasettype, AsyncCallback<Integer> asyncCallback);
    
//    public void resetPasswordByEmail( String email,  AsyncCallback<Boolean> asyncCallback );
    
//    public void changeAccountPassword( String email, String oldpassword, String newpassword, AsyncCallback<Boolean> asyncCallback );
    
//    public void deleteSessionID( String sessionid, AsyncCallback<Boolean> asyncCallback );

    public void changeUserSettings( String sessionid, String key, String value, AsyncCallback<Boolean> asyncCallback );

    public void getUserSettings( String sessionid, AsyncCallback<HashMap<String, String>> asyncCallback);

//    public void getProjectSkeletonsForDemos(AsyncCallback<ArrayList<UserDataProjectSkeleton>> asyncCallback);

    public void changeTreeActiveStatus(String sessionid, int treeDBSerial, AsyncCallback<Boolean> asyncCallback);

    public void exporTreeToTextFile( String treestr, String format, AsyncCallback<String> asyncCallback );

//    public void getVisits(AsyncCallback<ArrayList<VisitData>> asyncCallback);

//    public void deleteEmptyProject(String sessionID, int dBserialID, AsyncCallback<Integer> asyncCallback);
    
    public void getProteinDomainFromPFAM( ArrayList<String> leafnames, AsyncCallback<HashMap<String, ArrayList<String>>> asyncCallback );
    
    public void changeTreeContents( String sessionid, int treeDBSerial, String treestring, AsyncCallback<Boolean> asyncCallback );
    
    // Feb 18, 2013 --
	void checkSignInStatus(String sessionid, AsyncCallback<SignInStatus> callback);
	void signInAsTemporaryUser(AsyncCallback<String> callback);
//	void createNewAccount(String sessionid, String firstname, String lastname, String institute, String email, String password, boolean bTransferTempData, AsyncCallback<Integer> callback);
//	void socialSignInSignUp(SocialUser user, String oaProvider, AsyncCallback<SignInStatus> callback);


	////////////////////////////////////  Leaf selection

	  public void  treeLeafSelection( String sessionid, int treeDBSerial, String leaf, AsyncCallback<Boolean> asyncCallback );
	  public void  treeLeafUnSelection( String sessionid, int treeDBSerial, String leaf, AsyncCallback<Boolean> asyncCallback );

}
