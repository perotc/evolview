package org.evolview.client;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlexTable;

public class Table extends FlexTable {
    public Table() {
        super();
        sinkEvents(Event.ONMOUSEOVER | Event.ONMOUSEOUT);
    }

    @Override
    public void onBrowserEvent(Event event) {
        Element td = getEventTargetCell(event);
        if (td == null) {
            return;
        }
        Element tr = DOM.getParent(td);
        switch (DOM.eventGetType(event)) {
            case Event.ONMOUSEOVER: {
                DOM.setStyleAttribute(tr, "backgroundColor", "lightyellow");
                break;
            }
            case Event.ONMOUSEOUT: {
                DOM.setStyleAttribute(tr, "backgroundColor", "#ffffff");
                break;
            }
        }
    }
}
