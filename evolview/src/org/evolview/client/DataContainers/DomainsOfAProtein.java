package org.evolview.client.DataContainers;

import java.util.ArrayList;


/**
 *
 * @author E.T.
 * Created: May 10, 2012
 * Last Modified: May 10, 2012
 */
public class DomainsOfAProtein {
    
    /**
     * ********************************************
     * private variables
     * ********************************************
     */
    private int protein_length = 0;
    private ArrayList<ProteinDomain> domains = new ArrayList<ProteinDomain>();
    
    /**
     * ********************************************
     * constructors
     * ********************************************
     */
    
    public DomainsOfAProtein(){
        
    }

    /**
     * ********************************************
     * public methods
     * ********************************************
     */
    public void addDomain( ProteinDomain pd ){
        if( !domains.contains( pd ) ){
            domains.add(pd);
        }
    }
    
    /**
     * @return the protein_length
     */
    public int getProtein_length() {
        return protein_length;
    }

    /**
     * @param protein_length the protein_length to set
     */
    public void setProtein_length(int protein_length) {
        this.protein_length = protein_length;
    }

    /**
     * @return the domains
     */
    public ArrayList<ProteinDomain> getDomains() {
        return domains;
    }

    /**
     * @param domains the domains to set
     */
    public void setDomains(ArrayList<ProteinDomain> domains) {
        this.domains = domains;
    }
}
