package org.evolview.client.DataContainers;

import com.google.gwt.core.client.JsArrayString;

import java.util.ArrayList;
import java.util.HashMap;

import org.evolview.client.utils.JSFuncs;

/**
 * @author E.T. 
 * handle all modifiers  
 * Created on May 9, 2012 
 * --  May 10, 2012 ~ May 18, 2012 --
 * move all modifiers to here
 * 
 * --- May 30, 2013 --
 * add more modifiers
 * try to make modifier names easier to understand ...
 * 
 * 
 */
public class DataContainerParserBase {

	// basic information
	private String datasetID = "", description = "", // description of this piechart data
			original_datastring = "", title = "", // NOTE: title will be used as title in legends, if empty, the datasetID will be used instead 
			legendstyle = "", // legend style
			error_messages = "", lineStyle = "";
	private boolean isUserInputOpacity = false, valid = true, showLegends = true, // May 24, 2011; default is true
			active = true, showDomainNames = false, showGridLabel = false, plotgrid = false, showShadow = false,

			/**
			 * ********************************************
			 *   -- for strip plot ; may 30, 2013 --
			 * ******************************************
			 */
			isStripHeightPXSet = false,
			
			/**
			 * ********************************************
			 *   for bar plots 
			 * ******************************************
			 */
			fanplot = false, showvalue = false, alignindividualcolumn = false, isItemHeightPCTProvided = false, // may 30, 2013
			isItemHeightPXProvided = false,
			/**
			 * ********************************************
			 * for pie only 
			 * ******************************************
			 */
			byarea = true; // April 20, 2012 : for piechart only, the user input n

	private float plotwidth = 100f, // 	pixel width of the dataset on canvas; 100 by default 
			linewidth = 0f, opacity = 1f, // opacity; ranging from 0~1
			maxPixelRadius = 40, minPixelRadius = 10, // use 10; April 20, 2012
			objectRadius = 0, // for colorful objects
			
			/**
			 * heigth of labels , for heatmap at least
			 */
			labelHeight = 14f,
			/**
			 * -- may 30, 2013 --
			 * pixel / percentage height for strip
			 */
			stripHeightPCT = 100,
			stripHeightPX = 20,
			
			/**
			 * may 31, 2013, for color shapes, check, default line width = 2f 
			 */
			checklinewidth = 2f,
			
			/**
			 * apr 29, 2013, may 30, 2013 --
			 * percentage  of available space taken by the bar and pixel height of bar 
			 */
			itemheightPCT = 80, // by default it will take 80% of available height ; may 30, 2013 -- 
			itemHeightPX = 10; 
	
	/**
	 * 	list of group's color codes or names
	 */
	private ArrayList<String> colors = new ArrayList<String>(); 
	
	/**
	 * categories (will be show in legend)
	 */
	private ArrayList<String>groups = new ArrayList<String>(); 
	
	/**
	 * 	shapes to be plotted before the legend texts; default = rect
	 * List of values in : strip,rect,circle,star
	 */
	private ArrayList<String> plottypes = new ArrayList<String>();  
	
	/**
	 * default values for basic shapes
	 */
	private ArrayList<String> defaultStrokeColor;
	private int defaultStrokeWidth;
	
	/**
	 * For heatmap 
	 * numbers to be plotted in the legend next to the color gradient
	 * Also used to define min/max values 
	 */
	private ArrayList<Double> colorgradientMarkLegend;

	
	public DataContainerParserBase() {
	}

	/**
	 * All modifiers must be parsed here
	 * 
	 * @param lineparts
	 * @param lineidx
	 */
	public final void parseModifier(JsArrayString lineparts, int lineidx) {

		if (lineparts.length() > 0) {
			String part1 = lineparts.get(0).trim();
			/**
			 * ***********************************************
			 * legends . *********************************************
			 */
			if (part1.equalsIgnoreCase("!LegendTexts") || part1.equalsIgnoreCase("!Groups") 
					|| part1.equalsIgnoreCase("!heatmapColumnLabels")  || part1.equalsIgnoreCase("!LegendText")) { // case insensitive matching
				if (lineparts.length() >= 2) {
					this.setGroups(JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(lineparts.get(1).trim(), ",")));
				} else {
					this.setError("line " + lineidx + ": error parsing values for " + part1);
					this.setValid(false);
				}
			} else if (part1.equalsIgnoreCase("!LegendColors") || part1.equalsIgnoreCase("!Colors") || part1.equalsIgnoreCase("!colorgradient") || part1.equalsIgnoreCase("!legendColor")) {
				if (lineparts.length() >= 2) {
					colors = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(lineparts.get(1).trim(), ","));
				} else {
					this.setError("line " + lineidx + ": error parsing values for " + part1);
					this.setValid(false);
				}
			} 
			else if (part1.equalsIgnoreCase("!title") || part1.equalsIgnoreCase("!legendTitle") || part1.equalsIgnoreCase("!legend") || part1.equalsIgnoreCase("!legends")) {
				if (lineparts.length() >= 2) {
					title = lineparts.get(1).trim();
				} else {
					this.setError("line " + lineidx + ": error parsing values for " + part1);
					this.setValid(false);
				}
			} else if (part1.equalsIgnoreCase("!legendstyle") || part1.equalsIgnoreCase("!style")) {
				// April 19, 2011; get title for legends
				if (lineparts.length() >= 2) {
					this.legendstyle = lineparts.get(1).trim();
				} else {
					this.setError("line " + lineidx + ": error parsing values for " + part1);
					this.setValid(false);
				}
			} else if (part1.equalsIgnoreCase("!showLegends") || part1.equalsIgnoreCase("!showLegend")) {
				if (lineparts.length() >= 2) {
					// May 20, 2011;
					try {
						this.showLegends = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for " + part1);
						this.setValid(false);
					}
				} else {
					this.showLegends = true;
				}

			}
			/**
			 * Default values
			 * !defaultStrokeWidth	any value >= 0	stroke width that applies to both legends and the chart
			 * !defaultStrokeColor	any valid color name such as red and yellow and html hex color such as #FF00FF	
			 *                      stroke color that applies to both legends and the chart; see more about stroke colors;  
			 */
			 else if (part1.equalsIgnoreCase("!defaultStrokeWidth")) {
				if (lineparts.length() >= 2) {
					this.defaultStrokeWidth = Integer.parseInt(lineparts.get(1).trim());
				} else {
					this.setError("line " + lineidx + ": error parsing values for " + part1);
					this.setValid(false);
				}
			}
			 else if (part1.equalsIgnoreCase("!defaultStrokeColor")) {
				if (lineparts.length() >= 2) {
					this.defaultStrokeColor = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(lineparts.get(1).trim(), ","));
				} else {
					this.setError("line " + lineidx + ": error parsing values for " + part1);
					this.setValid(false);
				}
			}
			/**
			* ***********************************************
			* for color strips / shapes
			* -- May 30, 2013 --
			* ************************************************
			*/
			else if (part1.equalsIgnoreCase("!stripHeightPCT")) {
				try {
					this.setStripHeightPCT(Float.valueOf(lineparts.get(1).trim()).floatValue());
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing value for strip height (!stripHeightPCT);");
				}
			} else if (part1.equalsIgnoreCase("!stripHeightPX")) {
				try {
					this.setStripHeightPX(Float.valueOf(lineparts.get(1).trim()).floatValue());
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing value for strip height (!stripHeightPX);");
				}
			}
			
			/**
				* ***********************************************
				* plot width, height, opacity, shadow, line style.
				* ************************************************
				*/
			else if (part1.equalsIgnoreCase("!plotwidth") || part1.equalsIgnoreCase("!width")) {
				try {
					this.plotwidth = Float.parseFloat(lineparts.get(1).trim());
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing float value for plotwidth!!");
					this.setValid(false);
				}
			}  else if (part1.equalsIgnoreCase("!itemHeightPCT") || part1.equalsIgnoreCase("!barHeightPCT")) {
				try {
					this.setItemHeightPCT(Float.valueOf(lineparts.get(1).trim()).floatValue());
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing value for height of item / bar / protein-domain (!itemHeightPCT)");
				}
			} else if (part1.equalsIgnoreCase("!itemHeightPX") || part1.equalsIgnoreCase("!barHeightPX")  || part1.equalsIgnoreCase("!plotheight") || part1.equalsIgnoreCase("!height") ) {
				try {
					this.setItemHeightPX(Float.valueOf(lineparts.get(1).trim()).floatValue());
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing value for height of item / bar / protein-domain (!itemHeightPX)");
				}
			} else if (part1.equalsIgnoreCase("!opacity") || part1.equalsIgnoreCase("!op")) {
				try {
					this.opacity = Float.valueOf(lineparts.get(1).trim()).floatValue();
					this.setIsUserInputOpacity(true);
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing value for opacity;");
					this.setValid(false);
				}
			} else if (part1.equalsIgnoreCase("!showshadow") || part1.equalsIgnoreCase("!shadow")) {
				if (lineparts.length() >= 2) {
					try {
						this.showShadow = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
						this.setIsUserInputOpacity(true);
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for showShadow;");
					}
				} else {
					this.showShadow = true;
				}

			} else if (part1.equalsIgnoreCase("!showvalue") || part1.equalsIgnoreCase("!shownumbers")) {
				if (lineparts.length() >= 2) {
					try {
						this.showvalue = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for modifier !showvalue;");
					}
				} else {
					this.showvalue = true;
				}
			} else if (part1.equalsIgnoreCase("!linestyle") || part1.equalsIgnoreCase("!lty")) {
				try {
					this.lineStyle = lineparts.get(1).trim();
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing value for line style;");
					this.setValid(false);
				}
			}/**
				* ***********************************************
				* grid, grid interval, axis, axis labels.
				* ************************************************
				*/
			else if (part1.equalsIgnoreCase("!grid") || part1.equalsIgnoreCase("!showGrid")) {
				if (lineparts.length() >= 2) {
					try {
						this.plotgrid = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for showGrid;");
					}
				} else {
					this.plotgrid = true;
				}

			} else if (part1.equalsIgnoreCase("!axis") || part1.equalsIgnoreCase("!gridlabel") || part1.equalsIgnoreCase("!showGridLabel")) {
				if (lineparts.length() >= 2) {
					try {
						this.showGridLabel = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for showGridLabel;");
					}
				} else {
					this.showGridLabel = true;
				}
			}/**
				* ***********************************************
				* pie chart specific - radiuses.
				* ************************************************
				*/
			else if (part1.equalsIgnoreCase("!minradius")) {
				try {
					minPixelRadius = Float.parseFloat(lineparts.get(1).trim());
				} catch (Exception e) {
					this.setError("line " + lineidx + ": error parsing float value for minRadius!!");
					this.setValid(false);
				}
			} else if (part1.equalsIgnoreCase("!maxradius")) {
				try {
					maxPixelRadius = Float.parseFloat(lineparts.get(1).trim());
				} catch (Exception e) {
					this.setError("line " + lineidx + ":error parsing float value for maxRadius!!");
					this.setValid(false);
				}
			} else if (part1.equalsIgnoreCase("!area") || part1.equalsIgnoreCase("!byarea")) {
				if (lineparts.length() >= 2) {
					try {
						this.byarea = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for byarea!!");
						this.setValid(false);
					}
				} else {
					this.byarea = true;
				}
			} else if (part1.equalsIgnoreCase("!radius") || part1.equalsIgnoreCase("!radii") || part1.equalsIgnoreCase("!radiuses") || part1.equalsIgnoreCase("!byradius")
					|| part1.equalsIgnoreCase("!byradii") || part1.equalsIgnoreCase("!byradiuses")) {
				if (lineparts.length() >= 2) {
					try {
						this.byarea = Integer.parseInt(lineparts.get(1).trim()) > 0 ? false : true;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for " + part1);
						this.setValid(false);
					}
				} else {
					this.byarea = false;
				}
			}/**
				* ***********************************************
				* bar chart specific.
				* ************************************************
				*/
			else if (part1.equalsIgnoreCase("!fanplot") || part1.equalsIgnoreCase("!fan")) {
				if (lineparts.length() >= 2) {
					try {
						this.fanplot = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for modifier !fanplot / !fan");
						this.setValid(false);
					}
				} else {
					this.fanplot = true;
				}
			} else if (part1.equalsIgnoreCase("!alignIndividualColumn") || part1.equalsIgnoreCase("!align")) {
				if (lineparts.length() >= 2) {
					try {
						this.alignindividualcolumn = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + ": error parsing value for " + part1);
						this.setValid(false);
					}
				} else {
					this.alignindividualcolumn = true;
				}
			}/**
				* ***********************************************
				* protein domain chart specific.
				* ************************************************
				*/
			else if (part1.equalsIgnoreCase("!showdomainname") || part1.equalsIgnoreCase("!showdomainnames")) {
				if (lineparts.length() >= 2) {
					try {
						this.showDomainNames = Integer.parseInt(lineparts.get(1).trim()) > 0 ? true : false;
					} catch (Exception e) {
						this.setError("line " + lineidx + " error parsing value for " + part1);
					}
				} else {
					this.showDomainNames = true;
				}

			}
			/**
			* ************************************************
			* heatmap. 
			* ************************************************
			*/
			else if (part1.equalsIgnoreCase("!colorgradientMarkLabel") || part1.equalsIgnoreCase("!colorgradientMarkLegend") ) { 
				if (lineparts.length() >= 2) {
					this.colorgradientMarkLegend  = JSFuncs.JsArrayStringToArrayDouble(JSFuncs.splitStringBySeperator(lineparts.get(1).trim(), ","));
				} else {
					this.setError("line " + lineidx + ": error parsing " + part1);
				}
			} 
			else if (part1.equalsIgnoreCase("!labelHeight")  ) { 
				if (lineparts.length() >= 2) {
					this.labelHeight  = Float.valueOf(lineparts.get(1).trim()).floatValue();
				} else {
					this.setError("line " + lineidx + ": error parsing " + part1);
				}
			} 
			
			/**
			* ************************************************
			* color objects. 
			* ************************************************
			*/
			else if (part1.equalsIgnoreCase("!type") || part1.equalsIgnoreCase("!shape") || part1.equalsIgnoreCase("!types") || part1.equalsIgnoreCase("!shapes")) {
				if (lineparts.length() >= 2) {
					this.plottypes = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(lineparts.get(1).trim(), ","));
				} else {
					this.setError("line " + lineidx + ": error parsing " + part1);
				}
			} 
			// -- May 31, 2013 --
			else if (part1.equalsIgnoreCase("!checkLineWidth") || part1.equalsIgnoreCase("!checklwd")) {
				if (lineparts.length() >= 2) {
					setCheckLineWidth (Float.valueOf(lineparts.get(1).trim()).floatValue() );
				} else {
					this.setError("line " + lineidx + ": error parsing check line width ");
				}
			}
			
		}
	}
	
	
	/**
	 * may 31, 2013
	 */
	public float getCheckLineWidth(  ){
		return this.checklinewidth;
	}
	
	private void setCheckLineWidth( float floatvalue ){
		if( floatvalue > 0 ){
			this.checklinewidth = floatvalue;
		}
	}
	
	/**
	 * may 30, 2013
	 */
	private void setStripHeightPX(float floatValue) {
		if( floatValue > 0 ){
			this.stripHeightPX = floatValue;
			this.isStripHeightPXSet = true;
		}
	}
	
	private void setStripHeightPCT(float floatValue) {
		if (floatValue > 100) {
			floatValue = 100;
		} else if (floatValue < 1) {
			floatValue *= 100;
		}
		this.stripHeightPCT = floatValue;
	}
	
	public float getStripPixelHeight( float availableHeightPx ) {
		if (this.isStripHeightPXSet) {
			return availableHeightPx == 0 ? this.stripHeightPX : (stripHeightPX > availableHeightPx ? availableHeightPx : stripHeightPX);
		} else {
			float stripHeight = this.stripHeightPCT * availableHeightPx / 100f;
			return availableHeightPx == 0 ? stripHeight : (stripHeight > availableHeightPx ? availableHeightPx : stripHeight);
		}
	}
	
	public float getShapePixelHeight(float availableHeightPx) {
		if (this.isItemHeightPXProvided) {
			return availableHeightPx == 0 ? this.itemHeightPX : ( itemHeightPX > availableHeightPx ? availableHeightPx : itemHeightPX );
		} else {
			float itemHeight = this.itemheightPCT * availableHeightPx / 100f;
			return availableHeightPx == 0 ? this.itemHeightPX : (itemHeight > availableHeightPx ? availableHeightPx : itemHeight);
		}
	}
	
	/**
	 * -- May 30, 2013 --
	 * calculate pixel item height according to available height
	 * rules:
	 * 1. by default itemHeightPX = 20 is used
	 * 2. if user input both itemHeightPX and itemHeightPCT, the larger value will be used
	 * 3. however, the larger value cannot be larger than availableHeightPx
	 * @param availableHeightPx
	 * @return
	 */
	public float getItemPixelHeight(float availableHeightPx) {
		if (this.isItemHeightPCTProvided) {
			float itemHeight = this.itemheightPCT * availableHeightPx / 100f;
			return availableHeightPx == 0 ? this.itemHeightPX : (itemHeight > availableHeightPx ? availableHeightPx : itemHeight);
		} else {
			return availableHeightPx == 0 ? this.itemHeightPX : ( itemHeightPX > availableHeightPx ? availableHeightPx : itemHeightPX );
		}
	}

	// -- May 30, 2013 --
	private void setItemHeightPX(float floatValue) {
		if( floatValue > 0 ){
			this.itemHeightPX = floatValue;
			isItemHeightPXProvided = true;
		}
	}

	public void setItemHeightPCT(float barheightPCT) {
		if (barheightPCT > 100) {
			barheightPCT = 100;
		} else if (barheightPCT < 1) {
			barheightPCT *= 100;
		}
		this.itemheightPCT = barheightPCT;
		this.isItemHeightPCTProvided = true;
	}

	/**
	 * @return the datasetID
	 */
	public String getDatasetID() {
		return datasetID;
	}

	/**
	 * @param datasetID the datasetID to set
	 */
	public void setDatasetID(String datasetID) {
		this.datasetID = datasetID;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the original_datastring
	 */
	public String getOriginal_datastring() {
		return original_datastring;
	}

	/**
	 * @param original_datastring the original_datastring to set
	 */
	public void setOriginal_datastring(String original_datastring) {
		this.original_datastring = original_datastring;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return this.title.isEmpty() ? this.datasetID : this.title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the legendstyle
	 */
	public String getLegendstyle() {
		return (!this.legendstyle.isEmpty() && (this.legendstyle.equalsIgnoreCase("circle") || this.legendstyle.equalsIgnoreCase("star"))) ? this.legendstyle : "rect";
	}

	/**
	 * @param legendstyle the legendstyle to set
	 */
	public void setLegendstyle(String legendstyle) {
		this.legendstyle = legendstyle;
	}

	/**
	 * @return the isUserInputOpacity
	 */
	public boolean isIsUserInputOpacity() {
		return isUserInputOpacity;
	}

	/**
	 * @param isUserInputOpacity the isUserInputOpacity to set
	 */
	public void setIsUserInputOpacity(boolean isUserInputOpacity) {
		this.isUserInputOpacity = isUserInputOpacity;
	}

	/**
	 * @return the valid
	 */
	public boolean isValid() {
		return valid;
	}

	// 
	public boolean getDataStatus() {
		return valid;
	}

	public boolean isDataValid() {
		return valid;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * @return the showLegends
	 */
	public boolean isShowLegends() {
		return showLegends;
	}

	/**
	 * @param showLegends the showLegends to set
	 */
	public void setShowLegends(boolean showLegends) {
		this.showLegends = showLegends;
	}

	/**
	 * @return the colors
	 */
	public ArrayList<String> getColors() {
		return colors;
	}

	/**
	 * @param newcolors the colors to set
	 */
	public void setColors(ArrayList<String> newcolors) {
		this.colors = newcolors;
	}

	public void setLegendColors(ArrayList<String> newcolors) {
		this.colors = newcolors;
	}
 
	public HashMap<String, String> getGroup2ColorHash() {
		HashMap<String, String> cat2col = new HashMap<String, String>();
		for (int idx = 0; idx < colors.size(); idx++) {
			cat2col.put(groups.get(idx), colors.get(idx));
		}
		return cat2col;
	}

	/**
	 * @return the groups
	 */
	public ArrayList<String> getGroups() {
		return groups;
	}

	public int getGroupsCount() {
		return groups.size();
	}

	public String getGroupByIndex(int index) {
		return groups.get(index);
	}

	/*
	 * ***********************************************
	 *
	 * May 11, 2012: if groups_size and colors_size are both 0, match is also
	 * true
	 *
	 * ***********************************************
	 */
	public boolean isSizesMatchGroupsSizes() {
		return this.groups.size() == 	colors.size();
	}

	// the same as getColors
	public ArrayList<String> getLegendColors() {
		return colors;
	}

	// the same as getGroups
	public ArrayList<String> getLegendTexts() {
		return this.groups;
	}

	/**
	 * @return the plotwidth
	 */
	public float getPlotwidth() {
		return plotwidth;
	}

	/**
	 * @param plotwidth the plotwidth to set
	 */
	public void setPlotwidth(float plotwidth) {
		this.plotwidth = plotwidth;
	}

	/**
	 * @return the linewidth
	 */
	public float getLinewidth() {
		return linewidth;
	}

	/**
	 * @param linewidth the linewidth to set
	 */
	public void setLinewidth(float linewidth) {
		this.linewidth = linewidth;
	}

	/**
	 * @return the opacity
	 */
	public float getOpacity() {
		return opacity;
	}

	/**
	 * @param op the opacity to set
	 */
	public void setOpacity(float op) {
		if (op > 1) {
			op = 1;
		} else if (op < 0) {
			op = 0;
		}
		this.opacity = op;
	}

	/**
	 * @return the error_messages
	 */
	public String getErrorMessage() {
		return error_messages;
	}

	/**
	 * @param error_messages the error_messages to set
	 */
	public void addError_message(String err) {
		this.error_messages = err;
	}

	public void setError(String err) {
		this.error_messages = err;
		this.valid = false;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active && valid;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	public void deactivateDataSet() {
		this.active = false;
	}

	/**
	 * @return the showDomainNames
	 */
	public boolean isShowDomainNames() {
		return showDomainNames;
	}

	/**
	 * @param showDomainNames the showDomainNames to set
	 */
	public void setShowDomainNames(boolean showDomainNames) {
		this.showDomainNames = showDomainNames;
	}

	/**
	 * @return the showGridLabel
	 */
	public boolean isShowGridLabel() {
		return showGridLabel;
	}

	/**
	 * @param showGridLabel the showGridLabel to set
	 */
	public void setShowGridLabel(boolean showGridLabel) {
		this.showGridLabel = showGridLabel;
	}

	/**
	 * @return the plotgrid
	 */
	public boolean isPlotgrid() {
		return plotgrid;
	}

	/**
	 * @param plotgrid the plotgrid to set
	 */
	public void setPlotgrid(boolean plotgrid) {
		this.plotgrid = plotgrid;
	}

	/**
	 * @return the showShadow
	 */
	public boolean isShowShadow() {
		return showShadow;
	}

	/**
	 * @param showShadow the showShadow to set
	 */
	public void setShowShadow(boolean showShadow) {
		this.showShadow = showShadow;
	}

	/**
	 * @return the fanplot
	 */
	public boolean isFanplot() {
		return fanplot;
	}

	/**
	 * @param fanplot the fanplot to set
	 */
	public void setFanplot(boolean fanplot) {
		this.fanplot = fanplot;
	}

	/**
	 * @return the showvalue
	 */
	public boolean isShowvalue() {
		return showvalue;
	}

	/**
	 * @param showvalue the showvalue to set
	 */
	public void setShowvalue(boolean showvalue) {
		this.showvalue = showvalue;
	}

	/**
	 * @return the alignindividualcolumn
	 */
	public boolean isAlignindividualcolumn() {
		return alignindividualcolumn;
	}

	/**
	 * @param alignindividualcolumn the alignindividualcolumn to set
	 */
	public void setAlignindividualcolumn(boolean alignindividualcolumn) {
		this.alignindividualcolumn = alignindividualcolumn;
	}

	/**
	 * @return the byarea
	 */
	public boolean isByarea() {
		return byarea;
	}

	/**
	 * @param byarea the byarea to set
	 */
	public void setByarea(boolean byarea) {
		this.byarea = byarea;
	}

	/**
	 * @return the maxPixelRadius
	 */
	public float getMaxPixelRadius() {
		return maxPixelRadius;
	}

	/**
	 * @param maxPixelRadius the maxPixelRadius to set
	 */
	public void setMaxPixelRadius(float maxPixelRadius) {
		this.maxPixelRadius = maxPixelRadius;
	}

	/**
	 * @return the minPixelRadius
	 */
	public float getMinPixelRadius() {
		return minPixelRadius;
	}

	/**
	 * @param minPixelRadius the minPixelRadius to set
	 */
	public void setMinPixelRadius(float minPixelRadius) {
		this.minPixelRadius = minPixelRadius;
	}
 

	/**
	 * @return the objectRadius
	 */
	public float getObjectRadius() {
		return objectRadius;
	}
 
	public float getLabelHeight() {
		return labelHeight;
	}

	public void setLabelHeight(float labelHeight) {
		this.labelHeight = labelHeight;
	}

	/**
	 * @return the plottypes
	 */
	public ArrayList<String> getPlottypes() {
		return plottypes;
	}

	/**
	 * @return the lineStyle
	 */
	public String getLineStyle() {
		return lineStyle;
	}

	/**
	 * @param groups the groups to set
	 */
	public void setGroups(ArrayList<String> groups) {
		this.groups = groups;
	}

	public ArrayList<String> getDefaultStrokeColor() {
		return defaultStrokeColor;
	}

	public void setDefaultStrokeColor(ArrayList<String> defaultStrokeColor) {
		this.defaultStrokeColor = defaultStrokeColor;
	}

	public int getDefaultStrokeWidth() {
		return defaultStrokeWidth;
	}

	public void setDefaultStrokeWidth(int defaultStrokeWidth) {
		this.defaultStrokeWidth = defaultStrokeWidth;
	}

	public ArrayList<Double> getColorgradientMarkLegend() {
		return colorgradientMarkLegend;
	}

	public void setColorgradientMarkLegend(ArrayList<Double> colorgradientMarkLegend) {
		this.colorgradientMarkLegend = colorgradientMarkLegend;
	}
	

}
