package org.evolview.client.DataContainers;

import java.util.ArrayList;

/*
 * public class to hold color information for branch length 
 */
public class ColorEx {

    private ArrayList<String> nodeIDs = null;
    private ArrayList<String> colors = new ArrayList<String>();
    private String mode = "";

    public ColorEx(ArrayList<String> newnodeIDs, String newcolor, String newmode) {
        nodeIDs = newnodeIDs;
        colors.add(newcolor);
        mode = newmode;
    }
    
    public ColorEx(ArrayList<String> newnodeIDs, ArrayList<String> newcolors, String newmode) {
        nodeIDs = newnodeIDs;
        for(String color : newcolors){
            colors.add(color);
        }
        mode = newmode;
    }

    public ArrayList<String> getNodeIDs() {
        return this.nodeIDs;
    }

    public String getColor() {
        return this.colors.get(0);
    }

    public String getMode() {
        return this.mode;
    }
    
    public ArrayList<String> getColors(){
        return this.colors;
    }
    
} //