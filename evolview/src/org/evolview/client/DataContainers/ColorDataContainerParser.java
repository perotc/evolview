package org.evolview.client.DataContainers;

import java.util.ArrayList;

import org.evolview.client.utils.JSFuncs;

import com.google.gwt.core.client.JsArrayString;

/**
*
* March 19, 2011; for color strips
* 
* -- May 30, 2013 --
* NOTE: this class extends DataContainerParserBase, so all changes for additional modifiers 
*   should be done in DataContainerParserBase instead!!!
*/
public class ColorDataContainerParser extends DataContainerParserBase {

   // colorsets;
   private ArrayList<ColorEx> colorsets = new ArrayList<ColorEx>();

   // a constructor does nothing
   public ColorDataContainerParser() {
   }

   // and a constructor does everything
   public ColorDataContainerParser(String datasetid, String datasetContent) {
       this.setDatasetID(datasetid);
       this.setOriginal_datastring(datasetContent);

       /*
        * sample data: @ annotations 
        * !Legends text1,text2,text3,text4
        * !LegendColors color1,color2,color3,color4 !type rect | star |
        * !showLegends 1
        *
        * A,B color mode A color mode B color
        *
        * // NOTE; mode is optional; mode could be one of the following: AD ==
        * all descendents prefix == any label ID start with A (for leaf label)
        * suffix == any label ID end with A (for leaf label) others, to be
        * added
        *
        * March 22, 2011; lines start with '@' will be ignored
        *
        * add other columns
        *
        * March 22, 2011; now #type and color could be multiple, both should
        * seperate with ","
        *
        * Nov 07, 2011; now annotation line starts with #; action line starts
        * with !
        */

       JsArrayString lines = JSFuncs.splitStringByNewLine(datasetContent); // split input data into lines
       for (int idx = 0; idx < lines.length(); idx++) {
           String line = lines.get(idx);
           if (line.trim().length() > 0 && !JSFuncs.matchStringStart(line, "#")) { // March 22, 2011; ignore annotation lines
               JsArrayString lineparts = JSFuncs.splitStringByTab(line); // nov 07, 2011; split by tab; multiple consecutive tabs with be viewed as one
               String part1 = lineparts.get(0).trim();
               // Modifiers -----------------------------
               if (JSFuncs.matchStringStart(part1, "!")) {
                   this.parseModifier(lineparts, idx + 1);
               } 
               //values ---------------------------------
               else if (lineparts.length() >= 2) {
                   String part2 = lineparts.get(1).trim();
                   colorsets.add(new ColorEx(
                           JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(part1, ",")),
                           JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(part2, ",")),
                           lineparts.length() >= 3 ? lineparts.get(2).trim() : ""));
               }
           }

       }// end of for

       /*
        * April 4, 2011; check if current dataset is valid
        */
       if (this.colorsets.isEmpty()) {
           setError("error: at least one valid colorset data should be given");
       }

       if (!this.isSizesMatchGroupsSizes()) {
           setError("error: the numbers of legends and legend colors don't match;"); // TODO: ciculate colors if there are not enough colors
       }
   }// constructor
/*
    * public methods:
    */

   /**
    * @return the colorsets
    */
   public ArrayList<ColorEx> getColorsets() {
       return colorsets;
   }

   
}