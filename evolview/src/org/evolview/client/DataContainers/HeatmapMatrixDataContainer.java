package org.evolview.client.DataContainers;


import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArrayString;
  
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.evolview.client.phyloUtils.PhyloNode;
import org.evolview.client.phyloUtils.PhyloTree;
import org.evolview.client.utils.Color;
import org.evolview.client.utils.JSFuncs;

/**
 *
 * @author E.T.
 */
public class HeatmapMatrixDataContainer extends DataContainerParserBase {
 
    /**
     * keyed by leaf_internal_id  , array of data for piechart, for example [20, 40, 50]
     */
    private final HashMap<String, ArrayList<Float>> row2data = new HashMap<String, ArrayList<Float>>();
    private final HashMap<String, ArrayList<Float>> column2data = new HashMap<String, ArrayList<Float>>();  

	/**
	 *  internal ID 2 Array of colors, each color corresponds a plottype
	 */
	public HashMap<String, ArrayList<String>> row2color = new HashMap<String,ArrayList<String>>();  
    
	 
	
    public HashMap<String, ArrayList<Float>> getRow2data() {
		return row2data;
	} 

	public HashMap<String, ArrayList<Float>> getColumn2data() {
		return column2data;
	}
 

	public HashMap<String, ArrayList<String>> getRow2color() {
		return row2color;
	} 



	/*
     * constructor
     */
    public HeatmapMatrixDataContainer(String newdatasetid, String newdataString, PhyloTree phylotree) {
        this.setDatasetID(newdatasetid);
        this.setActive(true);
        this.setOriginal_datastring(newdataString); 
        this.numericMatrixDataParser( newdataString, phylotree );
    } // end of contructor
     
    

    /** 
     * 
     * @param newdataString
     */
    private void numericMatrixDataParser(String newdataString, PhyloTree phylotree) {
        /*
         * prepare to process input data Nov 07, 2011; now annotation line
         * starts with #; action line starts with !
         */
        JsArrayString lines = JSFuncs.splitStringByNewLine(newdataString); // split input data into lines
        for (int idx = 0; idx < lines.length(); idx++) {
            int current_line_index = idx + 1;
            String line = lines.get(idx);
            if (line.trim().length() > 0 && !JSFuncs.matchStringStart(line, "#")) { // Nov 7, 2011;
                JsArrayString lineparts = JSFuncs.splitStringByTab(line); // nov 07, 2011; split by tab"\t" 
                String part1 = lineparts.get(0).trim();
                
                // modifiers ---------------------------------------
                if (JSFuncs.matchStringStart(part1, "!")) {
                    this.parseModifier(lineparts, current_line_index);
                } 
                // values ------------------------------------------
                else if (lineparts.length() >= 2) {
                	 
                	// fetch node by natural ID
                	PhyloNode node = phylotree.getNodeByID(part1); 
                	if(node == null) {
                		continue;
                	}
                	String internalId = node.getInternalID();
                	
                	
                    String part2 = lineparts.get(1).trim(); 
                    /**
                     *  get array of data
                     */ 
                    ArrayList<Float> ardata = new ArrayList<Float>();
                    for (String strdata : JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(part2, ","))) {
                        try {
                            float fdata = Float.parseFloat(strdata);
                            ardata.add(fdata); 
                        } catch (Exception e) {
                            this.setError("error parsing data at line: " + current_line_index + "; float expected, got " + strdata);
                            this.setValid(false);
                        }
                    }
                    // validate against groups
                    ArrayList<String> groups = this.getGroups();
                    if( groups.isEmpty() ){
                        this.setError("both !groups and !colors are required for this type of dataset, please check your input!!");
                        this.setValid(false);
                    } else {
                        if (ardata.size() != groups.size()) {
                            this.setError("error parsing data at line: " + current_line_index + "; " + groups.size() + " valid values expected, got " + ardata.size());
                            this.setValid(false);
                        }
                    } 
                    this.row2data.put(internalId, ardata);   
                }
            } // end of if line isn't empty
        } // iterate string fragments split from pcdatastring
    
        /*
         * check if there is any valid data
         */
        if (this.row2data.isEmpty()) {
            this.setError("no valid data parsed, please check your input!!");
            this.setValid(false);
        } // if treenodeid2data empty

        if (! this.isValid()) {
        	return;
        }
        ArrayList<Double> gradientLegends = this.getColorgradientMarkLegend();
        if(gradientLegends.size()<2) {
            this.setError("needs two values in !colorgradientMarkLegend definition ");
        	return;
        	
        }

        for(String color : this.getColors()) {
            GWT.log("legendColors : "+ color);
        }
        
        // at the end, iterate row2data and get column2data; March 23, 2011 
        //  More important, build row2Color data ..
        List<Color> awtColorsGradient = new ArrayList<Color>(); 
        for( int iCoupleColor = 1; iCoupleColor<this.getColors().size(); iCoupleColor++) {
        	Color color1 = this.buildColor(this.getColors().get(iCoupleColor-1));
        	Color color2 = this.buildColor(this.getColors().get(iCoupleColor));
            GWT.log(iCoupleColor + " -couple 1+2 : "+ color1.getHexValue() + ", " + color2.getHexValue());
            int steps = 50;
            for (int i = 0; i < steps; i++) {
                float ratio = (float) i / (float) steps;
                int red = (int) (color2.getRed() * ratio + color1.getRed() * (1 - ratio));
                int green = (int) (color2.getGreen() * ratio + color1.getGreen() * (1 - ratio));
                int blue = (int) (color2.getBlue() * ratio + color1.getBlue() * (1 - ratio));
                awtColorsGradient.add(new Color(red, green, blue));
                GWT.log(red + ", " + green + ", " + blue);
            }
        }
        // compute gradient colors ...
        double minValue = gradientLegends.get(0);
        double maxValue = gradientLegends.get(gradientLegends.size()-1); 
        for (Entry<String, ArrayList<Float>> rowEntry : this.row2data.entrySet()) { 
        	String id = rowEntry.getKey();
        	ArrayList<String> rowHtmlColorData = new ArrayList<String>();
        	for(Float rowVal :  rowEntry.getValue()) {
        		Color gradientAwtColor = null;
        		if(rowVal -minValue  < 0.000001) {
        			gradientAwtColor = awtColorsGradient.get(0);
        		}
        		else if(maxValue -rowVal  < 0.000001) {
        			gradientAwtColor = awtColorsGradient.get(awtColorsGradient.size()-1);
        		}
        		else {
        			double ratio = (rowVal - minValue)/(maxValue- minValue);
        			int idx = (int) (ratio * awtColorsGradient.size());
        			gradientAwtColor = awtColorsGradient.get(idx);
                    GWT.log(idx + " : " + gradientAwtColor.getHexValue());
        		}
        		rowHtmlColorData.add(gradientAwtColor.getHexValue());
        	}
			this.row2color.put(id, rowHtmlColorData);
        } 
        
        for (ArrayList<Float> row : this.row2data.values()) { 
            for (int gidx = 0; gidx < this.getGroupsCount(); gidx++) {
                String group = getGroupByIndex(gidx);
                if (!this.column2data.containsKey(group)) {
                    ArrayList<Float> newarr = new ArrayList<Float>();
                    this.column2data.put(group, newarr);
                }
                this.column2data.get(group).add(row.get(gidx));
            }
        }// iterate treenodeid2data; 
	}

	public HashMap<String, ArrayList<Float>> getTreeNodeID2Data() {
        return this.row2data; // note ID is internal ID
    } 
	    

    public HashMap<String, ArrayList<Float>> getColumn2Data() {
        return this.column2data;
    }
 
    
    /**
     * can be either a descriptive string like 'balck', white' ... or a #RGB hexadecimal encoded colr ('#e73507')
     * @param sc
     * @return
     */
    private Color buildColor(String colorStr) { 
    	if(colorStr.startsWith("#")) {
    		return new Color(
    	            Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
    	            Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
    	            Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    	}
    	else {    	
    		return org.evolview.client.utils.Color.fromColorName(colorStr); 
    	} 
    }
}
