package org.evolview.client.DataContainers;


import com.google.gwt.core.client.JsArrayString;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.evolview.client.phyloUtils.PhyloNode;
import org.evolview.client.phyloUtils.PhyloTree;
import org.evolview.client.utils.JSFuncs;

/**
 *
 * @author E.T.
 */
public class NumericMatrixDataContainer extends DataContainerParserBase {

	/**
	 * keyed by internal category
	 */
    private HashMap<String, String> cat2col = new HashMap<String, String>(); // mapping between cat and color
    
    /**
     * Keyed by leaves ID
     * values : array of data for piechart, for example [20, 40, 50]
     */
    private HashMap<String, ArrayList<Float>> row2data = new HashMap<String, ArrayList<Float>>(), // data by row
            column2data = new HashMap<String, ArrayList<Float>>(); // data by column
    /**
     * Keyed by leaves ID
     * values : array of data for piechart, for example [20, 40, 50]
     */
    private HashMap<String, Float> id2sum = new HashMap<String, Float>(),
            id2radius = new HashMap<String, Float>(), // Fix a bug; April 8, 2011
            id2radiusByArea = new HashMap<String, Float>(); //April 20, 2012
            
    /**
     * Nov 5, 2013; after tree is rerooted, the data needs to be re-calculated 
     * because some old tree nodes may be removed and new nodes added
     * without this, some data mapped to internal nodes may cannot be drawn accurately 
     */
    private HashMap<String, ArrayList<String>> id2originalID = new HashMap<String, ArrayList<String>>(); 
    
    // floats
    private float cMaxPixelRadius = 40,
            cMinPixelRadius = 10, // use 10; April 20, 2012
            pixelRadiusPerValue = 0,
            maxPieSum = 0, // specify meaning here  
            minPieSum = 1000000f, // April 8, 2011; bug fix
            gridinterval = 0f, // grid interval

            minValue = 0f,
            maxValue = 0f;
    
    /*
     * constructor
     */
    public NumericMatrixDataContainer(String newdatasetid, String newdataString, final PhyloTree phylotree) {
        this.setDatasetID(newdatasetid);
        this.setActive(true);
        this.setOriginal_datastring(newdataString);
        
        numericMatrixDataParser( newdataString, phylotree );
    } // end of contructor
    
    /**
     * >>>>> created Nov 5, 2013 
     * recalculate internalIDs; this necessary if tree is rerooted ...
     * @param phylotree : the rerooted tree ... 
     */
    public void reMapDataAfterTreeReroot( PhyloTree phylotree ){
    	
    	System.err.println("  --> recalculate some data in NumericMatrixData --> ");
    	/**
    	 * all the following hashMaps will be recalculate
    	 * row2data ( but not 'column2data' ), id2sum, id2radius, id2radiusByArea
    	 */
    	for( Map.Entry<String, ArrayList<String>> entry : id2originalID.entrySet() ){
    		String oldInternalId = entry.getKey();
    		ArrayList<String> ids = entry.getValue();
    		
    		/**
    		 * get internal ID on current phylotree 
    		 */
    		PhyloNode lca = phylotree.getLCA(ids);
            if (lca != null) {
            	String internal_id = lca.getInternalID();
            	if( internal_id != null && !internal_id.isEmpty() ){
            		/**
            		 * if old internal ID does not match to new internal id,
            		 * change values 
            		 */
            		if( !oldInternalId.equals(internal_id) ){
            			row2data.put(internal_id, row2data.get(oldInternalId));
            			id2sum.put(internal_id, id2sum.get(oldInternalId));
            			id2radius.put(internal_id, id2radius.get(oldInternalId));
            			id2radiusByArea.put(internal_id, id2radiusByArea.get(oldInternalId));
            		}
            	}
            }// if lca if not null    		
    	}// entry set
    }
    

    /**
     * >>>>>>> created : April 2012;
     * @param newdataString
     * @param phylotree
     */
    private void numericMatrixDataParser(String newdataString, PhyloTree phylotree) {
        /*
         * prepare to process input data Nov 07, 2011; now annotation line
         * starts with #; action line starts with !
         */
        JsArrayString lines = JSFuncs.splitStringByNewLine(newdataString); // split input data into lines
        for (int idx = 0; idx < lines.length(); idx++) {
            int current_line_index = idx + 1;
            String line = lines.get(idx);
            if (line.trim().length() > 0 && !JSFuncs.matchStringStart(line, "#")) { // Nov 7, 2011;
                JsArrayString lineparts = JSFuncs.splitStringByTab(line); // nov 07, 2011; split by tab"\t" 
                String part1 = lineparts.get(0).trim();
                
                // modifiers ---------------------------------------
                if (JSFuncs.matchStringStart(part1, "!")) {
                    this.parseModifier(lineparts, current_line_index);
                } 
                // values ------------------------------------------
                else if (lineparts.length() >= 2) {
                    String part2 = lineparts.get(1).trim();

                    /*
                     * deal with data
                     */
                    String internal_id = null;
                    ArrayList<String> ids = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(part1, ","));
                    PhyloNode lca = phylotree.getLCA(ids);
                    if (lca != null) {
                        internal_id = lca.getInternalID();
                    }

                    if (internal_id != null) {
                    	/**
                    	 * Nov 5, 2013; keep tracking the internal-id <=> ids relationships
                    	 */
                    	id2originalID.put(internal_id, ids);
                    	
                        /**
                         *  get data
                         */
                        float sum = 0f;
                        ArrayList<Float> ardata = new ArrayList<Float>();
                        for (String strdata : JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(part2, ","))) {
                            try {
                                float fdata = Float.parseFloat(strdata);
                                ardata.add(fdata);
                                sum += fdata;
                            } catch (Exception e) {
                                this.setError("error parsing data at line: " + current_line_index + "; float expected, got " + strdata);
                                this.setValid(false);
                            }
                        }
                        ArrayList<String> groups = this.getGroups();
                        if( groups.isEmpty() ){
                            this.setError("both !groups and !colors are required for this type of dataset, please check your input!!");
                            this.setValid(false);
                        } else {
                            if (ardata.size() != groups.size()) {
                                this.setError("error parsing data at line: " + current_line_index + "; " + groups.size() + " valid values expected, got " + ardata.size());
                                this.setValid(false);
                            }
                        }

                        if (sum >= 0) {
                            row2data.put(internal_id, ardata);
                            id2sum.put(internal_id, sum);
                            if (maxPieSum < sum) {
                                maxPieSum = sum;
                            }
                            if (minPieSum > sum) {
                                minPieSum = sum;
                            }
                        } else {
                            this.setError("error parsing data at line: " + current_line_index + "; " + groups.size() + " valid values expected, got " + ardata.size());
                            this.setValid(false);
                        }
                    } // internal id 
                }
            } // end of if line isn't empty
        } // iterate string fragments split from pcdatastring

        // cat to colors
        if (this.isValid()) {
            if (isSizesMatchGroupsSizes()) {
                cat2col = this.getGroup2ColorHash();
            } else {
                this.setError(" the number of groups (from line: #groups) and colors (from #colors) don't match, please check your input data!");
                this.setValid(false);
            }
        }

        /*
         * check if user input max/ min radius values are valid April 20, 2012:
         * now allow min radius == 0
         */
//        if (userInput_maxRadius > 0 && userInput_minRadius >= 0 && userInput_maxRadius >= userInput_minRadius) {
//            this.maxPixelRadius = userInput_maxRadius;
//            this.minPixelRadius = userInput_minRadius;
//            System.out.println("user input radius used ");
//        } else if (userInput_maxRadius > 0 && userInput_maxRadius > this.minPixelRadius) {
//            this.maxPixelRadius = userInput_maxRadius;
//        } else if (userInput_minRadius >= 0 && userInput_minRadius < this.maxPixelRadius) {
//            this.minPixelRadius = userInput_minRadius;
//            System.out.println("user input radius used ");
//        }
        
        this.cMaxPixelRadius = this.getMaxPixelRadius();
        this.cMinPixelRadius = this.getMinPixelRadius();
        
        if( cMaxPixelRadius <=0 || cMaxPixelRadius < cMinPixelRadius ){
            this.setError("max radius cannot be smaller than min radius, please check your input!!");
            this.setValid(false);
        }

        /*
         * calculate radius values
         */
        if (this.isValid()) {
            caculateRadiusValues();
        }

        /*
         * check if there is any valid data
         */
        if (this.row2data.isEmpty()) {
            this.setError("no valid data parsed, please check your input!!");
            this.setValid(false);
        } // if treenodeid2data empty

        // at the end, iterate treenodeid2data and get group2data; March 23, 2011
        // also calculate the max and min values 
        ArrayList<Float> temp = new ArrayList<Float>();

        if (this.isValid()) {
            for (ArrayList<Float> row : row2data.values()) {
                temp.addAll(row); // 
                for (int gidx = 0; gidx < this.getGroupsCount(); gidx++) {
                    String group = getGroupByIndex(gidx);
                    if (!this.column2data.containsKey(group)) {
                        ArrayList<Float> newarr = new ArrayList<Float>();
                        this.column2data.put(group, newarr);
                    }
                    this.column2data.get(group).add(row.get(gidx));
                }
            }// iterate treenodeid2data;

            if (!temp.isEmpty()) {
                this.minValue = Collections.min(temp);
                this.maxValue = Collections.max(temp);
            } else {
                this.setError(" unknown error, please check your input!!");
                this.setValid(false);
            }
        }
	}

	public HashMap<String, ArrayList<Float>> getTreeNodeID2Data() {
        return this.row2data; // note ID is internal ID
    }

    public float getRadiusByID(String node_id) {
        return this.id2radius.get(node_id);
    }

    public HashMap<String, String> getGroup2Colors() {
        return this.cat2col;
    }

    // April 8, 2011; fix a bug here
    // April 20, 2012: area versus radius
    private boolean caculateRadiusValues() {
        boolean success = false;
        if (maxPieSum >= minPieSum) {// Feb 13, 2012; 
            // calculate
            float pixelAreaPerValue = 0f;
            if (this.cMinPixelRadius == 0) { // if it's default
                pixelRadiusPerValue = this.cMaxPixelRadius / maxPieSum; // important fix a bug Feb 22, 2012
                pixelAreaPerValue = (float) (this.cMaxPixelRadius / Math.sqrt(maxPieSum));
            } else {
                pixelRadiusPerValue = (maxPieSum > minPieSum)
                        ? (this.cMaxPixelRadius - this.cMinPixelRadius) / (maxPieSum - minPieSum) : 0; // important fix a bug Feb 22, 2012
                pixelAreaPerValue = (float) ((maxPieSum > minPieSum)
                        ? (this.cMaxPixelRadius - this.cMinPixelRadius) / (Math.sqrt(maxPieSum) - Math.sqrt(minPieSum)) : 0);
            }

            for (String node_id : this.id2sum.keySet()) {
                float currentSum = id2sum.get(node_id);
                if (this.cMinPixelRadius == 0) {
                    id2radius.put(node_id, currentSum * pixelRadiusPerValue);
                    id2radiusByArea.put(node_id, (float) Math.sqrt(currentSum) * pixelAreaPerValue);
                } else {
                    id2radius.put(node_id, (currentSum - minPieSum) * pixelRadiusPerValue + this.cMinPixelRadius);
                    id2radiusByArea.put(node_id, (float) (Math.sqrt(currentSum) - Math.sqrt(minPieSum)) * pixelAreaPerValue + this.cMinPixelRadius);
                }
            } // April 20, 2012; area versus radius 
            success = true;

        } else {
            this.addError_message(" error, possible reasons: \n"
                    + "a. the largest row sum and the smallest row sum are of the same size, please check your input!!\n"
                    + "b. you chose to plot pies using their areas but the the difference between ");
            this.setValid(false);
            success = false;
        }
        return success;
    }

    public void updateminPixelRadius(float newminPixelRadius) {
        if (newminPixelRadius != this.cMinPixelRadius && newminPixelRadius < this.cMaxPixelRadius) {
            cMinPixelRadius = newminPixelRadius;
            this.caculateRadiusValues();
        }
    }

    public void updatemaxPixelRadius(float newmaxPixelRadius) {
        if (newmaxPixelRadius != this.cMaxPixelRadius && newmaxPixelRadius > this.cMinPixelRadius) {
            cMaxPixelRadius = newmaxPixelRadius;
            this.caculateRadiusValues();
        }
    }

    public void updateMinmaxPixelRadius(float newminPixelRadius, float newmaxPixelRadius) {
        if (!(newmaxPixelRadius == this.cMaxPixelRadius && newminPixelRadius == this.cMinPixelRadius) && newmaxPixelRadius > newminPixelRadius) {
            cMaxPixelRadius = newmaxPixelRadius;
            cMinPixelRadius = newminPixelRadius;

            this.caculateRadiusValues();
        }
    }

    public void updateIncrementalPixelRadiusPerValue(float newIncrementalPixelRadiusPerValue) {
        if (newIncrementalPixelRadiusPerValue != this.pixelRadiusPerValue) {
            float newmaxPixelRadius = this.cMinPixelRadius + (maxPieSum - minPieSum) * newIncrementalPixelRadiusPerValue;
            this.updatemaxPixelRadius(newmaxPixelRadius);
        }
    }
    
    float getPixelRadiusPerValue() {
        return this.pixelRadiusPerValue;
    }

    public float getSumByID(String node_internalID) {
        return this.id2sum.get(node_internalID);
    }

    public HashMap<String, Float> getID2Sum() {
        return this.id2sum;
    }

    public float getGridinterval() {
        float ginterval = this.gridinterval;
        if (ginterval == 0) {
        }
        return ginterval;
    }

    public HashMap<String, ArrayList<Float>> getColumn2Data() {
        return this.column2data;
    }

    public float getMaxValue() {
        return this.maxValue;
    }

    public float getMinValue() {
        return this.minValue;
    }

    // = April 20, 2012 ==
    public float getAreaByID(String node_internalID) {
        return this.id2radiusByArea.get(node_internalID);
    } //
}
