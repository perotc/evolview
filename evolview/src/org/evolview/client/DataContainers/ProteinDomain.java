package org.evolview.client.DataContainers;

/**
*
* @author E.T.
* Created: May 10, 2012
* Last Modified: May 10, 2012
*/
public class ProteinDomain {
   
   /**
    * ********************************************
    * private variables
    * ********************************************
    */
   private int start = 0, stop = 0;
   private String name = "", source = "Pfam-A", color = "", shape = "",accession = "", evalue = "", bitscore = "";
   
   /**
    * ********************************************
    * constructors
    * ********************************************
    */
   public ProteinDomain(){
       
   }
   
   public ProteinDomain( int start, int stop, String name, String source, String color, String shape ){
       this.start = start;
       this.stop = stop;
       this.name = name;
       this.source = source;
       this.color = color;
       this.shape = shape;
   }
   
   public ProteinDomain( int start, int stop, String name ){
       this.start = start;
       this.stop = stop;
       this.name = name;
   }
   
   

   /**
    * ********************************************
    * public functions
    * ********************************************
    */
   
   public boolean isValid(){
       return !( start == 0 || stop == 0 || stop <= start );
   }
   
   /**
    * @return the start
    */
   public int getStart() {
       return start;
   }

   /**
    * @param start the start to set
    */
   public void setStart(int start) {
       this.start = start;
   }

   /**
    * @return the stop
    */
   public int getStop() {
       return stop;
   }

   /**
    * @param stop the stop to set
    */
   public void setStop(int stop) {
       this.stop = stop;
   }

   /**
    * @return the name
    */
   public String getName() {
       return name;
   }

   /**
    * @param name the name to set
    */
   public void setName(String name) {
       this.name = name;
   }

   /**
    * @return the source
    */
   public String getSource() {
       return source;
   }

   /**
    * @param source the source to set
    */
   public void setSource(String source) {
       this.source = source;
   }

   /**
    * @return the color
    */
   public String getColor() {
       return color;
   }

   /**
    * @param color the color to set
    */
   public void setColor(String color) {
       this.color = color;
   }

   /**
    * @return the shape
    */
   public String getShape() {
       return shape;
   }

   /**
    * @param shape the shape to set
    */
   public void setShape(String shape) {
       this.shape = shape;
   }

   /**
    * @return the accession
    */
   public String getAccession() {
       return accession;
   }

   /**
    * @param accession the accession to set
    */
   public void setAccession(String accession) {
       this.accession = accession;
   }

   /**
    * @return the evalue
    */
   public String getEvalue() {
       return evalue;
   }

   /**
    * @param evalue the evalue to set
    */
   public void setEvalue(String evalue) {
       this.evalue = evalue;
   }

   /**
    * @return the bitscore
    */
   public String getBitscore() {
       return bitscore;
   }

   /**
    * @param bitscore the bitscore to set
    */
   public void setBitscore(String bitscore) {
       this.bitscore = bitscore;
   }
   
   // May 14, 2012; check if domain annotations are from pfam-B
   public boolean isPfamB(){
       return source.equalsIgnoreCase("pfam-B");
   }
   
}
