package org.evolview.client.DataContainers;

import java.util.ArrayList;
import java.util.HashMap;

import org.evolview.client.utils.JAVAFuncs;
import org.evolview.client.utils.JSFuncs;
import org.evolview.client.utils.RandomColorNames;

import com.google.gwt.core.client.JsArrayString;

/**
 *
 * @author E.T.: Created: May 10, 2012 Last updated:
 * 
 *  * -- Oct 19, 2013 --
 * fix a problem with parsing domain structures
 * 
 * 
 * 
 * 
 */
public class ProteinDomainDataContainerParser extends DataContainerParserBase {

	/**
	 * ********************************************
	 * private variables ********************************************
	 */
	private HashMap<String, DomainsOfAProtein> gene2domains = new HashMap<String, DomainsOfAProtein>();
	private int maxProteinLength = 0;
	private final int default_plot_width = 300;
	private final RandomColorNames colors = new RandomColorNames();
	private boolean isDataModified = false;

	/**
	 * ********************************************
	 * constructors ********************************************
	 */
	public ProteinDomainDataContainerParser() {
		this.setPlotwidth(default_plot_width);
	}
	
	public ProteinDomainDataContainerParser(String datasetid, String datasetContent) {
		this.setDatasetID(datasetid);
		this.setOriginal_datastring(datasetContent);
		this.setPlotwidth(default_plot_width);

		/*
		 * sample data: # annotations ## NOTE: legends and legend colors will be
		 * ignored if the Legends doesnt match the real domain names !Legends
		 * text1,text2,text3,text4 !LegendColors color1,color2,color3,color4
		 * !showLegends 1 ## show name of domains !showText 1 !showName 1
		 * !showDomainName 1 !shadow 1 !grid !gridlabel
		 *
		 * ## -- plotwidth and height -- ## -- NOTE: height here refers to the
		 * height for a single protein !plotWidth !plotHeigth
		 *
		 * ## -- title of legend -- !title
		 *
		 * ## -- all fields separated by '\t' A length domain_architecture1
		 * domain_architecture2 ... B length
		 *
		 * ## -- typicaly, a domain architecutre contains the following fields
		 * deliniated by ',' ## -- the first two are manditory, other fields can
		 * be missing -- ##
		 * start,end,name,source,shape,color,accession,evalue,bitscore ## for
		 * example: ## 10,20,WD40,,red ## in this case, the shape and source are
		 * missing ## 50,60 ## in this case all the three optional variables are
		 * missing
		 *
		 */
		/**
		 * -- oct 20, 2013 --
		 * split user input into two separate parts, the modifiers, and the data
		 * in case the user groups and colors don't match with the total number of all domains, 
		 * !groups and !colors will be provided by us and appended to the end of modifiers  
		 */
		StringBuilder sbModifiers = new StringBuilder(), sbData = new StringBuilder(); // Oct 20, 2013 

		ArrayList<String> alldomains = new ArrayList<String>();

		JsArrayString lines = JSFuncs.splitStringByNewLine(datasetContent); // split input data into lines
		for (int idx = 0; idx < lines.length(); idx++) {
			int lineSerial = idx + 1;
			String line = lines.get(idx).trim(); // NOTE: string.trim() would remove newlines, tabs and spaces 
			if (line.trim().length() > 0 && !JSFuncs.matchStringStart(line, "#")) { // March 22, 2011; ignore annotation lines
				JsArrayString lineparts = JSFuncs.splitStringByTab(line); // nov 07, 2011; split by tab; multiple consecutive tabs with be viewed as one
				String part1 = lineparts.get(0).trim();

				/**
				 * **************************
				 * if modifiers. 
				 ***************************
				 */
				if (JSFuncs.matchStringStart(part1, "!")) {
					this.parseModifier(lineparts, idx + 1); // all modifiers would be parsed by the parent class 
					
					sbModifiers.append(line).append("\n"); // oct 20, 2013 --
				}
				/**
				* **************************
				* if data : part1 = gene
				*/
				else if (lineparts.length() >= 2) {
					String part2 = lineparts.get(1).trim();
					
					sbData.append(line).append("\n"); // oct 20, 2013 --
					
					/**
					 * ********************************************
					 * now parse the domain architecture
					 * ********************************************
					 */
					try {
						DomainsOfAProtein dap = new DomainsOfAProtein();

						int protein_length = Integer.parseInt(part2.trim());
						dap.setProtein_length(protein_length);

						// set max protein length
						if (maxProteinLength < protein_length) {
							maxProteinLength = protein_length;
						}

						// domains 

						for (int lp = 2; lp < lineparts.length(); lp++) {
							// get a domain architecture and split it into a arraylist
							String darchitString = lineparts.get(lp).trim();
							
							/**
							 * Oct 19, 2013;
							 * darchitString may be surrounded by characters like ' or ", remove them first
							 */
							darchitString = darchitString.replaceAll("'", "").replaceAll("\"", "");
							
							// <<<< oct 19, 2013 --
							
							// split the comain architecture / structure 
							ArrayList<String> domainArchit = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(darchitString, ","));

							int size_arraylist = domainArchit.size();
							if (size_arraylist >= 3) {
								/**
								 * ********************************************
								 * get different parts of a domain ...
								 * ********************************************
								 */
								int domain_start = Integer.parseInt(domainArchit.get(0).trim());
								int domain_end = Integer.parseInt(domainArchit.get(1).trim());
								String domain_name = domainArchit.get(2).trim();

								if (domain_start != 0 && domain_end != 0 && domain_end > domain_start) {
									ProteinDomain adomain = new ProteinDomain(domain_start, domain_end, domain_name);
									if (!alldomains.contains(domain_name)) {
										alldomains.add(domain_name);
									}

									// 0     1   2    3      4     5     6         7      8
									// start,end,name,source,shape,color,accession,evalue,bitscore
									int arrlen = 4;
									if (size_arraylist >= arrlen) {
										adomain.setSource(domainArchit.get(arrlen - 1));
									}

									// shape 
									arrlen++;
									if (size_arraylist >= arrlen) {
										adomain.setShape(domainArchit.get(arrlen - 1));
									}

									// color 
									arrlen++;
									if (size_arraylist >= arrlen) {
										adomain.setColor(domainArchit.get(arrlen - 1));
									}

									// accession 
									arrlen++;
									if (size_arraylist >= arrlen) {
										adomain.setAccession(domainArchit.get(arrlen - 1));
									}

									// evalue 
									arrlen++;
									if (size_arraylist >= arrlen) {
										adomain.setEvalue(domainArchit.get(arrlen - 1));
									}

									// set bitscore
									arrlen++;
									if (size_arraylist >= arrlen) {
										adomain.setBitscore(domainArchit.get(arrlen - 1));
									}

									// at the end, add current domain to DomainsOfAProtein
									dap.addDomain(adomain);

								} else {
									this.setError("line " + lineSerial + " error parsing protein domains\ndomain start/end incorrect : '" + darchitString + "'\n");
									break;
								}
							} else {
								this.setError("line " + lineSerial + " error parsing protein domains\ndomain arcitecture not complete : '" + darchitString + "'\n");
								break;
							}
						}// iterate domain architecture strings

						/**
						 * ********************************************
						 * at the end, add domains of a protein to HashMap ....
						 * ********************************************
						 */
						this.gene2domains.put(part1, dap);

					} catch (Exception e) {
						this.setError("line " + lineSerial + 1 + " error parsing protein domains;");
					}
				}// if length >= 2
			} // end of if line isn't empty and is not annotation line
		} // for

		/**
		 * *********************************************************************
		 * here, I would check if user input groups cover all possible domains,
		 * if not, I'll reset 'groups' and assign random colors to them.
		 * *********************************************************************
		 */
		// get groups from the base class; this important; otherwise
		// the colors will be randomlized each time the domains are loaded
		ArrayList<String> groups = this.getGroups(); // get groups from parent class
		boolean allcovered = true;
		for (String domain : alldomains) {
			if (!groups.contains(domain)) {
				allcovered = false;
				break;
			}
		}

		/**
		 * if not covered ...
		 * get random colors
		 * -- oct 20, 2013 --
		 * modify the original string 
		 */
		if (!allcovered) {
			this.setGroups(alldomains);
			ArrayList<String> cols = colors.getRandomColors(alldomains.size());
			this.setColors( cols );
			
			// oct 20, 2013; create new !groups and !colors and add them to the end of modifiers
			// actually it is also possible to add them to the end of the dataset .... 
			this.isDataModified = true;
			
			StringBuilder sbNewModifiers = new StringBuilder();
			sbNewModifiers.append("##the following modifiers are generated by EvolView!!").append("\n");
			sbNewModifiers.append("!groups").append("\t").append( JAVAFuncs.joinStringBySeparator( alldomains, "," ) ).append("\n");
			sbNewModifiers.append("!colors").append("\t").append( JAVAFuncs.joinStringBySeparator( cols, "," ) ).append("\n");
			sbNewModifiers.append("##<<<< automatically generated modifiers end here!!").append("\n\n");
			
			// assemble a new modifiers + data
			sbModifiers.append( sbNewModifiers.toString() ).append("\n").append( sbData.toString() );
			this.setOriginal_datastring( sbModifiers.toString() );
		} // end 

		// 
		if (!this.isSizesMatchGroupsSizes()) {
			setError("oh. the numbers of legends and legend colors don't match;\nYou can:\n\t1.revise your dataset to make the numbers match;\n\t2.remove legeneds and colors from your dataset\n\t  the system will get those information\n\t  automatically"); // TODO: ciculate colors if there are not enough colors
		} else {
			// add colors to domains
			HashMap<String, String> group2Color = this.getGroup2ColorHash();
			for (DomainsOfAProtein dap : this.gene2domains.values()) {
				for (ProteinDomain pd : dap.getDomains()) {
					if (group2Color.containsKey(pd.getName())) {
						pd.setColor(group2Color.get(pd.getName()));
					}
				}
			}
		} // if dataset valid 
	}// constructor

	/**
	 * @return the gene2domains
	 */
	public HashMap<String, DomainsOfAProtein> getGene2domainsHash() {
		return gene2domains;
	}

	public DomainsOfAProtein getDomainsByGeneID(String geneid) {
		DomainsOfAProtein dap = new DomainsOfAProtein();

		if (this.gene2domains.containsKey(geneid)) {
			dap = gene2domains.get(geneid);
		}

		return dap;
	}

	/**
	 * @return the maxProteinLength
	 */
	public int getMaxProteinLength() {
		return maxProteinLength;
	}

	public boolean isDataModified() {
		return this.isDataModified;
	}
}
