package org.evolview.client.utils;

import com.google.gwt.user.client.Random;
import java.util.ArrayList;
import java.util.Arrays;


/**
 *
 * @author E.T.
 * Created: May 10, 2012
 * Last Modified: May 10, 2012
 */
public class RandomColorNames {
    
    /*
     * in total 146 colors : http://www.w3schools.com/html/html_colornames.asp
     * 'white' and 'black' are removed
     */
    private  ArrayList<String> colors = new  ArrayList<String>(Arrays.asList( "AliceBlue","AntiqueWhite","Aqua","Aquamarine","Azure","Beige","Bisque","BlanchedAlmond","Blue","BlueViolet",
            "Brown","BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Cornsilk","Crimson","Cyan","DarkBlue","DarkCyan","DarkGoldenRod",
            "DarkGray","DarkGrey","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","Darkorange","DarkOrchid","DarkRed","DarkSalmon","DarkSeaGreen",
            "DarkSlateBlue","DarkSlateGray","DarkSlateGrey","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DimGray","DimGrey","DodgerBlue","FireBrick",
            "FloralWhite","ForestGreen","Fuchsia","Gainsboro","GhostWhite","Gold","GoldenRod","Gray","Grey","Green","GreenYellow","HoneyDew","HotPink",
            "IndianRed ","Indigo ","Ivory","Khaki","Lavender","LavenderBlush","LawnGreen","LemonChiffon","LightBlue","LightCoral","LightCyan",
            "LightGoldenRodYellow","LightGray","LightGrey","LightGreen","LightPink","LightSalmon","LightSeaGreen","LightSkyBlue","LightSlateGray",
            "LightSlateGrey","LightSteelBlue","LightYellow","Lime","LimeGreen","Linen","Magenta","Maroon","MediumAquaMarine","MediumBlue","MediumOrchid",
            "MediumPurple","MediumSeaGreen","MediumSlateBlue","MediumSpringGreen","MediumTurquoise","MediumVioletRed","MidnightBlue","MintCream","MistyRose",
            "Moccasin","NavajoWhite","Navy","OldLace","Olive","OliveDrab","Orange","OrangeRed","Orchid","PaleGoldenRod","PaleGreen","PaleTurquoise",
            "PaleVioletRed","PapayaWhip","PeachPuff","Peru","Pink","Plum","PowderBlue","Purple","Red","RosyBrown","RoyalBlue","SaddleBrown","Salmon",
            "SandyBrown","SeaGreen","SeaShell","Sienna","Silver","SkyBlue","SlateBlue","SlateGray","SlateGrey","Snow","SpringGreen","SteelBlue","Tan","Teal",
            "Thistle","Tomato","Turquoise","Violet","Wheat","WhiteSmoke","Yellow","YellowGreen" ));
    private final int ncolors = colors.size();
    
    public RandomColorNames(){
        
    }
    
    public ArrayList<String> getRandomColors( int n ){
        ArrayList<String> cols = new ArrayList<String>();
        
        while( cols.size() < n ){
            String nextcol = colors.get( Random.nextInt( ncolors ) );
            if( !cols.contains( nextcol ) ){
                cols.add( nextcol );
            }
        }
        return cols;
    }// may 14, 2012
    
    public String getARandomColor(){
        return colors.get( Random.nextInt( ncolors ) );
    }
}
