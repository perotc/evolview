package org.evolview.client.utils;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Image;

public class ImgBtn extends Image implements BlurHandler {

	private boolean enabled = true;
	private boolean highlighted = false, mouseover = false, pushbutton = false; //

	private String imageUrlNormal = null;
	private String imageUrlActive = null;
	private String imageUrlDisabled = null;
	private int default_size = 24;

	public ImgBtn(String urlBase, boolean enable) {
		super();

		setUrlBase(urlBase);
		this.initiate(enable);
	}// public constructor 1

	public ImgBtn(String urlBase, boolean enable, boolean push) {
		super();
		this.pushbutton = push;
		setUrlBase(urlBase);
		this.initiate(enable);
	}

	public final void setUrlBase(String urlBase) {
		this.imageUrlNormal = urlBase + "_normal.svg.png";
		this.imageUrlActive = urlBase + "_active.svg.png";
		this.imageUrlDisabled = urlBase + "_disabled.svg.png";
	}

	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean setenable) {
		enabled = setenable;
		this.setUrl(enabled ? this.imageUrlNormal : this.imageUrlDisabled);
	}// April 7, 2011;

	public void setPush(boolean push) {
		this.pushbutton = push;
	}

	public boolean getPushbuttonMode() {
		return this.pushbutton;
	}

	public boolean setSelect(boolean sethighlight) {
		if (enabled) {
			highlighted = this.pushbutton ? false : sethighlight;
			this.setUrl(highlighted || mouseover ? this.imageUrlActive : this.imageUrlNormal); // April 14, 2011;
		}
		return highlighted;
	}

	public boolean getSelect() {
		return this.highlighted;
	}

	private void initiate(boolean enable) {

		this.setPixelSize(default_size, default_size);

		// initial status is disabled/ enabled
		enabled = enable;

		if (enabled == true) {
			this.setUrl(imageUrlNormal);
		} else {
			this.setUrl(imageUrlDisabled);
		}

		// mouse handlers
		this.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				mouseover = true;
				if (enabled == true && highlighted == false) {
					setUrl(imageUrlActive);
				}
			}
		});

		this.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				mouseover = false;
				if (enabled == true && highlighted == false) {
					setUrl(imageUrlNormal);
				}
			}
		});

		this.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (enabled) {
					setSelect(!highlighted);
				}
			}
		}); // April 7, 2011;

	}

	// May not be very useful; Oct 26, 2011;
	@Override
	public void onBlur(BlurEvent event) {
		mouseover = false;
		if (enabled == true && highlighted == false) {
			setUrl(imageUrlNormal);
		}
	}
}