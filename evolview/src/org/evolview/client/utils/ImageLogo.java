package org.evolview.client.utils;
 


import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;

public class ImageLogo extends SimplePanel {
	
	public interface ImageLogoResources extends ClientBundle {
		@Source("resources/ImageLogoSmall.png")
		ImageResource Logo();

		@Source("resources/ImageLogo.css")
		CssResource css();
	}

	private static final ImageLogoResources resources = GWT.create(ImageLogoResources.class);
	private final Image logo = new Image(resources.Logo());
	
	public ImageLogo(  ){
		super();
		resources.css().ensureInjected();
		
		this.setStyleName("evolviewlogo");
		this.setTitle("click to see the stats on monthly visits to EvolView");
		this.setWidget( logo );
		DOM.setStyleAttribute( getElement(), "verticalAlign", "middle");
 
	}// constructor
	
 
	// <<<<<<<<<<<<<<<<<<<<<<<< time line <<<<<<<<<<<<<<<<<<<<<<<<<<<
}
