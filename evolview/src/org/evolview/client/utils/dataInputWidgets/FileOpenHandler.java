package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface FileOpenHandler extends EventHandler {
	void onFileOpenSubmitClicked( FileOpenEvent foEvt );
}
