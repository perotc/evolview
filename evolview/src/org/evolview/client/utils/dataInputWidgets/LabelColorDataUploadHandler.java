package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface LabelColorDataUploadHandler extends EventHandler {
	void onLabelColorDataUploaded( LabelColorDataUploadEvent evnt);
}
