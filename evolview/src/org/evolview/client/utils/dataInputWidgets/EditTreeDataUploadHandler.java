package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface EditTreeDataUploadHandler extends EventHandler {
	void onEditTreeDataUpload( EditTreeDataUploadEvent event );
}
