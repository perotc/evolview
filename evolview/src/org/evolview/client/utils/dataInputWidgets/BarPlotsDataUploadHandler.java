package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface BarPlotsDataUploadHandler extends EventHandler{
	void onBarPlotsDataUpload( BarPlotsDataUploadEvent evnt );
}
