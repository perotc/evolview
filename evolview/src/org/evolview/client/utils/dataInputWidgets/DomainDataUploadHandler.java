package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface DomainDataUploadHandler extends EventHandler {
	void onDomainDataUpload( DomainDataUploadEvent event );
}
