package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface LabelBKDataUploadHandler extends EventHandler {
	void onLabelBKDataUploaded( LabelBKDataUploadEvent event );
}
