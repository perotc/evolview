package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class PieChartDataUploadEvent extends GwtEvent<PieChartDataUploadHandler> {
	
	public static Type<PieChartDataUploadHandler> TYPE = new Type<PieChartDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<PieChartDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(PieChartDataUploadHandler handler) {
		handler.onPieChartDataUploaded( this );
	}
}
