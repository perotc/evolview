package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface PieChartDataUploadHandler extends EventHandler {
	void onPieChartDataUploaded( PieChartDataUploadEvent event );
}
