package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class BranchColorDataUploadEvent extends GwtEvent<BranchColorDataUploadHandler> {
	
	public static Type<BranchColorDataUploadHandler> TYPE = new Type<BranchColorDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<BranchColorDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(BranchColorDataUploadHandler handler) {
		handler.onBranchColorDataUpload( this );
	}

}
