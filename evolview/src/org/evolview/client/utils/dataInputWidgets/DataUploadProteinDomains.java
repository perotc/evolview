package org.evolview.client.utils.dataInputWidgets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.evolview.client.utils.AlertWidget;
import org.evolview.client.utils.AwesomeButton;
import org.evolview.client.utils.JAVAFuncs;
import org.evolview.client.utils.JSFuncs;
import org.evolview.client.utils.MC;
import org.evolview.client.utils.RandomColorNames;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;


/**
 *
 * @author E.T.
 * Created: May 11, 2012
 * Last Modified: May 11, 2012
 */
public class DataUploadProteinDomains extends DataInputWidgetBase {
    
    private ArrayList<String> proteins = new ArrayList<String>();
    private AlertWidget alertWgt = new AlertWidget();
    private SafeHtml warningMsg = null;
    private RandomColorNames colors = new RandomColorNames();
    
    public DataUploadProteinDomains( String title ){
        super( title );
        
        // error message
        SafeHtmlBuilder shb = new SafeHtmlBuilder();
        shb.appendEscaped("!!Warning!!").appendHtmlConstant("<br/>");
        shb.appendEscaped("Cannot retrieve data for one or more proteins.").appendHtmlConstant("<br/>");
        shb.appendEscaped("The corresponding protein ids will appear at the end of the textbox with a length of 0.").appendHtmlConstant("<br/>");
        shb.appendEscaped("You may want to add domain annotations for them manually.").appendHtmlConstant("<br/>");
        
        warningMsg = shb.toSafeHtml();
        
        /*
         * ******************************************
         *  new to this wiget: an awesome button
         *  to allow automatic retrieval of 
         *  protein domains from pfam
         * ******************************************
         */
        HTML copypaste = new HTML("paste your data bellow, or");
        
        AwesomeButton btnDataRetriever = new AwesomeButton("retrieve data from PFAM »", "green");
        btnDataRetriever.setSize("small");
        HTML help = new HTML("<a href=\"http://code.google.com/p/evolgenius/wiki/EvolViewProteinDomain\" target=\"_blank\">help</a>");
        help.addStyleName("marginleft10");
        
        HorizontalPanel hp = new HorizontalPanel();
        hp.add(copypaste);
        hp.add(btnDataRetriever);
        hp.add(help);
        hp.addStyleName("margintopbottom5");
        hp.setSpacing(5);
        
        hp.setCellVerticalAlignment(copypaste, HasVerticalAlignment.ALIGN_MIDDLE);
        hp.setCellVerticalAlignment(help, HasVerticalAlignment.ALIGN_MIDDLE);
        hp.setCellVerticalAlignment(btnDataRetriever, HasVerticalAlignment.ALIGN_MIDDLE);
        
        this.setHomeMadeWidgetForAutomaticDataRetrieval( hp );
        
        /*
         * ******************************************
         *   add click handler to btnDataRetriever
         *   after clicking, send protein IDs to 
         *   server
         * ******************************************
         */
        btnDataRetriever.addClickHandler( new ClickHandler(){

            @Override
            public void onClick(ClickEvent event) {
                /*
                * *****************************************
                *   retrieve data using RPC
                * *****************************************
                */
                MC.alert.setMessageAndShow("retrieving data ... ", AlertWidget.AlertWidgetType.busy);
                
                MC.rpc.getProteinDomainFromPFAM( proteins , new AsyncCallback<HashMap<String, ArrayList<String>>> () {

                    @Override
                    public void onFailure(Throwable caught) {
                        MC.alertFailConnectingServer();
                    }

                    @Override
                    public void onSuccess(HashMap<String, ArrayList<String>> result) {
                        /*
                        * *****************************************
                        *   process retrieved data
                        * *****************************************
                        */
                        ArrayList<String> failedPros = new ArrayList<String>();
                        
                        ArrayList<String> domains = new ArrayList<String>();
                        StringBuilder sb = new StringBuilder();
                        for (String pro : proteins) {
                            if (result.containsKey(pro)) {
                                sb.append(pro); // protein name
                                
                                int idx = 0;
                                for (String d : result.get( pro ) ) {
                                    sb.append("\t").append(d);
                                    
                                    /***************************************
                                     *   obtain a unique set of domain IDs
                                     * *************************************
                                     */
                                    idx ++;
                                    if( idx >= 2 ){
                                        ArrayList<String> piecies = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(d, ","));
                                        if( piecies.size() >=2 ){
                                            String domainid = piecies.get(2);
                                            domainid = domainid.isEmpty() ? "NA" : domainid;
                                            if( !domains.contains(domainid) ){
                                                domains.add(domainid);
                                            }
                                        }
                                    }
                                }
                                sb.append("\n");
                            }else {
                                failedPros.add(pro);
                            }
                        }
                        
                        // at the end, add failed proteins
                        for( String failedpro : failedPros ){
                            sb.append( failedpro ).append("\t0\n");
                        }
                        
                        /*
                        * *****************************************
                        *   if some proteins don't have recoreds
                        *   in pfam, warn the user
                        * *****************************************
                        */
                        if( !failedPros.isEmpty() ){
                            MC.alert.setMessageAndShow(warningMsg, AlertWidget.AlertWidgetType.warning);
                        } else {
                            alertWgt.hide();
                        }
                        
                        /**************************************
                         *   sort domain IDs, assign random colors
                         *   and then add then to the beginning 
                         *   of the dataset
                         * ************************************
                         */
                        Collections.sort(domains);
                        
                        ArrayList<String> cols = colors.getRandomColors( domains.size() );
                        String joinedcols = JAVAFuncs.joinStringBySeparator(cols, ",");
                        String joineddomains = JAVAFuncs.joinStringBySeparator(domains, ",");
                        
                        // assembl --
                        StringBuilder sball = new StringBuilder();
                        sball.append("!groups").append("\t").append(joineddomains).append("\n");
                        sball.append("!colors").append("\t").append(joinedcols).append("\n");
                        sball.append("!showlegend").append("\t").append("1").append("\n");
                        sball.append("!grid").append("\t").append("1").append("\n");
                        sball.append("!gridlabel").append("\t").append("1").append("\n");
                        
                        sball.append( sb.toString() ); // this is important !!
                        
                        // then set data
                        setDataSetContent( sball.toString() );
                    }
                });
            }
        } ); // click handler ends here
        
        this.btnSubmit.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				MC.ebus.fireEvent( new DomainDataUploadEvent());
			}});
        
    } // <<<<<<<<<<<<<<<< constructor <<<<<<<<<<<<<<<<
    
    public void setProteins(ArrayList<String> ps ){
        this.proteins = ps;
    }
    
}
