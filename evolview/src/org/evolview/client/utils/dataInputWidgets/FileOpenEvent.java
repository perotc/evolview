package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class FileOpenEvent extends GwtEvent<FileOpenHandler> {
	
	public static Type< FileOpenHandler > TYPE = new Type<FileOpenHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<FileOpenHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(FileOpenHandler handler) {
		handler.onFileOpenSubmitClicked( this );
	}
}
