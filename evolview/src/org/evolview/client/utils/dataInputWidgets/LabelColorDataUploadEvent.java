package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class LabelColorDataUploadEvent extends GwtEvent<LabelColorDataUploadHandler> {
	
	public static Type<LabelColorDataUploadHandler> TYPE = new Type<LabelColorDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LabelColorDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LabelColorDataUploadHandler handler) {
		handler.onLabelColorDataUploaded(this);
	}

}
