package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class LabelBKDataUploadEvent extends GwtEvent<LabelBKDataUploadHandler> {
	
	public static Type<LabelBKDataUploadHandler> TYPE = new Type<LabelBKDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LabelBKDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LabelBKDataUploadHandler handler) {
		handler.onLabelBKDataUploaded(this);
	}

}
