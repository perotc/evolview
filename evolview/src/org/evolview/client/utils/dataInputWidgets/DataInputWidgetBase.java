package org.evolview.client.utils.dataInputWidgets;


import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader;
import gwtupload.client.IUploader.UploadedInfo;
import gwtupload.client.MultiUploader;

import java.util.ArrayList;

import org.evolview.client.utils.AwesomeButton;
import org.evolview.client.utils.MC;
import org.evolview.shared.C4BProjectDto;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 *
 * @author E.T.
 * >>>> Nov 2, 2013; use eventBus to fire and capture submit events >>>>>
 */
public class DataInputWidgetBase extends DialogBox {
    /*
     * dialog boxes UIs
     *  this dialog box contains the following elements
     *
     *  a label for project name | textbox
     *  a label for name | text box
     *    text area | text area
     *  a label for copy/paste data | text area
     *  a label for "upload" | upload buttons
     *  cancel | submit buttons
     *
     * the widgets will be organized using flextable
     */

//    public final MultiWordSuggestOracle projectOracle = new MultiWordSuggestOracle();
    private final FlexTable flextable = new FlexTable();
    protected final AwesomeButton btnCancel = new AwesomeButton("Cancel", "black"), btnSubmit = new AwesomeButton("Submit","red");
    private final TextBox tbxName = new TextBox();
    private final TextArea txtAreaCopyPaste = new TextArea();
    private final Label proLabel = new Label("Project:");
    private final Label lblTreeFormat = new Label("Format:"), lblName = new Label("Name:");

    private final Label lblTraitsTypeSelection = new Label("Analysis from : ");
    private final Label lblLeafTypeSelection = new Label("Leaf name type: ");
    
//    private final SuggestBox tbxProjectName = new SuggestBox(projectOracle);
    //private final FileUpload fileUpload = new FileUpload();
    private final MultiUploader defaultUploader = new MultiUploader();
    private String filecontent = "";
    private ListBox lboxProjectSelection = new ListBox();
    
    private ListBox lboxTreeFormatSelection = new ListBox();
    
    private ListBox lboxLeafTypeSelection = new ListBox();
    
    private ListBox lboxTraitsTypeSelection = new ListBox();
    
    private final TextArea helpDataContent = new TextArea();
    
    /*
     * ****************************************************
     * 
     *   May 11, 2012
     *   add a simple panel above the txtAreaCopyPaste 
     *   to allow user to add other widgets
     * 
     * ****************************************************
     */
    private SimplePanel sp = new SimplePanel();

    public DataInputWidgetBase(String dboxTitle) {
        super();

        /*
         * assemble the flextable
         */
        flextable.setWidth("700px");
        flextable.setCellSpacing(2);

        // tooltips for textbox/ textarea
        tbxName.setTitle("enter name (not mandatory)");
        txtAreaCopyPaste.setTitle("copy/ paste your data");
        txtAreaCopyPaste.setPixelSize(600, 300);
        txtAreaCopyPaste.getElement().setAttribute("wrap", "off");
        txtAreaCopyPaste.setStyleName("fixwidthfont");// Nov 07, 2011 -- set font fixed-width
        
        // tree format selector : Nov 28, 2011
        lboxTreeFormatSelection.setVisibleItemCount(1);
        lboxTreeFormatSelection.addItem("newick (also phylip)", "newick"); // the sencond value will be returned
        lboxTreeFormatSelection.addItem("nhx", "nhx");
        lboxTreeFormatSelection.addItem("nexus", "nexus"); // Dec 2, 2011 
        lboxTreeFormatSelection.addItem("phyloXML", "phyloXML"); // Dec 2, 2011 
        lboxTreeFormatSelection.setSelectedIndex( 0 ); // start with newick
        

        lboxLeafTypeSelection.setVisibleItemCount(1);
        lboxLeafTypeSelection.addItem("NCBI protein accessiors", "NCBI_PROT"); 
        lboxLeafTypeSelection.addItem("NCBI gene accessiors", "NCBI_GENE");  
        lboxLeafTypeSelection.setSelectedIndex( 0 ); // start with NCBI_PROT


        lboxTraitsTypeSelection.setVisibleItemCount(1);
        lboxTraitsTypeSelection.addItem("Protein sequences", "PROTEIN_SEQUENCE"); 
        lboxTraitsTypeSelection.addItem("Gene sequences", "GENE_SEQUENCE"); 
        lboxTraitsTypeSelection.setSelectedIndex( 0 ); // start with PROTEIN_SEQUENCE
        
  
        MC.rpc.fetchCad4BioProjects( new AsyncCallback<ArrayList<C4BProjectDto>>(){

			@Override
			public void onFailure(Throwable caught) {
				MC.alertFailConnectingServer();
			}

			@Override
			public void onSuccess(ArrayList<C4BProjectDto> result) { 
				for( C4BProjectDto pro : result) {
					lboxProjectSelection.setVisibleItemCount(1);
			        lboxProjectSelection.addItem(pro.getName(), Long.toString(pro.getIdProject()));
			        lboxProjectSelection.setSelectedIndex( 0 );
				}				
			}}); // end of fetchCad4BioProjects
        
        
        
        
        // Capturing Tab key in GWT TextArea; Nov 07, 2011; 
        // borrow somde code from http://albertattard.blogspot.com/2009/11/capturing-tab-key-in-gwt-textarea.html
        txtAreaCopyPaste.addKeyDownHandler(new KeyDownHandler() {

            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeKeyCode() == 9) {
                    
                    // prevent default behaviors 
                    event.preventDefault();
                    event.stopPropagation();
                    
                    // insert tab into userinput
                    int index = txtAreaCopyPaste.getCursorPos();
                    String text = txtAreaCopyPaste.getText();
                    txtAreaCopyPaste.setText(text.substring(0, index)
                            + "\t" + text.substring(index));
                    txtAreaCopyPaste.setCursorPos(index + 1);
                }
            }
        });

        int row = 0;
        // project name
        addRowToFlextable(row, proLabel, lboxProjectSelection);
        proLabel.setVisible(false);
        lboxProjectSelection.setVisible(false);

        // dataset name:
        row ++;
        addRowToFlextable(row, lblName, tbxName);

        // tree format:
        row ++;
        addRowToFlextable(row, lblTreeFormat, lboxTreeFormatSelection);
        lblTreeFormat.setVisible(false);
        lboxTreeFormatSelection.setVisible(false);
 
        // leaf diaplay type
        row ++;
        addRowToFlextable(row, lblLeafTypeSelection, lboxLeafTypeSelection);
        lblLeafTypeSelection.setVisible(false);
        lboxLeafTypeSelection.setVisible(false);

        
        // analysis material/traits type
        row ++;
        addRowToFlextable(row, lblTraitsTypeSelection, lboxTraitsTypeSelection);
        lblTraitsTypeSelection.setVisible(false);
        lboxTraitsTypeSelection.setVisible(false);

         
        
        /*
         * ***************************************************
         * 
         *   May 11, 2012
         *   add a simple panel to the flex table, to allow 
         *   user to expand it
         * 
         * ***************************************************
         */
        VerticalPanel vp = new VerticalPanel();
        vp.add(sp);
        vp.add(txtAreaCopyPaste);
        // data
        row ++;
        addRowToFlextable(row, new Label("Data:"), vp);

        // or upload
        row ++;
        addRowToFlextable(row, new Label("Or upload:"), defaultUploader);
        // cancel
        row ++;
        HorizontalPanel hpButtons = new HorizontalPanel();
        hpButtons.add(btnCancel);
        hpButtons.add(btnSubmit);
        
        hpButtons.setSpacing(5);
        hpButtons.addStyleName("marginright10");
        
        addRowToFlextable(row, new Label(""), hpButtons);
        

        row ++;
        // extra lines to help
        helpDataContent.setText(""
        		+ "Modifiers (case insensitive)	Value	Description\n" + 
        		"!groups or !LegendText	Comma separated text	Legend texts\n" + 
        		"!LegendStyle or !Style	rect/circle/star	Shapes to be plotted before the legends\n" + 
        		"!LegendColors or !Colors	Comma separated color codes or names	Colors to be applied to the shapes specified by LegendStyle\n" + 
        		"!Title or !Legend	Text	Title of the legend\n" + 
        		"!showLegends or !showLegend	0/1	0: hide legends; 1: show legends\n" + 
        		"!opacity or !op	Any float number between 0 and 1	Opacity of the graphical annotation\n" + 
        		"!plotWidth	Any integer > 0	Graphical width of the dataset on the canvas\n" + 
        		"!minradius	Any float number > 0	Minimal radius of the pies, For pie charts only.\n" + 
        		"!maxradius	Any float number > 0	Maximal radius of the pies, For pie charts only.\n" + 
        		"!area	None	Use user inputs as the areas of the pies\n, For pie charts only." + 
        		"!radius	None	Use user inputs as the radiuses of the pies, For pie charts only.\n" + 
        		"!grid	None	Show grid lines behind the plots, for bar charts only. \n" + 
        		"!gridlabel or !axis	None	Show numbers corresponding to the grid lines,for bar charts only. \n" + 
        		"!fanPlot or !fan	None	Plot bars as fans in circular plot,for bar charts only. "+ 
        		"    @see https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3394307/ ");
        addRowToFlextable(row, new Label("Modifers Help:"), helpDataContent);
        
        
        flextable.getCellFormatter().setHorizontalAlignment(row, 1, HasHorizontalAlignment.ALIGN_RIGHT);

        /*
         * May 24, 2011; ad a finish handler which will load the image once the upload finishes
         */
        defaultUploader.addOnFinishUploadHandler(onFinishUploaderHandler);
        //defaultUploader.addOnStartUploadHandler(onStartUploaderHandler);
        defaultUploader.addOnChangeUploadHandler(onChangeUploadHandler);
        defaultUploader.setMaximumFiles(1); // only one file is allowed
        defaultUploader.avoidRepeatFiles(true);

        /*
         * add flextable to dialogbox and initiate dboxFileOpen
         */
        this.setGlassEnabled(true);
        this.add(flextable);
        this.setText(dboxTitle);
        btnCancel.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                hide();
                clearUserInput(); // this is necessary;
            }
        });
    }// constructor
    private IUploader.OnChangeUploaderHandler onChangeUploadHandler = new IUploader.OnChangeUploaderHandler() {

        @Override
        public void onChange(IUploader uploader) {
            btnSubmit.setEnabled(false);
            btnCancel.setEnabled(false);
        }
    };
    // Load the image in the document and in the case of success attach it to the viewer
    private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {

        @Override
        public void onFinish(IUploader uploader) {
            if (uploader.getStatus() == Status.SUCCESS) {

                // The server sends useful information to the client by default
                UploadedInfo info = uploader.getServerInfo();
                System.out.println("File name " + info.name);
                System.out.println("File content-type " + info.ctype);
                System.out.println("File size " + info.size);

                System.out.println("File url " + uploader.fileUrl());
                System.out.println("servlet path " + uploader.getServletPath());
                System.out.println("server response " + uploader.getServerResponse());

                // You can send any customized message and parse it 
                System.out.println("Server message " + info.message);

                btnSubmit.setEnabled(true);
                btnCancel.setEnabled(true);

                filecontent = info.message;
                filecontent.trim();
            }
        }
    };

    // public methods
    public void setProjectName(String string) {
//        this.tbxProjectName.setText(string);
    }

    public void setDataSetName(String str) {
        this.tbxName.setText(str);
    }

    public void setDataSetContent(String str) {
        this.txtAreaCopyPaste.setText(str);
    }

    public String getDataSetName() {
        return this.tbxName.getText();
    }
    
    // May 30, 2012
    public void hideNameLabelAndTextBox(){
        this.lblName.setVisible(false);
        this.tbxName.setVisible(false);
    }

    // May 25, 2011;
    public String getDataSetContent() {
        /*
         * NOTE: any illegal characters, such as ", ' will be removed
         *  -- if the txtAreaCopyPate is not empty, take it
         *  -- otherwise take the uploaded contents
         *  -- otherwise return ""
         */
        String dataContent = (this.txtAreaCopyPaste.getText().trim().length() > 0) ? this.txtAreaCopyPaste.getText() : (filecontent.length() > 0) ? filecontent : "";
        //return JSFuncs.JsReplaceSpacesAndIllegalCharactersWithSingleSpaceChar(dataContent); // tab will not be removed
        return dataContent; // dec 2, 2011; not to remove anything yet
    }

    public String getProjectName() {
        return this.lboxProjectSelection.getSelectedItemText();
    } // April 7, 2011;

    public long getProjectId() {
        return Long.parseLong(this.lboxProjectSelection.getSelectedValue());
    } // April 7, 2011;

    public final void setProjectRowVisible(boolean b) {
        proLabel.setVisible(b);
        lboxProjectSelection.setVisible(b);
    }
    
    public final void setTreeFormatRowVisible( boolean b){
        lblTreeFormat.setVisible( b );
        lboxTreeFormatSelection.setVisible( b );
        this.lblLeafTypeSelection.setVisible( b );
        lboxLeafTypeSelection.setVisible( b );
        this.lblTraitsTypeSelection.setVisible( b );
        lboxTraitsTypeSelection.setVisible( b );   
    }

    public final void clearUserInput() {
        this.setDataSetContent("");
        this.setDataSetName("");
        this.setProjectName("");
        this.defaultUploader.reset();
        btnSubmit.setEnabled(true);
        btnCancel.setEnabled(true);
    }
    
    public final String getTreeFormat(){
        return this.lboxTreeFormatSelection.getValue( lboxTreeFormatSelection.getSelectedIndex() );
    }
    

    public final String getLeafType(){
        return this.lboxLeafTypeSelection.getValue( lboxLeafTypeSelection.getSelectedIndex() );
    }

    public final String getTraitsType(){
        return this.lboxTraitsTypeSelection.getValue( lboxTraitsTypeSelection.getSelectedIndex() );
    }
     
	
	
    
    // May 30, 2012
    public final void setTreeFormat( String format ){
        for( int i = 0; i < lboxTreeFormatSelection.getItemCount(); i ++){
            if( lboxTreeFormatSelection.getValue(i).equalsIgnoreCase(format) || lboxTreeFormatSelection.getItemText(i).equalsIgnoreCase(format) ){
                lboxTreeFormatSelection.setSelectedIndex(i);
                break;
            }
        }
    }
    
    // May 11, 2012
    public void setHomeMadeWidgetForAutomaticDataRetrieval( Widget newwidget ){
        this.sp.clear();
        this.sp.add( newwidget );
    }

    /*
     * private functions
     */
    // April 7, 2011
    private void addRowToFlextable(int idx, Label label, Widget txtWidget) {
        DOM.setStyleAttribute(label.getElement(), "textAlign", "right");
        DOM.setStyleAttribute(label.getElement(), "color", "darkblue");

        flextable.setWidget(idx, 0, label);
        flextable.setWidget(idx, 1, txtWidget);
        flextable.getCellFormatter().setVerticalAlignment(idx, 0, HasVerticalAlignment.ALIGN_TOP);
    }
}
