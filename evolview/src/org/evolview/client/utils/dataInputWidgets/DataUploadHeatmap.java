package org.evolview.client.utils.dataInputWidgets;

import org.evolview.client.utils.MC;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;

public class DataUploadHeatmap  extends DataInputWidgetBase {

	public DataUploadHeatmap(String dboxTitle) {
		super(dboxTitle);
		this.btnSubmit.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				MC.ebus.fireEvent( new HeatmapDataUploadEvent() );
			}});
	}


}
