package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class HeatmapDataUploadEvent extends GwtEvent<HeatmapDataUploadHandler> {
	
	public static Type<HeatmapDataUploadHandler> TYPE = new Type<HeatmapDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<HeatmapDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(HeatmapDataUploadHandler handler) {
		handler.onHeatmapDataUploaded(this);
	}

}
