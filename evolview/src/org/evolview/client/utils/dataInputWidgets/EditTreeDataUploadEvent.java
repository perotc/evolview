package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class EditTreeDataUploadEvent extends GwtEvent<EditTreeDataUploadHandler> {
	
	public static Type<EditTreeDataUploadHandler> TYPE = new Type<EditTreeDataUploadHandler>();
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<EditTreeDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EditTreeDataUploadHandler handler) {
		handler.onEditTreeDataUpload( this );
	}

}
