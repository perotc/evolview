package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface BranchColorDataUploadHandler extends EventHandler{
	void onBranchColorDataUpload( BranchColorDataUploadEvent evnt );
}
