package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface ColorStripsDataUploadHandler extends EventHandler {
	void onColorStripsDataUpload( ColorStripsDataUploadEvent evnt );
}
