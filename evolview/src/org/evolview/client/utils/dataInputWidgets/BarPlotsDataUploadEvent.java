package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class BarPlotsDataUploadEvent extends GwtEvent<BarPlotsDataUploadHandler> {

	public static Type<BarPlotsDataUploadHandler> TYPE = new Type<BarPlotsDataUploadHandler>();
	
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<BarPlotsDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(BarPlotsDataUploadHandler handler) {
		handler.onBarPlotsDataUpload( this );
	}

}
