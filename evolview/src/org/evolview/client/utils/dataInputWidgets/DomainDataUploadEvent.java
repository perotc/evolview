package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class DomainDataUploadEvent extends GwtEvent<DomainDataUploadHandler> {
	
	public static Type<DomainDataUploadHandler> TYPE = new Type<DomainDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DomainDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DomainDataUploadHandler handler) {
		handler.onDomainDataUpload( this);
	}

}
