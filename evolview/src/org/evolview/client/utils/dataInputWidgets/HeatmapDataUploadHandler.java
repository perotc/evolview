package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.EventHandler;

public interface HeatmapDataUploadHandler extends EventHandler {
	void onHeatmapDataUploaded( HeatmapDataUploadEvent event );
}
