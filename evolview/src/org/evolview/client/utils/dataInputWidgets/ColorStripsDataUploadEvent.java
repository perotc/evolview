package org.evolview.client.utils.dataInputWidgets;

import com.google.gwt.event.shared.GwtEvent;

public class ColorStripsDataUploadEvent extends GwtEvent<ColorStripsDataUploadHandler> {
	
	public static Type<ColorStripsDataUploadHandler> TYPE = new Type<ColorStripsDataUploadHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ColorStripsDataUploadHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ColorStripsDataUploadHandler handler) {
		handler.onColorStripsDataUpload( this );
	}

}
