package org.evolview.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasMouseOutHandlers;
import com.google.gwt.event.dom.client.HasMouseOverHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * * NOTE: popupPanel is a simplePanel
 * @author E.T.
 * last modified : Jun 13, 2013
 * >>>> Oct 25, 2013: change the layout of current widget to make room for action buttons
 * also change the 
 * >>>> oct 26, 2013 : make the following changes:
 * 1. the first time it pops up, show it at 0,0
 * 2. when attached, check the width and height of this popup panel
 * 3. if it fits to the right-bottom to the cursor, move it there
 * 4. if not, move it to the left side of the cursor 
 * >>>> Nov 2, 2013; !!!! HOW TO USE IT:
 * when use setWidgetAndShow(widget ..., int left, int top),
 * here left and top should be obtained from event.getClientX, event.getClientY !!!!!!!
 */
public class InfoPanelWidget extends PopupPanel implements HasMouseOverHandlers, HasMouseOutHandlers {

	private SimplePanel spInforPanel = new SimplePanel(), spActionPanel = new SimplePanel();
	private VerticalPanel vpMain = new VerticalPanel();
	private boolean mouseover = false;
	private final int movex = 10, movey = 10;
	private int mousex = 0, mousey = 0;

	/**
	 * 
	 * an interface to load resources needed
	 * Feb 11, 2013 --
	 */
	public interface Resources extends ClientBundle {
		public static final Resources INSTANCE = GWT.create(Resources.class);
		@Source("resources/InfoPanelWidget.css")
		public CssResource cssTooltip();
	}

		public InfoPanelWidget() {
		super();
		initiate();
	}

	public InfoPanelWidget(boolean b) {
		super();
		this.setAutoHideEnabled(b);
		initiate();
	}

	public boolean getMouseOver() {
		return this.mouseover;
	}

	/**
	 * set information panel but no action panel  
	 * here left and top should be obtained from event.getClientX, event.getClientY !!!!!!!
	 */
	public void setWidgetAndShow(Widget ftab, int left, int top) {
		setWidgetAndShow( ftab, null, left, top );
	}

	/**
	 * here left and top should be obtained from event.getClientX, event.getClientY !!!!!!!
	 */
	public void setWidgetAndShow(Widget ftab, Widget actionWidet, int left, int top) {
		spInforPanel.clear();
		spInforPanel.add(ftab);
		
		this.spActionPanel.clear();
		
		if( actionWidet != null ){
			this.spActionPanel.setVisible(true);
			this.spActionPanel.add(actionWidet);
		} else {
			this.spActionPanel.setVisible(false);
		}

		hide(); // this is important!!
		
		mousex = left + this.movex;
		mousey = top + this.movey;

		// set popup position
		setPopupPosition(0, 0); // to show?
		show(); //
	}
	
	/*
	 * other initiating options
	 * a. set default style
	 * oct 25, 2013: use a gmail-like theme instead the blue-ish theme
	 */
	private void initiate() {
		Resources.INSTANCE.cssTooltip().ensureInjected();
		this.setStyleName("tooltipwidgetGmail");

		/**
		 * Oct 25, 2013 : 
		 * 
		 * two simple panels
		 * ----------------------------
		 * 
		 *  information panel : 
		 * ----------------------------
		 * 
		 *  action buttons  
		 * ----------------------------
		 */
		vpMain.add(this.spInforPanel);
		vpMain.add(this.spActionPanel);

		spActionPanel.setWidget(new Button("Action"));

		// also add color shemems
		this.spInforPanel.setStyleName("upperInformationPanel");
		this.spActionPanel.setStyleName("lowerActionPanel");

		// 
		this.add(vpMain);

		/**
		 * mouse handlers ...
		 */
		this.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				mouseover = false;
				new Timer() {

					@Override
					public void run() {
						hide();
					}
				}.schedule(200);
			}
		});
		this.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				mouseover = true;
			}
		});
		
		/**
		 * ==== Oct 26, 2013 ====
		 */
		this.addAttachHandler( new AttachEvent.Handler(){

			@Override
			public void onAttachOrDetach(AttachEvent event) {
				if( isVisible() ){
					/**
					 * get width and height of current popup panel
					 */
					int height = getOffsetHeight();
					int width = getOffsetWidth();
					
					int currentx = mousex; // !!!!
					int currenty = mousey; // !!!!
					
					if( ( currentx + width ) > Window.getClientWidth() ){
						currentx = currentx - movex * 2 - width;
						//System.err.println("   go left");
					}
					
					if( ( currenty + height ) > Window.getClientHeight() ){
						//System.err.println("   go top");
						currenty = currenty - movey * 2 - height;
					}
						
					setPopupPosition( currentx + Window.getScrollLeft(), currenty + Window.getScrollTop() );
				}
			}} );
	}

	@Override
	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		return addDomHandler(handler, MouseOverEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		return addDomHandler(handler, MouseOutEvent.getType());
	}
}
