package org.evolview.client.utils;


import java.util.ArrayList;

/**
 *
 * @author E.T.
 */
public class JAVAFuncs {

    /*
     * Intelligently calculating chart tick positions/ tick-steps
     * this function estimates steps from a given range and round, to set smartly top and bottom y limits and steps and also calculate every how many values show the x thick.
     * original code is in C published here: http://stackoverflow.com/questions/4947682/intelligently-calculating-chart-tick-positions
     */
    public static double NiceNumber(final double range, final int round) {

        double NiceFraction;

        int Exponent = (int) Math.floor(Math.log10(range));
        double Fraction = range / Math.pow(10, (double) Exponent);

        if (round > 0) {

            if (Fraction < 1.5) {
                NiceFraction = 1.0;
            } else if (Fraction < 3.0) {
                NiceFraction = 2.0;
            } else if (Fraction < 7.0) {
                NiceFraction = 5.0;
            } else {
                NiceFraction = 10.0;
            }

        } else {

            if (Fraction <= 1.0) {
                NiceFraction = 1.0;
            } else if (Fraction <= 2.0) {
                NiceFraction = 2.0;
            } else if (Fraction <= 5.0) {
                NiceFraction = 5.0;
            } else {
                NiceFraction = 10.0;
            }

        }// if round

        return NiceFraction * Math.pow(10, (double) Exponent);

    }

    public static ArrayList<Double> AxisStartEndStep(final double axisstart, final double axisend, final int NumTicks) {

        ArrayList<Double> ticks = new ArrayList<Double>();

        double width = axisend - axisstart;
        if (width > 0) {

            /* Compute the new nice range and ticks */
            double NiceRange = NiceNumber(axisend - axisstart, 0);
            double NiceTick = NiceNumber(NiceRange / (NumTicks - 1), 1);

            /* Compute the new nice start and end values */
            double NewAxisStart = Math.floor(axisstart / NiceTick) * NiceTick;
            double NewAxisEnd = Math.ceil(axisend / NiceTick) * NiceTick;

            double step = (NewAxisEnd - NewAxisStart) / NumTicks;

            ticks.add(NewAxisStart);
            ticks.add(NewAxisEnd);
            ticks.add(step);
        }

        return ticks;

    }

    public static String joinStringBySeparator(ArrayList<String> obj, String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < obj.size(); i++) {
            if (i > 0) {
                sb.append(separator);
            }
            sb.append(obj.get(i));
        }

        return sb.toString();
    }
    
    public static String joinStringBySeparator(String[] obj, String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < obj.length; i++) {
            if (i > 0) {
                sb.append(separator);
            }
            sb.append(obj[i]);
        }
        return sb.toString();
    }
}
