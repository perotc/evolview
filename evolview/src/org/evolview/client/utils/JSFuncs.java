package org.evolview.client.utils;

import java.util.ArrayList;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.core.client.JsArrayString;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.Window;

public class JSFuncs {
	public static native boolean matchStringStart(String string, String prefix) /*-{
		var regex = new RegExp("^" + prefix);
		return regex.test(string);
	}-*/;

	// -- case insensitive -- dec 22, 2011
	public static native boolean matchStringEnd(String string, String suffix) /*-{
		var regex = new RegExp(suffix + "$");
		return regex.test(string);
	}-*/;

	// March 21, 2011
	// Dec 22, 2011; case-insensitive
	public static native boolean matchStringAnywhere(String string, String anywhere) /*-{
		var regex = new RegExp(anywhere);
		return regex.test(string);
	}-*/;
	
	/*
	 * March 20, 2011 
	 * Apr 24, 2012
	 */
	public static native JsArrayInteger stringPixelWidthHeight(String string, String fontfamily, int fontsize)/*-{

		var f = document.createElement("aslttttttttttq39247100915");
		f.style.display = 'hidden';
		f.style.fontSize = fontsize + "px";
		f.style.fontFamily = fontfamily;

		document.body.appendChild(f);

		string = string.replace(/[\t .-]/gi, "l");
		f.innerHTML = string;

		var sizes = new Array(f.offsetWidth, f.offsetHeight);

		document.body.removeChild(f);

		return sizes;

	}-*/;

	/*
	 * using javascript code
	 */
	public static native JsArrayString splitStringByNewLine(String string) /*-{
		return string.split(/[\n\r]+/);
	}-*/;

	// by tab-, white-space ect
	public static native JsArrayString splitStringBySpace(String string) /*-{
		return string.split(/\s+/);
	}-*/;

	// tab
	public static native JsArrayString splitStringByTab(String string) /*-{
		return string.split(/\t+/);
	}-*/;

	public static native JsArrayString splitStringBySeperator(String string, String separator) /*-{
		return string.split(separator);
	}-*/;

	// although this isn't javascript
	public static ArrayList<String> JsArrayStringToArrayList(JsArrayString jsarray) {

		ArrayList<String> arstring = new ArrayList<String>(jsarray.length());
		for (int idx = 0; idx < jsarray.length(); idx++) {
			arstring.add(jsarray.get(idx));
		}
		return arstring;
	}
	// although this isn't javascript
	public static ArrayList<Double> JsArrayStringToArrayDouble(JsArrayString jsarray) {

		ArrayList<Double> arDouble = new ArrayList<Double>(jsarray.length());
		for (int idx = 0; idx < jsarray.length(); idx++) {
			arDouble.add(Double.parseDouble(jsarray.get(idx)));
		}
		return arDouble;
	}

	/*
	 * replace spaces including \b space and illegal characters such as ", ', , with a single space character
	 * @input a string
	 * @output a string
	 * April 12, 2011;
	 * April 14, 2011;
	 * Nov 07, 2011; don't remove \t
	 */
	public static native String JsReplaceSpacesAndIllegalCharactersWithSingleSpaceChar(String string) /*-{
		return string.replace(/[ \"\']+/gi, " ");
	}-*/;

	/*
	 * Oct 27, 2011; remove new lines from tree string
	 */
	public static native String JsRemoveNewLines(String string) /*-{
		return string.replace(/[\r\n]+/gi, "");
	}-*/;

	// Nov 07, 2011; replace underline "_" with a single space " "
	public static native String JsReplaceUnderlineWithSingleSpaceChar(String string) /*-{
		return string.replace(/_+/gi, " ");
	}-*/;

	// Dec 2, 2011 --
	public static native String JsReplaceStringWithString(String string, String tobeReplaced, String replaceWith) /*-{
		return string.replace(tobeReplaced, replaceWith);
	}-*/;

	// Dec 22, 2011; get around popup blocker --
	public static native JavaScriptObject newWindow(String url, String name, String features)/*-{
		var window = $wnd.open(url, name, features);
		return window;
	}-*/;

	public static native void setWindowTarget(JavaScriptObject window, String target)/*-{
		window.location = target;
	}-*/;

	public static void popupBlockerCheckAndPopup(String url, String winTitle, String features) {
		JavaScriptObject window = JSFuncs.newWindow("", winTitle, features);
		if (window == null) {
			Window.alert("the download (popup) window might be blocked; please check your browser settings and try again!!");
		} else {
			JSFuncs.setWindowTarget(window, url);
		}
	}

	/**
	 * Sep 11, 2012 validate email address using javascript; the java version
	 * can be seen at http
	 * ://www.mkyong.com/regular-expressions/how-to-validate-email-address-with
	 * -regular-expression/ how since some necessary functions are not
	 * implemented in GWT, the java version can only be used at the server side
	 */
	public static native boolean validateEmail(String email)/*-{
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}-*/;

	/**
	 * Sep 13, 2012 decode URL string; see here for more details:
	 * http://www.sislands.com/coin70/week6/encoder.htm
	 * 
	 * how it work: Suppose you have a message and it says:
	 * 
	 * Some+website+you+got%21 %0D%0AGood+luck%28you%27ll+need+it%29
	 * %0D%0A7E%7E+The+Slug+%7E%7E
	 * 
	 * What The Slug is trying to say is:
	 * 
	 * Some website you got! Good luck (you'll need it) ~~ The Slug ~~
	 */
	public static native String decodeURL(String str) /*-{
		return unescape(str.replace(/\+/g, " "));
	}-*/;

	/**
	 * Oct 4, 2012; test if a string contains white spaces
	 */
	public static native boolean hasWhiteSpace(String s) /*-{
		return /\s/g.test(s);
	}-*/;

	/**
	 * Oct 4, 2012: remove spaces
	 */
	public static native String removeSpaces(String string) /*-{
		return string.split(' ').join('');
	}-*/;

	public static native int getAbsoluteTop(Element elem) /*-{
		var top = 0;
		var curr = elem;
		while (elem) {
			top += elem.offsetTop;
			elem = elem.offsetParent;
		}
		return top;
	}-*/;

}
