package org.evolview.client.utils;

import com.google.gwt.user.client.Cookies;
import java.util.Date;

public class CookieMan {
	private final String cookieSessionName = "aWsdiCthnHasfgjELSDti5LKaasgNawetglhh233511rfadfhj346";

	public CookieMan() {
	}

	public String getSessionID() {
		String c = Cookies.getCookie(this.cookieSessionName);
		return c == null ? "" : c;
	}

	public void saveSessionID(String sessionID, int millis) {
		Cookies.setCookie(this.cookieSessionName, sessionID,
				new Date(System.currentTimeMillis() + millis));
	}

	public void saveSessionID(String sessionID) {
		final long DURATION = 1000 * 60 * 60 * 24 * 14; // duration remembering
														// login. 2 weeks in
														// this example.
		Date expires = new Date(System.currentTimeMillis() + DURATION);
		Cookies.setCookie(this.cookieSessionName, sessionID, expires);
	}

	public void removeCookie() {
		Cookies.removeCookie(cookieSessionName);
	}
	
	/**
	 * Oct 9, 2012
	 */
	public void saveSessionWithSessionName( String sessionName, String sessionID, int millis ){
		Cookies.setCookie(sessionName, sessionID,
				new Date(System.currentTimeMillis() + millis));
	}
	
	public String getSessionBySessionName( String sessionName ){
		String c = Cookies.getCookie(sessionName);
		return c == null ? "" : c;
	}
	
}
