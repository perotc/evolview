package org.evolview.client.utils;


import java.util.ArrayList;
import java.util.Arrays;

import org.evolview.client.IGWTService;
import org.evolview.client.IGWTServiceAsync;
import org.evolview.client.utils.AlertWidget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * My Constants...
 * @author E.T.
 *
 */
public class MC {
	public static CookieMan cookieman = new CookieMan();
	
	/**
	 * Nov 2, 2013;
	 */
	public static SimpleEventBus ebus = GWT.create(SimpleEventBus.class);
	
	/**
	 * dimensions for UIs; Feb 18, 2013 --
	 * if new buttons are added, change 'controllerPanel_width' accordingly;
	 * each button is about ~30px in width 
	 */
	public static final int userdatapanel_width = 250,
            controllerPanel_width = 877 + 30 + 80,
            scrlUserData_width = 268,
            vpLogoAndBrowse_width = 270,
            vpStripController_width = 15;
	public static final int incremental = 60;
	public static final int width_base = vpLogoAndBrowse_width + controllerPanel_width + vpStripController_width,
            headerbarWidth = 10 + width_base + incremental,
            controllerPanelWiderWidth = width_base - vpStripController_width + incremental,
            controllerPanelNarrowerWidth = width_base - vpStripController_width - vpLogoAndBrowse_width + incremental;

	// rpc 
	public static IGWTServiceAsync rpc = (IGWTServiceAsync) GWT.create(IGWTService.class);

	
	/**
	 * alert widget and alerts
	 */
	public static AlertWidget alert = new AlertWidget();
	public static AlertWidget alertModular = new AlertWidget( true ); // Feb 17, 2013; a modular widget --

	public static void alertFailConnectingServer() {
		alert.setMessageAndShow("fail to connect server, please try again later", AlertWidget.AlertWidgetType.error);
	}
	
	public static void alertLoginRequired(){
        alert.setMessageAndShow("log in is required", AlertWidget.AlertWidgetType.info);
    }
    
    public static void alertPrivilegesNotEnough(){
        alert.setMessageAndShow( "you do not have the priviledge to commit the changes here", AlertWidget.AlertWidgetType.info );
    }
    
    public static void alertCommunicatingWithServer(){
    	MC.alert.setMessageAndShow("communicating with server ...", AlertWidget.AlertWidgetType.busy);
    }
    
	/**
	 * balloon widget
	 */
	public static BalloonTips balloon = new BalloonTips( true, false );

	/**
	 *  history tokens -- not used directly in the source codes
	 *  Oct 9, 2012: also added some for redirecting:
	 *   - about redirecting:
	 *     login is required to perform some actions, when attempts to do that, the user will be redirect to the
	 *     login page; after login successfully, the user will be redirected again to the page before login.
	 */
	public static String SIGNIN = "login", SIGNOUT = "", REGISTER = "register", PASSWORD = "password";
	public static String SESSION_TOKEN_BEFORE_SIGNIN = "etatctt91389tjskt8800";

	/**
	 * some integers
	 */
	public static int TOP_BAR_HEIGHT = 30, PROJECT_HEADER_SEARCH_BAR_HEIGHT = 71;

	/**
	 * other fields
	 */
	public static String RESERVED_VALUE_FOR_LISTBOX = "RESERVED", CURRENTUSER_EMAIL = "" /* Sep 23, 2012 */;

	public static String current_project;

	public static final String[] WORKINGCOPY_DIRECTORIES = { "Data", "Scripts", "Documentation" }; // , "Wiki"

	/**
	 * ownerships
	 */
	public static final String[] OWNERCAT = { "Founder", "Owner", "Member", "Favorite" };
	public static final String FOUNDER = "Founder";
	public static final ArrayList<String> OWNERCAT_LITE = new ArrayList<String>(Arrays.asList("Founder", "Owner", "Member"));

}
