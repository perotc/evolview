package org.evolview.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasBlurHandlers;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasMouseOutHandlers;
import com.google.gwt.event.dom.client.HasMouseOverHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * March 31, 2013: a widget that can take care of itself
 * @author E.T.
 */

public class ClickableVerticalStripWidget extends VerticalPanel implements HasClickHandlers, HasMouseOverHandlers, HasMouseOutHandlers, HasBlurHandlers {
	
	public interface StipWidgetResources extends ClientBundle {
		@Source("resources/ClickableVerticalStripWidgetArrowRight.png")
		ImageResource arrowRight();

		@Source("resources/ClickableVerticalStripWidgetArrowLeft.png")
		ImageResource arrowLeft();

		@Source("resources/ClickableVerticalStripWidget.css")
		CssResource css();
	}
	
	private static final StipWidgetResources resources = GWT.create(StipWidgetResources.class);
	private Image lefty = new Image(resources.arrowLeft()), righty =  new Image(resources.arrowRight() );
	private SimplePanel sp = new SimplePanel();

	public ClickableVerticalStripWidget() {
		super();
		resources.css().ensureInjected();
		
		// set style
		this.setStyleName( "verticalstripController" );
		
		lefty.setStyleName("imageOnStripController");
		righty.setStyleName("imageOnStripController");
				
		this.add(sp);
		this.setCellHorizontalAlignment(sp, HasHorizontalAlignment.ALIGN_CENTER);
		
		/**
		 * add arrow 
		 */
		sp.setWidget(lefty);
		
		/**
		 * click handlers 
		 */
		this.addMouseOverHandler( new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				DOM.setStyleAttribute( getElement(), "backgroundColor", "lightgrey");
			}});
		
		this.addMouseOutHandler( new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				DOM.setStyleAttribute( getElement(), "backgroundColor", "whitesmoke");
			}});
		
		this.addBlurHandler( new BlurHandler(){

			@Override
			public void onBlur(BlurEvent event) {
				DOM.setStyleAttribute( getElement(), "backgroundColor", "lightgrey");
			}});
	}
	
	// left or right
	public void setArrowImageLeft( boolean bLeft ){
		if( bLeft ){
			sp.setWidget(lefty);
		} else {
			sp.setWidget(righty);
		}
	}

	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return addDomHandler(handler, ClickEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		return addDomHandler(handler, MouseOverEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		return addDomHandler(handler, MouseOutEvent.getType());
	}

	@Override
	public HandlerRegistration addBlurHandler(BlurHandler handler) {
		return addDomHandler(handler, BlurEvent.getType());
	}
}
