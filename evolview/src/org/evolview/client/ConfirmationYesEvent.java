package org.evolview.client;

import com.google.gwt.event.shared.GwtEvent;

/**
 * self-made event handlers using eventBus
 */
public class ConfirmationYesEvent extends GwtEvent<ConfirmationYesHandler> {
	public static Type<ConfirmationYesHandler> TYPE = new Type<ConfirmationYesHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ConfirmationYesHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ConfirmationYesHandler handler) {
		handler.onConfirmationYesClicked(this);
	}
}