package org.evolview.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.History;

public class Evolview implements EntryPoint {

	@Override
	public void onModuleLoad() {
		History.addValueChangeHandler(new MyHistoryListener());
        History.fireCurrentHistoryState();
	}
}
