package org.evolview.client;

import com.google.gwt.event.shared.EventHandler;

public interface ConfirmationYesHandler extends EventHandler {
	void onConfirmationYesClicked( ConfirmationYesEvent yesEvent );
}