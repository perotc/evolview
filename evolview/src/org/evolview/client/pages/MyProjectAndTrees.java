package org.evolview.client.pages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.evolview.client.ConfirmDialogbox;
import org.evolview.client.ConfirmationYesEvent;
import org.evolview.client.ConfirmationYesHandler;
import org.evolview.client.Table;
import org.evolview.client.proMode;
import org.evolview.client.DataContainers.ColorDataContainerParser;
import org.evolview.client.DataContainers.HeatmapMatrixDataContainer;
import org.evolview.client.DataContainers.NumericMatrixDataContainer;
import org.evolview.client.DataContainers.ProteinDomainDataContainerParser;
import org.evolview.client.phyloUtils.PhyloTree;
import org.evolview.client.phyloUtils.PhyloTreeSVGViewer;
import org.evolview.client.phyloUtils.TreeDecoType;
import org.evolview.client.phyloUtils.treePlotMode;
import org.evolview.client.utils.AlertWidget;
import org.evolview.client.utils.ClickableVerticalStripWidget;
import org.evolview.client.utils.ImgBtn;
import org.evolview.client.utils.JSFuncs;
import org.evolview.client.utils.MC;
import org.evolview.client.utils.colorPicker.ColorPicker;
import org.evolview.client.utils.dataInputWidgets.BarPlotsDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.BarPlotsDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.BranchColorDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.BranchColorDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.ColorStripsDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.ColorStripsDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.DataInputWidgetBase;
import org.evolview.client.utils.dataInputWidgets.DataUploadBarPlots;
import org.evolview.client.utils.dataInputWidgets.DataUploadBranchColor;
import org.evolview.client.utils.dataInputWidgets.DataUploadColorStrips;
import org.evolview.client.utils.dataInputWidgets.DataUploadEditTree;
import org.evolview.client.utils.dataInputWidgets.DataUploadHeatmap;
import org.evolview.client.utils.dataInputWidgets.DataUploadLabalBK;
import org.evolview.client.utils.dataInputWidgets.DataUploadLabelColor;
import org.evolview.client.utils.dataInputWidgets.DataUploadPieChart;
import org.evolview.client.utils.dataInputWidgets.DataUploadProteinDomains;
import org.evolview.client.utils.dataInputWidgets.DomainDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.DomainDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.EditTreeDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.EditTreeDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.FileOpenEvent;
import org.evolview.client.utils.dataInputWidgets.FileOpenHandler;
import org.evolview.client.utils.dataInputWidgets.FileOpenWidget;
import org.evolview.client.utils.dataInputWidgets.HeatmapDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.HeatmapDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.LabelBKDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.LabelBKDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.LabelColorDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.LabelColorDataUploadHandler;
import org.evolview.client.utils.dataInputWidgets.PieChartDataUploadEvent;
import org.evolview.client.utils.dataInputWidgets.PieChartDataUploadHandler;
import org.evolview.shared.UserData;
import org.evolview.shared.UserDataProjectSkeleton;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.ScrollEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable.ColumnFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;


/**
 * Last modified: 
 * April 2, 2013: change position of scrollUserDataBrowser panel to fixed / relative according to the Window.scroll positions
 * to make sure the the contents are always visible.
 * see 'Window.addWindowScrollHandler'
 * 
 * >>> april 4, 2013; fix a bug in changing project color
 * >>> nov 2, 2013; use eventBus to handle cross-class communications
 */
public class MyProjectAndTrees extends Composite{

	private static MyProjectAndTreesUiBinder uiBinder = GWT.create(MyProjectAndTreesUiBinder.class);

	interface MyProjectAndTreesUiBinder extends UiBinder<Widget, MyProjectAndTrees> {
	}
	
	/**
	 * data input widgets ; last modified: Nov 2, 2013 
	 * 
	 * https://github.com/evolgeniusteam/EvolviewDocumentation/blob/master/datasets/00_overview/DatasetOverview.md
	 */
	private final FileOpenWidget dboxFileOpen = new FileOpenWidget( "Upload a new tree" ); 
	// https://github.com/evolgeniusteam/EvolviewDocumentation/blob/master/datasets/02_pie/DatasetPieCharts.md
	private final DataUploadPieChart dboxDataUploadPieChart = new DataUploadPieChart("Upload your PieChart Data");
	private final DataUploadEditTree dboxEditTree = new DataUploadEditTree("(!! still in beta !!) Edit the existing tree or replace it with a new one");
	private final DataUploadBranchColor dboxBranchColor = new DataUploadBranchColor("Upload Colors for Branches");
	private final DataUploadLabelColor dboxLeafLabelColor = new DataUploadLabelColor("Upload colors for Leaf Labels");
	private final DataUploadLabalBK dboxLeafLabelBKColor = new DataUploadLabalBK("Upload Background Colors for Leaf Labels");
	// https://github.com/evolgeniusteam/EvolviewDocumentation/blob/master/datasets/06_strip_and_shape/DatasetColorStripShape.md
	private final DataUploadColorStrips dboxColorStrips = new DataUploadColorStrips("Upload data for color strips next to leaf labels");
	// https://github.com/evolgeniusteam/EvolviewDocumentation/blob/master/datasets/03_bar/DatasetBars.md
	private final DataUploadBarPlots dboxBarPlots = new DataUploadBarPlots("Upload data for barplots next to leaf labels");
	// https://github.com/evolgeniusteam/EvolviewDocumentation/blob/master/datasets/10_heatmap/DatasetHeatmap.md
	private final DataUploadHeatmap dboxHeatmap = new DataUploadHeatmap("Upload data for Heatmap");
	// https://github.com/evolgeniusteam/EvolviewDocumentation/blob/master/datasets/07_protein_domain/DatasetProteinDomain.md
	private final DataUploadProteinDomains dboxProteinDomains = new DataUploadProteinDomains("Upload data for protein domains that are plotted next to leaf labels"); // May 11, 2012


	@UiField
	VerticalPanel userdatabrowser, vpLogoAndBrowse;
	@UiField
	HorizontalPanel imageLogo;
	@UiField
	ClickableVerticalStripWidget vpStripController;
	@UiField
	ScrollPanel scrlUserData;
	@UiField
	Toolbar toolbar;
	@UiField
	SimplePanel spFake, spSVG;
	
	/*
	 * ============== click handlers for the UI ===============
	 */
	@UiHandler("vpStripController")
	void onStripClicked(ClickEvent e) {
		boolean b = !vpLogoAndBrowse.isVisible();

		// set visible or not
		vpLogoAndBrowse.setVisible(b);

		// March 31, 2013 --
		vpStripController.setArrowImageLeft(b); // if browser panel shown, the arrow should look like this: << 
		toolbar.setWidthAccoringToShowHideControllerPanel(b);
	}

	/*
	 * =============== the following copied from old files; Oct 21, 2011
	 * ==================
	 */
	//private final SimplePanel spanel = new SimplePanel();
	private PhyloTreeSVGViewer currentSVGViewer = null;

	/*
	 * *************************************************************************
	 *
	 * dialog boxes; NOTE: UI elements of dialog boxes are all defined locally
	 * within the dialog boxes
	 *
	 *************************************************************************
	 */

	/*
	 * a UserDataContainer to contain user data
	 */
	// hash{$uniqueID} = SimplePanel contains SVG plot; here UniqID = projectID + treeID;
	private HashMap<Integer, PhyloTreeSVGViewer> svgViewerPanels = new HashMap<Integer, PhyloTreeSVGViewer>();
	/*
	 * userdata browser; March 30, 2011;
	 */
	private HashMap<String, ProjectPanel> projectpanels = new HashMap<String, ProjectPanel>(); // $hash{$projectID} = project panel
	private HashMap<Integer, TreePanel> treepanels = new HashMap<Integer, TreePanel>(); // $hash{$projectID + $treeID} = treeID panel
	private HashMap<String, String> projectcolors = new HashMap<String, String>();
	// Oct 28, 2011;
	private final HashMap<Integer, RadioButton> uid2rbtn4datasetpanel = new HashMap<Integer, RadioButton>();
	/*
	 * names of category panels / dataset categories
	 */
	private  enum ECatAndDatasetType {
			SAVEDVIEWS("Saved Views"), 
			PIES("Pies"),
			BRANCHECOLORS("Branch Colors"), 
			LEAFLABELCOLORS("Leaf Colors"),
			LEAFBKCOLORS("Leaf BK Colors"), 
			CHARTS("Charts"),
			BARS("Bars"),
			HEATMAPS("Heatmaps"),
			STRIPS("Strips"),
			PROTEINDOMAINS("Protein Domains")
			;
			
			
			private final String label;

			private ECatAndDatasetType(String label) {
				this.label = label;
			}

			public String getLabel() {
				return label;
			} 

	};

	// == above copied from old files ==
	
	private String oldSessionID = "";
	private int scrlUserDataTop = 0, toolbarTop = 0, toolbarHeight = 0 ;
	private boolean bIsScrollBarFloating = false;

	public MyProjectAndTrees() {
		initWidget(uiBinder.createAndBindUi(this));

		/*
		 * April 22, 2012: initiate widths of several important layout widgets
		 * to make them align
		 */
		scrlUserData.setWidth(MC.scrlUserData_width + "px");
		vpLogoAndBrowse.setWidth(MC.vpLogoAndBrowse_width + "px");
		vpStripController.setWidth(MC.vpStripController_width + "px");
		
		this.scrlUserData.addScrollHandler( new ScrollHandler() {

			@Override
			public void onScroll(com.google.gwt.event.dom.client.ScrollEvent event) {
				event.stopPropagation();
			}});
		
		// <<<< April 22, 2012 <<<<

		/**
		 *  == Oct 25, 2011 ==
		 *  March 23, 2013; initiate toolbar
		 */
		toolbar.setSvgviewer(null);
		toolbar.setWidthAccoringToShowHideControllerPanel(true);

		/**
		 * initiate widgets for data-uploads (annotation uploads) 
		 */
		// some push buttons
		initiate_dboxes_for_dataupload();
		initiate_clickhandlers_for_datauploadIcons();

		/**
		 * April 2, 2013: load demo trees as well as user trees
		 */
		this.addAttachHandler( new AttachEvent.Handler() {
			
			@Override
			public void onAttachOrDetach(AttachEvent event) {
				if( isAttached() ){
					String sessionid = MC.cookieman.getSessionID();
					if( !oldSessionID.equals( sessionid ) ){
						oldSessionID = sessionid;
						
						// clear user data browser panel
						clearUerDataBrowswerPanel();
						
						MC.alertCommunicatingWithServer();
						
						// check if load demos
						MC.rpc.getUserSettings(sessionid, new AsyncCallback<HashMap<String,String>>(){

							@Override
							public void onFailure(Throwable caught) {
								MC.alertFailConnectingServer();
							}

							@Override
							public void onSuccess(HashMap<String, String> result) {
								loadUserProjectsAndTrees();
								
							}}); // end of get user settings
					}// if sessionid is different from old one
				}// if attached
				
				if( scrlUserData.isAttached() ){
					scrlUserDataTop = JSFuncs.getAbsoluteTop( scrlUserData.getElement() ) + 35;
					
					// also resize user data scroll bar 
					resizeUserDataScrollBar( );
				}
				
				if(toolbar.isAttached()){
					toolbarTop = JSFuncs.getAbsoluteTop( toolbar.getElement() ) + 35;
					toolbarHeight = toolbar.getOffsetHeight();
				}
			}// on attach
		}); // end of attach handler 
//		
		/**
		 * April 2-3, 2013; 
		 */
		Window.addWindowScrollHandler(new Window.ScrollHandler(){
			@Override
			public void onWindowScroll(ScrollEvent event) {
				/**
				 * user data and vertical clicable strip
				 */
				if( event.getScrollTop() > scrlUserDataTop || event.getScrollLeft() > 0 ){
					DOM.setStyleAttribute(scrlUserData.getElement(), "position", "fixed");
					DOM.setStyleAttribute(scrlUserData.getElement(), "top", "0");
					bIsScrollBarFloating = true;
					
					DOM.setStyleAttribute(vpStripController.getElement(), "position", "fixed");
					DOM.setStyleAttribute(vpStripController.getElement(), "top", "0");
					
					resizeUserDataScrollBar( );
				} else {
					DOM.setStyleAttribute(scrlUserData.getElement(), "position", "relative");
					DOM.setStyleAttribute(scrlUserData.getElement(), "position", "auto");
					bIsScrollBarFloating = false;
					
					DOM.setStyleAttribute(vpStripController.getElement(), "position", "relative");
					DOM.setStyleAttribute(vpStripController.getElement(), "position", "auto");
					
					resizeUserDataScrollBar( );
				}
				
				/**
				 * toolbar
				 */				
				if( event.getScrollTop() > toolbarTop || event.getScrollLeft() > 0 ){
					DOM.setStyleAttribute(toolbar.getElement(), "position", "fixed");
					DOM.setStyleAttribute(toolbar.getElement(), "top", "0");
					DOM.setStyleAttribute(toolbar.toolbarLayoutPanel.getElement(), "boxShadow", "0px 0px 1em grey");
					DOM.setStyleAttribute(spFake.getElement(), "height", toolbarHeight + "px");
				} else {
					DOM.setStyleAttribute(toolbar.getElement(), "position", "relative");
					DOM.setStyleAttribute(toolbar.getElement(), "position", "auto");
					DOM.setStyleAttribute(toolbar.toolbarLayoutPanel.getElement(), "boxShadow", "0px 0px 4px grey");
					DOM.setStyleAttribute(spFake.getElement(), "height", "0px");
				}
			}});
		
		Window.addResizeHandler( new ResizeHandler(){

			@Override
			public void onResize(ResizeEvent event) {
				if (scrlUserData.isAttached() ){
					resizeUserDataScrollBar();
				}
			}});
	}

	/**
	 * April 4, 2013 
	 */
	protected void resizeUserDataScrollBar() {
		int height = this.bIsScrollBarFloating ? Window.getClientHeight() : Window.getClientHeight() + Window.getScrollTop() - this.scrlUserDataTop;
		height -= 20;
		scrlUserData.setHeight( height + "px" );
	}


	
	/**
	 * April 2, 2013; modified from function 'private void retrieveUserDataFromServer(final String session_id)' 
	 */
	private void loadUserProjectsAndTrees(){
		/*
         * 1. first of all, get project skeletons, meaning only project names,
         * tree names are retrieved, and non of them are active
         */
        MC.rpc.getProjectSkeletons( MC.cookieman.getSessionID(), new AsyncCallback<ArrayList<UserDataProjectSkeleton>>() {

            @Override
            public void onFailure(Throwable thrwbl) {
                Window.alert("fail to connect to server and fectch project/ tree data, try again latter");
            }

            @Override
            public void onSuccess(ArrayList<UserDataProjectSkeleton> proSkelet) {
                for (final UserDataProjectSkeleton pros : proSkelet) {
                    addNewProjectAndTreePanels(pros.getProjectName(),
                    		pros.getProjectDBSerial(),
                    		pros.getProjectColor(),
                    		pros.getTreeName(),
                    		pros.getTreeFormat(),
                    		pros.getLeafType(),
                    		pros.getTraitsType(),
                    		null, 
                    		pros.getTreeDBSerial(),
                    		pros.getTreeActive());
                }
                MC.alert.hide(); // April 2, 2013; hide 
            }
        });
	}
	
	
	private void initiate_dboxes_for_dataupload() {
		/*
		 * April 7, 2011; use DataInputWidget instead of normal dialogbox,
		 */
		dboxFileOpen.setProjectRowVisible(true);
		dboxFileOpen.setTreeFormatRowVisible(true);
		
		MC.ebus.addHandler( org.evolview.client.utils.dataInputWidgets.FileOpenEvent.TYPE , new FileOpenHandler() {

			@Override
			public void onFileOpenSubmitClicked(FileOpenEvent foEvt) {
				String projectName = dboxFileOpen.getProjectName();
				long projectId = dboxFileOpen.getProjectId();  
				/*
				 * May 17, 2011; 1. create a project panel 2. create a treepanel
				 * 3. then add the tree to treepanel
				 *
				 * Nov 07, 2011; check if project name equals demoproject
				 */
				boolean success = true;
				String treeName = dboxFileOpen.getDataSetName().trim();
				String treeString = dboxFileOpen.getDataSetContent().trim();

				if (treeName.isEmpty() || treeString.isEmpty()) {
					success = false;
					Window.alert("Both tree name and tree string are required. Please check you input!!");
				}  else {
					PhyloTree phylotree = new PhyloTree(0, treeName, treeString, dboxFileOpen.getTreeFormat(), null);
					if (treeName.length() > 0 && treeString.length() > 0) {
						if (phylotree.isTreeDataValid()) {
							System.out.println(" new tree looks OK; continue");
							// add panels for project as well tree
							saveNewTree(projectName, (int) projectId, "red",
									dboxFileOpen.getDataSetName(),
									dboxFileOpen.getTreeFormat(),
									dboxFileOpen.getLeafType(),
									dboxFileOpen.getTraitsType(), 
									phylotree, 
									true);

						} else {
							Window.alert(" error parsing user input: " + phylotree.getErrorMessage());
						}
					} else {
						success = false;
						Window.alert("dataset is not valid");
						// do something here!!
					}
				}

				if (success) {
					dboxFileOpen.hide();
					dboxFileOpen.clearUserInput();
				} // close the dialogbox if only the tree is added successfully
			}});
		
		/*
		 * >>>>>>> ------------- branch colors/ leaf label colors/ leaf label
		 * backgrounds -------------- April 3, 2011; use Widget
		 * 'DataInputWidget' instead of dialogbox April 5, 2011; use
		 * addDataSet2Tree for all three types of data
		 */
		MC.ebus.addHandler( BranchColorDataUploadEvent.TYPE, new BranchColorDataUploadHandler(){
			@Override
			public void onBranchColorDataUpload(BranchColorDataUploadEvent evnt) {
				saveColorAndNumericDataToServer(currentSVGViewer.getTreeID(),dboxBranchColor, ECatAndDatasetType.BRANCHECOLORS); 
			}});

		// April 4, 2011; use Widget DataInputWidget instead of dialogbox
		MC.ebus.addHandler( LabelColorDataUploadEvent.TYPE, new LabelColorDataUploadHandler(){

			@Override
			public void onLabelColorDataUploaded(LabelColorDataUploadEvent evnt) {
				saveColorAndNumericDataToServer(currentSVGViewer.getTreeID(), dboxLeafLabelColor, ECatAndDatasetType.LEAFLABELCOLORS); 
			}});
		
		MC.ebus.addHandler( LabelBKDataUploadEvent.TYPE , new LabelBKDataUploadHandler(){

			@Override
			public void onLabelBKDataUploaded(LabelBKDataUploadEvent event) {
				saveColorAndNumericDataToServer( currentSVGViewer.getTreeID(), dboxLeafLabelBKColor , ECatAndDatasetType.LEAFBKCOLORS); 
			}});
		
		// >>>>>> ------------ color strips / bars -----------------
		MC.ebus.addHandler( ColorStripsDataUploadEvent.TYPE, new ColorStripsDataUploadHandler(){

			@Override
			public void onColorStripsDataUpload(ColorStripsDataUploadEvent evnt) {
				saveColorAndNumericDataToServer( currentSVGViewer.getTreeID(), dboxColorStrips , ECatAndDatasetType.STRIPS); 
			}});
		
		MC.ebus.addHandler( BarPlotsDataUploadEvent.TYPE , new BarPlotsDataUploadHandler(){

			@Override
			public void onBarPlotsDataUpload(BarPlotsDataUploadEvent evnt) {
				saveColorAndNumericDataToServer(currentSVGViewer.getTreeID(), dboxBarPlots , ECatAndDatasetType.BARS); 
			}});
		

		MC.ebus.addHandler( HeatmapDataUploadEvent.TYPE , new HeatmapDataUploadHandler(){
			@Override
			public void onHeatmapDataUploaded(HeatmapDataUploadEvent evnt) {
				saveColorAndNumericDataToServer(currentSVGViewer.getTreeID(), dboxHeatmap , ECatAndDatasetType.HEATMAPS); 
			}
		});
		
		/*
		 * >>> ----------------- pie chart -------------- >>>> April 3, 2011;
		 * use Widget 'DataInputDialogbox' instead of dialogbox
		 */
		MC.ebus.addHandler( PieChartDataUploadEvent.TYPE, new PieChartDataUploadHandler(){

			@Override
			public void onPieChartDataUploaded(PieChartDataUploadEvent event) {
				saveColorAndNumericDataToServer(currentSVGViewer.getTreeID(), dboxDataUploadPieChart , ECatAndDatasetType.PIES);
			}});

		/**
		 * **********************************************
		 * May 13, 2012; for protein domain data
		 * nov 2, 2013; use eventbus to handle events 
		 * **********************************************
		 */
		MC.ebus.addHandler( DomainDataUploadEvent.TYPE, new DomainDataUploadHandler(){
			@Override
			public void onDomainDataUpload(DomainDataUploadEvent event) {
				saveColorAndNumericDataToServer(currentSVGViewer.getTreeID(), dboxProteinDomains, ECatAndDatasetType.PROTEINDOMAINS); 
			}});
	} // end of initiate_BasicTab
	
	private void initiate_clickhandlers_for_datauploadIcons() {
		
		toolbar.ibtnFileOpen.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
//				ArrayList<String> projectsForOracle = new ArrayList<String>();
//		        for (String pro : projectpanels.keySet()) {
//		            if (!pro.equals(demoproject)) {
//		                projectsForOracle.add(pro);
//		            }
//		        }
//
//		        dboxFileOpen.projectOracle.addAll(projectsForOracle);
		        dboxFileOpen.clearUserInput(); // clear user input
		        dboxFileOpen.center();
			}});
		
		toolbar.ibtnDataUploadPiechart.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadPiechart.isEnabled() == true ){
					dboxDataUploadPieChart.center();
		    	}
			}});
		
		toolbar.ibtnDataUploadBranchColor.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadBranchColor.isEnabled() == true ){
					dboxBranchColor.center();
		    	}
			}});
		
		toolbar.ibtnDataUploadLabelColor.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadLabelColor.isEnabled() == true ){
					dboxLeafLabelColor.center();
		    	}
			}});
		
		toolbar.ibtnDataUploadBarPlots.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadBarPlots.isEnabled() == true ){
					dboxBarPlots.center();
		    	}
			}});
		
		
		toolbar.ibtnDataUploadLeafLabelBKColor.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadLeafLabelBKColor.isEnabled() == true ){
					dboxLeafLabelBKColor.center();
		    	}
			}});
		
		toolbar.ibtnDataUploadColorStrips.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadColorStrips.isEnabled() == true ){
					dboxColorStrips.center();
		    	}
			}});

		toolbar.ibtnDataUploadHeatmap.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadHeatmap.isEnabled() == true ){ 
		            dboxHeatmap.center();
		    	}
			}});
		
		
		toolbar.ibtnDataUploadProteinDomains.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if( currentSVGViewer != null && toolbar.ibtnDataUploadProteinDomains.isEnabled() == true ){
		            dboxProteinDomains.setProteins(currentSVGViewer.getPhyloTree().getAllLeafLabels()); // shit
					dboxProteinDomains.center();
		    	}
			}});
	}
	
	private void saveNewTree(
			final String projectName, 
			final int projectDBSerial,
			final String projectColor, 
			final String treeName, 
			final String treeFormat,
			final String leafType,
			final String traitsType,
			final PhyloTree phylotree, 
			final boolean treeactive) {
		final String sessionid = MC.cookieman.getSessionID();
		 
		// persist the tree
		MC.rpc.addTree(sessionid,
				projectDBSerial,
				treeName, 
				phylotree.getTreeString(), 
				treeFormat,
				leafType,
				traitsType,
				treeactive, 
				new AsyncCallback<Integer>() {
			@Override
			public void onFailure(Throwable caught) {
				// 
			}

			@Override
			public void onSuccess(Integer treeDBSerial) { 
				System.out.println("Tree serial is : " + treeDBSerial);
				// !! important 
				// setup the TreeId to phylotree
				phylotree.setTreeId(treeDBSerial);
				
				addNewProjectAndTreePanels(projectName, projectDBSerial, projectColor, 
						treeName,
						treeFormat,
						leafType,
						traitsType,
						phylotree,
						treeDBSerial, 
						treeactive);
			}
		});
			
			 
		
	}

	/*
	 * May 16, 2011; copy some code from addNewTreeAsString (which doesn't exist
	 * anymore in this class), with some changes Nov 07, 2011; add codes for
	 * trees in the "demos" project; (none new codes are necessary) Nov 08,
	 * 2011; keep tracking the last used tree Nov 28, 2011; add tree format
	 *
	 * Apr 27, 2012; improve the tracking of the last used tree --
	 */
	private void addNewProjectAndTreePanels(
			final String projectName,
			final int projectDBSerial,
			String projectColor, 
			final String treeName,
			final String treeFormat,
			final String leafType,
			final String traitsType,
			final PhyloTree phylotree,
			final int treeDBSerial, 
			final boolean treeactive) {
 		
		// 1. chekc if project panel exists; if not create it
		if (!this.projectpanels.containsKey(projectName)) {
			final ProjectPanel newpropanel = new ProjectPanel(projectName,projectName, projectColor);
			newpropanel.setDBserialID(projectDBSerial); // May 13, 2011
			newpropanel.setActive(treeactive, treeactive);

			projectColor = this.projectcolors.get(projectName); // NOTE: if input color is empty, a random will can generated 
			this.userdatabrowser.add(newpropanel);
			this.projectpanels.put(projectName, newpropanel);
   
		} 
		// 2. add TreePanel
		this.projectpanels.get(projectName).setExpandSubpanels(true);
		continueAddTreePanel(projectName, treeName,
				treeFormat, leafType,  traitsType, 
				phylotree, treeDBSerial, treeactive);
		
	} // addNewProjectAndTreePanels

	
	public void showTree( final int treeDBSerial) {
		final String sessionid = MC.cookieman.getSessionID();
		TreePanel treePanel = treepanels.get(treeDBSerial);
		if (treePanel!=null) {
			activateTree(sessionid, treePanel.getParentID(), treePanel.key, treeDBSerial, treePanel);
		}
	}
	
	// Feb 2, 2011; new --
	private void activateTree(final String sessionid, final String projectName, final String treeName, final int treeDBSerial, final TreePanel newtreePanel) {
 
		if (treeDBSerial != 0 && !newtreePanel.getTreeStringAdded()) {

			newtreePanel.setBusy(true);

			// this will also activate this tree; Feb 2, 2012
			MC.rpc.getTreeStringAndFormatByDBSerialID(sessionid, treeDBSerial, new AsyncCallback<HashMap<String, String>>() {

				@Override
				public void onFailure(Throwable thrwbl) {
					Window.alert(" fail to connect to server and retrieve tree string for treeID: " + treeDBSerial + "; try again latter ");
				}

				@Override
				public void onSuccess(final HashMap<String, String> treecontensAndformat) {
//					Window.alert("onSuccess / format= " + treecontensAndformat.get("format"));
//					Window.alert("onSuccess / selectedleaves= " + treecontensAndformat.get("selectedleaves"));
					/*
					 * Oct 28, 2011; this part is critical for controlling the
					 * behaviors of panels and datasets revised:
					 */
					// then get canvas status for current tree
					MC.rpc.getTreeCanvasInfor(sessionid, treeDBSerial, new AsyncCallback<HashMap<String, Float>>() {

						@Override
						public void onFailure(Throwable thrwbl) {
							Window.alert(" fail to connect to server and retrieve canvas information for treeID: " + treeDBSerial + "; try again latter ");
							newtreePanel.setBusy(false);
						}

						@Override
						public void onSuccess(HashMap<String, Float> canvasParams) {
							// 1. first of all add the tree and set parameters using hash: canvasParams
							PhyloTree phylotree = new PhyloTree(
									treeDBSerial,
									treeName,
									treecontensAndformat.get("tree"),
									treecontensAndformat.get("format"),
									treecontensAndformat.get("selectedleaves"));
							boolean success = addPhyloTree(newtreePanel, 
									projectName, 
									treeName, treecontensAndformat.get("tree"),
									treecontensAndformat.get("format"), 
									null,
									null,
									phylotree, 
									canvasParams, 
									treeDBSerial, 
									true, 
									treePlotMode.RECT_CLADOGRAM);

							/*
							 * 2. then retrieve data from server; this has to be
							 * done after the tree is succefully loaded
							 */
							final PhyloTreeSVGViewer svgview = getSVGViewerByID(treeDBSerial);
							if (svgview != null && success) {
								// get status of subpanels/ dataset categories
								MC.rpc.getCatPanelInfo(sessionid, treeDBSerial, new AsyncCallback<HashMap<String, Boolean>>() {

									@Override
									public void onFailure(Throwable caught) {
										//
									}

									@Override
									public void onSuccess(HashMap<String, Boolean> result) {
										for (Map.Entry<String, Boolean> entry : result.entrySet()) {
											svgview.setDatasetEnabled(entry.getKey(), entry.getValue(), false);// alert the svgviewer about the status of each dataset
											setValue2CategoryPanelsCheckBox(treeDBSerial, entry.getKey(), entry.getValue()); // also set the status of checkbox on each catpanel
										}

										// get datasets for this tree
										MC.rpc.getDatasets(sessionid, treeDBSerial, new AsyncCallback<ArrayList<UserData>>() {

											@Override
											public void onFailure(Throwable caught) {
												Window.alert("fail to get datasets for tree : " + treeName);
											}

											@Override
											public void onSuccess(ArrayList<UserData> datasets) {
//												Window.alert("MC.rpc.getDatasets onSuccess : " + treeName + " * " + datasets.size());
												for (final UserData dataset : datasets) {
													addDataSet2Tree(treeDBSerial,
															dataset.getDBSerial(),
															dataset.getName(),
															dataset.getContent(),
															proMode.LIVE,
															ECatAndDatasetType.valueOf(dataset.getTypeOrFormat()),
															dataset.getActive());
												}
//												Window.alert("MC.rpc.getDatasets onSuccess : " + treeName + " * " + datasets.size());

												// Oct 28, 2011; reset the status of radio button / checkbox on the dataset panel; this is necessary for radio buttons
												for (final UserData dataset : datasets) {
													 
													if (uid2rbtn4datasetpanel.containsKey(dataset.getDBSerial()) && dataset.getActive()) {
														uid2rbtn4datasetpanel.get(dataset.getDBSerial()).setValue(true);
														System.out.println(" ---> " + dataset.getTypeOrFormat() + " - " + dataset.getName() + " - "
																+ dataset.getActive());
													}
												}
//												Window.alert("newtreePanel.setBusy(false); " );

												newtreePanel.setBusy(false);

												// newtreePanel.setTreeStringAdded(true); // this important!! May 16, 2011; May 17, 2011, already deal with this in addTreeAsString!!
											}
										});// get datasets

									}
								});// get subcat panel status

							}// if svgview is valid and if tree is successfully added to the project panel
						} // // get canvas inform with success
					}); // // get canvas inform
				}
			});
		} else {
			activateTreeSVGViewerByUniqID(treeDBSerial);
			changeUserPanelActiveStatus(treeDBSerial, treeName);
		}
	}

	// Apr 27, 2012; 
	protected void continueAddTreePanel(final String projectName,
			final String treeName,
			final String treeFormat, 
			final String leafType, 
			final String traitsType, 
			final PhyloTree phylotree,
			final int treeDBSerial, 
			final boolean active) {
 
		if (!this.treepanels.containsKey(treeDBSerial)) {
			final TreePanel newtreePanel = new TreePanel(projectName, treeName);
			final ProjectPanel propanel = this.projectpanels.get(projectName);
			propanel.addSubPanel(newtreePanel);

			System.out.print("add tree : " + treeName + " to project: " + projectName);

			this.treepanels.put(treeDBSerial, newtreePanel); // April 3, 2011;
			newtreePanel.setDBserialID(treeDBSerial);
			// newtreePanel.setActive(active, active);
			newtreePanel.setBusy(false);

			// Feb 2, 2012; keep demoproject panel expanded!
			propanel.setActive(active, active);// Nov 8, 2011;
			// Nov 08, 2011
			if (phylotree != null && phylotree.isTreeDataValid()) {
				addPhyloTree(newtreePanel, 
						projectName, 
						treeName,
						phylotree.getTreeString(),
						treeFormat, 
						leafType, 
						traitsType, 
						phylotree, 
						new HashMap<String, Float>(),
						0, 
						true,
						treePlotMode.RECT_CLADOGRAM);
			} 
			// activate active tree --
			//final String sessionid = (useraccount != null && useraccount.getIsSessionIDValid()) ? useraccount.getSessionID() : projectName.equals(demoproject) ? demoproject : null;
			final String sessionid = MC.cookieman.getSessionID();
			if (active) {
				activateTree(sessionid, projectName, treeName, treeDBSerial, newtreePanel);
			}

			/*
			 * ****************************************************************
			 *
			 * add a click listener to the header text if the first time this
			 * tree is clicked, retrieve data from server for current tree if
			 * the tree is not initiated otherwise: 1. close the tree that is
			 * currently activated; also close the project if current project
			 * and the active project are not the save 2. active the tree that
			 * the label belongs to
			 *
			 * Last update: May 11, 2012
			 *
			 * ****************************************************************
			 */
			newtreePanel.getHeaderText().addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent ce) {
					// May 11, 2012; check if current tree panel is busy , if not, add the tree
					if (!newtreePanel.getBusy()) {
						System.out.println(" try active tree : " + treeName + " with serial " + treeDBSerial + " for project : " + projectName);
						activateTree(sessionid, projectName, treeName, treeDBSerial, newtreePanel);
					}
				}// click event
			}); // click handler
		}
	}

	// April 14, 2011;
	public PhyloTreeSVGViewer getSVGViewerByID(int treeDBSerial) {
		return this.svgViewerPanels.containsKey(treeDBSerial) ? svgViewerPanels.get(treeDBSerial) : null;
	}

	/*
	 * April 14, 2011; only change the status (checked/ notchecked) of the
	 * checkbox
	 */

	public void setValue2CategoryPanelsCheckBox(int treeDBSerial, String catpanelID, Boolean active) {
		if (this.treepanels.containsKey(treeDBSerial)) {
			TreePanel treepanel = this.treepanels.get(treeDBSerial);
			CatPanel cp = (CatPanel) treepanel.getSubPanelByID(catpanelID);
			if (cp != null) {
				cp.getDisableAllCheckBox().setValue(active);
			}
		}
	}

	/*
	 * april 14, 2011; active / non-active Feb 2, 2012; donot change project
	 * panel status, only change tree panel statuses
	 */
	private void changeUserPanelActiveStatus(int treeDBSerial, String projectName ) { 
		// if current tree is set active, set current project panel as active and deactive all other trees
		for (Map.Entry<String, ProjectPanel> entry : this.projectpanels.entrySet()) {
			String proID = entry.getKey();
			ProjectPanel propanel = entry.getValue();
			//if (! proID.equals( demoproject ) ) { // Feb 2, 2012
			if (proID.equals(projectName)) {
				propanel.setActive(true, true);
			} else {
				propanel.setActive(false); // change active status
			}
			//}
		}
		// the same goes for the trees
		for (Entry<Integer, TreePanel> entry : this.treepanels.entrySet()) {
			Integer uID = entry.getKey();
			TreePanel trpanel = entry.getValue();
			if (uID.equals(treeDBSerial)) {
				trpanel.setActive(true, true);
			} else {
				trpanel.setActive(false, false);
			}
		}
	}

	protected void activateTreeSVGViewerByUniqID(int treeDBSerial) {

		PhyloTreeSVGViewer newsvgviewer = this.svgViewerPanels.get(treeDBSerial);
		if (newsvgviewer != null) {
			if (currentSVGViewer == null) {
				// if the first time
				//spanel.removeFromParent();
				//vpMainToolBarAndPlot.remove( spanel );
			} else {
				// replace current viewer with new one
				currentSVGViewer.removeFromParent();
				//vpMainToolBarAndPlot.remove( currentSVGViewer );
			}

			currentSVGViewer = newsvgviewer;
			spSVG.setWidget(currentSVGViewer);

			toolbar.setSvgviewer(currentSVGViewer);
		} else {

			if (currentSVGViewer != null) {
				currentSVGViewer.removeFromParent();
				currentSVGViewer = null;
				//spSVG.setWidget(spanel);
			}

			toolbar.setSvgviewer(currentSVGViewer);
		} 

		System.out.println("================== to change");
		/*
		 * Nov 08, 2011; keep tracking the last used tree; Apr 27, 2012;
		 * improvement
		 */
		final String sessionid = MC.cookieman.getSessionID();
		if (currentSVGViewer != null && currentSVGViewer.getDBserialID() != 0 && sessionid != null && !sessionid.isEmpty()) {
			System.out.println("========================= willdo");
			MC.rpc.changeTreeActiveStatus(sessionid, currentSVGViewer.getDBserialID(), new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
					MC.alertFailConnectingServer();
				}

				@Override
				public void onSuccess(Boolean result) {
					// do nothing here
					System.out.println("====================== success");
				}
			});
		}// Nov 08, 2011;
	}

	/*
	 * May 16, 2011; copy some code from addNewTreeAsString, with minor changes
	 * ------------------------------- 0riginal notes from addNewTreeAsString
	 * function: * April 6, 2011; add new tree 1. check if currentSVGViewer is
	 * null: if yes, do nothing; if not, close current active tree; if the new
	 * tree belongs to another project, set current project non-active create a
	 * new SVGview for new tree; 2. check if there is a tree with the same in
	 * current project NOTE: trees with the same name cannot be in the same
	 * project 3.
	 *
	 * April 11, 2011; add param: active
	 *
	 * @param treename @param projectID @param treestr @param plotmode @param
	 * promde @param setactive
	 *
	 * @return true if sucessful, false if not
	 *
	 * May 2, 2011; add a treedelete imagebutton to the tree panel; and add
	 * clickhandler to it
	 *
	 * may 13, 2011; add a param 'tree_db_serial'; if it's 0, try to add to
	 * database, it not, ignore this step also add param: canvasParams
	 * 
	 * April 4, 2013; when add a tree with 'tree_db_serial' value of 0 to database,
	 * activate it 
	 */
	private boolean addPhyloTree(final TreePanel newtreePanel,
			final String projectName,
			final String treeName,
			final String treeContentAsString,
			final String treeFormat, 
			final String leafType,
			final String traitsType,
			final PhyloTree phylotree,
			final HashMap<String, Float> canvasParams,
			final int treeDBSerial,
			final boolean setactive,
			final treePlotMode treeplotmode) {

		boolean success = true;

		System.out.println(" adding tree : " + treeName + " to project : " + projectName);
 
		if (treeName.isEmpty()) {
			Window.alert("tree name is mandatory");
			success = false;
		} else {

			/*
			 * 1. if currentSVGViewer isn't null and setactive is true, close
			 * currentSVGView also close current project if the new project
			 * isn't the same April 11, 2011; April 14, 2011; change active
			 * status for current projects and trees
			 */

			// check if this tree is valid and non-existant
			if (phylotree.isTreeDataValid() && !svgViewerPanels.containsKey(treeDBSerial)) {

				newtreePanel.setTreeStringAdded(true); // May 17, 2011; very important!!
				/*
				 * 1. create a PhyloTreeSVGViewer for this tree 2. add this new
				 * tree to current SVG canvus 3. update TreeSVGview and toolbar
				 * status
				 */
				final PhyloTreeSVGViewer newSVGViewer = new PhyloTreeSVGViewer(phylotree, projectName, treeplotmode, canvasParams);

				// Oct 28, 2011; pass sessionID to the new SVGViewer; 
				String sessionID = MC.cookieman.getSessionID();
				if (sessionID != null && !sessionID.isEmpty()) {
					newSVGViewer.setSessionID(sessionID);
				}
				svgViewerPanels.put(phylotree.getTreeId(), newSVGViewer);
				if (setactive) {// April 11, 2011
					activateTreeSVGViewerByUniqID(treeDBSerial);
					changeUserPanelActiveStatus(treeDBSerial, projectName);
				}// if setactive is tree

				/*
				 * May 13, add this new tree to server, and get a new db_serial
				 * for this tree
				 */
				System.out.println(this.projectpanels.keySet().toString());
				int project_dbserial = this.projectpanels.get(projectName).getDBserialID();
				System.out.println("  treeDB serial is: " + treeDBSerial + "; project serial is: " + project_dbserial);

				if (treeDBSerial == 0 && project_dbserial != 0) {
					String sessionid = MC.cookieman.getSessionID();
					if (!sessionid.isEmpty()) {

						MC.rpc.addTree(sessionid, project_dbserial, 
								treeName,
								treeContentAsString, 
								treeFormat, 
								leafType,
								traitsType,
								true, 
								new AsyncCallback<Integer>() {

							@Override
							public void onFailure(Throwable caught) {
								//
							}

							@Override
							public void onSuccess(Integer tree_dbserial) {
								newtreePanel.setDBserialID(tree_dbserial);
								newSVGViewer.setDBSerialID(tree_dbserial);
							}
						});
					}//

				} else {
					newtreePanel.setDBserialID(treeDBSerial);
					newSVGViewer.setDBSerialID(treeDBSerial);
				}

				/*
				 * 3. create all subpanels and add them to this tree; initially
				 * the subpanel are all invisibal becase they contain nothing
				 * Saved Views Piecharts on branches branch colors tree leaf
				 * label colors tree leaf label background colors charts
				 */
				final CatPanel savedviewsCP = new CatPanel(ECatAndDatasetType.SAVEDVIEWS);
				savedviewsCP.setVisible(false);
				newtreePanel.addSubPanel(savedviewsCP);

				final CatPanel piesCP = new CatPanel(ECatAndDatasetType.PIES);
				newtreePanel.addSubPanel(piesCP);
				piesCP.getDisableAllCheckBox().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						boolean active = piesCP.getDisableAllCheckBox().getValue();
						newSVGViewer.getPies().setDisableAllPiechartData(!active); // April 6, 2011
						changeCatpanelActiveStatusOnServer(phylotree.getTreeId(), ECatAndDatasetType.PIES, active);
					}
				});
				piesCP.setVisible(false);
				newtreePanel.addSubPanel(piesCP);

				final CatPanel branchcolorsCP = new CatPanel(ECatAndDatasetType.BRANCHECOLORS);
				newtreePanel.addSubPanel(branchcolorsCP);
				branchcolorsCP.getDisableAllCheckBox().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						boolean active = branchcolorsCP.getDisableAllCheckBox().getValue();
						newSVGViewer.getTreeSkeleton().setDisableAllBranchColorData(!active);
						changeCatpanelActiveStatusOnServer(treeDBSerial, ECatAndDatasetType.BRANCHECOLORS, active);
					}
				});
				branchcolorsCP.setVisible(false);
				newtreePanel.addSubPanel(branchcolorsCP);

				final CatPanel leafcolorsCP = new CatPanel(ECatAndDatasetType.LEAFLABELCOLORS);
				newtreePanel.addSubPanel(leafcolorsCP);
				leafcolorsCP.getDisableAllCheckBox().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						boolean active = leafcolorsCP.getDisableAllCheckBox().getValue();
						newSVGViewer.getTreeSkeleton().setDisableAllLabelColorData(!active);
						changeCatpanelActiveStatusOnServer(treeDBSerial, ECatAndDatasetType.LEAFLABELCOLORS, active);
					}
				});
				leafcolorsCP.setVisible(false);
				newtreePanel.addSubPanel(leafcolorsCP);

				final CatPanel leafBKcolorsCP = new CatPanel(ECatAndDatasetType.LEAFBKCOLORS);
				newtreePanel.addSubPanel(leafBKcolorsCP);
				leafBKcolorsCP.getDisableAllCheckBox().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						boolean active = leafBKcolorsCP.getDisableAllCheckBox().getValue();
						newSVGViewer.getTreeSkeleton().setDisableAllLabelBKColorData(!active);
						changeCatpanelActiveStatusOnServer(treeDBSerial, ECatAndDatasetType.LEAFBKCOLORS, active);
					}
				});
				leafBKcolorsCP.setVisible(false);
				newtreePanel.addSubPanel(leafBKcolorsCP);

				final CatPanel chartsCP = new CatPanel(ECatAndDatasetType.CHARTS);
				newtreePanel.addSubPanel(chartsCP);
				chartsCP.getDisableAllCheckBox().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						boolean active = chartsCP.getDisableAllCheckBox().getValue();
						newSVGViewer.setDisableAllCharts(!active);
						changeCatpanelActiveStatusOnServer(phylotree.getTreeId(), ECatAndDatasetType.CHARTS, active);
					}
				});
				chartsCP.setVisible(false);
				newtreePanel.addSubPanel(chartsCP);

				/*
				 * maybe part 4: May 2, 2011; add clickhandler to treedelete
				 * panel
				 */
				newtreePanel.getTreeDelImg().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						/*
						 * two possibilities: a. current tree is active b.
						 * current tree is not
						 */
						// if clicked, popup a dialog ask user to confirm deleteion
						final ConfirmDialogbox confirm = new ConfirmDialogbox("All datasets of this tree will also be deleted. Continue?");
						confirm.center();
						
						MC.ebus.addHandler(org.evolview.client.ConfirmationYesEvent.TYPE, new ConfirmationYesHandler(){
							
							@Override
							public void onConfirmationYesClicked(org.evolview.client.ConfirmationYesEvent yesEvent) {
								System.err.println("lets do it!!");
								confirm.hide();

								// first of all, delete this tree and related data from server
								final String sessionid = MC.cookieman.getSessionID();
								if (!sessionid.isEmpty()) {
									MC.rpc.deleteTree(sessionid, newtreePanel.getDBserialID(), new AsyncCallback<Integer>() {

										@Override
										public void onFailure(Throwable caught) {
											//
										}

										@Override
										public void onSuccess(Integer treeid) {
											if (treeid != 0) {
												treepanels.remove(treeid);
												projectpanels.get(projectName).removeSubPanel(newtreePanel);
												svgViewerPanels.remove(treeid); // also remove from hash 

												// if current tree is active 
												int current_uniqueID =  currentSVGViewer==null ? -1 :currentSVGViewer.getTreeID() ;
												if (treeid.equals(current_uniqueID)) {
													projectpanels.get(projectName).setActive(false, true);
													// 2. activate the tree being clicked
													activateTreeSVGViewerByUniqID(-1); //
												}//
											}
										}
									});

								} else {// if session IS null
									treepanels.remove(newtreePanel.getDBserialID());
									projectpanels.get(projectName).removeSubPanel(newtreePanel);
									svgViewerPanels.remove(newtreePanel.getDBserialID()); // also remove from hash 

									// if current tree is active 
									int current_uniqueID = currentSVGViewer==null ? -1 : currentSVGViewer.getTreeID()  ;
									if (newtreePanel.getDBserialID() == current_uniqueID) {
										projectpanels.get(projectName).setActive(false, true);
										// 2. activate the tree being clicked
										activateTreeSVGViewerByUniqID(-1); //
									}//
								} // if session is null

							}});

					}
				});

				/**
				 * *********************************************
				 * May 30, 2012; handler for editing a tree.
				 * *********************************************
				 */
				newtreePanel.getTreeEditImg().addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						/*
						 * only possible if current tree is active
						 */
						int current_uniqueID = currentSVGViewer == null ? -1 :  currentSVGViewer.getTreeID()  ;
						if (treeDBSerial == current_uniqueID) {

							// edit the tree
							dboxEditTree.hideNameLabelAndTextBox();
							dboxEditTree.setTreeFormatRowVisible(true);
							dboxEditTree.setTreeFormat(currentSVGViewer.getPhyloTree().getTreeFormat());
							dboxEditTree.setDataSetContent(currentSVGViewer.getPhyloTree().getTreeString());
							final Set<String> currentSelectedLeaves = currentSVGViewer.getPhyloTree().getLeafSelectionSet(); 
							dboxEditTree.center();
							MC.ebus.addHandler( EditTreeDataUploadEvent.TYPE, new EditTreeDataUploadHandler(){

								@Override
								public void onEditTreeDataUpload(EditTreeDataUploadEvent event) {
									/*
									 * make a new phyloTree object
									 */
									String treeString = dboxEditTree.getDataSetContent().trim();
									if (treeString.isEmpty()) {
										MC.alert.setMessageAndShow("tree string is empty" + treeString, AlertWidget.AlertWidgetType.error);
									} else {
										PhyloTree newPhyloTree = new PhyloTree(
												currentSVGViewer.getPhyloTree().getTreeId(), 
												currentSVGViewer.getPhyloTree().getTreeName(), 
												treeString,
												dboxEditTree .getTreeFormat(),
												null);
										if ( !newPhyloTree.isTreeDataValid() ) {
											SafeHtmlBuilder shb = new SafeHtmlBuilder();
											shb.appendEscaped("!! Error !!").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
											shb.appendEscaped(newPhyloTree.getErrorMessage()).appendHtmlConstant("</br>").appendHtmlConstant("</br>");
											shb.appendEscaped("please edit your input").appendHtmlConstant("</br>");
											MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.error);
										} else {
											/*
											 * check if the input is different
											 * from original
											 */
											if ( treeString.equalsIgnoreCase(currentSVGViewer.getPhyloTree().getTreeString()) ) {
												MC.alert.setMessageAndShow("your input seems to be the same as the old tree; nothing would be changed",
														AlertWidget.AlertWidgetType.info);
											} else {
												/*
												 * check if all old tree leaf
												 * nodes are covered by the new
												 * tree.
												 */
												boolean isAllCovered = true;
												ArrayList<String> newLeafNames = newPhyloTree.getAllLeafLabels();
												for (String oldleaf : currentSVGViewer.getPhyloTree().getAllLeafLabels()) {
													if (!newLeafNames.contains(oldleaf)) {
														isAllCovered = false;
														break;
													}
												}

												if (!isAllCovered) {
													SafeHtmlBuilder shb = new SafeHtmlBuilder();
													shb.appendEscaped("!! Warning !!").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
													shb.appendEscaped("your new tree is valid, ").appendHtmlConstant("</br>");
													shb.appendEscaped("however some old leafs are not in your new tree!").appendHtmlConstant("</br>")
															.appendHtmlConstant("</br>");
													shb.appendEscaped("Dismiss this message to continue!").appendHtmlConstant("</br>")
															.appendHtmlConstant("</br>");
													MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.warning);
												}

												/*
												 * the modify the old tree and update all legends and charts
												 * last modified: May 18, 2013 --
												 */
												currentSVGViewer.getPhyloTree().setTree(phylotree.getTreeId(),
														treeName, 
														treeString, 
														treeFormat,
														currentSelectedLeaves);
												currentSVGViewer.drawPhyloTreeUsingSVG(); // draw the tree
												currentSVGViewer.updateAllCharts();
												currentSVGViewer.updateAllLegends();
												// <<< -- modified : may 18, 2013 -- <<<

												/*
												 * warning:
												 */
												SafeHtmlBuilder shb = new SafeHtmlBuilder();
												shb.appendEscaped("!! NOTE !!").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
												shb.appendEscaped("if some annotations are not displayed correctly, ").appendHtmlConstant("</br>");
												shb.appendEscaped("please press the 'refresh' button of your browser!!").appendHtmlConstant("</br>")
														.appendHtmlConstant("</br>");
												MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.info);

												/*
												 * save changes to the tree on
												 * server
												 */
												final String sessionid = MC.cookieman.getSessionID();
												if (!sessionid.isEmpty()) {
													MC.rpc.changeTreeContents(sessionid, currentSVGViewer.getDBserialID(), treeString,
															new AsyncCallback<Boolean>() {

																@Override
																public void onFailure(Throwable caught) {
																	SafeHtmlBuilder shb = new SafeHtmlBuilder();
																	shb.appendEscaped("!! Warning !!").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
																	shb.appendEscaped("fail to save your changes to server, ").appendHtmlConstant("</br>");
																	shb.appendEscaped("please try again latter!!").appendHtmlConstant("</br>")
																			.appendHtmlConstant("</br>");
																	MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.warning);
																}

																@Override
																public void onSuccess(Boolean result) {
																}
															});
												}
											}

											/*
											 * hide dialogbox
											 */
											dboxEditTree.hide();
										} // if phylotree object is valid
									} // if tree isn't empty
								}});
						} else {
							MC.alert.setMessageAndShow("only active tree can be edited.", AlertWidget.AlertWidgetType.warning);
						}
					}
				});

				/*
				 * before hiding this dialog, empty text boxes
				 */
				//dboxFileOpen.clearUserInput(); // this sometimes may cause trouble
				//dboxFileOpen.hide();

			} else {
				if (this.svgViewerPanels.containsKey(treeDBSerial)) {
					Window.alert("tree with the same exists in the same project, try a different name or delete existing tree!");
				} else if (!phylotree.isTreeDataValid()) {
					String errormsg = phylotree.getErrorMessage();
					Window.alert("error: " + errormsg);
					success = false;
				}
			}// if tree is valid
		}// if tree name is empty

		return success;
	}// add tree as string, created May 16, 2011;

	

	/*
	 * April 5, 2011; Here datatype can be one of the following:
	 * this.LEAFBKCOLORS | this.LEAFLABELCOLORS | this.BRANCHECOLORS | April 6,
	 * 2011; try to add the following two datasets: this.PIES | this.CHARTS
	 * change the function name to : addDataSet2Tree (from
	 * addColorData2ActiveTree);
	 *
	 * steps: 1. add dataset to branches/ labels ... 2. create new or using
	 * existing user-data-browsing panel 3. go through the flextable
	 * corresponding to each category panel and check if current dataset exists
	 * (by datasetname) 3.1 if exists, activate it 3.2 if not, add it
	 *
	 * April 11, 2011; Add two param: projectID and treeName Oct 28, 2011; add
	 * one more param: active; Nov 07, 2011; add codes for handling trees in the
	 * "demos" project; any changes to demos-trees will never be save to server
	 *
	 * -------------------------------------------------------------------------
	 * May 13, 2012; add new datatype: protein domains
	 */
	public boolean addDataSet2Tree(final int treeeId, 
			final int datasetId, 
			final String datasetName,
			String datasetContent,
			final proMode promode,
			final ECatAndDatasetType datatype,
			final boolean active) {

//		Window.alert("addDataSet2Tree : " + treeName  +" , " + datasetName);
		boolean success = true;
		final PhyloTreeSVGViewer phylotreesvgviewer = this.svgViewerPanels.get(treeeId);

		if (phylotreesvgviewer != null) {
			if (datasetName.isEmpty()) {
				success = false;
				if (promode.equals(proMode.LIVE)) {
					MC.alert.setMessageAndShow("please provide a name for this dataset", AlertWidget.AlertWidgetType.error);
				}
			} else {

				boolean isDataValid = true;
				String errorMSG = ""; 
				final ECatAndDatasetType catpanalType = 
						(datatype.equals(ECatAndDatasetType.BARS) || datatype.equals(ECatAndDatasetType.STRIPS)|| datatype.equals(ECatAndDatasetType.HEATMAPS) || datatype.equals(ECatAndDatasetType.PROTEINDOMAINS)) ? 
								ECatAndDatasetType.CHARTS : datatype;
//				final String catpanalName = catpanalType.label;
//				Window.alert("addDataSet2Tree , catpanalName : " + catpanalName);
				/*
				 * 1. add dataset to targets April 6, 2011; Pies and Bars are
				 * numerical
				 */
				if (datatype.equals(ECatAndDatasetType.PIES) || datatype.equals(ECatAndDatasetType.BARS)) {
					NumericMatrixDataContainer numdata = new NumericMatrixDataContainer(datasetName, datasetContent, phylotreesvgviewer.getPhyloTree());
					isDataValid = numdata.isDataValid();
					errorMSG = numdata.getErrorMessage();
					if (isDataValid) {
						if (datatype.equals(ECatAndDatasetType.PIES)) {
							/*
							 * add current piechart data to phylotreesvgviewer
							 * && treedata container
							 */
							phylotreesvgviewer.getPies().addPCData(datasetId, numdata, active);
						} else if (datatype.equals(ECatAndDatasetType.BARS)) {
							phylotreesvgviewer.addBarplotData(datasetId, numdata, active); // April 6, 2011
						}
					} else {
						MC.alert.setMessageAndShow(" error: " + errorMSG + "; data not upload/ updated ", AlertWidget.AlertWidgetType.error);
						success = false;
					}
				} else if (datatype.equals(ECatAndDatasetType.HEATMAPS)) {
					HeatmapMatrixDataContainer hmData = new HeatmapMatrixDataContainer(datasetName, datasetContent, phylotreesvgviewer.getPhyloTree());
					isDataValid = hmData.isDataValid();
					errorMSG = hmData.getErrorMessage();
					if (isDataValid) { 
						phylotreesvgviewer.addHeatMapData(datasetId, hmData, active); // April 6, 2011
					} else {
						SafeHtmlBuilder shb = new SafeHtmlBuilder();
						shb.appendEscaped("!! Error !!").appendHtmlConstant("</br>");
						shb.appendEscaped(errorMSG).appendHtmlConstant("</br>");
						shb.appendEscaped("data not upload/ updated").appendHtmlConstant("</br>");
						MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.error);
						success = false;
					}
				} else if (datatype.equals(ECatAndDatasetType.PROTEINDOMAINS)) {
					/**
					 * *************************************************
					 * if dataset is protein domain May 13, 2012
					 * **************************************************
					 * -- oct 20, 2013 --
					 * if no colors are specified, random colors will be generated and added to the dataset
					 * this modified dataset will be uploaded to our server, instead of the original datase!!!!!!
					 */
					ProteinDomainDataContainerParser domaindata = new ProteinDomainDataContainerParser(datasetName, datasetContent);
					isDataValid = domaindata.isDataValid();
					errorMSG = domaindata.getErrorMessage();
					if (isDataValid) {
						if( domaindata.isDataModified() ){
							datasetContent = domaindata.getOriginal_datastring(); // get modified data; Oct 20, 2013 --
							this.dboxProteinDomains.setDataSetContent( datasetContent ); // Oct 20, 2013; also change the original dataset content --
						}
						phylotreesvgviewer.addProteinDomainData(datasetId, domaindata, active); // April 6, 2011
					} else {
						SafeHtmlBuilder shb = new SafeHtmlBuilder();
						shb.appendEscaped("!! Error !!").appendHtmlConstant("</br>");
						shb.appendEscaped(errorMSG).appendHtmlConstant("</br>");
						shb.appendEscaped("data not upload/ updated").appendHtmlConstant("</br>");
						MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.error);
						success = false;
					}

				} else { // if not numerical data
					/*
					 * this.LEAFBKCOLORS | this.LEAFLABELCOLORS |
					 * this.BRANCHECOLORS | this.STRIPS
					 */
					ColorDataContainerParser colordata = new ColorDataContainerParser(datasetName, datasetContent);
					isDataValid = colordata.isDataValid();
					errorMSG = colordata.getErrorMessage();
					if (isDataValid) {
						/*
						 * add current data data to phylotreesvgviewer &&
						 * treedata container
						 */
						if (datatype.equals(ECatAndDatasetType.LEAFBKCOLORS)) {
							phylotreesvgviewer.getTreeSkeleton().addColorsetToTreeNodes(datasetId, colordata, TreeDecoType.LEAFBKCOLOR, active);
						} else if (datatype.equals(ECatAndDatasetType.LEAFLABELCOLORS)) {
							phylotreesvgviewer.getTreeSkeleton().addColorsetToTreeNodes(datasetId, colordata, TreeDecoType.LEAFCOLOR, active);
						} else if (datatype.equals(ECatAndDatasetType.BRANCHECOLORS)) {
							phylotreesvgviewer.getTreeSkeleton().addColorsetToTreeNodes(datasetId, colordata, TreeDecoType.BRANCHCOLOR, active);
						} else if (datatype.equals(ECatAndDatasetType.STRIPS)) {
							phylotreesvgviewer.addColorStripData(datasetId, colordata, active); // full dataset name isn't necessary; but I just keep it here at the moment
						}
					} else {
						Window.alert(" error: " + errorMSG + "; data not upload/ updated ");
						success = false;
					}
				}// different datatypes
//				Window.alert("addDataSet2Tree, isDataValid ? " + isDataValid);
				if (isDataValid) {
					/*
					 * 2. create new or use existing user browsing panels
					 */   
					TreePanel treepanel = this.treepanels.get(treeeId);
					if (treepanel != null) {

						boolean datset_exists = false;
						/*
						 * 3. go through the flextable corresponding to each
						 * category panel and check if current dataset exists
						 * (by datasetname) if true, activate it; if not, add it
						 * April 3, 2011; check if current data exists, if not,
						 * added it to the flextable; get the flex table
						 */
						//Window.alert( datatype );
						CatPanel newCatPanel = (CatPanel) treepanel.getSubPanelByID(catpanalType.name());// APril 6, 2011; here I use catpanalName
						if(newCatPanel == null) {
							newCatPanel = new CatPanel(catpanalType);
							newCatPanel.setVisible(false);
							treepanel.addSubPanel(newCatPanel); 
						}
						newCatPanel.setVisible(true);

						final FlexTable flextable = newCatPanel.getFlexTable();
						int idx = flextable.getRowCount();

						/*
						 * April 3, 2011; 3.1 if user edit an inactive dataset,
						 * active it.
						 */
						for (int dsidx = 0; dsidx < idx; dsidx++) {
							if (catpanalType.equals(ECatAndDatasetType.CHARTS)) { // here I use catpanalName; APril 6, 2011
								CheckBox c = (CheckBox) flextable.getWidget(dsidx, 0); 
								if (c.getText().equals(datasetName)) {
									datset_exists = true;
									c.setValue(true);
									break;
								}
							} else {
								RadioButton c = (RadioButton) flextable.getWidget(dsidx, 0); 
								if (c.getText().equals(datasetName)) {
									datset_exists = true;
									c.setValue(true);
									break;
								}
							}
						}//  check if current pcdata exists

						
						/*
						 * 3.2 if dataset not exists in corresponding flextable,
						 * add it
						 */
						if (!datset_exists) { // if current datset not exists
							/*
							 * there will be five widgets in this arraylist
							 * Radiobutton/check | edit | moveup | movedown |
							 * delete
							 */
							// 1. prepare widgets
							final String rbtnGroupID = treeeId + datatype.name();
							final String firstWidgetLabel = datasetName;
							final ImgBtn btnDelete = new ImgBtn("images/icons/12_new/delete", true);
							final ImgBtn btnMoveUp = new ImgBtn("images/icons/12_new/moveup", true);
							final ImgBtn btnMoveDown = new ImgBtn("images/icons/12_new/movedown", true);
							final ImgBtn btnEdit = new ImgBtn("images/icons/12_new/edit", true);

							final int iconsize = 10;
							btnDelete.setPixelSize(iconsize, iconsize);
							btnMoveUp.setPixelSize(iconsize, iconsize);
							btnMoveDown.setPixelSize(iconsize, iconsize);
							btnEdit.setPixelSize(iconsize, iconsize);

							btnDelete.setTitle("delete current dataset and activate the next dataset (or the first if current is the last)");
							btnMoveUp.setTitle("move one row up if not the first row and change the drawing order of the datasets accordingly");
							btnMoveDown.setTitle("move one row down if not the last row and change the order of the datasets accordingly");
							btnEdit.setTitle("edit current dataset");

							// 2. add widgets to flex table;
							int flexidx = flextable.getRowCount();

							/*
							 * April 3, 2011; if the btnEdit button is clicked,
							 * show the data dialogbox and add data from current
							 * piechart to textareas/ textboxes of this
							 * dialogbox
							 *
							 */
							btnEdit.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									/*
									 * get original user input and show it in
									 * the popup dialogbox
									 */ 
									if (catpanalType.equals(ECatAndDatasetType.CHARTS)) {
										String original_user_input = phylotreesvgviewer.getOriginalUserInputAsStringByID(datasetId);
										if (datatype.equals(ECatAndDatasetType.STRIPS)) {
											dboxColorStrips.setDataSetName(datasetName);
											dboxColorStrips.setDataSetContent(original_user_input);
											dboxColorStrips.center();
											dboxColorStrips.show();
										} else if (datatype.equals(ECatAndDatasetType.BARS)) {
											dboxBarPlots.setDataSetName(datasetName);
											dboxBarPlots.setDataSetContent(original_user_input);
											dboxBarPlots.center();
											dboxBarPlots.show();
										} else if (datatype.equals(ECatAndDatasetType.HEATMAPS)) {
											dboxHeatmap.setDataSetName(datasetName);
											dboxHeatmap.setDataSetContent(original_user_input);
											dboxHeatmap.center();
											dboxHeatmap.show();
										} else if (datatype.equals(ECatAndDatasetType.PROTEINDOMAINS)) {
											dboxProteinDomains.setDataSetName(datasetName);
											dboxProteinDomains.setDataSetContent(original_user_input);
											dboxProteinDomains.center();
											dboxProteinDomains.show();
										}
									} else {
										String original_user_input = phylotreesvgviewer.getTreeSkeleton().getOriginalUserInputAsStringByID(datasetId);
										if (datatype.equals(ECatAndDatasetType.LEAFBKCOLORS)) {
											dboxLeafLabelBKColor.setDataSetName(datasetName);
											dboxLeafLabelBKColor.setDataSetContent(original_user_input);
											dboxLeafLabelBKColor.center();
											dboxLeafLabelBKColor.show();
										} else if (datatype.equals(ECatAndDatasetType.LEAFLABELCOLORS)) {
											dboxLeafLabelColor.setDataSetName(datasetName);
											dboxLeafLabelColor.setDataSetContent(original_user_input);
											dboxLeafLabelColor.center();
											dboxLeafLabelColor.show();
										} else if (datatype.equals(ECatAndDatasetType.BRANCHECOLORS)) {
											dboxBranchColor.setDataSetName(datasetName);
											dboxBranchColor.setDataSetContent(original_user_input);
											dboxBranchColor.center();
											dboxBranchColor.show();
										} else if (datatype.equals(ECatAndDatasetType.PIES)) {
											dboxDataUploadPieChart.setDataSetName(datasetName);
											dboxDataUploadPieChart.setDataSetContent(phylotreesvgviewer.getPies().getOriginalUserInputByDatasetID(datasetId));
											dboxDataUploadPieChart.center();
											dboxDataUploadPieChart.show();
										}
									}
								}
							}); // a new edit button

							/*
							 * add click handler to Buttons: April 6, 2011
							 * activate/ deactivate datasets delete datasets
							 *
							 * April 13, 2011; comunicate with server; activate
							 * and deactivate datasets
							 *
							 */
							if (catpanalType.equals(ECatAndDatasetType.CHARTS)) {

								// checkbox for CHARTS
								final CheckBox checkbox = new CheckBox(firstWidgetLabel);
								DOM.setStyleAttribute(checkbox.getElement(), "fontSize", "90%"); // April 14, 2011;

								checkbox.setValue(true);
								checkbox.setFormValue(Integer.toString(datasetId));
								checkbox.setTitle("click to activate this dataset");
								flextable.setWidget(flexidx, 0, checkbox);

								/*
								 * this will deal with both BARS and STRIPS;
								 * April 6, 2011
								 */
								checkbox.setValue(active);// Oct 28, 2011;
								checkbox.addClickHandler(new ClickHandler() {  
									@Override
									public void onClick(ClickEvent event) { 
										phylotreesvgviewer.setActiveChartByID(datasetId, checkbox.getValue());
										changeDatasetActiveStatusOnServer(treeeId, datasetName, datatype, checkbox.getValue());
									}
								});// April 5, 2011;

								/*
								 * if delete button is clicked, delete current
								 * dataset; if current is active, the function
								 * in 'phylotreesvgviewer' will take care of it
								 */
								btnDelete.addClickHandler(new ClickHandler() {

									@Override
									public void onClick(final ClickEvent event) {

										final int current_row = flextable.getCellForEvent(event).getRowIndex(); // there are two clickEvents, the one on btnDelete has to be used!!

										/*
										 * April 14, 2011; delete Oct 26, 2011;
										 * ask for confirmation before deletion
										 */
										final ConfirmDialogbox confirm = new ConfirmDialogbox("You're about to delete this dataset, continue?");
										confirm.center();
										MC.ebus.addHandler( org.evolview.client.ConfirmationYesEvent.TYPE, new ConfirmationYesHandler(){

											@Override
											public void onConfirmationYesClicked(ConfirmationYesEvent yesEvent) {
												deleteDatasetFromServer( 
														currentSVGViewer.getTreeID(), 
														checkbox.getText(),
														datatype.name());

												phylotreesvgviewer.deleteChartByID(datasetId);
												flextable.removeRow(current_row); // fix a bug here; Dec 15, 2011 
												confirm.hide();
											}} );
									}
								}); // delete button

							} else {

								final RadioButton rbutton = new RadioButton(rbtnGroupID, firstWidgetLabel);

								DOM.setStyleAttribute(rbutton.getElement(), "fontSize", "90%"); // April 14, 2011;
								rbutton.setValue(true);
								rbutton.setFormValue(Integer.toString(datasetId));
								rbutton.setTitle("click to activate this dataset");
								flextable.setWidget(flexidx, 0, rbutton);

								this.uid2rbtn4datasetpanel.put(datasetId, rbutton);

								/*
								 * >>>>>> the following is for LEAFLABELCOLORS,
								 * LEAFBKCOLORS and BRANCHECOLORS and PIES
								 * >>>>>> if radio button is clicked and current
								 * dataset isn't active, activate it
								 */
								rbutton.addClickHandler(new ClickHandler() {

									@Override
									public void onClick(ClickEvent event) {
										if (rbutton.getValue()) {
											System.out.println("  --> rbutton clicked; " + datatype + "; datasetid: " + datasetId);
											if (datatype.equals(ECatAndDatasetType.LEAFBKCOLORS)) {
												phylotreesvgviewer.getTreeSkeleton().setActiveLeafBKcolorByDatasetID(datasetId);
											} else if (datatype.equals(ECatAndDatasetType.LEAFBKCOLORS)) {
												phylotreesvgviewer.getTreeSkeleton().setActiveLeafColorSetByID(datasetId);
											} else if (datatype.equals(ECatAndDatasetType.BRANCHECOLORS)) {
												phylotreesvgviewer.getTreeSkeleton().setActiveBranchColorByDatasetID(datasetId);
											} else if (datatype.equals(ECatAndDatasetType.PIES)) {
												phylotreesvgviewer.getPies().setActivePCDataByID(datasetId);
											}

											changeDatasetActiveStatusOnServer(treeeId, datasetName, datatype, rbutton.getValue());
										}
									}
								});// April 5, 2011;

								/*
								 * if delete button is clicked, delete current
								 * dataset; if current is active, there will be
								 * three possibilities NOTE: is the value of
								 * this button is true, then it's activated 1.
								 * if it's the only row, then do nothing 2. if
								 * current row is the last, then activate the
								 * first row 3. if it's in the middle, activate
								 * the next
								 */
								btnDelete.addClickHandler(new ClickHandler() {

									@Override
									public void onClick(final ClickEvent event) {
										final int rows_total = flextable.getRowCount();
										final int current_row = flextable.getCellForEvent(event).getRowIndex(); // there are two clickEvents, the one on btnDelete has to be used!!

										// show a popup panel then;
										final ConfirmDialogbox confirm = new ConfirmDialogbox("You're about to delete this dataset, continue?");
										confirm.center();
										
										MC.ebus.addHandler( org.evolview.client.ConfirmationYesEvent.TYPE, new ConfirmationYesHandler(){
											@Override
											public void onConfirmationYesClicked(ConfirmationYesEvent yesEvent) {
												if (rbutton.getValue()) { // if current dataset is active
													Integer next_active_datasetID = null;
													if (rows_total == 1) { // 1, if there is only one pcdata, do nothing
														next_active_datasetID = -1;
													} else if (rows_total == (current_row + 1)) { // 2.if current data is the last row, activate the first row
														RadioButton nextRbtn = (RadioButton) flextable.getWidget(0, 0);
														nextRbtn.setValue(Boolean.TRUE);
														next_active_datasetID = Integer.parseInt(nextRbtn.getFormValue());
													} else {
														// April 3, 2011; activate the next one;
														RadioButton nextRbtn = (RadioButton) flextable.getWidget(current_row + 1, 0);
														nextRbtn.setValue(Boolean.TRUE);
														next_active_datasetID = Integer.parseInt(nextRbtn.getFormValue());
													}

													if (datatype.equals(ECatAndDatasetType.LEAFBKCOLORS)) {
														phylotreesvgviewer.getTreeSkeleton().setActiveLeafBKcolorByDatasetID(next_active_datasetID); // set it to empty
													} else if (datatype.equals(ECatAndDatasetType.LEAFLABELCOLORS)) {
														phylotreesvgviewer.getTreeSkeleton().setActiveLeafColorSetByID(next_active_datasetID);
													} else if (datatype.equals(ECatAndDatasetType.BRANCHECOLORS)) {
														phylotreesvgviewer.getTreeSkeleton().setActiveBranchColorByDatasetID(next_active_datasetID);
													} else if (datatype.equals(ECatAndDatasetType.PIES)) {
														phylotreesvgviewer.getPies().setActivePCDataByID(next_active_datasetID);
													}
												}

												/*
												 * delete current row; if the
												 * flextalbe is empty, hide the
												 * control panel; April 6, 2011;
												 */
												flextable.removeRow(current_row);
												// April 14, 2011;
												deleteDatasetFromServer(currentSVGViewer.getTreeID(), rbutton.getText(), datatype.name());

												// Nov 7, 2011; hide the dialog box at the end
												confirm.hide();
											}});// ask for confirmation before deleteion; Oct 26, 2011
									}
								}); // delete button

							}// click handlers for delete and the checkbox/ radiobutton ; April 5, 2011;

							/*
							 * Also to the moveUp and moveDown buttons the
							 * moveup button; April 5, 2011
							 */
							btnMoveUp.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {

									/*
									 * click and this row goes up HOW it works:
									 * 1. store all widgets from current row to
									 * an ArrayList 2. delete current row 3.
									 * insert a new row before the previous row
									 * 4. insert all widgets in the ArrayList
									 */
									final int current_row = flextable.getCellForEvent(event).getRowIndex();

									if (current_row != 0) {// if not the first row
										// 1
										ArrayList<Widget> row = new ArrayList<Widget>();
										for (int c = 0; c < flextable.getCellCount(current_row); c++) {
											row.add(flextable.getWidget(current_row, c));
										}
										// 2
										flextable.removeRow(current_row);
										// 3 
										int newrowidx = flextable.insertRow(current_row - 1);
										// 4
										for (int nc = 0; nc < row.size(); nc++) {
											flextable.setWidget(newrowidx, nc, row.get(nc));
										}

										/*
										 * get dataset IDs
										 */
										Integer dataset1 = null, dataset2 = null;
										if (catpanalType.equals(ECatAndDatasetType.CHARTS)) {
											dataset1 = Integer.parseInt(((CheckBox) flextable.getWidget(newrowidx, 0)).getFormValue());
											dataset2 = Integer.parseInt(((CheckBox) flextable.getWidget(newrowidx + 1, 0)).getFormValue());

											/*
											 * if plot type is CHART, swap the
											 * two rows
											 */
											phylotreesvgviewer.swapChartsByDatasetIDs(dataset1, // ID of the first)
													dataset2 // ID of the second
											);
										} else {
											dataset1 = Integer.parseInt(((RadioButton) flextable.getWidget(newrowidx, 0)).getFormValue());
											dataset2 = Integer.parseInt(((RadioButton) flextable.getWidget(newrowidx + 1, 0)).getFormValue());
										}

										// April 14, 2011; also change their orders on the server side
										swapDatasetOrdersOnServer(MC.cookieman.getSessionID(), currentSVGViewer.getTreeID(),
												dataset1, dataset2, catpanalType.name());

									}// if current row isn't the first; April 5, 2011
								}
							});

							/*
							 * the moveDown button; April 5, 2011
							 */
							btnMoveDown.addClickHandler(new ClickHandler() {

								@Override
								public void onClick(ClickEvent event) {
									/*
									 * click and this row goes up HOW it works:
									 * 1. store all widgets from current row to
									 * an ArrayList 2. delete current row 3.
									 * insert a new row after the previous row
									 * 4. insert all widgets in the ArrayList
									 */
									final int idx_last_row = flextable.getRowCount() - 1;
									final int current_row = flextable.getCellForEvent(event).getRowIndex();

									if (current_row != idx_last_row) {// if not the last row
										// 1
										ArrayList<Widget> row = new ArrayList<Widget>();
										for (int c = 0; c < flextable.getCellCount(current_row); c++) {
											row.add(flextable.getWidget(current_row, c));
										}
										// 2
										flextable.removeRow(current_row);
										// 3 
										int newrowidx = flextable.insertRow(current_row + 1);
										// 4
										for (int nc = 0; nc < row.size(); nc++) {
											flextable.setWidget(newrowidx, nc, row.get(nc));
										}

										/*
										 * get dataset IDs May 16, 2011; fix a
										 * bug that rowidx output boundary
										 */
										Integer dataset1 = null, dataset2 = null;
										if (catpanalType.equals(ECatAndDatasetType.CHARTS)) {
											dataset1 =Integer.parseInt( ((CheckBox) flextable.getWidget(newrowidx - 1, 0)).getFormValue()) ;
											dataset2 =Integer.parseInt(((CheckBox) flextable.getWidget(newrowidx, 0)).getFormValue());

											/*
											 * if plot type is CHART, swap the
											 * two rows
											 */
											phylotreesvgviewer.swapChartsByDatasetIDs( dataset1, // ID of the first
													 dataset2 // ID of the second
											);
										} else {
											dataset1 =Integer.parseInt(((RadioButton) flextable.getWidget(newrowidx - 1, 0)).getFormValue());
											dataset2 =Integer.parseInt(((RadioButton) flextable.getWidget(newrowidx, 0)).getFormValue());
										}

										// April 14, 2011; also change their orders on the server side
										swapDatasetOrdersOnServer(
												MC.cookieman.getSessionID(),
												currentSVGViewer.getTreeID(),
												dataset1, dataset2, catpanalType.name());
									}// if current row isn't the last; April 5, 2011
								}
							});// the move down button

							// continue to add widgets for flex table;
							flextable.setWidget(flexidx, 1, btnEdit);
							flextable.setWidget(flexidx, 2, btnMoveUp);
							flextable.setWidget(flexidx, 3, btnMoveDown);
							flextable.setWidget(flexidx, 4, btnDelete);

						}// if user-input dataset doesn't exist yet; if a dataset exists, the user-interface will not change
					}// if current tree is in current project ID
				} else {
				} // pcdata is valid
			} // pcdataname is not empty
		} else {
			success = false;
			if (promode.equals(proMode.LIVE)) {
				MC.alert.setMessageAndShow("cannot find active project or tree!!", AlertWidget.AlertWidgetType.error);
			}
		}// phylotreesvgviewer != null
//		Window.alert("addDataSet2Tree, success ? " + success);
		return success;
	}// addDataSet2Tree

	//Nov 07, 2011;
	private void swapDatasetOrdersOnServer(String sessionid, int treeID, int dataset1, int dataset2, String catpanalName) {
		// Nov 07, 2011; if only the project is not demoproject
		if (!sessionid.isEmpty()) {
			MC.rpc.swapDatasetOrdersOnServer(sessionid, treeID, dataset1, dataset2, catpanalName, new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(Boolean result) {
				}
			});
		}
	}

	/*
	 * Nov 07, 2011; do this if only the project is not demoproject
	 */
	private void changeCatpanelActiveStatusOnServer(int treeDBSerial, ECatAndDatasetType datasettype, boolean active) {

		String sessionid = MC.cookieman.getSessionID();
		if (!sessionid.isEmpty()) {
			MC.rpc.changeCatpanelActiveStatusOnServer(sessionid, treeDBSerial, datasettype.name(), active, new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
					//
				}

				@Override
				public void onSuccess(Boolean result) {
					//
				}
			});
		}
	}

	// Nov 07, 2011; do this if only the project is not demoproject
	private void changeDatasetActiveStatusOnServer(int treeDBSerial, String datasetName, ECatAndDatasetType datasettype, Boolean active) {
		String sessionid = MC.cookieman.getSessionID();
		if (!sessionid.isEmpty() ) {
			MC.rpc.changeDatasetActiveStatusOnServer(sessionid, treeDBSerial, datasetName, datasettype.name(), active, new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(Boolean result) {
				}
			});
		}
	}

	// Nov 07, 2011; do this if only the project is not demoproject
	private void deleteDatasetFromServer(int treeID, String datasetID, String datatype) {
		String sessionid = MC.cookieman.getSessionID();
		if (!sessionid.isEmpty()) {
			MC.rpc.deleteDataset(sessionid, treeID, datasetID, datatype, new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
				}

				@Override
				public void onSuccess(Boolean result) {
				}
			});
		}
	}

	/*
	 * April 11, 2011; 1. remove everything from 'userdatabrowser'
	 */
	public void clearUerDataBrowswerPanel() {

		// 1. clear the user data broswering panel
		userdatabrowser.clear();

		// 2. clear some variables
		projectpanels.clear();
		treepanels.clear();
		projectcolors.clear();
		svgViewerPanels.clear(); // May 12, 2011; necessary

		// 3. clear currentSVGViewer, if exists
		if (currentSVGViewer != null) {
			currentSVGViewer.removeFromParent();
			currentSVGViewer = null;

			//SVGViewerAndControllerPanel.add(spanel);// is this necessary? April 12, 2011
		}
		toolbar.setSvgviewer(null);
	}

	/*
	 * private methods
	 */

	/*
	 * update/ add colorAndNumericData; April 12, 2011; @input projectName
	 * @input treeName @input datasetName @input datasetype @output none
	 *
	 * nov 07, 2011; if the project ID is 'demos', all changes will be not sent
	 * to server
	 *
	 * May 13, 2012; add new data type: protein domains
	 */
	private boolean saveColorAndNumericDataToServer(final int treeID, final DataInputWidgetBase widget, final ECatAndDatasetType datasettype) {
		boolean success = false;
		String sessionid = MC.cookieman.getSessionID();
		if (!sessionid.isEmpty()) {
			MC.rpc.addDataset(sessionid, treeID,
					widget.getDataSetName(),
					widget.getDataSetContent(), 
					datasettype.name(), 
					new AsyncCallback<Integer>() {

				@Override
				public void onFailure(Throwable caught) {
					MC.alert.setMessageAndShow("failed to connect to server, please try latter", AlertWidget.AlertWidgetType.error);
				}
				
				@Override
				public void onSuccess(Integer datasetId) {
					if(datasetId == null) {
						return;
					}
					addDataSet2Tree(currentSVGViewer.getTreeID(), 
							datasetId,
							widget.getDataSetName(),
							widget.getDataSetContent(),
							proMode.LIVE,
							datasettype, 
							true); 

					widget.hide();
					widget.clearUserInput();
				}
			});
		}
		return success;
	}

	public class PanelBase extends VerticalPanel {

		private String gplusImg = "images/icons/10_new/gplus.png", gminusImg = "images/icons/10_new/gminus.png",
				hollowHeartPNG = "images/icons/10_new/Heart_icon_grey_hollow.svg.png", filledHeartPNG = "images/icons/10_new/Heart_icon_red_filled.svg.png",
				deletePNG = "images/icons/12_new/delete_normal.svg.png", editPNG = "images/icons/12_new/edit_normal.svg.png"; // edit png
		// NOTE: some other images are also listed bellow
		boolean expanded = true, showsubpanelcount = false, headerTextHighlight = true, favorite = false, active = false, // Feb 2, 2012; set it to active; change it back to false --
				mouseover = false;
		String key = null;
		String name = null;
		String subpaneltypeSingle = "tree", subpaneltypePlural = "trees", active_color_string = "#d0e4f6", mouseover_color_string = "#d0e4f6";
		private int db_serialID = 0; // db serialID
		HashMap<String, PanelBase> subpanels = new HashMap<String, PanelBase>();
		VerticalPanel bodyVP = new VerticalPanel();
		HorizontalPanel headerHP = new HorizontalPanel();
		HTML headerText = new HTML();
		Image gplusminusImg = new Image(), favIconImg = new Image();
		private String parentPanelID = "";
		private PanelBase parentWidget = null;
		Image activeIndicator = new Image("images/icons/12_new/tree_active_status.svg.png");
		// May 2, 2011;
		Image deleteImg = new Image(this.deletePNG), editImg = new Image(this.editPNG);

		public PanelBase() {
			super();

			// NOTE: the header panel isn't assembled here
			DOM.setStyleAttribute(bodyVP.getElement(), "paddingLeft", "10px");

			/*
			 * header horizontal panel and elements elements are :
			 *
			 * a html text; a star indicates whether current tree/ project is
			 * active a plus/ minus sign to indicate the current panel is
			 * expanded
			 *
			 */
			headerHP.setSpacing(1);
			headerHP.setHeight("18px");
			headerHP.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);

			// a star
			activeIndicator.setPixelSize(10, 10);
			activeIndicator.setTitle("active project/ tree");

			// image; plus minus signs
			gplusminusImg.setPixelSize(10, 10);
			setExpandSubpanels(this.expanded);

			// image: fav icon
			this.favIconImg.setPixelSize(10, 9);
			setFavorite(this.favorite);
			DOM.setStyleAttribute(favIconImg.getElement(), "cursor", "pointer");

			// May 2, 2011; a delete button 
			this.deleteImg.setPixelSize(12, 12);
			this.editImg.setPixelSize(12, 12);

			// html text 
			headerText.setTitle(this.name + ";  click to expand/ close memeber view");
			DOM.setStyleAttribute(headerText.getElement(), "cursor", "POINTER");

			headerText.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					setExpandSubpanels(!expanded);
				}
			});

			this.favIconImg.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					setFavorite(!favorite);
				}
			});

			headerHP.addDomHandler(new MouseOverHandler() {

				@Override
				public void onMouseOver(MouseOverEvent event) {
					mouseover = true;
					DOM.setStyleAttribute(headerHP.getElement(), "backgroundColor", mouseover_color_string);
				}
			}, MouseOverEvent.getType());

			headerHP.addDomHandler(new MouseOutHandler() {

				@Override
				public void onMouseOut(MouseOutEvent event) {
					mouseover = false;
					DOM.setStyleAttribute(headerHP.getElement(), "backgroundColor", !active ? "white" : active_color_string);
				}
			}, MouseOutEvent.getType());

			this.add(this.headerHP);
			this.add(this.bodyVP);

		}// constructor

		public boolean getActive() {
			return this.active;
		}

		public boolean setActive(boolean b, boolean bExpand) {
			setActive(b);
			this.setExpandSubpanels(bExpand);
			return active;
		}

		public boolean setActive(boolean b) {
			active = b;
			DOM.setStyleAttribute(headerHP.getElement(), "backgroundColor", !active ? "white" : mouseover ? mouseover_color_string : active_color_string);
			return active;
		}

		/*
		 * public methods
		 */

		public boolean addSubPanel(PanelBase subpanel) {

			System.out.println("addSubPanel typed " + subpanel.getClass().getName() + " : " + subpanel.key);
			boolean success = false;
			String key_of_subpanel = subpanel.getKey();
			if (!this.subpanels.containsKey(key_of_subpanel)) {
				success = true;
				this.subpanels.put(key_of_subpanel, subpanel);
				this.bodyVP.add(subpanel);

				// change count
				int count = this.subpanels.size();
				String subpaneltype = count > 1 ? this.subpaneltypePlural : this.subpaneltypeSingle;
				String html = this.showsubpanelcount ? "<b>" + this.name + " (" + count + " " + subpaneltype + ")</b>" : "<b>" + this.name + "</b>";

				this.headerText.setHTML(html);

				subpanel.setParentID(this.key );
				subpanel.setParentWidget(this);

			}
			return success;
		}

		/*
		 * May 2, 2011;
		 */
		public boolean removeSubPanel(PanelBase subpanel) {
			boolean success = false;
			String key_of_subpanel = subpanel.getKey();
			if (this.subpanels.containsKey(key_of_subpanel)) {
				this.subpanels.remove(key_of_subpanel);
				success = true;

				// necessary
				subpanel.removeFromParent();

				// change count
				int count = this.subpanels.size();
				String subpaneltype = count > 1 ? this.subpaneltypePlural : this.subpaneltypeSingle;
				String html = this.showsubpanelcount ? "<b>" + this.name + " (" + count + " " + subpaneltype + ")</b>" : "<b>" + this.name + "</b>";
				this.headerText.setHTML(html);
			}
			return success;
		}

		public int getSubPanelCounts() {
			return this.subpanels.size();
		}

		public VerticalPanel getBodyPanel() {
			return this.bodyVP;
		}

		public void setHeaderWidth(int headerwidth) {
			this.headerHP.setWidth(headerwidth + "px");
		}

		public void setKeyAndName(String key, String name) {
			this.key = key;
			this.name = name;
			int count = this.subpanels.size();
			String subpaneltype = count > 1 ? this.subpaneltypePlural : this.subpaneltypeSingle;
			String html = this.showsubpanelcount ? "<b>" + this.name + " (" + count + " " + subpaneltype + ")</b>" : "<b>" + this.name + "</b>";

			if (this.headerTextHighlight) {
				html = "<b>" + html + "</b>";
			}

			this.headerText.setHTML(html);
		}

		public void setShowSubpanelCount(boolean show) {
			this.showsubpanelcount = show;
		}

		public void setColorToSubPanels(String newcolor) {
			for (PanelBase p : this.subpanels.values()) {
				DOM.setStyleAttribute(p.headerHP.getElement(), "borderBottom", "thin solid " + newcolor);
			}
		}

		public void setHeaderTextHighlight(boolean hl) {
			this.headerTextHighlight = hl;
		}

		public final boolean setExpandSubpanels(boolean exp) {
			expanded = exp;
			bodyVP.setVisible(expanded);

			if (expanded) {
				gplusminusImg.setUrl(gminusImg);
				gplusminusImg.setTitle("subpanel is expanded / visible");
			} else {
				gplusminusImg.setUrl(gplusImg);
				gplusminusImg.setTitle("subpanel is closed / invisible");
			}
			return this.expanded;
		}// April 3, 2011

		/*
		 * get methods
		 */
		public boolean getShowSubpanelCount() {
			return this.showsubpanelcount;
		}

		public String getKey() {
			return this.key;
		}

		public boolean getHeaderTextHight() {
			return this.headerTextHighlight;
		}

		private void setParentID(String pid) {
			this.parentPanelID = pid;
		}

		public String getParentID() {
			return this.parentPanelID;
		}

		private void setParentWidget(PanelBase aThis) {
			this.parentWidget = aThis;
		}

		public PanelBase getParentWidget() {
			return this.parentWidget;
		}

		public PanelBase getSubPanelByID(String subpanelID) {
			return this.subpanels.get(subpanelID);
		}

		/*
		 * April 4, 2011; favicon
		 */
		public final boolean setFavorite(boolean fav) {
			this.favorite = fav;

			if (this.favorite) {
				this.favIconImg.setUrl(this.filledHeartPNG);
				this.favIconImg.setTitle("favorite; click to cancel");
			} else {
				this.favIconImg.setUrl(this.hollowHeartPNG);
				this.favIconImg.setTitle("click to mark as favorite");
			}
			return this.favorite;
		}

		public boolean getFavorite() {
			return this.favorite;
		}

		public void setSubpanelTypeStringSingle(String str) {
			this.subpaneltypeSingle = str;
		}

		public void setSubpanelTypeStringPlural(String str) {
			this.subpaneltypePlural = str;
		}

		/*
		 * April 12, 2011;
		 */
		public void setDBserialID(int serial) {
			this.db_serialID = serial;
		}

		public int getDBserialID() {
			return this.db_serialID == 0 ? 0 : this.db_serialID;
		}

		/*
		 * May 16, 2011
		 */
		public HTML getHeaderText() {
			return this.headerText;
		}
	}// a base class for panels

	/*
	 * April 3, 2011; each project will have a random color to start with
	 */
	public class ProjectPanel extends PanelBase {

		public ProjectPanel(final String projectAcronyme, final String projectName, String color) {
			super();

			if (color.isEmpty()) {
				String colors_string = "AliceBlue,AntiqueWhite,Aqua,Aquamarine,Azure,Beige,Bisque,Black,BlanchedAlmond,Blue,BlueViolet,Brown,BurlyWood,CadetBlue,Chartreuse,Chocolate,Coral,CornflowerBlue,Cornsilk,Crimson,Cyan,DarkBlue,DarkCyan,DarkGoldenRod,DarkGray,DarkGrey,DarkGreen,DarkKhaki,DarkMagenta,DarkOliveGreen,Darkorange,DarkOrchid,DarkRed,DarkSalmon,DarkSeaGreen,DarkSlateBlue,DarkSlateGray,DarkSlateGrey,DarkTurquoise,DarkViolet,DeepPink,DeepSkyBlue,DimGray,DimGrey,DodgerBlue,FireBrick,FloralWhite,ForestGreen,Fuchsia,Gainsboro,GhostWhite,Gold,GoldenRod,Gray,Grey,Green,GreenYellow,HoneyDew,HotPink,IndianRed,Indigo,Ivory,Khaki,Lavender,LavenderBlush,LawnGreen,LemonChiffon,LightBlue,LightCoral,LightCyan,LightGoldenRodYellow,LightGray,LightGrey,LightGreen,LightPink,LightSalmon,LightSeaGreen,LightSkyBlue,LightSlateGray,LightSlateGrey,LightSteelBlue,LightYellow,Lime,LimeGreen,Linen,Magenta,Maroon,MediumAquaMarine,MediumBlue,MediumOrchid,MediumPurple,MediumSeaGreen,MediumSlateBlue,MediumSpringGreen,MediumTurquoise,MediumVioletRed,MidnightBlue,MintCream,MistyRose,Moccasin,NavajoWhite,Navy,OldLace,Olive,OliveDrab,Orange,OrangeRed,Orchid,PaleGoldenRod,PaleGreen,PaleTurquoise,PaleVioletRed,PapayaWhip,PeachPuff,Peru,Pink,Plum,PowderBlue,Purple,Red,RosyBrown,RoyalBlue,SaddleBrown,Salmon,SandyBrown,SeaGreen,SeaShell,Sienna,Silver,SkyBlue,SlateBlue,SlateGray,SlateGrey,Snow,SpringGreen,SteelBlue,Tan,Teal,Thistle,Tomato,Turquoise,Violet,Wheat,White,WhiteSmoke,Yellow,YellowGreen";
				ArrayList<String> colors = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(colors_string, ","));
				color = (String) colors.get(Random.nextInt(colors.size()));
			}

			projectcolors.put(projectAcronyme, color);

			/*
			 * setup and assemble headerHP
			 */
			headerHP.setWidth(MC.userdatapanel_width + "px");
			DOM.setStyleAttribute(headerHP.getElement(), "boxShadow", "2px 2px 4px " + color);
			DOM.setStyleAttribute(headerHP.getElement(), "marginTop", "10px");

			/*
			 * a rectangle to show view / alow user to choose colors this
			 * element is specific to this project panel
			 */
			final HTML colorselector = new HTML();

			colorselector.setPixelSize(12, 12);
			DOM.setStyleAttribute(colorselector.getElement(), "backgroundColor", color);
			DOM.setStyleAttribute(colorselector.getElement(), "borderRadius", "2px");

			colorselector.setTitle("Click to pick a different color, currently is : " + color);

			colorselector.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {

					final DialogBox colorpickerDBox = new DialogBox();
					colorpickerDBox.setHTML("<b>ColorPicker</b>");

					final ColorPicker picker = new ColorPicker();

					HorizontalPanel buttonsHP = new HorizontalPanel();
					buttonsHP.setSpacing(5);
					Button cancelButton = new Button("Cancel");
					Button pickButton = new Button("Pick!");
					buttonsHP.add(pickButton);
					buttonsHP.add(cancelButton);

					cancelButton.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {

							colorpickerDBox.hide();
							colorpickerDBox.clear();

						}
					});

					pickButton.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {
							String hexcolor = "#" + picker.getHexColor();
							DOM.setStyleAttribute(colorselector.getElement(), "backgroundColor", hexcolor);
							// change boxshadow color
							DOM.setStyleAttribute(headerHP.getElement(), "boxShadow", "2px 2px 2px " + hexcolor);
							// also change colors of the underline of header HP of sub-treepanels
							setColorToSubPanels(hexcolor);
							
							/**
							 * also update project color; April 4, 2013; 
							 */
							projectcolors.put(projectAcronyme, hexcolor);
							
							/*
							 * Also sent data to server
							 */
							String sessionid = MC.cookieman.getSessionID();
							if (!sessionid.isEmpty()) {
								MC.rpc.changeProjectColor(sessionid, projectAcronyme, hexcolor, new AsyncCallback<Boolean>() {

									@Override
									public void onFailure(Throwable caught) {
										//
									}

									@Override
									public void onSuccess(Boolean result) {
										//
									}
								});
							}
							// change tooltip message
							colorselector.setTitle("Click to pick a different color, currently is : " + hexcolor);
							colorpickerDBox.hide();
							colorpickerDBox.clear();
						}
					});

					VerticalPanel mainVP = new VerticalPanel();
					mainVP.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
					mainVP.setSpacing(5);
					mainVP.add(picker);
					mainVP.add(buttonsHP);

					colorpickerDBox.setWidget(mainVP);
					colorpickerDBox.center();
					colorpickerDBox.setModal(false);

				}
			});

			// a header text
			this.setKeyAndName(projectName, projectName);
			this.setShowSubpanelCount(true);
			headerText.setTitle(projectName + ";  click to expand/ close memeber view");

			// only here I start to assemble the header horizontal panel
			headerHP.add(colorselector);
			headerHP.setCellVerticalAlignment(colorselector, HasVerticalAlignment.ALIGN_MIDDLE);

			headerHP.add(headerText);
			headerHP.setCellVerticalAlignment(headerText, HasVerticalAlignment.ALIGN_MIDDLE);

			headerHP.add(gplusminusImg);
			headerHP.setCellVerticalAlignment(gplusminusImg, HasVerticalAlignment.ALIGN_MIDDLE);
			headerHP.setCellHorizontalAlignment(gplusminusImg, HasHorizontalAlignment.ALIGN_RIGHT);
			headerHP.setCellWidth(this.gplusminusImg, this.gplusminusImg.getWidth() + 5 + "px"); // April 3, 2011;

		}// constructor
	}// public class ProjectPanel extends VerticalPanel

	public class TreePanel extends PanelBase {

		private boolean busy = false, // by default, the busy indicator isn't shown
				treeStringAdded = false;
		// May 16, 2011;
		private Image busyImg = new Image("images/busy-indicator-facebook.gif"); // 

		public TreePanel(String projectID, String treeName) {
			super();

			DOM.setStyleAttribute(bodyVP.getElement(), "paddingLeft", "10px");

			this.key = treeName;
			this.name = treeName;

			/*
			 * header HP: HTML text | a star indicate whether current tree is
			 * active | a plus minus sign indicate whether subpanel is extended
			 */
			// header HP
			headerHP.setWidth(Integer.toString(MC.userdatapanel_width - 10) + "px");
			headerHP.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
			DOM.setStyleAttribute(headerHP.getElement(), "borderBottom", "thin solid " + projectcolors.get(projectID));
			DOM.setStyleAttribute(headerHP.getElement(), "marginTop", "5px");

			// May 16, 2011; a busy indicator
			this.busyImg.setPixelSize(16, 11);
			busyImg.setTitle(" don't you see i'm busy here ?? ");
			headerHP.add(this.busyImg);
			headerHP.setCellVerticalAlignment(this.busyImg, HasVerticalAlignment.ALIGN_MIDDLE);
			headerHP.setCellHorizontalAlignment(this.busyImg, HasHorizontalAlignment.ALIGN_LEFT);
			this.busyImg.setVisible(this.busy);
			headerHP.setCellWidth(busyImg, this.busyImg.getWidth() + 3 + "px"); // May 17, 2011;

			// a header text
			this.setKeyAndName(treeName, treeName);
			DOM.setStyleAttribute(headerText.getElement(), "fontSize", "90%");
			headerText.setTitle(treeName + ";  click to active current tree");

			// assemble header horizontal panel
			headerHP.add(headerText);
			headerHP.setCellVerticalAlignment(headerText, HasVerticalAlignment.ALIGN_MIDDLE);

			// edit button/img; May 30, 2012
			headerHP.add(this.editImg);
			headerHP.setCellVerticalAlignment(editImg, HasVerticalAlignment.ALIGN_MIDDLE);
			headerHP.setCellHorizontalAlignment(editImg, HasHorizontalAlignment.ALIGN_RIGHT);
			headerHP.setCellWidth(editImg, this.editImg.getWidth() + 3 + "px");

			// delete button; May 2, 2011
			headerHP.add(this.deleteImg);
			headerHP.setCellVerticalAlignment(deleteImg, HasVerticalAlignment.ALIGN_MIDDLE);
			headerHP.setCellHorizontalAlignment(deleteImg, HasHorizontalAlignment.ALIGN_RIGHT);
			headerHP.setCellWidth(deleteImg, this.deleteImg.getWidth() + 3 + "px");

			// plus / minus indicator
			headerHP.add(gplusminusImg);
			headerHP.setCellVerticalAlignment(gplusminusImg, HasVerticalAlignment.ALIGN_MIDDLE);
			headerHP.setCellHorizontalAlignment(gplusminusImg, HasHorizontalAlignment.ALIGN_RIGHT);
			headerHP.setCellWidth(this.gplusminusImg, this.gplusminusImg.getWidth() + 5 + "px"); // April 3, 2011;

		}

		public Image getTreeDelImg() {
			return this.deleteImg;
		}

		public Image getTreeEditImg() {
			return this.editImg;
		}

		/*
		 * May 16, 2011
		 */
		public boolean setTreeStringAdded(boolean b) {
			this.treeStringAdded = b;
			return b;
		}

		public boolean getTreeStringAdded() {
			return this.treeStringAdded;
		}

		/*
		 * May 16, 2011; add a busy indicator: fackbook style
		 */
		public boolean setBusy(boolean b) {
			busy = b;
			this.busyImg.setVisible(this.busy); // change visibality of the busy indicator

			/*
			 * NOTE: the following codes will cause problem in IE9
			 */
			if (!busy) {
				headerHP.setCellWidth(busyImg, 3 + "px"); // May 17, 2011;
			} else {
				headerHP.setCellWidth(busyImg, this.busyImg.getWidth() + 3 + "px"); // May 17, 2011;
			}

			return busy;
		}

		public boolean getBusy() {
			return this.busy;
		}
	} // treepanel

	public class CatPanel extends PanelBase {

		private Table datatable = new Table();
		private HMwgt disableAllCKBox = new HMwgt("images/icons/checkboxs.png", 0, 0, 12, 12, 0, 13, 12, 12);

		public CatPanel(ECatAndDatasetType catType) {
			super();
			/*
			 * flextable; April 4, 2011;
			 */
			ColumnFormatter cf = datatable.getColumnFormatter();

			int spacing = 1;
			int width = 12;
			cf.setWidth(1, width + spacing + "px"); // the edit button
			cf.setWidth(2, width + spacing + "px"); // the move up button
			cf.setWidth(3, width + spacing + "px"); // the move down button
			cf.setWidth(4, width + spacing + "px"); // the delete button

			/*
			 * layout of the header panel label | checkbox to allow user
			 * disable/ enable plots in the subpanel | a plus/minus sign to
			 * indicate whether subpanels are expanded
			 */
			disableAllCKBox.setTitle("disable/ enable datasets in this panel");

			// 
			datatable.setWidth(Integer.toString(MC.userdatapanel_width - 30) + "px");
			datatable.setCellSpacing(0);
			datatable.setBorderWidth(0);

			// id
			this.setHeaderTextHighlight(false);
			this.setKeyAndName(catType.name(), catType.getLabel());

			// 
			headerHP.setWidth(Integer.toString(MC.userdatapanel_width - 20) + "px");
			DOM.setStyleAttribute(headerHP.getElement(), "borderBottom", "thin solid lightblue");

			// header Text
			DOM.setStyleAttribute(headerText.getElement(), "fontSize", "90%");
			headerText.setTitle(catType.getLabel() + ";  click to expand subpanel");

			// assemble 
			headerHP.add(headerText);
			headerHP.setCellVerticalAlignment(headerText, HasVerticalAlignment.ALIGN_MIDDLE);

			headerHP.add(disableAllCKBox);
			DOM.setStyleAttribute(disableAllCKBox.getElement(), "position", "absolute;");
			headerHP.setCellVerticalAlignment(disableAllCKBox, HasVerticalAlignment.ALIGN_BOTTOM);
			headerHP.setCellHorizontalAlignment(disableAllCKBox, HasHorizontalAlignment.ALIGN_RIGHT);
			headerHP.setCellWidth(this.disableAllCKBox, this.disableAllCKBox.getWidth() + 5 + "px"); // April 3, 2011;

			headerHP.add(gplusminusImg);
			headerHP.setCellVerticalAlignment(gplusminusImg, HasVerticalAlignment.ALIGN_MIDDLE);
			headerHP.setCellHorizontalAlignment(gplusminusImg, HasHorizontalAlignment.ALIGN_RIGHT);
			headerHP.setCellWidth(this.gplusminusImg, this.gplusminusImg.getWidth() + 5 + "px"); // April 3, 2011;

			bodyVP.add(datatable);

		}// constructor

		// public functions
		public HMwgt getDisableAllCheckBox() {
			return this.disableAllCKBox;
		}

		public FlexTable getFlexTable() {
			return this.datatable;
		}// April 1, 2011; return this table to user to allow user to modify it.
	}//cat panel

	// <<<<<<<<<<<<<<<<< user-data-browsing UI <<<<<<<<<<<<<<<
	/*
	 * several home made widgets
	 */
	public final class HMwgt extends Image {

		private boolean value = true;
		private boolean enabled = true;
		private int x1, y1, w1, h1, x2, y2, w2, h2;

		public HMwgt(String imgURL, int nx1, int ny1, int nw1, int nh1, // image coordinates for false
				int nx2, int ny2, int nw2, int nh2) {

			super();
			x1 = nx1;
			x2 = nx2;

			y1 = ny1;
			y2 = ny2;

			w1 = nw1;
			w2 = nw2;

			h1 = nh1;
			h2 = nh2;

			this.setUrl(imgURL);
			this.setValue(true);

			this.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					if (enabled) {
						setValue(!value);
					} else {
						Window.alert("disabled");
					}

				}
			});// click handler on this

		}

		public void setValue(boolean newvalue) {
			value = newvalue;
			if (value) {
				setVisibleRect(x2, y2, w2, h2);
			} else {
				setVisibleRect(x1, y1, w1, h1);
			}
		}

		public boolean getValue() {
			return this.value;
		}

		public void setEnabled(boolean enable) {
			if (this.enabled != enable) {
				this.enabled = enable;
			}
		}

		public boolean getEnabled() {
			return this.enabled;
		}
	}// home made widgets

	
}
