package org.evolview.client.pages;


import org.evolview.client.resources.MyImages;
import org.evolview.client.resources.MyResources;
import org.evolview.client.utils.AwesomeButton;
import org.evolview.client.utils.MC;
import org.evolview.shared.SignInStatus;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.AttachEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * created April 2013;
 * April 1, 2013: before logging out, check if user is temporary user
 * April 5, 2013; set visiable of certain links and buttons according to signed-in status
 * June 14, 2013 : add new social media images 
 */
public class HeaderBar extends Composite {

	private static HeaderBarUiBinder uiBinder = GWT.create(HeaderBarUiBinder.class);

	interface HeaderBarUiBinder extends UiBinder<Widget, HeaderBar> {
	}

	/**
	 * May 14, 2013; auth provider images 
	 */
	/**
	 * May 14, 2013; auth provider images 
	 */
	public static MyImages images = MyResources.INSTANCE.images(); 
	private Image imgAuthProvider = new Image(); // May 14, 2013

	private final DialogBox con = new DialogBox();
	private final DialogBox dbConnectionDialog = new DialogBox();
	private final Hyperlink signup = new Hyperlink("Sign up", "signup");

	@UiField
	HorizontalPanel hpHeaderbar; // April 22, 2012
	@UiField
	HTML htmlUserInfor;
	@UiField
	HorizontalPanel hpSignupLink;
	@UiField
	Anchor anchorProviderHelp;
	@UiField
	AwesomeButton btnDBConnection;
	 


	@UiHandler("btnDBConnection")
	void onDBConnectionClicked(ClickEvent evnt) {
		// April 1, 2013: check if temporary user
		MC.rpc.checkSignInStatus(MC.cookieman.getSessionID(), new AsyncCallback<SignInStatus>() {
			@Override
			public void onFailure(Throwable caught) {
				MC.alertFailConnectingServer();
			}
			
			@Override
			public void onSuccess(SignInStatus sin) {
				if (sin.isSignedIn() && !sin.isTemporaryUser()) { 
					doDBConnection();
				}
			}
		});
	}
 
	
	@UiHandler("btnLogout")
	void onLogoutClicked(ClickEvent evnt) {
		// April 1, 2013: check if temporary user
		MC.rpc.checkSignInStatus(MC.cookieman.getSessionID(), new AsyncCallback<SignInStatus>() {
			@Override
			public void onFailure(Throwable caught) {
				MC.alertFailConnectingServer();
			}
			
			@Override
			public void onSuccess(SignInStatus sin) {
				if (sin.isSignedIn() && sin.isTemporaryUser()) {
					con.center();
				} else {
					doLogout();
				}
			}
		});
	}

	protected void doLogout() {
		MC.cookieman.removeCookie();
		History.newItem("", true); // fire a none-history event --;
	}


	protected void doDBConnection() { 
	}

	
	
	public HeaderBar() {
		initWidget(uiBinder.createAndBindUi(this));
		this.hpHeaderbar.setWidth(MC.headerbarWidth + "px"); // set headerBar width --
		//this.hpHeaderbar.setWidth( "100%");

		/*
		 * >>>>> assemble  the dialogbox --
		 */
		con.setModal(true);
		con.setGlassEnabled(true);
		SafeHtmlBuilder shb = new SafeHtmlBuilder();
		shb.appendEscaped("You're a temporary user; logging out means losing all your projects/trees/datasets.").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
		shb.appendEscaped("To keep them, please sign up and transfer your data to your new account.").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
		shb.appendEscaped("To create a new account, click 'Sign up' on the header bar.").appendHtmlConstant("</br>").appendHtmlConstant("</br>");
		shb.appendEscaped("Continue logging out?").appendHtmlConstant("</br>");

		AwesomeButton no = new AwesomeButton("Cancel", AwesomeButton.AwesomeButtonColor.green, "small");
		AwesomeButton yes = new AwesomeButton("Sign out", AwesomeButton.AwesomeButtonColor.red, "small");

		HorizontalPanel hp = new HorizontalPanel();
		hp.setSpacing(2);
		hp.add(no);
		hp.add(yes);

		VerticalPanel vp = new VerticalPanel();
		vp.setSpacing(5);
		vp.add(new HTML(shb.toSafeHtml()));
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);

		con.setWidget(vp);
		con.setText("!!Note!!");

		no.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				con.hide();
			}
		});

		yes.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				con.hide();
				doLogout(); // May 14, 2013 --
			}
		});
		// <<<<< dialog box assembled <<<<<<

		

		/*
		 * >>>>> assemble  the dialogbox --
		 */
		dbConnectionDialog.setModal(true);
		dbConnectionDialog.setGlassEnabled(true);

		no = new AwesomeButton("Cancel", AwesomeButton.AwesomeButtonColor.green, "small");
		yes = new AwesomeButton("Sign out", AwesomeButton.AwesomeButtonColor.red, "small");

		hp = new HorizontalPanel();
		hp.setSpacing(2);
		hp.add(no);
		hp.add(yes);

		vp = new VerticalPanel();
		vp.setSpacing(5);
		vp.add(new HTML(shb.toSafeHtml()));
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);

		dbConnectionDialog.setWidget(vp); 
		no.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dbConnectionDialog.hide();
			}
		}); 
		yes.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// save properties ..
				
				dbConnectionDialog.hide(); 
			}
		});
		// <<<<< dialog box assembled <<<<<<

		
		
		/**
		 * May 14, 2013
		 * add imgAuthProvider to anchor; doesn't work in UiBinder --
		 */
		anchorProviderHelp.getElement().appendChild(imgAuthProvider.getElement());

		/*
		 * >>>>> attach hander >>>>>>
		 * April 5, 2013; set visiable for "account setting" button according to signin status
		 */
		this.addAttachHandler(new AttachEvent.Handler() {

			@Override
			public void onAttachOrDetach(AttachEvent event) {
				if (isAttached()) {
					MC.rpc.checkSignInStatus(MC.cookieman.getSessionID(), new AsyncCallback<SignInStatus>() {

						@Override
						public void onFailure(Throwable caught) { 
							btnDBConnection.setVisible(false);
							imgAuthProvider.setUrl(""); // is this doable ? May 14, 2013 --
							imgAuthProvider.setSize("0px", "0px");
							imgAuthProvider.setTitle("");
						}

						@Override
						public void onSuccess(SignInStatus sin) {
							if (sin.isSignedIn()) { 
								btnDBConnection.setVisible(true);
								
								hpSignupLink.clear(); // -- always clear this??? May 18, 2013 --
								
								String usertype = "temporary";
								if (sin.isTemporaryUser()) {
									hpSignupLink.add(signup);
									htmlUserInfor.setHTML("Welcome " + "<font color=\"#0000A0\">" + sin.getUserID() + "</font>");

									// auth provider
									imgAuthProvider.setUrl(images.tempUser().getSafeUri());
								} else {
									htmlUserInfor.setHTML(sin.getUsername().isEmpty() ? "Welcome " : "Welcome " + "<font color=\"#0000A0\">" + sin.getUsername() + "</font>");

									signup.removeFromParent(); 
									imgAuthProvider.setUrl(images.ev().getSafeUri()); 
								}
								imgAuthProvider.setSize("20px", "20px");
								imgAuthProvider.setTitle(usertype + " user; click the icon for more details.");
							} else { 
								btnDBConnection.setVisible(false);
								
								imgAuthProvider.setUrl(""); // is this doable ? May 14, 2013 --
								imgAuthProvider.setSize("0px", "0px");
							}
						}
					});
				}
			}
		});
	}// constructor
 

}
