package org.evolview.client.pages;

import org.evolview.client.phyloUtils.PhyloTreeSVGViewer;
import org.evolview.client.phyloUtils.treePlotMode;
import org.evolview.client.utils.AlertWidget;
import org.evolview.client.utils.ImgBtn;
import org.evolview.client.utils.InfoPanelWidget;
import org.evolview.client.utils.MC;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TabLayoutPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * March 31, 2013, 
 * to reduce the complicity of class 'MyProjectAndTrees', its contents were split into
 * several small classes 
 * 
 * Apr 29, 2013, introduce button ibtnWhereIsMyTree
 * 
 * @author E.T.
 */

public class Toolbar extends Composite {

	
	public interface ToolbarResources extends ClientBundle {
		@Source("../utils/resources/barplots.png")
		ImageResource barplots();

		@Source("../utils/resources/branchcolors.png")
		ImageResource branchcolors();

		@Source("../utils/resources/colorshapesandstrips.png")
		ImageResource colorshapesandstrips();

		@Source("../utils/resources/leaflabelbackgroud.png")
		ImageResource leafbk();

		@Source("../utils/resources/leaflabelcolors.png")
		ImageResource leafcolors();
		
		@Source("../utils/resources/piecharts.png")
		ImageResource piecharts();
		
		@Source("../utils/resources/proteindomains.png")
		ImageResource proteindomains();
		
		@Source("../utils/resources/heatMap_explained.png")
		ImageResource heatmap();
	}
	
	private static ToolbarUiBinder uiBinder = GWT.create(ToolbarUiBinder.class);
	private static final ToolbarResources resources = GWT.create(ToolbarResources.class);

	interface ToolbarUiBinder extends UiBinder<Widget, Toolbar> {
	}
	
	/**
	 * global variables
	 */
	private PhyloTreeSVGViewer currentSVGViewer = null;
	
	// format float number
	private final NumberFormat formatterDeci3 = NumberFormat.getFormat("#.###");

	/**
	 * >>>>>> uifields and uihandlers >>>>>
	 */
	/*
     * ============== allow customized widgets to take parameters from UiBinder
     * XML file ============
     */
	private String urlBase = "";
    @UiFactory
    ImgBtn makeImgBtn() { // method name is insignificant
        return new ImgBtn(urlBase, true);
    }
    
    
    /**
     * added May 12, 2013 
     * UIfield of child widgets for tablayout panel 
     */
    @UiField
    HorizontalPanel tabBasic, tabAdvanced, tabDataUpload, tabExport; 
    
    @UiField
    Label tabLabelBasic, tabLabelAdvanced, tabLabelExport, tabLabelAnnotationUpload;
    
    @UiHandler("tabLabelExport")
    void onMouserOvertabLabelExport( MouseOverEvent moevent ){
    	toolbarLayoutPanel.selectTab(tabExport);
    }
    
    @UiHandler("tabLabelAdvanced")
    void onMouserOvertabLabelAdvanced( MouseOverEvent moevent ){
    	toolbarLayoutPanel.selectTab(tabAdvanced);
    }
    
    @UiHandler("tabLabelBasic")
    void onMouserOvertabLabelBasic( MouseOverEvent moevent ){
    	toolbarLayoutPanel.selectTab(tabBasic);
    }
    
    @UiHandler("tabLabelAnnotationUpload")
    void onMouserOvertabLabelAnnotationUpload( MouseOverEvent moevent ){
    	toolbarLayoutPanel.selectTab(tabDataUpload);
    }
    
    // <<<< ---- May 12, 2013 -----
    
	@UiField
	ImgBtn ibtnSave2PDF2, ibtnRectClado, ibtnRectPhylo, ibtnSlantedClado, ibtnBootstrapShowHide, ibtnBranchlengthShowHide, ibtnCicularClockwise,
			ibtnCicularCounterClockwise, ibtnCicularRotateClockwise, ibtnCicularRotateCounterClockwise, ibtnCicularSpanDecrease, ibtnCicularSpanIncrease,
			ibtnCircularPhylo, ibtnDataUploadBarPlots, ibtnDataUploadBranchColor, ibtnDataUploadColorStrips, ibtnDataUploadLabelColor,
			ibtnDataUploadLeafLabelBKColor, ibtnDataUploadPiechart, ibtnExport2JPEG, ibtnExport2PNG, ibtnSave2SVG2, ibtnExport2SVG, ibtnExport2TIFF,
			ibtnFontSizeDecrease, ibtnFontSizeIncrease, ibtnHorizScaleDecrease, ibtnHorizScaleIncrease, ibtnLeafLabelAlign, ibtnLeafLabelShowHide,
			ibtnVerticScaleDecrease, ibtnVerticScaleIncrease, ibtnZoomin, ibtnZoomout, ibtnZoomrestore, ibtnCircularClado, ibtnFontItalic,
			ibtnLinewidthIncrease, ibtnLinewidthDecrease, ibtnExport2Newick, ibtnExport2Nexus, ibtnExport2Nhx, ibtnExport2PhyloXML, ibtnMouseWheelZoominout,
			ibtnDataUploadHeatmap,ibtnDataUploadProteinDomains, // May 11, 2012
			ibtnFitCanvas2plotWidth, ibtnFitCanvas2plotHeight, ibtnFitCanvas2plot,
			ibtnWhereIsMyTree, // Apr 29, 2013 --
			
			/**
			 * Sep 10, 2013; add the following buttons 
			 */
			ibtnFontItalicBootStrap, ibtnFontItalicBranchLength
			; 

	// ibtnWhereIsMyTree;ibtnWhereIsMyTree
	@UiField
	ImgBtn ibtnFileOpen;
	@UiField
	TextBox tbxCicularSpan, tbxCicularStartAngel, tbxFontSize, tbxHorizScale, tbxVerticScale, tbxLienwidth, tbxFontSizeBootStrap, tbxFontSizeBranchLength;
	@UiField
	TabLayoutPanel toolbarLayoutPanel;
	
	/**
     * Oct 29, 2013: mouse over event for icons on annotation upload / data upload tab
     */
	private final InfoPanelWidget tooltipWidget = new InfoPanelWidget(true);
	private Widget mouse_over_widget = null;
	private final int delayShowMiliSec = 500, delayHideMiliSec = 200;
	private int tooltipx = 0, tooltipy = 0;
	
    @UiHandler("ibtnDataUploadPiechart")
    void onMouserOveribtnDataUploadPiechart( MouseOverEvent event ){
    	if (currentSVGViewer != null && ibtnDataUploadPiechart.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadPiechart, event.getClientX(), event.getClientY(),
    				"upload data for pie chart", "https://code.google.com/p/evolgenius/wiki/EvolViewDatasetPieCharts", new Image(resources.piecharts()));
    	}
    }
    
    @UiHandler("ibtnDataUploadPiechart")
    void onMouserOutibtnDataUploadPiechart( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadPiechart.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }
    
    
   
    // branch color
    @UiHandler("ibtnDataUploadBranchColor")
    void onMouserOveriibtnDataUploadBranchColor( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadBranchColor.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadBranchColor, moevent.getClientX(), moevent.getClientY(),
    				"upload data for coloring branches", "https://code.google.com/p/evolgenius/wiki/EvolViewDatasetBranchColor", new Image(resources.branchcolors()));
    	}
    }
    
    @UiHandler("ibtnDataUploadBranchColor")
    void onMouserOutibtnDataUploadBranchColor( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadBranchColor.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }

    // label color 
    @UiHandler("ibtnDataUploadLabelColor")
    void onMouserOveriibtnDataUploadLabelColor( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadLabelColor.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadLabelColor, moevent.getClientX(), moevent.getClientY(),
    				"upload data for coloring leaf labels", "https://code.google.com/p/evolgenius/wiki/EvolViewDatasetLeafBKColor", new Image(resources.leafcolors()));
    	}
    }
    
    @UiHandler("ibtnDataUploadLabelColor")
    void onMouserOutibtnDataUploadLabelColor( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadLabelColor.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }
    
    // barplots 
    @UiHandler("ibtnDataUploadBarPlots")
    void onMouserOveriibtnDataUploadBarPlots( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadBarPlots.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadBarPlots, moevent.getClientX(), moevent.getClientY(),
    				"upload data for bar plot", "https://code.google.com/p/evolgenius/wiki/EvolViewDatasetBars", new Image(resources.barplots()));
    	}
    }
    
    @UiHandler("ibtnDataUploadBarPlots")
    void onMouserOutibtnDataUploadBarPlots( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadBarPlots.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }
    
    // leaf bk 
    @UiHandler("ibtnDataUploadLeafLabelBKColor")
    void onMouserOveriibtnDataUploadLeafLabelBKColor( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadLeafLabelBKColor.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadLeafLabelBKColor, moevent.getClientX(), moevent.getClientY(),
    				"upload data for leaf background colors", "https://code.google.com/p/evolgenius/wiki/EvolViewDatasetLeafBKColor", new Image(resources.leafbk()));
    	}
    }
    
    @UiHandler("ibtnDataUploadLeafLabelBKColor")
    void onMouserOutibtnDataUploadLeafLabelBKColor( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadLeafLabelBKColor.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }
    
    // color strips 
    @UiHandler("ibtnDataUploadColorStrips")
    void onMouserOveribtnDataUploadColorStrips( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadColorStrips.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadColorStrips, moevent.getClientX(), moevent.getClientY(),
    				"upload data for color shapes and strips", "https://code.google.com/p/evolgenius/wiki/EvolViewDatasetColorStrip", new Image(resources.colorshapesandstrips()));
    	}
    }
    @UiHandler("ibtnDataUploadColorStrips")
    void onMouserOutibtnDataUploadColorStrips( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadColorStrips.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }

    // protein domains -
    @UiHandler("ibtnDataUploadProteinDomains")
    void onMouserOveribtnDataUploadProteinDomains( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadProteinDomains.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadProteinDomains, moevent.getClientX(), moevent.getClientY(),
    				"upload data for protein domains", "https://code.google.com/p/evolgenius/wiki/EvolViewProteinDomain", new Image(resources.proteindomains()));
    	}
    }
    @UiHandler("ibtnDataUploadProteinDomains")
    void onMouserOutibtnDataUploadProteinDomains( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadProteinDomains.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }

    // protein domains -
    @UiHandler("ibtnDataUploadHeatmap")
    void onMouserOveribtnDataUploadHeatmap( MouseOverEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadHeatmap.isEnabled() == true) {
    		handleMouseOverEvent4dataUploadIcons( ibtnDataUploadHeatmap, moevent.getClientX(), moevent.getClientY(),
    				"upload data for Heatmap", "http://evdocs.medgenius.info/#DatasetHeatmap", new Image(resources.heatmap()));
    	}
    }
    @UiHandler("ibtnDataUploadHeatmap")
    void onMouserOutibtnDataUploadHeatmap( MouseOutEvent moevent ){
    	if (currentSVGViewer != null && ibtnDataUploadHeatmap.isEnabled() == true) {
    		handleMouseOutEvent4dataUploadIcons();
    	}
    }
    
    
    
    
    private void handleMouseOverEvent4dataUploadIcons( final ImgBtn ibtn, final int mousex, final int mousey, final String msg, final String url, final Image img2show ){
    	mouse_over_widget = ibtn;
    	tooltipx = mousex;
    	tooltipy = mousey;
		// >>>> TIMER >>>>
		new Timer() {

			@Override
			public void run() {
				if (mouse_over_widget != null && mouse_over_widget.equals( ibtn )) { // if mouse is still on the same element as delay_pshow mili sec before
					VerticalPanel vp = new VerticalPanel();
					vp.add( new Anchor(msg, url, "_blank"));
					vp.add(img2show);
					tooltipWidget.setWidgetAndShow(vp, tooltipx, tooltipy); // because there will be delays, so use global variables for mouse positions
				}
			}
		}.schedule(delayShowMiliSec);
    }
    
    private void handleMouseOutEvent4dataUploadIcons(){
    	mouse_over_widget = null;
		// >>>> TIMER >>>>
		new Timer() {

			@Override
			public void run() {
				if (mouse_over_widget == null && !tooltipWidget.getMouseOver()) { // if mouse is not on any evelement and not on popup panel, close the panel
					tooltipWidget.hide();
				}
			}
		}.schedule(delayHideMiliSec);
    }
    // <<<<<< oct 29, 2013 <<<<<<<< 
    
    // 
	
	@UiHandler("ibtnSave2SVG2")
    void onSave2SVG2Clicked(ClickEvent event) {
		if (currentSVGViewer != null && ibtnSave2SVG2.isEnabled() == true) {
            exportTreePlotToFile(currentSVGViewer.toXMLString(), "svg");
        }
    }
	
    /**
     *  basic tab: change plot modes
     */
    @UiHandler("ibtnRectClado")
    void onRectCladoClicked(ClickEvent event) {
        changeTreePlotMode(treePlotMode.RECT_CLADOGRAM, ibtnRectClado);
    }

    @UiHandler("ibtnRectPhylo")
    void onRectPhyloClicked(ClickEvent event) {
        changeTreePlotMode(treePlotMode.RECT_PHYLOGRAM, ibtnRectPhylo);
    }

    @UiHandler("ibtnSlantedClado")
    void onSlantedCladoClicked(ClickEvent event) {
        changeTreePlotMode(treePlotMode.SLANTED_CLADOGRAM_NORMAL, ibtnSlantedClado);
    }

    @UiHandler("ibtnCircularClado")
    void onCirculeCladoClicked(ClickEvent event) {
        changeTreePlotMode(treePlotMode.CIRCULAR_CLADOGRAM, ibtnCircularClado);
    }

    @UiHandler("ibtnCircularPhylo")
    void onCirculePhyloClicked(ClickEvent event) {
        changeTreePlotMode(treePlotMode.CIRCULAR_PHYLOGRAM, ibtnCircularPhylo);
    }

    /**
     *  basic tab: show/hide extra information on tree
     */
    @UiHandler("ibtnBootstrapShowHide")
    void ibtnBootstrapShowHideClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnBootstrapShowHide.isEnabled() == true) {
        	boolean bShow = ibtnBootstrapShowHide.getSelect();
            currentSVGViewer.setShowBootstrapScores( bShow );
            
            // Sep 10, 2013 
            ibtnFontItalicBootStrap.setEnabled(bShow);
            tbxFontSizeBootStrap.setEnabled(bShow);
            
            this.ibtnFontItalicBootStrap.setSelect( currentSVGViewer.getBootStrapFontItalic() & bShow );
        }
    }

    @UiHandler("ibtnBranchlengthShowHide")
    void ibtnBranchlengthShowHideClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnBranchlengthShowHide.isEnabled() == true) {
        	boolean bShow = ibtnBranchlengthShowHide.getSelect();
            currentSVGViewer.setShowTreeBranchLengths( bShow );
            
         // Sep 10, 2013 
            ibtnFontItalicBranchLength.setEnabled(bShow);
            tbxFontSizeBranchLength.setEnabled(bShow);
            
            this.ibtnFontItalicBranchLength.setSelect( currentSVGViewer.getBranchLengthFontItalic() & bShow );
        }
    }

    @UiHandler("ibtnLeafLabelShowHide")
    void ibtnLeafLabelShowHideClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnLeafLabelShowHide.isEnabled() == true) {
            currentSVGViewer.setShowTreeLeafLabels(ibtnLeafLabelShowHide.getSelect());
        }
    }

    @UiHandler("ibtnLeafLabelAlign")
    void ibtnLeafLabelAlignClicked(ClickEvent event) {
    	System.out.println( "--> button: ibtnLeafLabelAlign clicked" );
    	System.out.println( "   : svgview is null? " + currentSVGViewer == null );
    	System.out.println( "   : is the button? " + ibtnLeafLabelAlign.isEnabled() );
    	System.out.println( "   : is the button selected? " + ibtnLeafLabelAlign.getSelect() );
        if (currentSVGViewer != null & ibtnLeafLabelAlign.isEnabled() == true) {
            currentSVGViewer.getTreeSkeleton().setAlignLeafLabels(ibtnLeafLabelAlign.getSelect());
        }
    }

    /**
     *  basic tab: zoom in / out
     */
    @UiHandler("ibtnMouseWheelZoominout")
    void ibtnMouseWheelZoominoutClicked(ClickEvent e) {
        if (currentSVGViewer != null & ibtnMouseWheelZoominout.isEnabled() == true) {
            ibtnMouseWheelZoominout.setSelect(!currentSVGViewer.getMouseWheelEnabled());
            currentSVGViewer.setMouseWheelEnabled(ibtnMouseWheelZoominout.getSelect());
        }
    }

    @UiHandler("ibtnZoomin")
    void ibtnZoominClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnZoomin.isEnabled() == true) {
            currentSVGViewer.zoom("in");
        }
    }

    @UiHandler("ibtnZoomout")
    void ibtnZoomoutClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnZoomout.isEnabled() == true) {
            currentSVGViewer.zoom("out");
        }
    }

    @UiHandler("ibtnZoomrestore")
    void ibtnZoomrestoreClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnZoomrestore.isEnabled() == true) {
            currentSVGViewer.zoom("Restore");
        }
    }

    /**
     *  basic tab: horizontal scale
     */
    @UiHandler("ibtnHorizScaleIncrease")
    void ibtnHorizScaleIncreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnHorizScaleIncrease.isEnabled() == true) {
            float pxperwidth = currentSVGViewer.getPxPerWidth();
            pxperwidth *= (1 + 0.05);
            float modifiedPxPerWidth = currentSVGViewer.setPxPerWidth(pxperwidth);
            tbxHorizScale.setText(formatterDeci3.format(modifiedPxPerWidth));
        }
    }

    @UiHandler("ibtnHorizScaleDecrease")
    void ibtnHorizScaleDecreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnHorizScaleDecrease.isEnabled() == true) {
            float pxperwidth = currentSVGViewer.getPxPerWidth();
            pxperwidth *= (1 - 0.05);
            float modifiedPxPerWidth = currentSVGViewer.setPxPerWidth(pxperwidth);
            tbxHorizScale.setText(formatterDeci3.format(modifiedPxPerWidth));
        }
    }

    @UiHandler("tbxHorizScale")
    void tbxHorizScaleKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxHorizScale.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    float newPxPerWidth = Float.parseFloat(tbxHorizScale.getText());
                    if (newPxPerWidth > 0) {
                        currentSVGViewer.setPxPerWidth(newPxPerWidth);
                    } else {
                        tbxHorizScale.setText(Float.toString(currentSVGViewer.getPxPerWidth()));
                    }

                } catch (Exception e) {
                    // do nothing
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxHorizScale")
    void tbxHorizScaleBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxHorizScale.isEnabled() == true) {
            try {
                float newPxPerWidth = Float.parseFloat(tbxHorizScale.getText());
                if (newPxPerWidth > 0) {
                    currentSVGViewer.setPxPerWidth(newPxPerWidth);
                } else {
                    tbxHorizScale.setText(Float.toString(currentSVGViewer.getPxPerWidth()));
                }
            } catch (Exception e) {
                // do nothing
            }
        }
    }

    /**
     *  basic tab: vertical scale
     */
    @UiHandler("ibtnVerticScaleIncrease")
    void ibtnVerticScaleIncreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnVerticScaleIncrease.isEnabled() == true) {
            float pxperheight = currentSVGViewer.getPxPerHeight();
            pxperheight *= (1 + 0.05);
            currentSVGViewer.setPxPerHeight(pxperheight);
            tbxVerticScale.setText(formatterDeci3.format(pxperheight));

        }
    }

    @UiHandler("ibtnVerticScaleDecrease")
    void ibtnVerticScaleDecreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnVerticScaleDecrease.isEnabled() == true) {
            float pxperheight = currentSVGViewer.getPxPerHeight();
            pxperheight *= (1 - 0.05);
            currentSVGViewer.setPxPerHeight(pxperheight);
            tbxVerticScale.setText(formatterDeci3.format(pxperheight));
        }
    }

    @UiHandler("tbxVerticScale")
    void tbxVerticScaleKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxVerticScale.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    float newPxPerHeight = Float.parseFloat(tbxVerticScale.getText());
                    if (newPxPerHeight > 0) {
                        currentSVGViewer.setPxPerHeight(newPxPerHeight);
                    } else {
                        tbxVerticScale.setText(Float.toString(currentSVGViewer.getPxPerHeight()));
                    }

                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxVerticScale")
    void tbxVerticScaleBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxVerticScale.isEnabled() == true) {
            try {
                float newPxPerHeight = Float.parseFloat(tbxVerticScale.getText());
                if (newPxPerHeight > 0) {
                    currentSVGViewer.setPxPerHeight(newPxPerHeight);
                } else {
                    tbxVerticScale.setText(Float.toString(currentSVGViewer.getPxPerHeight()));
                }
            } catch (Exception e) {
            }
        }
    }

    /**
     *  basic tab: font size and font italic
     */
    // Sep 10, 2013 
    @UiHandler("ibtnFontItalicBootStrap")
    void ibtnFontItalicBootStrapClicked( ClickEvent e ){
    	if (currentSVGViewer != null & ibtnFontItalicBootStrap.isEnabled() == true) {
            currentSVGViewer.setBootStrapFontItalic(!currentSVGViewer.getBootStrapFontItalic());
        }
    }
    
    @UiHandler("tbxFontSizeBootStrap")
    void tbxFontSizeBootStrapKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxFontSizeBootStrap.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    int newFontSize = Integer.parseInt(tbxFontSizeBootStrap.getText());
                    if (newFontSize > 0) {
                        currentSVGViewer.setBootStrapFontSize(newFontSize);
                    } else {
                    	tbxFontSizeBootStrap.setText(Integer.toString(currentSVGViewer.getBootStrapFontSize()));
                    }
                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxFontSizeBootStrap")
    void tbxFontSizeBootStrapBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxFontSizeBootStrap.isEnabled() == true) {
        	try {
                int newFontSize = Integer.parseInt(tbxFontSizeBootStrap.getText());
                if (newFontSize > 0) {
                    currentSVGViewer.setBootStrapFontSize(newFontSize);
                } else {
                	tbxFontSizeBootStrap.setText(Integer.toString(currentSVGViewer.getBootStrapFontSize()));
                }
            } catch (Exception e) {
            }
        }
    }
    
    @UiHandler("ibtnFontItalicBranchLength")
    void ibtnFontItalicBranchLengthClicked( ClickEvent e ){
    	if (currentSVGViewer != null & ibtnFontItalicBranchLength.isEnabled() == true) {
            currentSVGViewer.setBranchLengthFontItalic(!currentSVGViewer.getBranchLengthFontItalic());
        }
    } // sep 10, 2013 
    

    @UiHandler("tbxFontSizeBranchLength")
    void tbxFontSizeBranchLengthKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxFontSizeBranchLength.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    int newFontSize = Integer.parseInt(tbxFontSizeBranchLength.getText());
                    if (newFontSize > 0) {
                        currentSVGViewer.setBranthLengthFontSize(newFontSize);
                    } else {
                    	tbxFontSizeBootStrap.setText(Integer.toString(currentSVGViewer.getBranchLengthFontSize()));
                    }
                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxFontSizeBranchLength")
    void tbxFontSizeBranchLengthBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxFontSizeBranchLength.isEnabled() == true) {
        	try {
        		int newFontSize = Integer.parseInt(tbxFontSizeBranchLength.getText());
                if (newFontSize > 0) {
                    currentSVGViewer.setBranthLengthFontSize(newFontSize);
                } else {
                	tbxFontSizeBootStrap.setText(Integer.toString(currentSVGViewer.getBranchLengthFontSize()));
                }
            } catch (Exception e) {
            }
        }
    }
    
    // <<<<<< sep 10, 2013 <<<<<<<<
    @UiHandler("ibtnFontItalic")
    void ibtnFontItalicClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnFontItalic.isEnabled() == true) {
            currentSVGViewer.setLeafFontItalic(!currentSVGViewer.getLeafFontItalic());
        }
    }

    @UiHandler("ibtnFontSizeIncrease")
    void ibtnFontSizeIncreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnFontSizeIncrease.isEnabled() == true) {
            currentSVGViewer.getTreeSkeleton().setLeafLabelFontSize(currentSVGViewer.getFontSize() + 1);
            tbxFontSize.setText(Integer.toString(currentSVGViewer.getFontSize()));
        }
    }

    @UiHandler("ibtnFontSizeDecrease")
    void ibtnFontSizeDecreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnFontSizeDecrease.isEnabled() == true) {
            currentSVGViewer.getTreeSkeleton().setLeafLabelFontSize(currentSVGViewer.getFontSize() - 1);
            tbxFontSize.setText(Integer.toString(currentSVGViewer.getFontSize()));
        }
    }

    @UiHandler("tbxFontSize")
    void tbxFontSizeKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxFontSize.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    int newFontSize = Integer.parseInt(tbxFontSize.getText());
                    if (newFontSize > 0) {
                        currentSVGViewer.getTreeSkeleton().setLeafLabelFontSize(newFontSize);
                    } else {
                        tbxFontSize.setText(Integer.toString(currentSVGViewer.getFontSize()));
                    }
                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxFontSize")
    void tbxFontSizeBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxFontSize.isEnabled() == true) {
            try {
                int newFontSize = Integer.parseInt(tbxFontSize.getText());
                if (newFontSize > 0) {
                    currentSVGViewer.getTreeSkeleton().setLeafLabelFontSize(newFontSize);
                } else {
                    tbxFontSize.setText(Integer.toString(currentSVGViewer.getFontSize()));
                }
            } catch (Exception e) {
            }
        }
    }

    // Nov 07, 2011; handlers for treeBranchLineWidth
    @UiHandler("ibtnLinewidthIncrease")
    void ibtnLinewidthIncreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnFontSizeIncrease.isEnabled() == true) {
            float current_tree_branch_linewidth = currentSVGViewer.increaseTreeBranchLineWidth();
            tbxLienwidth.setText(formatterDeci3.format(current_tree_branch_linewidth));
        }
    }

    @UiHandler("ibtnLinewidthDecrease")
    void ibtnLinewidthDecreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null & ibtnFontSizeIncrease.isEnabled() == true) {
            float current_tree_branch_linewidth = currentSVGViewer.decreaseTreeBranchLineWidth();
            tbxLienwidth.setText(formatterDeci3.format(current_tree_branch_linewidth));
        }
    }

    @UiHandler("tbxLienwidth")
    void tbxLienwidthKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxLienwidth.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    float newLinewidth = Float.parseFloat(tbxLienwidth.getText());
                    newLinewidth = currentSVGViewer.setTreeBranchLineWidth(newLinewidth);
                    tbxLienwidth.setText(formatterDeci3.format(newLinewidth));
                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxLienwidth")
    void tbxLienwidthBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxLienwidth.isEnabled() == true) {
            try {
                float newLinewidth = Float.parseFloat(tbxLienwidth.getText());
                newLinewidth = currentSVGViewer.setTreeBranchLineWidth(newLinewidth);
                tbxLienwidth.setText(formatterDeci3.format(newLinewidth));
            } catch (Exception e) {
            }
        }
    }

    /*
     * advanced tab
     */
    // advanced tab: circular clock/ counterclock wise
    @UiHandler("ibtnCicularClockwise")
    void ibtnCicularClockwiseClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnCicularClockwise.isEnabled() == true) {
            currentSVGViewer.setCircularModeClockwise(true);
            ibtnCicularClockwise.setSelect(true);
            ibtnCicularCounterClockwise.setSelect(false);
        }
    }

    @UiHandler("ibtnCicularCounterClockwise")
    void ibtnCicularCounterClockwiseClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnCicularCounterClockwise.isEnabled() == true) {
            currentSVGViewer.setCircularModeClockwise(false);
            ibtnCicularClockwise.setSelect(false);
            ibtnCicularCounterClockwise.setSelect(true);
        }
    }

    // advanced tab: circular rotate
    @UiHandler("ibtnCicularRotateClockwise")
    void ibtnCicularRotateClockwiseClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnCicularRotateClockwise.isEnabled() == true) {
            currentSVGViewer.rotateCicularTree(-20);
            tbxCicularStartAngel.setText(Integer.toString(Math.round(currentSVGViewer.getCicularStartAngle())));
        }
    }

    @UiHandler("ibtnCicularRotateCounterClockwise")
    void ibtnCicularRotateCounterClockwiseClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnCicularRotateCounterClockwise.isEnabled() == true) {
            currentSVGViewer.rotateCicularTree(20);
            tbxCicularStartAngel.setText(Integer.toString(Math.round(currentSVGViewer.getCicularStartAngle())));
        }
    }

    @UiHandler("tbxCicularStartAngel")
    void tbxCicularStartAngelKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxCicularStartAngel.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    int newStartAngle = Integer.parseInt(tbxCicularStartAngel.getText());
                    currentSVGViewer.setCicularStartAngle(newStartAngle);
                    tbxCicularStartAngel.setText(Integer.toString(Math.round(currentSVGViewer.getCicularStartAngle())));
                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxCicularStartAngel")
    void tbxCicularStartAngelBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxCicularStartAngel.isEnabled() == true) {
            try {
                int newStartAngle = Integer.parseInt(tbxCicularStartAngel.getText());
                currentSVGViewer.setCicularStartAngle(newStartAngle);
                tbxCicularStartAngel.setText(Integer.toString(Math.round(currentSVGViewer.getCicularStartAngle())));
            } catch (Exception e) {
            }
        }
    }

    // advanced tab: cicular span
    @UiHandler("ibtnCicularSpanIncrease")
    void ibtnCicularSpanIncreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnCicularSpanIncrease.isEnabled() == true) {
            currentSVGViewer.setCicularAngleSpan(Math.round(currentSVGViewer.getCicularAngleSpan()) + 20);
            tbxCicularSpan.setText(Integer.toString(Math.round(currentSVGViewer.getCicularAngleSpan())));
            tbxCicularStartAngel.setText(Integer.toString(Math.round(currentSVGViewer.getCicularStartAngle()))); // necessary 
        }
    }

    @UiHandler("ibtnCicularSpanDecrease")
    void ibtnCicularSpanDecreaseClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnCicularSpanDecrease.isEnabled() == true) {
            currentSVGViewer.setCicularAngleSpan(Math.round(currentSVGViewer.getCicularAngleSpan()) - 20);
            tbxCicularSpan.setText(Integer.toString(Math.round(currentSVGViewer.getCicularAngleSpan())));
            tbxCicularStartAngel.setText(Integer.toString(Math.round(currentSVGViewer.getCicularStartAngle()))); // necessary
        }
    }

    @UiHandler("tbxCicularSpan")
    void tbxCicularSpanKeyUp(KeyUpEvent event) {
        if (currentSVGViewer != null && tbxCicularSpan.isEnabled() == true) {
            int charcode = event.getNativeKeyCode();
            if (charcode == KeyCodes.KEY_ENTER) {
                try {
                    int newAngleSpan = Integer.parseInt(tbxCicularSpan.getText());
                    currentSVGViewer.setCicularAngleSpan(newAngleSpan);
                    tbxCicularSpan.setText(Integer.toString(Math.round(currentSVGViewer.getCicularAngleSpan())));
                } catch (Exception e) {
                }
            } else if (isCharCodeValidForPositiveFloat(charcode)) {
                event.preventDefault();
            }
        }
    }

    @UiHandler("tbxCicularSpan")
    void tbxCicularSpanBlurred(BlurEvent event) {
        if (currentSVGViewer != null && tbxCicularSpan.isEnabled() == true) {
            try {
                int newAngleSpan = Integer.parseInt(tbxCicularSpan.getText());
                currentSVGViewer.setCicularAngleSpan(newAngleSpan);
                tbxCicularSpan.setText(Integer.toString(Math.round(currentSVGViewer.getCicularAngleSpan())));
            } catch (Exception e) {
            }
        }
    }
    // more to advanced tab; Dec 27, 2011
//    @UiField ImgBtn ibtnFit2Width, ibtnFit2Height, ibtnFit2Canvas;
//    
//    @UiHandler("ibtnFit2Width")
//    void ibtnFit2WidthClicked(ClickEvent event) {
//        if (currentSVGViewer != null && ibtnFit2Width.isEnabled() == true) {
//            currentSVGViewer.fitPlot2CanvasWidth();
//        }
//    }
//    
//    @UiHandler("ibtnFit2Height")
//    void ibtnFit2HeightClicked(ClickEvent event) {
//        if (currentSVGViewer != null && ibtnFit2Height.isEnabled() == true) {
//            currentSVGViewer.fitPlot2CanvasHeight();
//        }
//    }
//    
//    @UiHandler("ibtnFit2Canvas")
//    void ibtnFit2CanvasClicked(ClickEvent event) {
//        if (currentSVGViewer != null && ibtnFit2Canvas.isEnabled() == true) {
//            currentSVGViewer.fitPlot2Canvas();
//        }
//    }
    
    @UiHandler("ibtnFitCanvas2plotWidth")
    void ibtnFitCanvas2plotWidthClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnFitCanvas2plotWidth.isEnabled() == true) {
            currentSVGViewer.fitCanvas2PlotWidth();
        }
    }

    @UiHandler("ibtnFitCanvas2plotHeight")
    void ibtnFitCanvas2plotHeightClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnFitCanvas2plotHeight.isEnabled() == true) {
            currentSVGViewer.fitCanvas2PlotHeight();
        }
    }

    @UiHandler("ibtnFitCanvas2plot")
    void ibtnFitCanvas2plotClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnFitCanvas2plot.isEnabled() == true) {
            currentSVGViewer.fitCanvas2Plot();
        }
    }

    // where is my tree: Apr 24, 2012
    @UiHandler("ibtnWhereIsMyTree")
    void ibtnWhereIsMyTreeClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnWhereIsMyTree.isEnabled() == true) {
            currentSVGViewer.whereIsMyTree();
        }
    }
    
    /*
     * ******************************************
     * tab: annotation upload
     * ******************************************
     */
    
    
    /*
     * export tab
     */
    // export tab: basic
    @UiHandler("ibtnSave2PDF2")
    void ibtnSave2PDF2Clicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnSave2PDF2.isEnabled() == true) {
            exportTreePlotToFile(currentSVGViewer.toXMLString(), "pdf");
        }
    }

//    @UiField ImgBtn ibtnExport2PS;
//    @UiHandler("ibtnExport2PS")
//    void ibtnExport2PSClicked(ClickEvent event) {
//        if (currentSVGViewer != null && ibtnExport2PS.isEnabled() == true) {
//            exportTreePlotToFile(currentSVGViewer.toXMLString(), "ps");
//        }
//    }
//
//    @UiField ImgBtn ibtnExport2EPS;
//    @UiHandler("ibtnExport2EPS")
//    void ibtnExport2EPSClicked(ClickEvent event) {
//        if (currentSVGViewer != null && ibtnExport2EPS.isEnabled() == true) {
//            exportTreePlotToFile(currentSVGViewer.toXMLString(), "eps");
//        }
//    }
    @UiHandler("ibtnExport2SVG")
    void ibtnExport2SVGClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnExport2SVG.isEnabled() == true) {
            exportTreePlotToFile(currentSVGViewer.toXMLString(), "svg");
        }
    }

    // export tab: images
    @UiHandler("ibtnExport2PNG")
    void ibtnExport2PNGClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnExport2PNG.isEnabled() == true) {
            exportTreePlotToFile(currentSVGViewer.toXMLString(), "png");
        }
    }

    @UiHandler("ibtnExport2TIFF")
    void ibtnExport2TIFFClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnExport2TIFF.isEnabled() == true) {
            exportTreePlotToFile(currentSVGViewer.toXMLString(), "tiff");
        }
    }

    @UiHandler("ibtnExport2JPEG")
    void ibtnExport2JPEGClicked(ClickEvent event) {
        if (currentSVGViewer != null && ibtnExport2JPEG.isEnabled() == true) {
            exportTreePlotToFile(currentSVGViewer.toXMLString(), "jpeg");
        }
    }

    /*
     * Dec 5, 2011
     */
    @UiHandler("ibtnExport2Newick")
    void ibtnExport2NewickClicked(ClickEvent e) {
        if (currentSVGViewer != null && ibtnExport2Newick.isEnabled() == true) {
            String treestr = currentSVGViewer.getPhyloTree().toTreeString("newick", true, true, false);
            exportTreeToTextFile(treestr, "newick");
        }
    }

    @UiHandler("ibtnExport2Nexus")
    void ibtnExport2NexusClicked(ClickEvent e) {
        if (currentSVGViewer != null && ibtnExport2Nexus.isEnabled() == true) {
            String treestr = currentSVGViewer.getPhyloTree().toTreeString("nexus", true, true, false);
            exportTreeToTextFile(treestr, "nexus");
        }
    }

    @UiHandler("ibtnExport2Nhx")
    void ibtnExport2NhxClicked(ClickEvent e) {
        if (currentSVGViewer != null && ibtnExport2Nhx.isEnabled() == true) {
            String treestr = currentSVGViewer.getPhyloTree().toTreeString("nhx", true, true, false);
            exportTreeToTextFile(treestr, "nhx");
        }
    }

    @UiHandler("ibtnExport2PhyloXML")
    void ibtnExport2PhyloxmlClicked(ClickEvent e) {
        if (currentSVGViewer != null && ibtnExport2PhyloXML.isEnabled() == true) {
            String treestr = currentSVGViewer.getPhyloTree().toXMLString();
            exportTreeToTextFile(treestr, "xml");
        }
    }
    
	// <<<<<<< uifields and uihandlers <<<<<<<
	
    public Toolbar() {
		initWidget(uiBinder.createAndBindUi(this));
		
		// set width --
		//this.controllerPanel.setWidth(bBrowserPanelShown ? controllerPanelNarrowerWidth + "px" : controllerPanelWiderWidth + "px");
		
		// set a null svgviewer
		//this.setSvgviewer(null);
		
		/**
		 * Oct 29, 2013 
		 */
		
	}



	public PhyloTreeSVGViewer getSvgviewer() {
		return currentSVGViewer;
	}
	
	/*
	 * March 31, 2013 --
	 */
	public void setSvgviewer(PhyloTreeSVGViewer svgviewer) {
		this.currentSVGViewer = svgviewer;
		setToolbarButtonStatus( svgviewer ); 
	}
	
	public void setWidthAccoringToShowHideControllerPanel( boolean b ){
		this.toolbarLayoutPanel.setWidth( b ? MC.controllerPanelNarrowerWidth + "px" : MC.controllerPanelWiderWidth + "px");
	}
	
	/**
	 * private methods --
	 */
	
	/*
     * Export tree plot to external files in varity of formats, such as SVG | PS
     * | EPS | PDF | TIF | PNG | JPEG May 12, 2011;
     */
    private void exportTreePlotToFile(String svgcontents, final String outfileformat) {

        MC.alert.setMessageAndShow("preparing your file ... ", AlertWidget.AlertWidgetType.busy );

        // 1. export to SVG first
        MC.rpc.exportTreeToSVG(svgcontents, new AsyncCallback<String>() {

            @Override
            public void onFailure(Throwable caught) {
                MC.alert.setMessageAndShow("Oops... an error has occurred, please try again later!!", AlertWidget.AlertWidgetType.error);
                //Window.alert("fail to connect to server, please try again latter");
            }

            @Override
            public void onSuccess(String svgfileprefix) {
                if (outfileformat.equalsIgnoreCase("svg")) {
                    String link = GWT.getHostPageBaseURL() + "userfiles/" + svgfileprefix + ".svg";
                    SafeHtmlBuilder shb = new SafeHtmlBuilder();
                    shb.appendEscaped("Your file is ready.").appendHtmlConstant("</br>");
                    shb.appendHtmlConstant("<a href=\"" + link + "\" target=\"_blank\">click here to download</a>");
                    MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.success);
                    //JSFuncs.popupBlockerCheckAndPopup(link, "download svg", "");
                    //Window.open(link, "downloadWindow", "");
                } else {

                    // 2. if the export format isn't SVG
                    MC.rpc.exportTreeImage(svgfileprefix, outfileformat, currentSVGViewer.getOffsetWidth(), currentSVGViewer.getOffsetHeight(), new AsyncCallback<String>() {

                        @Override
                        public void onFailure(Throwable caught) {
                            MC.alert.setMessageAndShow("Oops... an error has occurred, please try again later!!", AlertWidget.AlertWidgetType.error);
                        }

                        @Override
                        public void onSuccess(String file) {
                            String link = GWT.getHostPageBaseURL() + file;
                            SafeHtmlBuilder shb = new SafeHtmlBuilder();
                            shb.appendEscaped("Your file is ready.").appendHtmlConstant("</br>");
                            shb.appendHtmlConstant("<a href=\"" + link + "\" target=\"_blank\">click here to download</a>");
                            MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.success);

                            //JSFuncs.popupBlockerCheckAndPopup(link, "download " + outfileformat, "");
                            //Window.open(link, "downloadWindow", "");
                        }
                    });
                }// svg file or not svg file
            } // if svg exportting successful
        }); // export to svg
    }

    private void exportTreeToTextFile(String treestr, final String format) {

        MC.alert.setMessageAndShow("preparing your file ... ", AlertWidget.AlertWidgetType.busy);

        MC.rpc.exporTreeToTextFile(treestr, format, new AsyncCallback<String>() {

            @Override
            public void onFailure(Throwable caught) {
                MC.alert.setMessageAndShow("Oops... an error has occurred, please try again later!!", AlertWidget.AlertWidgetType.error);
            }

            @Override
            public void onSuccess(String file) {
                String link = GWT.getHostPageBaseURL() + file;
                SafeHtmlBuilder shb = new SafeHtmlBuilder();
                shb.appendEscaped("Your file is ready.").appendHtmlConstant("</br>");
                shb.appendHtmlConstant("<a href=\"" + link + "\" target=\"_blank\">click here to download</a>");
                MC.alert.setMessageAndShow(shb.toSafeHtml(), AlertWidget.AlertWidgetType.success);

                //JSFuncs.popupBlockerCheckAndPopup(link, "download " + format, "");
            }
        });
    } // exportTreeToTextFile using server side codes

    private void changeTreePlotMode(treePlotMode newplotmode, ImgBtn ibtnClicked) {
        if (currentSVGViewer != null & ibtnClicked.isEnabled() == true) {
            // unselect current ibutton
            treePlotMode currenttreeplotmode = currentSVGViewer.getTreePlotMode();
            switch (currenttreeplotmode) {
                case RECT_CLADOGRAM:
                    this.ibtnRectClado.setSelect(false);
                    break;
                case RECT_PHYLOGRAM:
                    this.ibtnRectPhylo.setSelect(false);
                    break;
                case SLANTED_CLADOGRAM_NORMAL:
                    this.ibtnSlantedClado.setSelect(false);
                    break;
                case CIRCULAR_CLADOGRAM:
                    this.ibtnCircularClado.setSelect(false);
                    break;
                case CIRCULAR_PHYLOGRAM:
                    this.ibtnCircularPhylo.setSelect(false);
                    break;
			default:
				this.ibtnRectClado.setSelect(false);
				break;
            }

            // change
            currentSVGViewer.setTreePlotMode(newplotmode);
            ibtnClicked.setSelect(true); // May 16, 2011; fix a bug here

            // April 23, 2012
            tbxHorizScale.setText(formatterDeci3.format(currentSVGViewer.getPxPerWidth()));

            // 
            setToolbarStatusOnAdvancedTab(currentSVGViewer, newplotmode);
        }
    }

    private boolean isCharCodeValidForPositiveFloat(int charcode) {
        boolean valid = true;
        if (charcode > 57 && charcode < 48
                && charcode != '.'
                && charcode != KeyCodes.KEY_BACKSPACE
                && charcode != KeyCodes.KEY_DELETE
                && charcode != KeyCodes.KEY_DOWN
                && charcode != KeyCodes.KEY_HOME
                && charcode != KeyCodes.KEY_END
                && charcode != KeyCodes.KEY_LEFT
                && charcode != KeyCodes.KEY_RIGHT
                && charcode != KeyCodes.KEY_DELETE
                && charcode != KeyCodes.KEY_CTRL
                && charcode != KeyCodes.KEY_SHIFT
                && charcode != KeyCodes.KEY_ENTER
                && charcode != KeyCodes.KEY_UP) {
            valid = false;
        }
        return valid;
    }// April 7, 2011;
	
	/*
	 * April 14, disable all except the fileopen button Apr 24, 2012
	 * ibtnWhereIsMyTree
	 */
	private void setToolbarButtonStatus(PhyloTreeSVGViewer svgviewer) {
		boolean isvalid = (svgviewer == null) ? false : true;
		/*
		 * first of all, set buttons that are controlled by the status of
		 * current svg viewers
		 */
		this.ibtnFileOpen.setEnabled(true);
		
		/**
		 * Sep 10, 2013
		 */
		boolean hasBootStrap = isvalid ? ( svgviewer.hasBootStrap() ? true : false ) : false;
		boolean hasBranchLength = isvalid ? ( svgviewer.hasBranchLength()  ? true : false ) : false;
		boolean showBootStrap = isvalid ? ( svgviewer.getShowBootStrapScores()  ? true : false ) : false ;
		boolean showBranthLength = isvalid ? ( svgviewer.getShowBranchLength()  ? true : false ) : false;
		boolean isSelectedBootStrap = isvalid ? ( svgviewer.getBootStrapFontItalic()  ? true : false ) : false;
		boolean isSelectedBranchLength = isvalid ? ( svgviewer.getBranchLengthFontItalic()  ? true : false ) : false;
		this.ibtnFontItalicBootStrap.setEnabled(isvalid & hasBootStrap & showBootStrap );
		this.ibtnFontItalicBranchLength.setEnabled(isvalid & hasBranchLength & showBranthLength );
		// new since sep 10, 2013
		this.tbxFontSizeBootStrap.setEnabled( isvalid & hasBootStrap & showBootStrap  );
		this.tbxFontSizeBranchLength.setEnabled(isvalid & hasBranchLength & showBranthLength);
		// select 
		this.ibtnFontItalicBootStrap.setSelect( isvalid & hasBootStrap & showBootStrap & isSelectedBootStrap );
		this.ibtnFontItalicBranchLength.setSelect( isvalid & hasBootStrap & showBootStrap & isSelectedBranchLength );
		
		// file and export 
		//        this.ibtnSave2PDF.setEnabled(isvalid);

		// view clado modes
		this.ibtnCircularClado.setEnabled(isvalid);
		this.ibtnRectClado.setEnabled(isvalid);
		this.ibtnSlantedClado.setEnabled(isvalid);

		// view option panel
		// NOTE: remining widgets from this subpanel are set in the following part
		this.ibtnLeafLabelShowHide.setEnabled(isvalid);

		// zoom in out 
		this.ibtnMouseWheelZoominout.setEnabled(isvalid); // Dec 6, 2011
		if (isvalid) {
			this.ibtnMouseWheelZoominout.setSelect(svgviewer.getMouseWheelEnabled());
		}
		this.ibtnZoomin.setEnabled(isvalid);
		this.ibtnZoomout.setEnabled(isvalid);
		this.ibtnZoomrestore.setEnabled(isvalid);

		// horiz 
		this.ibtnHorizScaleDecrease.setEnabled(isvalid);
		this.ibtnHorizScaleIncrease.setEnabled(isvalid);
		this.tbxHorizScale.setEnabled(isvalid);

		// vertical 
		this.ibtnVerticScaleDecrease.setEnabled(isvalid);
		this.ibtnVerticScaleIncrease.setEnabled(isvalid);
		this.tbxVerticScale.setEnabled(isvalid);

		// fontsize and font italic
		this.ibtnFontItalic.setEnabled(isvalid);
		this.ibtnFontSizeDecrease.setEnabled(isvalid);
		this.ibtnFontSizeIncrease.setEnabled(isvalid);
		this.tbxFontSize.setEnabled(isvalid);
		
		// branch line width
		this.ibtnLinewidthIncrease.setEnabled(isvalid);
		this.ibtnLinewidthDecrease.setEnabled(isvalid);
		this.tbxLienwidth.setEnabled(isvalid);

		/*
		 * initiate buttons on Advanced tab
		 */
		//        this.ibtnFit2Width.setEnabled(isvalid);
		//        this.ibtnFit2Height.setEnabled(isvalid);
		//        this.ibtnFit2Canvas.setEnabled(isvalid);
		//        
		ibtnFitCanvas2plotWidth.setEnabled(isvalid);
		ibtnFitCanvas2plotHeight.setEnabled(isvalid);
		ibtnFitCanvas2plot.setEnabled(isvalid);
		this.ibtnWhereIsMyTree.setEnabled(isvalid);
		

		//ibtnWhereIsMyTree.setEnabled(isvalid); // Apr 24, 2012

		/**
		 * *********************************************
		 * initiate buttons on tabDataUpload
		 * *********************************************
		 */
		this.ibtnDataUploadPiechart.setEnabled(isvalid);
		//        this.tbxPiechartMinRadius.setEnabled(isvalid);
		//        this.tbxPiechartMaxRadius.setEnabled(isvalid);
		//        this.tbxPiechartIncRadiusPerValue.setEnabled(isvalid); // this isn't shown ; April 14, 2011;

		this.ibtnDataUploadProteinDomains.setEnabled(isvalid); // May 11, 2012

		this.ibtnDataUploadHeatmap.setEnabled(isvalid);
		
		
		this.ibtnDataUploadBarPlots.setEnabled(isvalid);
		this.ibtnDataUploadBranchColor.setEnabled(isvalid);
		this.ibtnDataUploadColorStrips.setEnabled(isvalid);
		this.ibtnDataUploadLabelColor.setEnabled(isvalid);
		this.ibtnDataUploadLeafLabelBKColor.setEnabled(isvalid);

		/*
		 * initiate buttons on the export tab
		 */
		this.ibtnSave2PDF2.setEnabled(isvalid);
		this.ibtnExport2SVG.setEnabled(isvalid);
		this.ibtnSave2SVG2.setEnabled(isvalid);
		this.ibtnExport2PNG.setEnabled(isvalid);
		//        this.ibtnExport2EPS.setEnabled(isvalid);
		//        this.ibtnExport2PS.setEnabled(isvalid);
		this.ibtnExport2JPEG.setEnabled(isvalid);
		this.ibtnExport2TIFF.setEnabled(isvalid);
		this.ibtnExport2Newick.setEnabled(isvalid);
		this.ibtnExport2Nexus.setEnabled(isvalid);
		this.ibtnExport2Nhx.setEnabled(isvalid);
		this.ibtnExport2PhyloXML.setEnabled(isvalid);

		if (svgviewer == null) {

			// basic tab text boxes
			this.tbxHorizScale.setText("");
			this.tbxVerticScale.setText("");
			this.tbxFontSize.setText("");
			// sep 10, 2013
			tbxFontSizeBootStrap.setText("");
			this.tbxFontSizeBranchLength.setText("");

			// view option buttons
			this.ibtnCircularPhylo.setEnabled(false);
			this.ibtnRectPhylo.setEnabled(false);

			this.ibtnBootstrapShowHide.setEnabled(false);
			this.ibtnBranchlengthShowHide.setEnabled(false);
			this.ibtnLeafLabelAlign.setEnabled(false);

			/*
			 * advanced tab
			 */
			this.ibtnCicularClockwise.setEnabled(false);
			this.ibtnCicularCounterClockwise.setEnabled(false);
			this.ibtnCicularRotateClockwise.setEnabled(false);
			this.ibtnCicularRotateCounterClockwise.setEnabled(false);
			this.ibtnCicularSpanDecrease.setEnabled(false);
			this.ibtnCicularSpanIncrease.setEnabled(false);

			tbxCicularStartAngel.setEnabled(false);
			tbxCicularStartAngel.setText("");
			tbxCicularSpan.setEnabled(false);
			tbxCicularStartAngel.setText("");
			tbxLienwidth.setText("");

		} else {
			treePlotMode plotmode = svgviewer.getTreePlotMode();
			/*
			 * icons on basic tab
			 */
			// phylo modes are depending on branchlength
			boolean hasbranchlength = svgviewer.hasBranchLength();
			this.ibtnCircularPhylo.setEnabled(hasbranchlength);
			this.ibtnRectPhylo.setEnabled(hasbranchlength);

			// Oct 26, 2011; if a tree has no branch length, then it's no need to align the leaf labels
			this.ibtnLeafLabelAlign.setEnabled(hasbranchlength);

			this.ibtnBootstrapShowHide.setEnabled(svgviewer.hasBootStrap());
			this.ibtnBootstrapShowHide.setSelect(svgviewer.getShowBootStrapScores());

			this.ibtnLeafLabelShowHide.setSelect(svgviewer.getShowLeafLable()); // whether or not the leaf label is shown

			this.ibtnBranchlengthShowHide.setEnabled(hasbranchlength);
			this.ibtnBranchlengthShowHide.setSelect(svgviewer.getShowBranchLength());

			this.ibtnLeafLabelAlign.setSelect(svgviewer.getTreeSkeleton().getAlignLeafLabels());

			switch (plotmode) {
			case RECT_CLADOGRAM:
				this.ibtnRectClado.setSelect(true);
				break;
			case RECT_PHYLOGRAM:
				this.ibtnRectPhylo.setSelect(true);
				break;
			case SLANTED_CLADOGRAM_NORMAL:
				this.ibtnSlantedClado.setSelect(true);
				break;
			case CIRCULAR_CLADOGRAM:
				this.ibtnCircularClado.setSelect(true);
				break;
			case CIRCULAR_PHYLOGRAM:
				this.ibtnCircularPhylo.setSelect(true);
				break;
			default:
				this.ibtnRectClado.setSelect(true);
				break;
			}

			// nov 07, 2011; font italic
			this.ibtnFontItalic.setSelect(svgviewer.getLeafFontItalic());

			// textboxes on basic tab
			this.tbxFontSize.setText(Integer.toString(svgviewer.getCurrentFontSize()));
			this.tbxHorizScale.setText(formatterDeci3.format(svgviewer.getPxPerWidth()));
			this.tbxVerticScale.setText(formatterDeci3.format(svgviewer.getPxPerHeight()));
			this.tbxLienwidth.setText(formatterDeci3.format(svgviewer.getTreeBranchLineWidth()));// Nov 07, 2011
			// sep 10, 2013
			this.tbxFontSizeBootStrap.setText(Integer.toString(svgviewer.getCurrentFontSize4BootStrap()));
			this.tbxFontSizeBranchLength.setText(Integer.toString(svgviewer.getCurrentFontSize4BranchLength()));

			setToolbarStatusOnAdvancedTab(svgviewer, plotmode);
		}// if valid 

	}// setToolbarButtonStatus -- ends here 
	
	private void setToolbarStatusOnAdvancedTab(PhyloTreeSVGViewer svgviewer, treePlotMode plotmode) {
		/*
		 * advanced tab
		 */
		if (plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
			this.ibtnCicularClockwise.setEnabled(true);
			this.ibtnCicularCounterClockwise.setEnabled(true);
			this.ibtnCicularRotateClockwise.setEnabled(true);
			this.ibtnCicularRotateCounterClockwise.setEnabled(true);
			this.ibtnCicularSpanDecrease.setEnabled(true);
			this.ibtnCicularSpanIncrease.setEnabled(true);

			// set values for textboxes in advanced tab
			tbxCicularStartAngel.setEnabled(true);
			tbxCicularSpan.setEnabled(true);
			this.tbxCicularStartAngel.setText(Integer.toString(Math.round(svgviewer.getCicularStartAngle())));
			this.tbxCicularSpan.setText(Integer.toString(Math.round(svgviewer.getCicularSpan())));

			// set status for clock and conterclose mode
			boolean clockwise = this.currentSVGViewer.getCircularModeClockwise();
			this.ibtnCicularClockwise.setSelect(clockwise);
			this.ibtnCicularCounterClockwise.setSelect(!clockwise);

			/*
			 * also disable "vertical scale" related buttons
			 */
			this.ibtnVerticScaleDecrease.setEnabled(false);
			this.ibtnVerticScaleIncrease.setEnabled(false);
			this.tbxVerticScale.setEnabled(false);
			this.tbxVerticScale.setText("");

		} else {
			this.ibtnCicularClockwise.setEnabled(false);
			this.ibtnCicularCounterClockwise.setEnabled(false);
			this.ibtnCicularRotateClockwise.setEnabled(false);
			this.ibtnCicularRotateCounterClockwise.setEnabled(false);
			this.ibtnCicularSpanDecrease.setEnabled(false);
			this.ibtnCicularSpanIncrease.setEnabled(false);

			tbxCicularStartAngel.setEnabled(false);
			tbxCicularStartAngel.setText("");
			tbxCicularSpan.setEnabled(false);
			tbxCicularSpan.setText("");

			/*
			 * then enable "vertical scale" related buttons
			 */
			this.ibtnVerticScaleDecrease.setEnabled(true);
			this.ibtnVerticScaleIncrease.setEnabled(true);
			this.tbxVerticScale.setEnabled(true);
			this.tbxVerticScale.setText(formatterDeci3.format(svgviewer.getPxPerHeight()));

		}
	} // setToolbarStatusOnAdvancedTab
}
