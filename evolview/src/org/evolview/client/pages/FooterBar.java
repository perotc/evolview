package org.evolview.client.pages;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

public class FooterBar extends Composite {

	private static FooterBarUiBinder uiBinder = GWT.create(FooterBarUiBinder.class);

	interface FooterBarUiBinder extends UiBinder<Widget, FooterBar> {
	}
	
	public interface Resources extends ClientBundle {
        public static final Resources INSTANCE = GWT.create(Resources.class);
        @Source( "FooterBar.css" )
        public CssResource css();
    }

	public FooterBar() {
		initWidget(uiBinder.createAndBindUi(this));
		Resources.INSTANCE.css().ensureInjected();
		this.setStyleName("footer-bar");
	}

}
