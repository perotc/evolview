package org.evolview.client.pages;


import org.evolview.client.utils.AlertWidget;
import org.evolview.client.utils.BalloonTips; 
import org.evolview.client.utils.MC;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginPage extends Composite {

	private static LoginPageUiBinder uiBinder = GWT.create(LoginPageUiBinder.class);

	interface LoginPageUiBinder extends UiBinder<Widget, LoginPage> {
	}
	
	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 *                  June 14, 2013
	 *   social login using accounts from 
	 *   eastern / Chinese companies 
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
	 */    

	/**
	 * UI fields
	 */
	@UiField
	TextBox tboxLogin;
	@UiField
	PasswordTextBox pboxPassword;
	@UiField
	CheckBox ckboxStaySignedIn;

	/**
	 * UI handlers
	 */ 

	/*
	 * try without an account
	 * Feb 19, 2013 --
	 * feb 26, 2013 --
	 * will not check if browser is supported since it will be checked at the beginning of the program
	 */
	@UiHandler("btnTryWithoutAnAccount")
	void tryWithoutAnAccount(ClickEvent e) {
		if (MC.cookieman.getSessionID().isEmpty()) {
			// create a temporary seesionID 
			MC.rpc.signInAsTemporaryUser(new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					MC.alertFailConnectingServer();
				}

				@Override
				public void onSuccess(String sessionID) {
					processPostSignIn(sessionID);
				}
			});
		}
	}

	/*
	 * Oct 25 2011; After signin successfuly, all errors will be reset; all
	 * inputs will be wiped out
	 */
	@UiHandler("btnSignIn")
	void processSignIn(ClickEvent e) {
		doProcessSignin();
	}

	// Feb 3, 2012 --
	@UiHandler("pboxPassword")
	void onBoxPasswordEnterKey(KeyUpEvent event) {
		int charcode = event.getNativeKeyCode();
		if (charcode == KeyCodes.KEY_ENTER) {
			doProcessSignin();
		}
	}

	// April 9, 2013; 
	@UiHandler("tboxLogin")
	void onTBoxLoginEnterKey(KeyUpEvent event) {
		int charcode = event.getNativeKeyCode();
		if (charcode == KeyCodes.KEY_ENTER) {
			doProcessSignin();
		}
	}

	public LoginPage() {
		initWidget(uiBinder.createAndBindUi(this));

		// Apr 23, 2012; set ckboxStaySignedIn to true
		this.ckboxStaySignedIn.setValue(true);

		// window title; feb 26, 2013
		Window.setTitle("EvolView for BGene : login");
	} 

	/**
	 * private and protected functions
	 */ 
	/**
	 * Feb 17, 2013: new way of process signIn --
	 */
	private void doProcessSignin() {
		tboxLogin.removeStyleName("textInputError");
		pboxPassword.removeStyleName("textInputError");

		// check if user input is valid
		//final String login = tboxLogin.getText();
		//final String passwrd = pboxPassword.getText();
		if (tboxLogin.getText().length() < 3) {
			MC.balloon.setWidgetToShow(new HTML("login is too short, it should be >= 3 letters"));
			MC.balloon.showRelativeToWidget(tboxLogin, BalloonTips.TipPos.topTo);
			tboxLogin.addStyleName("textInputError");
		} if (pboxPassword.getText().length() < 3) {
			MC.balloon.setWidgetToShow(new HTML("password is too short, it should be >= 3 letters"));
			MC.balloon.showRelativeToWidget(pboxPassword, BalloonTips.TipPos.topTo);
			pboxPassword.addStyleName("textInputError");
		} else {
			// Feb 17, 2013; new way of dealing with sign in --
			MC.rpc.processSignIn(tboxLogin.getText(), pboxPassword.getText(), new AsyncCallback<String>() {
				@Override
				public void onFailure(Throwable caught) {
					MC.alertFailConnectingServer();
				}

				@Override
				public void onSuccess(String sessionID) {
					processPostSignIn(sessionID);
				}
			}); // end of RPC
		}
	}

	private void processPostSignIn(String sessionID) {
		if (sessionID == null || sessionID.isEmpty()) {
			MC.alert.setMessageAndShow("email and password do not match or account does not exist!!", AlertWidget.AlertWidgetType.error);
		} else {
			System.out.println(" --> sign in successful ");
			if (ckboxStaySignedIn.getValue()) {
				MC.cookieman.saveSessionID(sessionID);
				System.out.println("   --> save sessionID :" + sessionID);
			} else {
				MC.cookieman.saveSessionID(sessionID, 1000 * 60); // one minute
			}

			// redirect to previous page after login successfully; Oct 30, 2012
			//String historyToken = MyStates.INSTANCE.getPreviousToken();
			//if (historyToken != null && !historyToken.isEmpty()) {
			//History.newItem(historyToken, true);
			//} else {
			System.out.println("   --> goto mytrees");
			History.newItem("mytrees", true);
			//}//

			// clear user inpu
			pboxPassword.setText("");
		}
	}

}
