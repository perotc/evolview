package org.evolview.client;

import com.google.gwt.event.shared.GwtEvent;

public class ConfirmationNoEvent extends GwtEvent<ConfirmationNoHandler> {
	public static Type<ConfirmationNoHandler> TYPE = new Type<ConfirmationNoHandler>();

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ConfirmationNoHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ConfirmationNoHandler handler) {
		handler.onConfirmationNoClicked(this);
	}

}
