package org.evolview.client;

import org.evolview.client.utils.AwesomeButton;
import org.evolview.client.utils.MC;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;


/**
 *
 * @author E.T.; May 2, 2011;
 * Mar 28, 2013: use AwesomeButton now 
 */
public class ConfirmDialogbox extends DialogBox {
	
	private String message = "";
    private final AwesomeButton yes = new AwesomeButton( "Yes", AwesomeButton.AwesomeButtonColor.red, "small" ),
    		no = new AwesomeButton( "No", AwesomeButton.AwesomeButtonColor.green, "small" ),
    		cancel = new AwesomeButton( "Cancel", AwesomeButton.AwesomeButtonColor.black, "small" );
    
    public ConfirmDialogbox(){
        super();
        initiate();
    }
    
    public ConfirmDialogbox(String msg){
        super();
        this.message = msg;
        initiate();
    }

    public void setCancelButtonVisible( boolean b ){
    	this.cancel.setVisible(b);
    }
    
    /*
     * private functions
     */
    private void initiate() {
        
        setModal(true);
        setGlassEnabled(true);
        
        this.setText("Confirm");
        FlexTable flextable = new FlexTable();
        
        /*
         * assemble the flextable
         */
        flextable.setCellSpacing(5);
        
        // widgets at row 1
        flextable.setWidget(0, 0, new HTML( this.message ));
        
        // widgets at row 2:
        HorizontalPanel hpButtons = new HorizontalPanel();
        
        //yes.setWidth("50px");
        //no.setWidth("50px");
        
        cancel.setVisible(false); // by default cancel is invisible
        
        hpButtons.add(cancel);
        hpButtons.add(no);
        hpButtons.add(yes);
        hpButtons.setSpacing(5);
        hpButtons.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        
        no.addClickHandler( new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
            	MC.ebus.fireEvent( new ConfirmationNoEvent());
                hide();
            }
        }) ;
        
        yes.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				MC.ebus.fireEvent(new ConfirmationYesEvent());
				hide();
			}});
        
        cancel.addClickHandler( new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				hide();
			}});
        
        flextable.setWidget(1, 0, hpButtons);
        flextable.getCellFormatter().setHorizontalAlignment(1, 0, HasHorizontalAlignment.ALIGN_RIGHT);
        
        //flextable.setPixelSize(500, 400);
        this.add(flextable);
    }
}
