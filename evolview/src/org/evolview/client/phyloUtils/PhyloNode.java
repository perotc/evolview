package org.evolview.client.phyloUtils;


import com.google.gwt.user.client.rpc.IsSerializable;
import java.util.ArrayList;
import java.util.HashMap;

public class PhyloNode implements IsSerializable{

    /*
     * variables I need to describe a tree node
     */
	/**
	 * Natural ID, used for the label of the nodes
	 */
    private String                  id;
    /**
     * Intern. technical Id
     */
    private String internal_id;
    private String description;  
    private float                   branch_length = 0, bootstrap;
    private PhyloNode               parent; // only one parent is allowed
    private ArrayList<PhyloNode>    descendents; // multiple (>= two) descendents are allowed
    private boolean                 isRoot = false, isLeaf = false;
    private float                   max_distance_to_tip = -1, // max_distance_to_tip (height) is the max branchlength to get to the tip;
                                    branchlength_to_root = -1; // total branch length from current node to root
    
    private boolean                 processed = false; // May 20, 2011; check if current node is processed; used to fix a bug
    
    private HashMap<Integer, String>
            branchColors = new HashMap<Integer, String>(), // $hash{$colorsetID} = color
            leafColors = new HashMap<Integer, String>(),
            leafBackgroundColors = new HashMap<Integer, String>(),
            
            colors = new HashMap<Integer, String>(); // April 8, 2011; NOTE: use this instead of branchcolors, leafcolors and leafbackground colors ...

    /*
     * level_vertical is the relative vertical level from the top, start with 1;
     *   for example, given a tree like the following:
     *     |----- A 1
     *  ---| C    |--- E 2
     *     |------| B 2.5      a sample tree
     *     	      |----F 3
     *
     *   A is level 1, E is level 2, F is 3, B is 2.5 (2+3)/2; C is (2.5 + 1) / 2 = 1.725
     *
     * level_horizontal is the relative horizontal level from the right, start with 1; therefore all leaves are at level 1
     *   in this sample tree, AEF are at level 1, B is 2, C is 3
     *
     * here I use two integers to store the two levels
     *
     * >>>> how levels are assigned >>>>
     *   vertical levels: in a tree like ((A,B), (C,D)), levels for ABCD are 1,2,3,4 respectively; internal levels are assigned using a function: ""
     *   horizontal levels: all leaf are assigned with 1, internal levels are assigned using a function: ""
     *
     */
    private float 					level_horizontal = 0, level_vertical = 0;

    /*
     * jan 7, 2011; I need this to plot SLANTED_CLADOGRAM_MIDDLE;
     *   in this mode, the position of internal node
     *       = min_leaf_vertical_level + (max_leaf_vertical_level - min_leaf_vertical_level) / 2
     *   while the leaf node has the same with level_vertical
     */
    private float                   level_vertical_slanted = 0;

    /*
     * jan 7, 2011; plot SLANTED_CLADOGRAM_NORMAL; similar to dendroscope
     * in this mode, for each internal node, I need :
         min_leaf_vertical_level and max_leaf_vertical_leve
     */
    private float                   min_leaf_vertical_level = 0, max_leaf_vertical_level = 0;


    /*
     * jan 7, 2011; plot CIRCULAR_CLADOGRAM; similar to itol
     * in this mode, for each internal node, I need :
         min_descendent_vertical_level and max_descendent_vertical_leve
     */
    //private float                   min_des_vertical_level = 0, max_des_vertical_level = 0;


    /*
     * jan 7, 2011; add position x and y
     */
    private float                   x = 0, y = 0;
    private HashMap<String, String> nhxAttributes   = new HashMap<String, String>();

    // constructor
    public PhyloNode (){
    	descendents = new ArrayList<PhyloNode> ();
    }

    // public methods
    public float getLevelHorizontal(){
        return level_horizontal;
    }

    public void setLevelHorizontal(float new_level_horizontal){
        level_horizontal = new_level_horizontal;
    }

    public float getLevelVertical(){
        return level_vertical;
    }

    public void setLevelVertical(float new_level_vertical){
    	level_vertical = new_level_vertical;
    }

    // jan 7, 2011; if current node is leaf, level_vertical_slanted = level_vertical
    // so there is not necessary to assign level_vertical_slanted to leaf node;
    // for slanted middle mode;
    public float getLevelVerticalSlanted(){
        return this.isLeaf() ? level_vertical : level_vertical_slanted;
    }

    public void setLevelVerticalSlanted(float new_level_vertical_slanted){
    	level_vertical_slanted = new_level_vertical_slanted;
    }

    // jan 7, 2011; if leaf node, min_leaf_vertical_level and max_leaf_vertical_level = level_vertical
    // for slanted normal mode
    public float getMinLeafVerticalLevel(){
        return this.isLeaf() ? level_vertical : min_leaf_vertical_level;
    }

    public void setMinLeafVerticalLevel(float new_min_leaf_level_vertical){
    	min_leaf_vertical_level = new_min_leaf_level_vertical;
    }
    public float getMaxLeafVerticalLevel(){
        return this.isLeaf() ? level_vertical : max_leaf_vertical_level;
    }

    public void setMaxLeafVerticalLevel(float new_max_leaf_level_vertical){
    	max_leaf_vertical_level = new_max_leaf_level_vertical;
    }
    // <<<< jan 7, 2011;

    /*
     * x and y; position;
     */
    public float getX(){
        return x;
    }

    public void setX(float newx){
        x = newx;
    }

    public float getY(){
        return y;
    }

    public void setY(float newy){
        y = newy;
    }
    // <<<<< x and y <<<<<<<

    public String getID(){
        return id;
    }

    public void setID(String newid){
        id = newid;
    }

    public String getInternalID(){
        return internal_id;
    }

    public void setInternalID(String newid){
        internal_id = newid;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String newdesc){
        description = newdesc;
    }

    public float getBranchLength(){
        return branch_length;
    }

    public void setBranchLength(float new_bl){
        branch_length = new_bl;
    }

    public float getBootStrap(){
        return bootstrap;
    }

    public void setBootStrap(float newbootstrapscore){
        bootstrap = newbootstrapscore;
    }

    public PhyloNode getParent(){
        return parent;
    }

    public void setParent(PhyloNode newparent){
        parent = newparent;
    }

    public ArrayList<PhyloNode> getDescendents(){
        return descendents;
    }

    public void setDescendents(ArrayList<PhyloNode> newdescendents){
        descendents = newdescendents;
    }

    public void addADescendent(PhyloNode newdescendent){
        if(descendents.contains(newdescendent) == false){
            descendents.add(newdescendent);
            // also change the parent automatically
            newdescendent.setParent(this);
        }
    }

    public void removeADescendent(PhyloNode descendent){
        if(descendents.contains(descendent) == true){
            int idx = descendents.indexOf(descendent);
            descendents.remove(idx);
        }
    }
    
    // -- remove from parent; oct 26, 2013 -- 
    public PhyloNode removeFromParent(){
    	PhyloNode parent = this.getParent();
    	if( parent != null ){
    		parent.removeADescendent(this);
    	}
    	return parent;
    }

    public int getNumberOfDescendents(){
        return descendents.size();
    }
    
    public int getNumberOfLeafDescendents(){
        int nLeafDescendents = 0;
        for( PhyloNode node : this.descendents ){
            if( node.isLeaf() ){
                nLeafDescendents ++;
            } else {
                nLeafDescendents += countLeafDescendents( node );
            }
        }
        return nLeafDescendents;
    }
    
    private int countLeafDescendents( PhyloNode node ){
        int count = 0;
        for( PhyloNode dnode : node.getDescendents() ){
            if( dnode.isLeaf() ){
                count ++;
            } else {
                count += countLeafDescendents(dnode);
            }
        }
        return count;
    }

    public boolean isLeaf(){
        return isLeaf;
    }

    public void setLeaf(boolean leaf){
        isLeaf = leaf;
    }

    public boolean isRoot(){
        return isRoot;
    }

    public void setRoot(boolean root){
        isRoot = root;
    }

    public float getMaxDistanceToTip(){
        return max_distance_to_tip;
    }

    public void setMaxDistanceToTip(float newheight){
    	max_distance_to_tip = newheight;
    }

    public float getBranchLengthToRoot(){
        return branchlength_to_root;
    }

    public void setBranchLengthToRoot(float new_distance_to_root){
    	branchlength_to_root = new_distance_to_root;
    }

    public boolean isNodeComplete(){ // if all necessary information is there
        boolean isComplete = true;

        if(branch_length <= 0 || id.trim().length() == 0 || (parent == null && descendents.isEmpty())){
            isComplete = false;
        }

        return isComplete;
    }

    public PhyloNode clone(){
        // make a new copy of current node
        PhyloNode newNode = new PhyloNode();

        newNode.setBootStrap(bootstrap);
        newNode.setBranchLength(branch_length);
        newNode.setDescendents(descendents); // this isn't clone yet
        newNode.setDescription(description);
        newNode.setID(id);
        newNode.setInternalID(internal_id);
        newNode.setParent(parent); // this isn't clone yet

        return newNode;
    }
    
    /*
     * branch colors
     */
    public void addColorToBranch(int colorsetID, String color){
        this.branchColors.put(colorsetID, color);
    }
    
    public String getBranchColorByColorsetID(int colorsetID){
        
        return this.branchColors.containsKey(colorsetID) ? this.branchColors.get(colorsetID) : "black";
        
    }
    
    public void removeBranchColorByColorsetID(int colorsetID){
        this.branchColors.remove(colorsetID);
    }
    
    public void removeAllBranchColors(){
        this.branchColors.clear();
    }
    
    /*
     * leaf colors
     */
    public void addColorToLeaf(int colorsetID, String color){
        this.leafColors.put(colorsetID, color);
    }
    
    public String getLeafColorByColorsetID(int colorsetID){
        return this.leafColors.containsKey(colorsetID) ? this.leafColors.get(colorsetID) : "black";
    }
    
    public void removeLeafColorByColorsetID(int colorsetID){
        this.leafColors.remove(colorsetID);
    }
    
    public void removeAllLeafColors(){
        this.leafColors.clear();
    }
    
    /*
     * leaf background colors
     */
    public void addColorToLeafBK(int colorsetID, String color){
        this.leafBackgroundColors.put(colorsetID, color);
    }
    
    public String getLeafBKColorByColorsetID(int colorsetID){
        return this.leafBackgroundColors.containsKey(colorsetID) ? this.leafBackgroundColors.get(colorsetID) : "white"; // March 21, 2011; change default color from 'black' to 'white'
    }
    
    public void removeLeafBKColorByColorsetID(int colorsetID){
        this.leafBackgroundColors.remove(colorsetID);
    }
    
    public void removeAllLeafBKColors(){
        this.leafBackgroundColors.clear();
    }
    
    public String getColorByDatasetID(int id){
        return this.colors.containsKey(id) ? this.colors.get(id) : "";
    }
    
    public void addColorWithID(int datasetID, String color){
        this.colors.put(datasetID, color);
    } // April 8, 2011
    
    public String getColorByDataTypeAndID( String type, int id ){
        String col = "";
        if( type.equalsIgnoreCase("branch") ){
            col = this.getBranchColorByColorsetID(id);
        }else if ( type.equalsIgnoreCase("leaf") ){
            col = this.getLeafColorByColorsetID(id);
        }else if ( type.equalsIgnoreCase("leafbk") ){
            col = this.getLeafBKColorByColorsetID(id);
        }
        
        return col;
    }// April 8, 2011;
    
    public void addColorByDataTypeAndID( TreeDecoType type, int id, String col ){
        if( type.equals( TreeDecoType.BRANCHCOLOR ) ){
            this.addColorToBranch(id, col);
        }else if ( type.equals( TreeDecoType.LEAFCOLOR ) ){
            this.addColorToLeaf(id, col);
        }else if (  type.equals( TreeDecoType.LEAFBKCOLOR ) ){
            this.addColorToLeafBK(id, col);
        }
    }// April 8, 2011;
    
    public void removeColorByDataTypeAndID( TreeDecoType type, int id ){
        if( type.equals( TreeDecoType.BRANCHCOLOR ) ){
            this.removeBranchColorByColorsetID(id);
        }else if ( type.equals( TreeDecoType.LEAFCOLOR ) ){
            this.removeLeafColorByColorsetID(id);
        }else if ( type.equals( TreeDecoType.LEAFBKCOLOR ) ){
            this.removeLeafBKColorByColorsetID(id);
        }
    }// April 8, 2011;
    
    // May 20, 2011;
    public boolean setProcessed( boolean b ){
        this.processed = b;
        return this.processed;
    }
    
    public boolean getProcessed(){
        return this.processed;
    }

    public void setAdditionalAttributes(HashMap<String, String> nhxAttrs) {
        this.nhxAttributes = nhxAttrs;
    }
    
    public HashMap<String, String> getAdditionalAttributs(  ){
        return this.nhxAttributes;
    }
    
    public int getDistanceToRoot(){
        int distance_to_root = 0;
        if( !this.isRoot() ){
            distance_to_root ++;
            PhyloNode node = this.getParent();
            while( node != null && !node.isRoot() ){
                distance_to_root ++;
                node = node.getParent();
            }
        }
        return distance_to_root;
    }

	public void removeAllDescendents() {
		this.descendents.clear();
	}
}
