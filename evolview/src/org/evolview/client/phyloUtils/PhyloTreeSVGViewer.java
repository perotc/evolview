package org.evolview.client.phyloUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import org.evolview.client.DataContainers.ColorDataContainerParser;
import org.evolview.client.DataContainers.ColorEx;
import org.evolview.client.DataContainers.DomainsOfAProtein;
import org.evolview.client.DataContainers.HeatmapMatrixDataContainer;
import org.evolview.client.DataContainers.NumericMatrixDataContainer;
import org.evolview.client.DataContainers.ProteinDomain;
import org.evolview.client.DataContainers.ProteinDomainDataContainerParser;
import org.evolview.client.utils.AlertWidget;
import org.evolview.client.utils.AwesomeButton;
import org.evolview.client.utils.JAVAFuncs;
import org.evolview.client.utils.JSFuncs;
import org.evolview.client.utils.MC;
import org.evolview.client.utils.InfoPanelWidget;
import org.vectomatic.dom.svg.OMElement;
import org.vectomatic.dom.svg.OMNode;
import org.vectomatic.dom.svg.OMSVGCircleElement;
import org.vectomatic.dom.svg.OMSVGDefsElement;
import org.vectomatic.dom.svg.OMSVGDocument;
import org.vectomatic.dom.svg.OMSVGFEBlendElement;
import org.vectomatic.dom.svg.OMSVGFEGaussianBlurElement;
import org.vectomatic.dom.svg.OMSVGFEMergeElement;
import org.vectomatic.dom.svg.OMSVGFEMergeNodeElement;
import org.vectomatic.dom.svg.OMSVGFEOffsetElement;
import org.vectomatic.dom.svg.OMSVGFilterElement;
import org.vectomatic.dom.svg.OMSVGGElement;
import org.vectomatic.dom.svg.OMSVGLength;
import org.vectomatic.dom.svg.OMSVGLineElement;
import org.vectomatic.dom.svg.OMSVGMatrix;
import org.vectomatic.dom.svg.OMSVGPathElement;
import org.vectomatic.dom.svg.OMSVGPathSegList;
import org.vectomatic.dom.svg.OMSVGPoint;
import org.vectomatic.dom.svg.OMSVGPolygonElement;
import org.vectomatic.dom.svg.OMSVGRect;
import org.vectomatic.dom.svg.OMSVGRectElement;
import org.vectomatic.dom.svg.OMSVGSVGElement;
import org.vectomatic.dom.svg.OMSVGTextElement;
import org.vectomatic.dom.svg.OMSVGTransform;
import org.vectomatic.dom.svg.utils.DOMHelper;
import org.vectomatic.dom.svg.utils.OMSVGParser;
import org.vectomatic.dom.svg.utils.SVGConstants;

import com.google.gwt.core.client.JsArrayInteger;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasMouseDownHandlers;
import com.google.gwt.event.dom.client.HasMouseMoveHandlers;
import com.google.gwt.event.dom.client.HasMouseOutHandlers;
import com.google.gwt.event.dom.client.HasMouseOverHandlers;
import com.google.gwt.event.dom.client.HasMouseUpHandlers;
import com.google.gwt.event.dom.client.HasMouseWheelHandlers;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseEvent;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * @author E.T.; jan 12, 2011; add toolbar
 *
 * March 25, 2011; add internal management system; replot all April 7, 2011;
 * clean up the codes May 12, 2011; make this panel resizable May 17, 2011; fix
 * a bug in barplot/circular mode Oct 28, 2011; control a dataset is active or
 * not; if a new dataset is not active, it'll be added to the tree, but the plot
 * is not updated
 *
 * -- Apr 23, 2012 -- text label orientation for better readability
 *
 * -- Apr 24, 2012 -- check visibility after changing the px-perwidth or
 * plotmode
 * 
 * -- March 31, 2013 -- only deal with mouse left button -down event 
 * -- April 1, 2013 -- use DOM to add css styles for this SimplePanel --
 * -- May 5, 2013 -- whereIsMyTree?? done --
 * >>>> oct 25, 2013: implement rerootTree( PhyloNode inode ); inode can be a leaf node as well as an internal node --
 */
public class PhyloTreeSVGViewer extends SimplePanel
		implements
			HasMouseDownHandlers,
			HasMouseUpHandlers,
			HasMouseOverHandlers,
			HasMouseOutHandlers,
			HasMouseWheelHandlers,
			HasMouseMoveHandlers {
	
	/*
	 * NOTE: here the 'extends' part could be SimplePanel, VP, HP and any other
	 * panels derived from SimplePanel; jan 7, 2011
	 */

	private String sessionid = "";
	private static String demoproject = "DEMOS";
	
	/**
	 * Camel case
	 */
	private static final String LEAF_SELECTION_CSSS_ATTRIBUTE = "font-weight";
	private static final String LEAF_NOTSELECTED_STYLE = "normal";
	private static final String LEAF_SELECTED_STYLE = "bold";
	private static final String LEAF_SELECTION_CSSS_ATTRIBUTE2 = "font-size";
	private static final String LEAF_NOTSELECTED_STYLE2 = "14px";
	private static final String LEAF_SELECTED_STYLE2 = "24px";
	private static final String LEAF_SELECTION_CSSS_ATTRIBUTE3 = "text-shadow";
	private static final String LEAF_NOTSELECTED_STYLE3 = "";
	private static final String LEAF_SELECTED_STYLE3 = "2px 2px blue";
	
	
	// >>>>>>>>>>>>>> global variables >>>>>>>>>>>>>>>>
	/*
	 * tree plot modes; phylogram/ cladogram; cladogram styles don't show branch
	 * length Jan 9, 2011; currently RADIAL_CLADOGRAM mode not implemented
	 */
	private treePlotMode plotmode = treePlotMode.RECT_CLADOGRAM;
	/*
	 * viewing options in the toolbar
	 */
	private boolean showBrachLength = false, showNodeLabel = true, showBootStrap = false, 
			draw_branch_length_scale = false, circular_tree_plot_in_clockwise_mode = true,
	/*
	 * March 25, 2011; a global parameter to indicate whether all
	 * datasets are disabled:
	 */
	disable_all_dataset = false, chart_plots_enabled = true, // a boolean indicates whether charts (those plotted next to leaf labels) are diabled
			leaf_font_italic = false, // Nov 07, 2011;
			bMouseWheelEnabled = true,  

			/**
			 * Sep 10, 2013
			 */
			bootStrapFontItalic = false, branchLengthFontItalic = false

			;

	/*
	 * some global parameters for tree plot
	 */
	private float pxPerWidthDefault = 20f; // -- April 23, 2012 --
	private float pxPerHeight = 20f, pxPerWidthCurrentMode = 0, // -- April 23, 2012 --
			pxPerBranchLengthScaling = 5, pxPerBranchLength = pxPerWidthDefault * pxPerBranchLengthScaling, // not used in CLAD modes

			// min and max allowed px perwidth
			//			pxPerWidthMinAllowed = 0, pxPerWidthMaxAllowed = pxPerWidthDefault * 20,
			// min and max allowed px per height
			//			pxPerHeightMinAllowed = 0, pxPerHeightMaxAllowed = pxPerHeight * 20, defaultPxPerWidth = 20, defaultPxPerHeight = 20,
			// Margins ...
			//			margin_normal = 0.15f, margin_with_text = 0.2f,

			margin_x = 50f, margin_y = 50f,
			// default line width 
			default_line_width = 1f,
			// for circular  mode
			angle_per_vertical_level = 0, // Oct 27, 2011: this should be float, instead of int
			angle_start = 0f, angle_span = 350f, scale_tree_branch_linewidth = 1.5f; // Nov 07, 2011; 

	/**
	 * margins; May 5, 2013
	 */
	private final int margin_top = 10, margin_left = 10;

	/*
	 * ===== April 23, 2012 ===== -- improve 1: pxPerWidth changes according to
	 * plot mode -- howto: by default, the default values for each mode will be
	 * used; however if user changes the pxPerWidth for a plotmode, that value
	 * will be always used (for the specific plot mode) and saved to server
	 */
	private final String sPxPerWidth = "horizScale4PlotMode";
	private EnumMap<treePlotMode, Float> hmPlotMode2pxPerWidth = new EnumMap<treePlotMode, Float>(treePlotMode.class);
	// -- improve 2: independent tranlatedXY for each plot mode --
	private final String sTranslateX = "translateX4PlotMode";
	private final String sTranslateY = "translateY4PlotMode";
	private EnumMap<treePlotMode, Float> hmPlotMode2TranslateX = new EnumMap<treePlotMode, Float>(treePlotMode.class);
	private EnumMap<treePlotMode, Float> hmPlotMode2TranslateY = new EnumMap<treePlotMode, Float>(treePlotMode.class);
	// <<<<< April 23, 2012 <<<<<
	// for CIRCULAR mode;
	private int max_angle_span = 360, min_angle_span = 20;
	/*
	 * a variable to keep record of the most right position
	 *
	 * plotting order of Legends for subpanels tree skeleton / tree label / tree
	 * label background piechart / other charts
	 */
	private float currentMostRightPosition = 0f; // current most right position
	/*
	 * canvas size/ svg canvas size
	 */
	private int canvas_width = 800, canvas_height = 600;
	/*
	 * font sizes ...
	 */
	private int default_font_size = 14, default_bootstrap_font_size = 10, default_branchlength_font_size = 10,
			default_tick_label_font_size = 8;
	private final String default_font_family = "Arial";

	/*
	 * some parameters for plots of datasets
	 */
	private final float space_between_datasets = 10, space_between_columns_within_a_dataset = 3, // for multicolumn plot
			space_between_treeskeleton_and_leaflabels = 5;
	/*
	 * a phylotree object; the root node
	 */
	private PhyloTree phylotree = null; // a phyloTree object
	private PhyloNode rootnode = null;
	private int treeID;
	private String projectID = "";

	/*
	 * svg objects, including a svg canvas, and several layers of svg elements;
	 * each layer is independent from others, meaning I can move/ zoom-in-out a
	 * certain layer without affecting others.
	 */
	private OMSVGDocument doc = OMSVGParser.currentDocument();
	private OMSVGSVGElement svg = doc.createSVGSVGElement();
	private OMSVGGElement layer_tree = doc.createSVGGElement(), // the main layer
			layer_tree_leaf_labels = doc.createSVGGElement(),
			layer_tree_skeleton = doc.createSVGGElement(), // the tree skeleton
			layer_tree_branchlength = doc.createSVGGElement(), 
			layer_tree_bootstrap = doc.createSVGGElement(),
			layer_tree_piecharts = doc.createSVGGElement(),
			layer_scale_branch_length = doc.createSVGGElement(), 
			layer_legend = doc.createSVGGElement(), 
			layer_tree_align_leaf_labels = doc.createSVGGElement(), // March 20, 2011;
			layer_tree_leaflabel_background = doc.createSVGGElement(); // march 21, 2011;
	private OMSVGRectElement markerrect = doc.createSVGRectElement(0, 0, 10, 10, 0, 0);
	private OMSVGTransform layer_tree_translate = svg.createSVGTransform(), layer_tree_scale = svg.createSVGTransform();
	private OMSVGDefsElement defs = doc.createSVGDefsElement(); // May 10, 2012
	/*
	 * manipulation of the svg_tree_layer, including zoom in/out, pan keep
	 * records of mouse positions
	 *
	 */
	private OMSVGPoint mouse_pos, // mouse position when mouse_down event happen; jan 14, 2011;
			rootXY = svg.createSVGPoint(), mouse_moved;
	private boolean dragging = false, // mouse left button down and drag
			resizable = false, // May 12, 2011
			mousedown = false, // May 12, 2011;
			resizing = false; // May 12, 2011
	private int mouse_client_x = 0, mouse_client_y = 0; // May 12, 2011
	private static float scale_factor = 0.9f; // scaling factor for mouse wheel events
	/**
	 * use the following to store positions for various text-labels NOTE: all
	 * IDs are internal ID
	 */
	private HashMap<String, APosition> BranchLengthPositions = new HashMap<String, APosition>(), LeafInternalID2NodePosisionts = new HashMap<String, APosition>(), // leaf internal id to positions
			BootstrapPositions = new HashMap<String, APosition>();

	/**
	 * decimal formater
	 */
	private final NumberFormat formatterDeci4 = NumberFormat.getFormat("#.####");
	//private final NumberFormat formatterScientific = NumberFormat.getScientificFormat();
	// a new Pies class that take cares of the piechartdata
	private Pies pie = new Pies();
	private TreeSkeleton treeskeleton = new TreeSkeleton();
	/**
	 * April 20, 2011; add a system to track changes in datasets and upadate
	 * legends
	 */
	private HashMap<Integer, LegendEntry> legendEntries = new HashMap<Integer, LegendEntry>(); // Here String is the unique ID = datasettype + legendentry name
	/**
	 * march 25, 2011; plot management system
	 */
	private HashMap<Integer, PlotBase> charts = new HashMap<Integer, PlotBase>();
	// NOTE: activePlotBase contains not only active PlotBase, but also inactive PlotBases; the status are defined by 'active'; April 5, 2011;
	private ArrayList<PlotBase> activeCharts = new ArrayList<PlotBase>();
	/**
	 * May 13, 2011; add the following variables to initiate svg canvas
	 * transformation/ translation infor
	 */
	private float svg_translate_x = 0, svg_translate_y = 0, svg_scale = 1f; // keep tracking of current scale (used for drawing tree scale);
	
	/**
	 * Tree db technical Id
	 */
	private int db_serialID = 0;
	/**
	 * May 20, 2011: move some variables from Legends and Branch length scale
	 */
	private final int legend_start_pos_x = 30, //
			legend_start_pos_y = 80, branchlength_scale_y1 = 70, default_legend_font_size = 12;
	private final float space_between_legends = 10f, space_between_legend_and_text = 5f, space_between_legend_items = 5f;
	/**
	 * ***********************************************
	 * May 14, 2012: Tooltips ************************************************
	 */
	private final InfoPanelWidget tooltipWidget = new InfoPanelWidget(true);
	private OMElement oeelement_mouse_over = null;
	private final int delayShowMiliSec = 500, delayHideMiliSec = 200;
	private int tooltipx = 0, tooltipy = 0;

	/**
	 * >>>> oct 25, 2013 : 
	 */
	private final int flexTableCellSpacing = 2;
	


	public PhyloTreeSVGViewer(PhyloTree newtree, String proID) {
		super();
		setPhyloTree(newtree, proID, null);
		initiate();
	}

	public PhyloTreeSVGViewer(PhyloTree newtree, String projectID, treePlotMode pmode) {
		super();
		this.plotmode = pmode;
		setPhyloTree(newtree, projectID, null);
		initiate();
	}

	public PhyloTreeSVGViewer(PhyloTree newtree, String projectID, treePlotMode pmode, HashMap<String, Float> canvasParam) {
		super();
		this.plotmode = pmode;
		setPhyloTree(newtree, projectID, canvasParam);
		initiate();
	}

	public PhyloTreeSVGViewer() {
		super();
		initiate();
	}

	public void setSessionID(String sid) {
		this.sessionid = sid;
	}

	public void setMouseWheelEnabled(boolean b) {
		this.bMouseWheelEnabled = b;
	}

	public boolean getMouseWheelEnabled() {
		return this.bMouseWheelEnabled;
	}

	/*
	 * May 12, 2011; add more mouse Handlers;
	 */
	@Override
	public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
		return addDomHandler(handler, MouseDownEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
		return addDomHandler(handler, MouseUpEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		return addDomHandler(handler, MouseOverEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		return addDomHandler(handler, MouseOutEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseWheelHandler(MouseWheelHandler handler) {
		return addDomHandler(handler, MouseWheelEvent.getType());
	}

	@Override
	public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
		return addDomHandler(handler, MouseMoveEvent.getType());
	}

	/*
	 * April 10, 2011; add border; April 19, 2011; deal with legend May 12, 2011
	 */
	private void initiate() {

		// -- March 31, 2013 --
		DOM.setStyleAttribute(this.getElement(), "boxShadow", "0px 0px 4px grey");
		//DOM.setStyleAttribute(this.getElement(), "boxShadow", "2px 2px 4px grey");
		DOM.setStyleAttribute(this.getElement(), "display", "inline-block");

		/*
		 * aseemble the main panel
		 */
		//this.setWidth(canvas_width + "px");
		//this.setHeight(canvas_height + "px");
		this.setPixelSize(this.canvas_width, this.canvas_height);
		this.getElement().appendChild(svg.getElement());

		/**
		 * ************************************************
		 * SVGs ------------------------------------------------ May 2, 2011;
		 * add header information for svg
		 * ************************************************
		 */
		svg.setAttribute("version", "1.1");
		svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
		svg.setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
		svg.setAttribute("xml:space", "preserve");

		/*
		 * set canvas size for svg
		 */
		svg.setViewBox(0, 0, this.canvas_width, this.canvas_height);
		svg.getStyle().setWidth(this.canvas_width, Unit.PX);
		svg.getStyle().setHeight(this.canvas_height, Unit.PX);

		/*
		 * >>>>>>>> add layers to layer_tree >>>>>>>>>
		 */
		// a invisible rect object to calculate the changes during zoom-in/ out of layer_tree
		// why this is necessary? Oct 30, 2011;
		this.markerrect.setAttribute("fill-opacity", Float.toString(0));
		this.layer_tree.appendChild(this.markerrect);

		// the tree skeleton is at the bottom, so add it first; and also apply certain attribute to this whole group
		this.layer_tree_skeleton.setId("layer_tree_skeleton");
		this.layer_tree.appendChild(this.layer_tree_skeleton);

		layer_tree_skeleton.setAttribute("stroke-width", Float.toString(default_line_width));
		layer_tree_skeleton.setAttribute("stroke-linecap", "round");

		// leaf label background ; march 21, 2011; add it before leaf label so the background will appear under the leaf labels
		layer_tree_leaflabel_background.setId("layer_tree_leaflabel_background");
		layer_tree.appendChild(layer_tree_leaflabel_background);

		// leaf label: font-family + font size
		this.layer_tree_leaf_labels.setId("layer_tree_leaf_labels");
		this.layer_tree_leaf_labels.getStyle().setFontSize(default_font_size, Unit.PX);
		this.layer_tree_leaf_labels.setAttribute("font-family", this.default_font_family);
		this.layer_tree_leaf_labels.setAttribute("font-style", this.leaf_font_italic ? "italic" : "normal");
		layer_tree.appendChild(layer_tree_leaf_labels);

		// align leaf label
		layer_tree_align_leaf_labels.setId("layer_tree_align_leaf_labels");
		layer_tree_align_leaf_labels.setAttribute("stroke", "grey");
		layer_tree_align_leaf_labels.setAttribute("stroke-width", "0.4");
		layer_tree_align_leaf_labels.setAttribute("stroke-dasharray", "5,5");
		layer_tree.appendChild(layer_tree_align_leaf_labels);

		// bootstrap layer: font-family + font size + italic 
		this.layer_tree_bootstrap.setId("layer_tree_bootstrap");
		this.layer_tree_bootstrap.getStyle().setFontSize(default_bootstrap_font_size, Unit.PX);
		this.layer_tree_bootstrap.setAttribute("font-family", this.default_font_family);
		this.layer_tree_bootstrap.setAttribute("font-style", this.bootStrapFontItalic ? "italic" : "normal");
		this.layer_tree.appendChild(layer_tree_bootstrap);

		// branch length layer: font-family + font size
		this.layer_tree_branchlength.setId("layer_tree_branchlength");
		this.layer_tree_branchlength.getStyle().setFontSize(default_branchlength_font_size, Unit.PX);
		this.layer_tree_branchlength.setAttribute("font-family", this.default_font_family);
		this.layer_tree_branchlength.setAttribute("font-style", this.branchLengthFontItalic ? "italic" : "normal");
		this.layer_tree.appendChild(layer_tree_branchlength);

		// piecharts layer
		this.layer_tree_piecharts.setId("layer_tree_piecharts");
		this.layer_tree.appendChild(layer_tree_piecharts);

		/*
		 * add layers to svg; NOTE: layers added directly to 'svg' will not be
		 * moved on screen
		 */
		// layer tree
		layer_tree.setId("layer_tree");
		svg.appendChild(layer_tree);

		// scale and translate for layertree
		this.layer_tree_translate.setTranslate(0f, 0f);
		this.layer_tree.getTransform().getBaseVal().appendItem(layer_tree_translate);

		this.layer_tree_scale.setScale(1f, 1f);
		this.layer_tree.getTransform().getBaseVal().appendItem(layer_tree_scale);

		// layer_scale branch length
		layer_scale_branch_length.setId("layer_branch_length_scale");
		svg.appendChild(layer_scale_branch_length);

		// layer legend
		layer_legend.setId("layer_legend");
		this.layer_legend.setAttribute("font-family", this.default_font_family);
		svg.appendChild(layer_legend);

		/**
		 * *********************************************
		 * May 10, 2012; defs *********************************************
		 */
		initiate_defs();
		svg.appendChild(defs);

		/**
		 * ******************************************************************
		 * May 12, 2011; use native mouse event handler in stead of on svg
		 * canvas
		 *
		 * 1. if mouse is up, check if mouse is at the margin and change cursor
		 * accordingly 2. if mouse is down, check if resizable/ dragging and
		 * change cursor accordingly
		 * *******************************************************************
		 */
		this.addMouseMoveHandler(new MouseMoveHandler() {

			@Override
			public void onMouseMove(MouseMoveEvent event) {
				if (!mousedown) {
					resizable = isCursorResize(event.getX(), event.getY(), getOffsetWidth(), getOffsetHeight());
					if (!resizable) {
						DOM.setStyleAttribute(getElement(), "cursor", "DEFAULT");
					}
				} else {
					if (dragging) {
						mouse_moved = getLocalCoordinates(event).substract(mouse_pos);
						OMSVGTransform svg_translate = svg.createSVGTransform();
						svg_translate.setTranslate(mouse_moved.getX(), mouse_moved.getY());
						layer_tree.getTransform().getBaseVal().appendItem(svg_translate);

						// May 13, 2011;
						svg_translate_x += mouse_moved.getX();
						svg_translate_y += mouse_moved.getY();

					} else { // resize
						resizing = true;

						int cx = event.getClientX();
						int cy = event.getClientY();

						int cwidth = canvas_width + (cx - mouse_client_x);
						int cheight = canvas_height + (cy - mouse_client_y);

						setWidth(cwidth + "px");
						canvas_width = cwidth;
						setHeight(cheight + "px");
						canvas_height = cheight;

						mouse_client_x = cx;
						mouse_client_y = cy;

					}
				} // if mouse button is down

				event.stopPropagation();
				event.preventDefault();
			}// on mouse move
		}); 

		// zoom in/out double click ....
//		this.addDomHandler(new DoubleClickHandler() { 
//			@Override
//			public void onDoubleClick(final DoubleClickEvent event) {
//				// automatic zoom ..
//				if(event.isAltKeyDown()) {
//					zoom("restore") ;
//					event.stopPropagation();
//					return;
//				}
//
//				switch (event.getNativeButton()) {
//				case NativeEvent.BUTTON_LEFT :
//					zoom("in") ;
//					break;
//				case NativeEvent.BUTTON_RIGHT :
//					zoom("out") ;
//					break;
//				}
//				event.stopPropagation();
//			} 
//		},   DoubleClickEvent.getType());
		
		 
		
		/**	
		 * march 31, 2013; only deal with mouse left button --
		 */
		this.addMouseDownHandler(new MouseDownHandler() {

			@Override
			public void onMouseDown(MouseDownEvent event) { 
				// get current position
				final OMSVGPoint pos = getLocalCoordinates(event);
				// Ctrl : selection ... defined when building the nodes 
				if(event.getNativeEvent().getCtrlKey()) { 
					event.stopPropagation();
					return;
				}
				// else is for drag n' drop
				int button = event.getNativeEvent().getButton();
				if (button == NativeEvent.BUTTON_LEFT) {
					mousedown = true;
					DOM.setCapture(getElement()); // 

					// get current position
					mouse_pos = pos;
					switch (event.getNativeButton()) {
						case NativeEvent.BUTTON_LEFT :
							if (!resizable) {
								dragging = true;
								DOM.setStyleAttribute(getElement(), "cursor", "MOVE");
							} else {
								dragging = false;
								mouse_client_x = event.getClientX();
								mouse_client_y = event.getClientY();
							}
							break;
					}
					event.preventDefault();
					event.stopPropagation();
				}
			}
		});

		this.addMouseUpHandler(new MouseUpHandler() {

			@Override
			public void onMouseUp(MouseUpEvent event) {
 
				DOM.setStyleAttribute(getElement(), "cursor", "DEFAULT");
				DOM.releaseCapture(getElement()); // 

				/*
				 * here at each mouse-up event, i merge every transform items
				 * into one matrix by using the following command!! Jan 25, 2011
				 */
				layer_tree.getTransform().getBaseVal().consolidate(); // GOOD!!

				mousedown = false;

				if (dragging) {
					float tx = layer_tree.getCTM().getE();
					float ty = layer_tree.getCTM().getF();
					float cx = layer_tree.getCTM().getA();
					float cy = layer_tree.getCTM().getD();

					// Masked on Apr 23, 2012
					//updateTreeCanvasInfoOnServer("svgTranslateX", tx / cx);
					//updateTreeCanvasInfoOnServer("svgTranslateY", ty / cy);

					// Apr 23, 2012
					// 1. keep tracking rootxy in a hash
					hmPlotMode2TranslateX.put(plotmode, tx / cx);
					hmPlotMode2TranslateY.put(plotmode, ty / cy);

					// 2. keep tracking rootxy on the server
					updateTreeCanvasInfoOnServer(sTranslateX + plotmode.toString(), tx / cx);
					updateTreeCanvasInfoOnServer(sTranslateY + plotmode.toString(), ty / cy);

				} else {
					updateTreeCanvasInfoOnServer("canvasWidth", canvas_width);
					updateTreeCanvasInfoOnServer("canvasHeight", canvas_height);
				}

				dragging = false;

				/*
				 * the following section is moved from onmouse moving so that
				 * the canvas will not be resized on every mouse move; otherwise
				 * the performance will be decreased on FF Oct 28, 2011;
				 */
				if (resizing) {
					resizing = false;
					svg.setViewBox(0, 0, canvas_width, canvas_height);
					svg.getStyle().setWidth(canvas_width, Unit.PX);
					svg.getStyle().setHeight(canvas_height, Unit.PX);
				}

				event.preventDefault();
				event.stopPropagation();
			}
		}); // May 12, 2011
 
 
		
		/*
		 * Jan 16, 2012 -- zoom in/out at the position of the mouse --
		 */
		this.addMouseWheelHandler(new MouseWheelHandler() {

			@Override
			public void onMouseWheel(MouseWheelEvent event) {

				// Dec 6, 2011 --
				if (bMouseWheelEnabled) {
					float scale_local = 0;
					if (event.getDeltaY() >= 1) {
						scale_local = scale_factor; // zoom out/ mouse wheel down
					} else if (event.getDeltaY() <= -1) {
						scale_local = 1 / scale_factor; // zoom in/ mouse wheel up
					}

					if (scale_local != 0) {
						/*
						 * do transforms, including scaling and transformation
						 */
						float x = event.getClientX();
						float y = event.getClientY();

						// keep tracking mouse poisition before the event takes effects
						OMSVGPoint xy = getLocalCoordinates(x, y);

						// do transform
						OMSVGTransform scale = svg.createSVGTransform();
						scale.setScale(scale_local, scale_local);
						layer_tree.getTransform().getBaseVal().appendItem(scale);

						// calculate the mouse position on current layer_tree
						OMSVGPoint xy2 = getLocalCoordinates(x, y);

						// move the layer_tree back to make xy and xy2 overlap!!
						OMSVGTransform translate = svg.createSVGTransform();
						translate.setTranslate(xy2.getX() - xy.getX(), xy2.getY() - xy.getY());
						layer_tree.getTransform().getBaseVal().appendItem(translate);

						// consolidate all the transforms associated with layer_tree
						layer_tree.getTransform().getBaseVal().consolidate(); // GOOD!!

						/*
						 * keep trakcing the changes
						 */
						// global svg_scale
						svg_scale *= scale_local;
						updateTreeCanvasInfoOnServer("svgScale", svg_scale); // this is also important

						// apr 24, 2012
						updateTranslateXY4layerTree();

						// make branch length scale according to the global svg_scale
						makeBranchLengthScale(); // Jan 25, 2011;
					}
					event.preventDefault();
					event.stopPropagation();
				} // if mosue wheel is allowed
			}// onMouseWheel
		}); // May 12, 2011; Jan 16, 2012
	}// and mouse wheel handler to current canvas; Jan 16, 2012

	/*
	 * May 13, 2011; the same function as in SVGViewerAndManagementWidget Oct
	 * 28, 2011; the session ID will be got from class SVGViwerMan, not from
	 * cookie; because for user may not choose to save sessionID to cookie
	 *
	 * Nov 07, 2011; all changes to "demos" will not be sent to server
	 */
	private void updateTreeCanvasInfoOnServer(String key, float value) {
		if (sessionid != null && !sessionid.isEmpty() && !this.projectID.equals(demoproject)) {
			if (getDBserialID() > 0) {
				MC.rpc.updateTreeCanvasInfoOnserver(sessionid, getDBserialID(), key, value, new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						//
					}

					@Override
					public void onSuccess(Boolean result) {
						// do something
					}
				});
			}
		}
	}

	/*
	 * May 12, 2011: check mouse position and set resize @param mouse_x
	 * position_x of mouse in current widget @param mouse_y position_y of mouse
	 * in current widget @param offsetWidth width of current widget @param
	 * offsetHeight height of current widget
	 *
	 * margin = 20px
	 */
	private boolean isCursorResize(int mouse_x, int mouse_y, int offsetWidth, int offsetHeight) {
		final int margin = 20;
		boolean edge_r = (mouse_x >= (offsetWidth - margin) && mouse_x < offsetWidth) ? true : false;
		boolean edge_b = (mouse_y >= (offsetHeight - margin) && mouse_y < offsetHeight) ? true : false;

		if (edge_r && edge_b) {
			DOM.setStyleAttribute(getElement(), "cursor", "se-resize");
		}
		return edge_r && edge_b;
	}

	// Apr 24, 2012
	private void updateTranslateXY4layerTree() {
		// translate x,y; only do this after consolidate all transforms
		float tx = layer_tree.getCTM().getE();
		float ty = layer_tree.getCTM().getF();
		float cx = layer_tree.getCTM().getA();
		float cy = layer_tree.getCTM().getD();

		float transX = tx / cx;
		float transY = ty / cy;

		hmPlotMode2TranslateX.put(plotmode, transX);
		hmPlotMode2TranslateY.put(plotmode, transY);

		updateTreeCanvasInfoOnServer(sTranslateX + plotmode.toString(), transX);
		updateTreeCanvasInfoOnServer(sTranslateY + plotmode.toString(), transY);
	}//

	/*
	 * >>>>>>>> draw phylotree using SVG >>>>>>>>>
	 */
	public void drawPhyloTreeUsingSVG(){
		drawPhyloTreeUsingSVG( false );
	}
	public void drawPhyloTreeUsingSVG( boolean bReroot ) {

		System.err.println("---------- draw phylotree starts ---------");
		this.rootnode = this.phylotree.getRootNode();
		
		System.err.println("\tcurrent rootnode is : " + rootnode.getInternalID() );

		/*
		 * recalculate some global variables
		 */
		recalculate_some_global_variables();

		/*
		 * make tree skeleton
		 */
		System.err.println("\tmake tree skeleton ... ");
		treeskeleton.makeTreeSkeleton();
		treeskeleton.makeLeafLabels();
		treeskeleton.makeLeafLabelBackground();

		/*
		 * plot layer_scale_branch_length; if branch length is available and
		 * used
		 */
		makeBranchLengthScale();

		/*
		 * make or update datasets including piechart (implemented) (not
		 * implemented) heatmap, barchart, and others
		 */
		pie.makePies( bReroot );

		/*
		 * re-draw all active
		 */
		this.updateAllCharts();

		/*
		 * update / replot legends
		 */
		this.updateAllLegends(); // May 24, 2011
		
		/**
		 * fit canvas to plot width and height; Oct 25, 2013 
		 * somehow this does not work
		 */
		// this.fitCanvas2Plot();
	} // drawPhyloTreeUsingSVG

	/*
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>> public methods for the host class
	 * >>>>>>>>>>>>>>>>>>>>>>>
	 */
	// Nov 07, 2011; tree branch linewidth
	public float getTreeBranchLineWidth() {
		return this.default_line_width;
	}

	public float setTreeBranchLineWidth(float newlinewidth) {
		if (newlinewidth > 0 && newlinewidth != this.default_line_width) {
			default_line_width = newlinewidth;
			layer_tree_skeleton.setAttribute("stroke-width", Float.toString(default_line_width));

			this.updateTreeCanvasInfoOnServer("treeBranchLineWidth", default_line_width);
		}
		return this.default_line_width;
	}

	public float increaseTreeBranchLineWidth() {
		return setTreeBranchLineWidth(default_line_width * this.scale_tree_branch_linewidth);
	}

	public float decreaseTreeBranchLineWidth() {
		return setTreeBranchLineWidth(default_line_width / this.scale_tree_branch_linewidth);
	}

	public void setLeafFontItalic(boolean italic) {
		if (this.leaf_font_italic != italic) {
			this.leaf_font_italic = italic;
			layer_tree_leaf_labels.setAttribute("font-style", this.leaf_font_italic ? "italic" : "normal");

			this.updateTreeCanvasInfoOnServer("leafFontItalic", leaf_font_italic ? 1 : 0);
		}
	} // Nov 07, 2011;

	public boolean getLeafFontItalic() {
		return this.leaf_font_italic;
	}

	/*
	 * >>>>>>>>>>>>>> March 25, 2011; the plotbase management system
	 * >>>>>>>>>>>>>>>> April 5, 2011; add 'chart_plots_enabled' Oct 28, 2011;
	 * add param: active
	 */
	public void addColorStripData(int datasetID, ColorDataContainerParser coldatCon, boolean active) {
		boolean replot = active;
		/*
		 * if dataset exists; repalce it NOTE: datasetID2plotbase and
		 * activePlotBase contain the same set of PlotBase classes; April 5,
		 * 2011;
		 */
		if (this.charts.containsKey(datasetID)) {
			ColorStrips colstrip = (ColorStrips) charts.get(datasetID);
			colstrip.setColorData(datasetID, coldatCon);

			if (!activeCharts.contains(colstrip)) {
				this.activeCharts.add(colstrip);
			}
			colstrip.setActive(true);

			replot = true;
		} else {
			ColorStrips colstrip = new ColorStrips(datasetID, coldatCon);
			colstrip.setColorData(datasetID, coldatCon);
			this.charts.put(datasetID, colstrip);
			colstrip.setActive(active);

			// if active; Oct 28, 2011;
			if (active) {
				colstrip.setActive(true);
				this.activeCharts.add(colstrip);
			}
		}

		if (replot) {
			this.updateAllCharts();
		}

		/*
		 * May 24, 2011; make legendentry; delete old ones if exists
		 */
		this.makeLegendEntryAndUpdateAllLegend(coldatCon.isShowLegends(),
				coldatCon.getLegendColors(), 
				coldatCon.getLegendTexts(),
				datasetID,
				coldatCon.getTitle(),
				coldatCon.getLegendstyle(), TreeDecoType.CHARTS, replot);
	}

	public void addHeatMapData(int datasetID, HeatmapMatrixDataContainer hmData, boolean active) {

		boolean replot = active;
		if (this.charts.containsKey(datasetID)) { 
			HeatMap heatMap = (HeatMap) charts.get(datasetID);
			heatMap.setNumericDataSet(datasetID, hmData);
			heatMap.setActive(true);
			replot = true;
		} else { 
			// 1. add dataset 
			HeatMap heatMap = new HeatMap(datasetID, hmData); 
			heatMap.setActive(active);
			this.charts.put(datasetID, heatMap);
			if (active) {
				this.activeCharts.add(heatMap);
			}
		}
		if (replot) {
			this.updateAllCharts();
		}

		/**make legendentry; delete old ones if exists
		 */
		this.makeLegendEntryAndUpdateAllLegend(hmData.isShowLegends(), hmData.getLegendColors(), hmData.getLegendTexts(), datasetID, hmData.getTitle(), hmData.getLegendstyle(),
				TreeDecoType.CHARTS, replot);
	}
	
	// Oct 28, 2011; add new param: active
	public void addBarplotData(int datasetID, NumericMatrixDataContainer datCon, boolean active) {
		/*
		 * NOTE: datasetID2plotbase and activePlotBase contain the same set of
		 * PlotBase classes; April 5, 2011;
		 */
		boolean replot = active;
		if (this.charts.containsKey(datasetID)) {
			Bars barplot = (Bars) charts.get(datasetID);
			barplot.setNumericDataSet(datasetID, datCon);
			barplot.setActive(true);

			replot = true;
		} else {

			// Oct 28, 2011;
			// 1. add dataset
			Bars barplot = new Bars(datasetID, datCon);
			barplot.setNumericDataSet(datasetID, datCon);
			barplot.setActive(active);
			this.charts.put(datasetID, barplot);

			if (active) {
				this.activeCharts.add(barplot);
			}
		}

		if (replot) {
			this.updateAllCharts();
		}

		/**make legendentry; delete old ones if exists
		 */
		this.makeLegendEntryAndUpdateAllLegend(datCon.isShowLegends(), datCon.getLegendColors(), datCon.getLegendTexts(), datasetID, datCon.getTitle(), datCon.getLegendstyle(),
				TreeDecoType.CHARTS, replot);
	}

	public void deleteChartByID(int datasetID) {
		if (this.charts.containsKey(datasetID)) {
			PlotBase pb = charts.get(datasetID);
			this.charts.remove(datasetID); // remove it from the hash;
			// if is in the active list: this is almost always true
			this.activeCharts.remove(this.activeCharts.indexOf(pb)); // also remove it from the active list

			// May 24, 2011; also remove from the legend
			LegendEntry le = legendEntries.remove( datasetID);
			if (le != null && le.getShowLegends()) {
				updateAllLegends();
			}

			// if active, set it as inactive and then replot 
			if (pb.getActive()) {
				pb.setActive(false);
				pb.makeOrUpdatePlot(); // this is vital
				this.updateAllCharts();
			}
		}
	}

	// 
	public void setActiveChartByID(int datasetID, boolean active) {
		//
		System.out.println("  --> try to activate : " + datasetID + "; existing? " + this.charts.containsKey(datasetID));
		if (this.charts.containsKey(datasetID)) {
			PlotBase pb = charts.get(datasetID);

			// May 1, 2012; fix a bug here!!
			if (active && !this.activeCharts.contains(pb)) {
				this.activeCharts.add(pb);
			}

			pb.setActive(active);
			this.updateAllCharts();

			// update legends; May 24, 2011
			LegendEntry le = legendEntries.get(  datasetID);
			if (le != null && le.getShowLegends()) {
				updateAllLegends();
			}
		}
	}

	public boolean setDisableAllCharts(boolean b) {
		if (!this.chart_plots_enabled != b) {
			this.chart_plots_enabled = !b;
			this.updateAllCharts();

			// update legend
			updateAllLegends();

		}
		return this.chart_plots_enabled;
	} // April 5, 2011;

	public String getOriginalUserInputAsStringByID(int datasetID) {
		return this.charts.containsKey(datasetID) ? this.charts.get(datasetID).getOriginalUserInputAsString() : "";
	} // April 5, 2011;

	/*
	 * swap two Charts by datasetID; if both are active, redo-plot
	 */
	public void swapChartsByDatasetIDs(int dataset1, int dataset2) {
		if (this.charts.containsKey(dataset1) && this.charts.containsKey(dataset2)) {
			PlotBase pb1 = charts.get(dataset1);
			PlotBase pb2 = charts.get(dataset2);
			Collections.swap(this.activeCharts, this.activeCharts.indexOf(pb1), this.activeCharts.indexOf(pb2));
			if (pb1.getActive() && pb2.getActive()) {
				this.updateAllCharts();

				updateAllLegends(); // May 24, 2011
			}
		}
	}// April 5, 2011;
	
	/**
	 * simple public methods; set and get pairs
	 */
	public String toXMLString() {
		return svg.getMarkup(); // 0.5.1; lib-gwt-svg
	}

	public Document toDoc() {
		return doc.getDocument();
	}

	public void setCicularStartAngle(int newStartAngle) {
		newStartAngle %= 360; // mod
		if (newStartAngle < 0) {
			newStartAngle += 360;
		}
		if (newStartAngle != this.angle_start) {
			angle_start = newStartAngle;
			this.drawPhyloTreeUsingSVG();

			updateTreeCanvasInfoOnServer("circularStartAngle", angle_start);
		}
	}

	public void setCicularStartAngle(int newStartAngle, boolean bReplot) {
		newStartAngle %= 360; // mod
		if (newStartAngle < 0) {
			newStartAngle += 360;
		}
		if (newStartAngle != this.angle_start) {
			angle_start = newStartAngle;

			if (bReplot) {
				this.drawPhyloTreeUsingSVG();
			}
			updateTreeCanvasInfoOnServer("circularStartAngle", angle_start);
		}
	}

	// revised April 23, 2012: let each plot mode has its own px_per_width
	public float setPxPerWidth(float px_per_width) {
		if (this.hmPlotMode2pxPerWidth.containsKey(this.plotmode) && this.hmPlotMode2pxPerWidth.get(this.plotmode) == px_per_width) {
			// do nothing
		} else {
			// keep tracking the pxPerWidth in a hash
			this.hmPlotMode2pxPerWidth.put(this.plotmode, px_per_width);
			// replot whole tree
			this.drawPhyloTreeUsingSVG();
			// upload pxPerWidthByMode on server
			updateTreeCanvasInfoOnServer(this.sPxPerWidth + this.plotmode.toString(), px_per_width);
		}

		return this.hmPlotMode2pxPerWidth.get(this.plotmode);
	}// April 23, 2012

	public void setPxPerHeight(float px_per_height) {
		if (this.pxPerHeight != px_per_height) {
			this.pxPerHeight = px_per_height;
			this.drawPhyloTreeUsingSVG();

			updateTreeCanvasInfoOnServer("verticalScale", pxPerHeight);
		}
	}

	public void setTreePlotMode(treePlotMode newplotmode) {
		if (!newplotmode.equals(this.plotmode)) {

			/*
			 * === April 23, 2012 === set translateXY for each plotmode
			 */
			float txold = hmPlotMode2TranslateX.containsKey(plotmode) ? hmPlotMode2TranslateX.get(plotmode) : 0;
			float txnew = hmPlotMode2TranslateX.containsKey(newplotmode) ? hmPlotMode2TranslateX.get(newplotmode) : 0;
			float tyold = hmPlotMode2TranslateY.containsKey(plotmode) ? hmPlotMode2TranslateY.get(plotmode) : 0;
			float tynew = hmPlotMode2TranslateY.containsKey(newplotmode) ? hmPlotMode2TranslateY.get(newplotmode) : 0;

			float tx = txnew - txold;
			float ty = tynew - tyold;
			if (tx != 0 || ty != 0) {
				OMSVGTransform svg_translate = svg.createSVGTransform();
				svg_translate.setTranslate(txnew - txold, tynew - tyold);
				layer_tree.getTransform().getBaseVal().appendItem(svg_translate);
				layer_tree.getTransform().getBaseVal().consolidate();
			}
			// == << == Apr 23, 2012 == << == 

			// set plotmode and replot the whole tree
			this.plotmode = newplotmode;
			this.drawPhyloTreeUsingSVG();

			// update server
			float mode = 1f;
			if (plotmode.equals(treePlotMode.RECT_CLADOGRAM)) {
				mode = 1f;
			} else if (plotmode.equals(treePlotMode.RECT_PHYLOGRAM)) {
				mode = 2f;
			} else if (plotmode.equals(treePlotMode.SLANTED_CLADOGRAM_NORMAL)) {
				mode = 3f;
			} else if (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM)) {
				mode = 4f;
			} else if (plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
				mode = 5f;
			}

			updateTreeCanvasInfoOnServer("plotMode", mode); // May 14
		}
	}

	public boolean setCircularModeClockwise(boolean clockwise) {
		if (circular_tree_plot_in_clockwise_mode != clockwise && (this.plotmode == treePlotMode.CIRCULAR_CLADOGRAM || this.plotmode == treePlotMode.CIRCULAR_PHYLOGRAM)) {
			circular_tree_plot_in_clockwise_mode = clockwise;
			this.drawPhyloTreeUsingSVG();

			updateTreeCanvasInfoOnServer("circularClockWise", circular_tree_plot_in_clockwise_mode ? 1f : 0);// May 14, 2011
		}
		return this.circular_tree_plot_in_clockwise_mode;
	} // April 14, 2011;

	public boolean getCircularModeClockwise() {
		return this.circular_tree_plot_in_clockwise_mode;
	} // April 14, 2011;

	public void rotateCicularTree(int angle) {
		this.angle_start += angle;
		if (this.angle_start > 360) {
			this.angle_start -= 360;
		} else if (this.angle_start < 0) {
			this.angle_start += 360;
		}

		this.drawPhyloTreeUsingSVG();
		updateTreeCanvasInfoOnServer("circularStartAngle", angle_start); // May 14, 2011
	}

	public float setCicularAngleSpan(int newspan) {
		if (newspan <= max_angle_span && newspan >= this.min_angle_span) {
			if (newspan != this.angle_span) {

				this.setCicularStartAngle(
						this.getCircularModeClockwise()
								? (int) (this.getCicularStartAngle() - (this.angle_span - newspan) / 2)
								: (int) (this.getCicularStartAngle() + (this.angle_span - newspan) / 2), false); // don't replot yet

				this.angle_span = newspan;
				this.drawPhyloTreeUsingSVG();

				updateTreeCanvasInfoOnServer("circularAngleSpan", angle_span); // May 14, 2011
			}
		}
		return this.angle_span;
	}// April 7, 2011

	public void ResetCicularTreeAngleSpan() {

	}

	public float getSVGZoomScale() {
		return svg_scale;
	}

	/*
	 * -- Apr 23, 2012; when restore, set translate XY to the values in
	 * hmTranslateXY
	 */
	public void zoom(String zoomtype) {
		OMSVGTransform scale = svg.createSVGTransform();
		if (zoomtype.equalsIgnoreCase("restore")) {
			layer_tree.getTransform().getBaseVal().clear();
			svg_scale = 1;

			// Apr 23, 2012 --
			OMSVGTransform svg_translate = svg.createSVGTransform();
			svg_translate.setTranslate(this.hmPlotMode2TranslateX.containsKey(plotmode) ? this.hmPlotMode2TranslateX.get(plotmode) : 0,
					this.hmPlotMode2TranslateY.containsKey(plotmode) ? this.hmPlotMode2TranslateY.get(plotmode) : 0);
			layer_tree.getTransform().getBaseVal().appendItem(svg_translate);
			layer_tree.getTransform().getBaseVal().consolidate();
			// <<< Apr 23, 2012 <<<

		} else if (zoomtype.equalsIgnoreCase("in")) {
			scale.setScale(1 / scale_factor, 1 / scale_factor);
			layer_tree.getTransform().getBaseVal().appendItem(scale);
			layer_tree.getTransform().getBaseVal().consolidate();
			svg_scale /= scale_factor;
		} else if (zoomtype.equalsIgnoreCase("out")) {
			scale.setScale(scale_factor, scale_factor);
			layer_tree.getTransform().getBaseVal().appendItem(scale);
			layer_tree.getTransform().getBaseVal().consolidate();
			svg_scale *= scale_factor;
		}
		makeBranchLengthScale();

		updateTreeCanvasInfoOnServer("svgScale", svg_scale); // May 14, 2011
		/*
		 * == Apr 24, 2012 == recalculate translatedXY after zoomin/out
		 */
		updateTranslateXY4layerTree();

	}// April 7, 2011; zoom-in/out/restore

	public boolean setShowBootstrapScores(boolean b) {
		if (this.showBootStrap != b) {
			this.showBootStrap = b;
			if (this.showBootStrap == false) {
				this.layer_tree_bootstrap.getStyle().setVisibility(Visibility.HIDDEN);
				this.layer_tree_bootstrap.getStyle().setDisplay(Display.NONE);
			} else {
				this.layer_tree_bootstrap.getStyle().setVisibility(Visibility.VISIBLE);
				this.layer_tree_bootstrap.getStyle().setDisplay(Display.INLINE);
			}

			// server
			updateTreeCanvasInfoOnServer("showBootstrap", showBootStrap ? 1f : 0f); // May 14, 2011
		}
		return this.showBootStrap;
	} // April 7, 2011;

	public boolean setShowTreeBranchLengths(boolean b) {
		//if (this.showBrachLength != b) {
		this.showBrachLength = b;
		if (this.showBrachLength == false) {
			this.layer_tree_branchlength.getStyle().setVisibility(Visibility.HIDDEN);
			this.layer_tree_branchlength.getStyle().setDisplay(Display.NONE);
		} else {
			this.layer_tree_branchlength.getStyle().setVisibility(Visibility.VISIBLE);
			this.layer_tree_branchlength.getStyle().setDisplay(Display.INLINE);
		}

		updateTreeCanvasInfoOnServer("showBranchlength", showBrachLength ? 1f : 0f);
		//}
		return this.showBrachLength;
	}// April 7, 2011

	public boolean setShowTreeLeafLabels(boolean b) {
		if (this.showNodeLabel != b) {
			this.showNodeLabel = b;
			if (this.showNodeLabel == false) {
				this.layer_tree_leaf_labels.getStyle().setVisibility(Visibility.HIDDEN);
				this.layer_tree_leaf_labels.getStyle().setDisplay(Display.NONE);
			} else {
				this.layer_tree_leaf_labels.getStyle().setVisibility(Visibility.VISIBLE);
				this.layer_tree_leaf_labels.getStyle().setDisplay(Display.INLINE);
			}
		}

		updateTreeCanvasInfoOnServer("showNodeLabel", showNodeLabel ? 1f : 0f);

		return this.showNodeLabel;
	}// April 7, 2011;

	public float getCicularStartAngle() {
		return this.angle_start;
	}

	public float getCicularSpan() {
		return this.angle_span;
	}

	public float getCicularAngleSpan() {
		return this.angle_span;
	}

	public int getCurrentFontSize() {
		return this.default_font_size;
	}

	public int getDefautFontSize() {
		return this.default_font_size;
	}

	public treePlotMode getTreePlotMode() {
		return plotmode;
	}

	public float getPxPerHeight() {
		return this.pxPerHeight;
	}

	public float getPxPerWidth() {
		return this.hmPlotMode2pxPerWidth.containsKey(this.plotmode) ? this.hmPlotMode2pxPerWidth.get(this.plotmode) : this.pxPerWidthDefault;
	}

	public boolean hasLeafLabel() {
		return true;
	}

	public boolean getShowLeafLable() {
		return this.showNodeLabel && this.hasLeafLabel();
	}

	public boolean hasBootStrap() {
		return this.phylotree.hasBootstrapScores();
	}

	public boolean getShowBootStrapScores() {
		return this.showBootStrap && this.hasBootStrap(); // April 14, 2011
	}

	public boolean hasBranchLength() {
		return this.phylotree.hasBranchlength();
	}

	public boolean getShowBranchLength() {
		return this.showBrachLength && this.hasBranchLength();
	}

	public PhyloTree getPhyloTree() {
		return this.phylotree;
	}

	public int getFontSize() {
		return this.default_font_size;
	}

	/*
	 * May 13, 2011; add canvasparam parameter Apr 23, 2012; add mroe canvas
	 * parameters
	 */
	public final void setPhyloTree(PhyloTree newtree, String proID, HashMap<String, Float> canvasParam) {
		/*
		 * May 13, 2011;
		 */
		if (canvasParam == null) {
			canvasParam = new HashMap<String, Float>();
		}
		// set parameters
		for (Map.Entry<String, Float> entry : canvasParam.entrySet()) {
			String key = entry.getKey();
			float value = entry.getValue();

			// canvas sizes
			if (key.equalsIgnoreCase("canvasWidth")) {
				this.canvas_width = (int) value;
			} else if (key.equalsIgnoreCase("canvasHeight")) {
				this.canvas_height = (int) value;

				// svg translate/ transform
			} else if (key.equalsIgnoreCase("svgScale")) { // always do scale first
				this.svg_scale = value;
			} else if (key.equalsIgnoreCase("svgTranslateX")) {
				this.svg_translate_x = value;
			} else if (key.equalsIgnoreCase("svgTranslateY")) {
				this.svg_translate_y = value;

				// horiz and vertical scales
			} else if (key.equalsIgnoreCase("horizScale")) {
				this.pxPerWidthDefault = value;
			} else if (key.equalsIgnoreCase("verticalScale")) {
				this.pxPerHeight = value;

				// fonts
			} else if (key.equalsIgnoreCase("fontSize")) {
				this.default_font_size = (int) value;

				// plotmode
			} else if (key.equalsIgnoreCase("plotMode")) {
				int plotmodeserial = (int) value;

				if (plotmodeserial == 1) {
					this.plotmode = treePlotMode.RECT_CLADOGRAM;
				} else if (plotmodeserial == 2) {
					this.plotmode = treePlotMode.RECT_PHYLOGRAM;
				} else if (plotmodeserial == 3) {
					this.plotmode = treePlotMode.SLANTED_CLADOGRAM_NORMAL;
				} else if (plotmodeserial == 4) {
					this.plotmode = treePlotMode.CIRCULAR_CLADOGRAM;
				} else if (plotmodeserial == 5) {
					this.plotmode = treePlotMode.CIRCULAR_PHYLOGRAM;
				}
			} else if (key.equalsIgnoreCase("showBootstrap")) {
				this.showBootStrap = (value > 0) ? true : false;
			} else if (key.equalsIgnoreCase("showBranchlength")) {
				this.showBrachLength = (value > 0) ? true : false;
			} else if (key.equalsIgnoreCase("showNodeLabel")) {
				this.showNodeLabel = (value > 0) ? true : false;
			} else if (key.equalsIgnoreCase("alignLeafLabel")) {
				this.getTreeSkeleton().alignLeafLabels = (value > 0) ? true : false;
			} else if (key.equalsIgnoreCase("circularClockWise")) {
				this.circular_tree_plot_in_clockwise_mode = (value > 0) ? true : false;
			} else if (key.equalsIgnoreCase("circularStartAngle")) {
				this.angle_start = value;
			} else if (key.equalsIgnoreCase("circularAngleSpan")) {
				this.angle_span = value;
			} else if (key.equalsIgnoreCase("treeBranchLineWidth")) {
				this.default_line_width = value;
			} else if (key.equalsIgnoreCase("leafFontItalic")) {
				this.leaf_font_italic = value > 0 ? true : false;
			} /*
				* ==== April 23, 2012 ==== -- now deffierent plot modes have their
				* own pxPerWidth --
				*/else if (key.equalsIgnoreCase(this.sPxPerWidth + treePlotMode.RECT_CLADOGRAM.toString())) {
				this.hmPlotMode2pxPerWidth.put(treePlotMode.RECT_CLADOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sPxPerWidth + treePlotMode.RECT_PHYLOGRAM.toString())) {
				this.hmPlotMode2pxPerWidth.put(treePlotMode.RECT_PHYLOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sPxPerWidth + treePlotMode.SLANTED_CLADOGRAM_NORMAL.toString())) {
				this.hmPlotMode2pxPerWidth.put(treePlotMode.SLANTED_CLADOGRAM_NORMAL, value);
			} else if (key.equalsIgnoreCase(this.sPxPerWidth + treePlotMode.CIRCULAR_CLADOGRAM.toString())) {
				this.hmPlotMode2pxPerWidth.put(treePlotMode.CIRCULAR_CLADOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sPxPerWidth + treePlotMode.CIRCULAR_PHYLOGRAM.toString())) {
				this.hmPlotMode2pxPerWidth.put(treePlotMode.CIRCULAR_PHYLOGRAM, value);
			} /*
				* ===== Arpil 23, 2012 ==== -- now different plotmodes have
				* different rootxy --
				*/// -- root X --
			else if (key.equalsIgnoreCase(this.sTranslateX + treePlotMode.RECT_CLADOGRAM.toString())) {
				this.hmPlotMode2TranslateX.put(treePlotMode.RECT_CLADOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sTranslateX + treePlotMode.RECT_PHYLOGRAM.toString())) {
				this.hmPlotMode2TranslateX.put(treePlotMode.RECT_PHYLOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sTranslateX + treePlotMode.SLANTED_CLADOGRAM_NORMAL.toString())) {
				this.hmPlotMode2TranslateX.put(treePlotMode.SLANTED_CLADOGRAM_NORMAL, value);
			} else if (key.equalsIgnoreCase(this.sTranslateX + treePlotMode.CIRCULAR_CLADOGRAM.toString())) {
				this.hmPlotMode2TranslateX.put(treePlotMode.CIRCULAR_CLADOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sTranslateX + treePlotMode.CIRCULAR_PHYLOGRAM.toString())) {
				this.hmPlotMode2TranslateX.put(treePlotMode.CIRCULAR_PHYLOGRAM, value);
			} // -- root Y --
			else if (key.equalsIgnoreCase(this.sTranslateY + treePlotMode.RECT_CLADOGRAM.toString())) {
				this.hmPlotMode2TranslateY.put(treePlotMode.RECT_CLADOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sTranslateY + treePlotMode.RECT_PHYLOGRAM.toString())) {
				this.hmPlotMode2TranslateY.put(treePlotMode.RECT_PHYLOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sTranslateY + treePlotMode.SLANTED_CLADOGRAM_NORMAL.toString())) {
				this.hmPlotMode2TranslateY.put(treePlotMode.SLANTED_CLADOGRAM_NORMAL, value);
			} else if (key.equalsIgnoreCase(this.sTranslateY + treePlotMode.CIRCULAR_CLADOGRAM.toString())) {
				this.hmPlotMode2TranslateY.put(treePlotMode.CIRCULAR_CLADOGRAM, value);
			} else if (key.equalsIgnoreCase(this.sTranslateY + treePlotMode.CIRCULAR_PHYLOGRAM.toString())) {
				this.hmPlotMode2TranslateY.put(treePlotMode.CIRCULAR_PHYLOGRAM, value);
			}
			// <<<<< April 23, 2012 <<<<<
			/**
			 * Sep 10, 2013 --
			 */
			else if (key.equalsIgnoreCase("bootStrapFontItalic")) {
				this.bootStrapFontItalic = value > 0 ? true : false;
			}else if (key.equalsIgnoreCase("branchLengthFontItalic")) {
				this.branchLengthFontItalic = value > 0 ? true : false;
			}else if (key.equalsIgnoreCase("bootStrapFontSize")) {
				this.default_bootstrap_font_size = (int) value;
			}else if (key.equalsIgnoreCase("branchLengthFontSize")) {
				this.default_branchlength_font_size = (int) value;
			}
			// <<<<<< sep 10, 2013 <<<<<<<<			
		} // to through retrieve data

		// initiate transform/ translate for svg
		OMSVGTransform scale = svg.createSVGTransform();
		scale.setScale(this.svg_scale, this.svg_scale);
		layer_tree.getTransform().getBaseVal().appendItem(scale);

		/*
		 * -- Apr 23, 2012 -- note: svg_translate_x and svg_translate_y are for
		 * compatability-purpose only --
		 */
		float translate_x = this.hmPlotMode2TranslateX.containsKey(plotmode) ? this.hmPlotMode2TranslateX.get(plotmode) : svg_translate_x;
		float translate_y = this.hmPlotMode2TranslateY.containsKey(plotmode) ? this.hmPlotMode2TranslateY.get(plotmode) : svg_translate_y;
		OMSVGTransform t = svg.createSVGTransform();
		t.setTranslate(translate_x, translate_y);
		layer_tree.getTransform().getBaseVal().appendItem(t);

		/*
		 * set canvas size for svg
		 */
		this.setPixelSize(this.canvas_width, this.canvas_height);

		svg.setViewBox(0, 0, this.canvas_width, this.canvas_height);
		svg.getStyle().setWidth(this.canvas_width, Unit.PX);
		svg.getStyle().setHeight(this.canvas_height, Unit.PX);
		// <<<<<<<<<<<<< may 13, 2011 <<<<<<<<<<

		// 
		phylotree = newtree;
		rootnode = phylotree.getRootNode();

		this.projectID = proID;
		this.treeID = phylotree.getTreeId();

		/*
		 * set visiable status for different layers
		 */
		if (this.showBootStrap == false) {
			layer_tree_bootstrap.getStyle().setVisibility(Visibility.HIDDEN);
			layer_tree_bootstrap.getStyle().setDisplay(Display.NONE);
		}

		if (this.showBrachLength == false) {
			layer_tree_branchlength.getStyle().setVisibility(Visibility.HIDDEN);
			layer_tree_branchlength.getStyle().setDisplay(Display.NONE);
		}

		if (this.showNodeLabel == false) {
			layer_tree_leaf_labels.getStyle().setVisibility(Visibility.HIDDEN);
			layer_tree_leaf_labels.getStyle().setDisplay(Display.NONE);
		}

		/*
		 * and draw the tree
		 */
		drawPhyloTreeUsingSVG();
	}

	/*
	 * march 25, 2011;
	 */

	public void setDisableAllDatasets(boolean disableall) {
		this.disable_all_dataset = disableall;
	}

	public boolean getDisableAllDatasets() {
		return this.disable_all_dataset;
	}

	public String getProjectID() {
		return this.projectID;
	}

	public int getTreeID() {
		return this.treeID;
	}

	public Pies getPies() {
		return this.pie;
	}

	public TreeSkeleton getTreeSkeleton() {
		return this.treeskeleton;
	}

	/*
	 * private functions; April 7, 2011;
	 */
	private void clearAllChildNodesFromSVGGElement(OMSVGGElement svgg) {
		// March 11, 2011; according to suggestions by google group: lib-gwt-svg
		while (svgg.hasChildNodes()) {
			svgg.removeChild(svgg.getLastChild());
		}
	}

	private boolean deleteChildNodeFromLayerTree(OMNode svgnode) {
		boolean success = false;
		for (OMNode omnode : layer_tree.getChildNodes()) {

			if (omnode.equals(svgnode)) {
				layer_tree.removeChild(omnode);
				success = true;
				break;
			}
		}
		return success;
	}

	/*
	 * -- revise: Apr 23, 2012 -- 1. each plot mode has its own pixal per width
	 * 2. each plot mode has its own rootXY
	 */
	private void recalculate_some_global_variables() {
		pxPerWidthCurrentMode = this.hmPlotMode2pxPerWidth.containsKey(this.plotmode) ? this.hmPlotMode2pxPerWidth.get(this.plotmode) : this.pxPerWidthDefault;
		pxPerBranchLength = pxPerWidthCurrentMode * this.pxPerBranchLengthScaling; // not used in CLAD modes

		// for circular mode
		angle_per_vertical_level = ((float) this.angle_span) / ((float) phylotree.getMaxVerticalLevel());

		// draw scale or not
		draw_branch_length_scale = (plotmode == treePlotMode.CIRCULAR_PHYLOGRAM || plotmode == treePlotMode.RECT_PHYLOGRAM) ? true : false;

		if (phylotree.hasBranchlength() == false) {
			draw_branch_length_scale = false;
		}

		// rootxy --
		// revisiion:  Apr 24, 2012; Apr 27, 2012
		if (plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
			float center = (plotmode == treePlotMode.CIRCULAR_CLADOGRAM) ? phylotree.getMaxHorizontalLevel() * this.pxPerWidthCurrentMode : phylotree
					.getMaxTotalBranchLengthFromRootToAnyTip() * this.pxPerBranchLength;
			// Apr 27, 2012: improved, otherwise the rootXY will be gone very far away

			rootXY.setX(center + margin_x + this.default_font_size * 5);
			rootXY.setY(center + margin_y + this.default_font_size * 5);
		} else {
			rootXY.setX(margin_x);
			if (plotmode == treePlotMode.SLANTED_CLADOGRAM_NORMAL) {
				rootXY.setY(rootnode.getLevelVerticalSlanted() * pxPerHeight + margin_y);
			} else {
				rootXY.setY(rootnode.getLevelVertical() * this.pxPerHeight + margin_y);
			}
		}

		// Jan 24, 2011; necessary for piechart at rootnode
		// may 30, 2012; clear all contents
		this.BranchLengthPositions.clear();
		this.LeafInternalID2NodePosisionts.clear();
		this.BootstrapPositions.clear();

		this.BranchLengthPositions.put(this.rootnode.getInternalID(), new APosition(rootXY, 0));
	}

	/*
	 * May 18, 2013; this method was made public
	 */
	public void updateAllCharts() {
		this.currentMostRightPosition = this.treeskeleton.getRightMostTreeAndLabelPosisions().get(1);
		for (PlotBase pb : activeCharts) {
			// runtime instances check
			if (pb instanceof ColorStrips) {
				((ColorStrips) pb).makeOrUpdatePlot();

				//System.out.println(" A: " + ((ColorStrips) pb).getDataSetID());
			} else if (pb instanceof Bars) {
				((Bars) pb).makeOrUpdatePlot();
			} else if (pb instanceof ProteinDomains) {
				((ProteinDomains) pb).makeOrUpdatePlot();
			}else if (pb instanceof HeatMap) {
				((HeatMap) pb).makeOrUpdatePlot();
			}
		}
	}

	/*
	 * this layer is added directly to svg; therefore it wouldn't be moved by
	 * mouse the scale changes according to user selection (of cause); jan 17,
	 * 2011 April 7, 2011; always make new ones April 8, 2011; bug fix May 13,
	 * 2011; change the position to the left of the plot
	 */
	private void makeBranchLengthScale() {
		this.clearAllChildNodesFromSVGGElement(layer_scale_branch_length);

		if (draw_branch_length_scale == true) {
			layer_scale_branch_length.getStyle().setVisibility(Visibility.VISIBLE);

			//this.layer_scale_branch_length
			float x1 = 30, x2 = x1 + pxPerBranchLength / 10 * svg_scale, // Jan 25, 2011; use current scale to adjust
			y2 = branchlength_scale_y1;

			OMSVGLineElement hline = doc.createSVGLineElement(x1, branchlength_scale_y1, x2, y2);
			OMSVGLineElement tick_left = doc.createSVGLineElement(x1, branchlength_scale_y1, x1, branchlength_scale_y1 - 4);
			OMSVGLineElement tick_right = doc.createSVGLineElement(x2, y2, x2, y2 - 4);

			OMSVGTextElement mark = doc.createSVGTextElement(x1 + (x2 - x1) / 2, branchlength_scale_y1 - 7, OMSVGLength.SVG_LENGTHTYPE_PX, "0.1");
			mark.setAttribute(SVGConstants.SVG_TEXT_ANCHOR_ATTRIBUTE, SVGConstants.SVG_MIDDLE_VALUE);
			mark.getStyle().setFontSize(default_tick_label_font_size, Unit.PX);
			mark.getStyle().setFontStyle(FontStyle.ITALIC);

			layer_scale_branch_length.appendChild(hline);
			layer_scale_branch_length.appendChild(tick_left);
			layer_scale_branch_length.appendChild(tick_right);
			layer_scale_branch_length.appendChild(mark);

			layer_scale_branch_length.getStyle().setSVGProperty(SVGConstants.CSS_STROKE_PROPERTY, SVGConstants.CSS_GREY_VALUE);
			layer_scale_branch_length.getStyle().setWidth(default_line_width, Unit.PX);

		}
	}

	private OMSVGPoint getLocalCoordinates(MouseEvent e) {
		OMSVGPoint p = svg.createSVGPoint(e.getClientX(), e.getClientY());
		OMSVGMatrix m = layer_tree.getScreenCTM().inverse();
		return p.matrixTransform(m);
	}

	private OMSVGPoint getLocalCoordinates(float x, float y) {
		OMSVGPoint p = svg.createSVGPoint(x, y);
		OMSVGMatrix m = layer_tree.getScreenCTM().inverse();
		return p.matrixTransform(m);
	} // Jan 16, 2012

	/*
	 * on a cirle, given its center, radius and an angle, calculate the
	 * coordinates of the corresponding point on this circle
	 */
	private OMSVGPoint getCoordinatesOnCircle(OMSVGPoint xy, float radius, float angle) {
		// angle cannot be higher than 360
		angle = angle % (float) 360;
		// narrow angle within range of 0~90
		float nangle = angle % (float) 90.0;
		// translate angle to radians
		float radians = (float) (nangle * 2.0 * Math.PI / 360.0);
		// assume that the center is at (0,0)
		float sin = (float) Math.sin(radians);
		float cos = (float) Math.cos(radians);
		float x = cos * radius;
		float y = sin * radius;
		// adjust x,y according to original angle
		float newx = 0, newy = 0;
		if (angle >= 0 && angle < 90) {
			newx = -x;
			newy = y;
		} else if (angle >= 90 && angle < 180) {
			newx = y;
			newy = x;
		} else if (angle >= 180 && angle < 270) {
			newx = x;
			newy = -y;
		} else if (angle >= 270 && angle < 360) {
			newx = -y;
			newy = -x;
		}
		// make a SVGpoint based on newx, newy; and move it to xy
		OMSVGPoint newxy = svg.createSVGPoint(newx, newy);
		newxy.add(xy);
		return newxy;
	}

	/*
	 * March 22, 2011; radius refers to the distance from center to tips; factor
	 * ranging from 0.2 to 0.8
	 */
	private OMSVGPolygonElement makeStar(OMSVGPoint center, float radius, float factor) {
		OMSVGPolygonElement star = doc.createSVGPolygonElement();
		for (int angle = -18; angle < 360; angle += 72) {
			star.getPoints().appendItem(getCoordinatesOnCircle(center, radius, angle));
			star.getPoints().appendItem(getCoordinatesOnCircle(center, radius * factor, angle + 36));
		}
		return star;
	}

	private OMSVGGElement makeCheckSymbol(OMSVGPoint start, float plotwidth) {
		OMSVGGElement check = doc.createSVGGElement();
		OMSVGPoint pleft = svg.createSVGPoint(start.getX() + plotwidth * (1 / 10f), start.getY() - plotwidth * (1 / 20f));
		OMSVGPoint pmiddle = svg.createSVGPoint(start.getX() + plotwidth * (2 / 5f), start.getY() + plotwidth * (1 / 5f));
		OMSVGPoint pright = svg.createSVGPoint(start.getX() + plotwidth * (4 / 5f), start.getY() - plotwidth * (1 / 3f));

		OMSVGLineElement line1 = doc.createSVGLineElement(pleft, pmiddle);
		OMSVGLineElement line2 = doc.createSVGLineElement(pright, pmiddle);

		check.appendChild(line1);
		check.appendChild(line2);

		return check;
	}

	private OMSVGPathElement makeFanPlot(OMSVGPoint center, float innerRadius, float outterRadius, float anglespan) {
		/*
		 * 1. get four points that mark the fan-shape locations of the four
		 * points are: 1 ---> 2 | | |arc | arc | v 4 <--- 3
		 */
		OMSVGPoint p1 = getCoordinatesOnCircle(center, innerRadius, 360 - anglespan / 2);
		OMSVGPoint p2 = getCoordinatesOnCircle(center, outterRadius, 360 - anglespan / 2);
		OMSVGPoint p3 = getCoordinatesOnCircle(center, outterRadius, anglespan / 2);
		OMSVGPoint p4 = getCoordinatesOnCircle(center, innerRadius, anglespan / 2);

		// 2. make fan using path
		OMSVGPathElement fan = doc.createSVGPathElement();
		OMSVGPathSegList segs = fan.getPathSegList();

		// Feb 12, 2013: large_arc
		boolean large_arc = (anglespan >= 180) ? true : false; // Feb 12, 2013; use global angle_span?

		segs.appendItem(fan.createSVGPathSegMovetoAbs(p1.getX(), p1.getY())); // move to p1
		segs.appendItem(fan.createSVGPathSegLinetoAbs(p2.getX(), p2.getY())); // line from p1 to p2;
		segs.appendItem(fan.createSVGPathSegArcAbs(p3.getX(), p3.getY(), outterRadius, outterRadius, 0, large_arc, false)); // arc from p2 to p3
		segs.appendItem(fan.createSVGPathSegLinetoAbs(p4.getX(), p4.getY())); // line from p3 to p4;
		segs.appendItem(fan.createSVGPathSegArcAbs(p1.getX(), p1.getY(), innerRadius, innerRadius, 0, large_arc, true)); // arc from p4 to p1
		segs.appendItem(fan.createSVGPathSegClosePath()); // close path

		return fan;
	} // makeFanPlot

	/*
	 * April 14, 2011;
	 */
	public void setDatasetEnabled(String datasettype, Boolean active, boolean b) {
		if (datasettype.equalsIgnoreCase("Pies")) {
			this.getPies().setDisableAllPiechartData(!active);
		} else if (datasettype.equalsIgnoreCase("Branch colors")) {
			this.getTreeSkeleton().setDisableAllBranchColorData(!active);
		} else if (datasettype.equalsIgnoreCase("Leaf colors")) {
			this.getTreeSkeleton().setDisableAllLabelColorData(!active);
		} else if (datasettype.equalsIgnoreCase("leaf BK colors")) {
			this.getTreeSkeleton().setDisableAllLabelBKColorData(!active);
		} else if (datasettype.equalsIgnoreCase("Charts")) {
			this.setDisableAllCharts(!active);
		}

		// Legend here?
		updateAllLegends(); // May 24, 2011
	}

	/*
	 * May 13, 2011; set width and height
	 */
	public void setCanvasWidth(int width) {
		if (this.canvas_width != width) {
			this.setWidth(width + "px");

			canvas_width = width;

			svg.setViewBox(0, 0, canvas_width, canvas_height);
			svg.getStyle().setWidth(canvas_width, Unit.PX);

			updateTreeCanvasInfoOnServer("canvasWidth", canvas_width);
		}

	} // May 13, 2011; Dec 28, 2011;

	public void setCanvasHeight(int height) {
		if (this.canvas_height != height) {
			this.setHeight(height + "px");

			canvas_height = height;

			svg.setViewBox(0, 0, canvas_width, canvas_height);
			svg.getStyle().setHeight(canvas_height, Unit.PX);

			updateTreeCanvasInfoOnServer("canvasHeight", canvas_height);
		}

	} // May 13, 2011; Dec 28, 2011;

	public void setCanvasSize(int width, int height) {

		if (this.canvas_height != height || this.canvas_width != width) {
			this.setWidth(width + "px");
			this.setHeight(height + "px");

			canvas_width = width;
			canvas_height = height;

			svg.setViewBox(0, 0, canvas_width, canvas_height);
			svg.getStyle().setWidth(canvas_width, Unit.PX);
			svg.getStyle().setHeight(canvas_height, Unit.PX);

			updateTreeCanvasInfoOnServer("canvasHeight", canvas_height);
			updateTreeCanvasInfoOnServer("canvasWidth", canvas_width);

		}
	} // May 13, 2011; Dec 28, 2011;

	public void setDBSerialID(Integer tree_dbserial) {
		this.db_serialID = tree_dbserial;
	}

	public int getDBserialID() {
		return this.db_serialID;
	}

	/*
	 * ============= dec 28, 2011 ===============; fit cavnvas to plot; it's
	 * easy to do because gwt-lib-svg provides a lot useful functions
	 */
	public void fitCanvas2PlotWidth() {
		OMSVGRect bBox = this.svg.getBBox();
		setCanvasWidth(Math.round((bBox.getX() + bBox.getWidth()) + margin_x));
	}

	public void fitCanvas2PlotHeight() {
		OMSVGRect bBox = this.svg.getBBox();
		setCanvasHeight(Math.round((bBox.getY() + bBox.getHeight()) + margin_y));
	} // Dec 28, 2011;

	public void fitCanvas2Plot() {
		OMSVGRect bBox = this.svg.getBBox();
		setCanvasSize(Math.round((bBox.getX() + bBox.getWidth()) + margin_x), Math.round((bBox.getY() + bBox.getHeight()) + margin_y));
	} // Dec 28, 2011;

	/*
	 * ============= dec 28, 2011 ===============; fit plot to canvas; this is a
	 * little more difficult to do than 'fitting canvas to plot'
	 */
	public void fitPlot2CanvasWidth() {
		OMSVGRect bBoxSkeleton = this.layer_tree_leaf_labels.getBBox();
		OMSVGRect bBoxTree = this.layer_tree.getBBox();

		// calculate new pxPerWidth / pxPerBranchLength
		float previous_width = bBoxSkeleton.getWidth();
		float new_width = this.canvas_width / 1.05f - (bBoxTree.getWidth() - previous_width);

		if (previous_width != new_width) {

			float newPxPerWidth = (this.plotmode == treePlotMode.CIRCULAR_CLADOGRAM || this.plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) ? pxPerWidthCurrentMode * new_width
					/ previous_width / 2 : pxPerWidthCurrentMode * new_width / previous_width;

			this.setPxPerWidth(newPxPerWidth);
		}
	}

	public void fitPlot2CanvasHeight() {
		OMSVGRect bBoxSkeleton = this.layer_tree_skeleton.getBBox();
		OMSVGRect bBoxTree = this.layer_tree.getBBox();

		// calculate new pxPerHeight / pxPerBranchLength
		float previous_height = bBoxSkeleton.getX() + bBoxSkeleton.getHeight();
		float new_height = this.canvas_height / 1.05f - ((bBoxTree.getX() + bBoxTree.getHeight()) - previous_height);

		if (previous_height != new_height) {
			float newPxPerHeight = (this.plotmode == treePlotMode.CIRCULAR_CLADOGRAM || this.plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) ? pxPerHeight * new_height
					/ previous_height / 2 : pxPerHeight * new_height / previous_height;

			this.setPxPerHeight(newPxPerHeight);
		}
	}

	public void fitPlot2Canvas() {
		OMSVGRect bBoxSkeleton = this.layer_tree_skeleton.getBBox();
		OMSVGRect bBoxTree = this.layer_tree.getBBox();

		// calculate new pxPerWidth / pxPerBranchLength
		float previous_width = bBoxSkeleton.getX() + bBoxSkeleton.getWidth();
		float new_width = this.canvas_width / 1.05f - ((bBoxTree.getX() + bBoxTree.getWidth()) - previous_width);

		// calculate new pxPerHeight / pxPerBranchLength
		float previous_height = bBoxSkeleton.getX() + bBoxSkeleton.getHeight();
		float new_height = this.canvas_height / 1.05f - ((bBoxTree.getX() + bBoxTree.getHeight()) - previous_height);

		if (previous_height != new_height || previous_width != new_width) {
			this.pxPerWidthCurrentMode *= new_width / previous_width;
			this.pxPerHeight *= new_height / previous_height;

			// replot everything
			this.drawPhyloTreeUsingSVG();
		}
	}

	/*
	 * May 26, 2011;
	 * Apr 29, 2013
	 * May 14, 2013; fix a bug here
	 */
	private FlexTable makeFlextableForToolTip(ArrayList<String> colors, ArrayList<Float> data, ArrayList<String> groups, String id) {
		return makeFlextableForToolTip(colors, data, groups, id, "rect");
	}

	private FlexTable makeFlextableForToolTip(ArrayList<String> colors, ArrayList<Float> data, ArrayList<String> groups, String id, String shape) {
		FlexTable f = new FlexTable();
		f.setCellSpacing( flexTableCellSpacing ); // cell spacing 

		int row = 0;
		if (id != null && !id.isEmpty()) {
			f.setWidget(0, 0, new HTML("<i>" + id + "</i>"));
			f.getFlexCellFormatter().setColSpan(row, 0, 3);
			row++;
		}

		for (int idx = 0; idx < groups.size(); idx++) {
			// a rect or a circle and STYLE
			String color = (colors != null) ? (colors.size() > idx) ? colors.get(idx) : colors.get(idx % colors.size()) : "white";
			f.setWidget(row, 0, makeNewLabelForTooltip(color, shape, 10));
			f.getCellFormatter().setVerticalAlignment(row, 0, HasVerticalAlignment.ALIGN_MIDDLE);

			// the group/cat
			f.setWidget(row, 1, makeNewTextForTooltip(groups.get(idx)));

			// the value and STYLE
			f.setWidget(row, 2, makeNewTextForTooltip(data.get(idx).toString()));
			f.getCellFormatter().setHorizontalAlignment(row, 2, HasHorizontalAlignment.ALIGN_RIGHT);

			// incremental row; May 14, 2013 -- 
			row++;
		}

		return f;
	} // May 26, 2011;

	private HTML makeNewTextForTooltip(String str) {
		HTML html = new HTML(str);
		DOM.setStyleAttribute(html.getElement(), "fontSize", "smaller");
		return html;
	} // May 26, 2011;

	private Label makeNewLabelForTooltip(String color, String type, int size) {
		Label lbl = new Label();
		lbl.setPixelSize(10, 10);
		DOM.setStyleAttribute(lbl.getElement(), "display", "block");
		DOM.setStyleAttribute(lbl.getElement(), "backgroundColor", color);

		if (type.equalsIgnoreCase("circle")) {
			float radius = (float) size / 2;
			DOM.setStyleAttribute(lbl.getElement(), "borderRadius", radius + "px");
		}
		return lbl;
	}

	/*
	 * where is my tree?
	 * May 5, 2013; work fine using Chrome
	 * May 14, 2013; position the tree next to the legend 
	 */
	public void whereIsMyTree() {

		// May 5, 2013 --
		System.err.println("   tree left: " + layer_tree.getElement().getAbsoluteLeft() + " svg-left:" + this.getElement().getAbsoluteLeft());
		System.err.println("   tree top: " + layer_tree.getElement().getAbsoluteTop() + " svg-top:" + this.getElement().getAbsoluteTop());

		float mx = this.getElement().getAbsoluteLeft() - layer_tree.getElement().getAbsoluteLeft() + this.margin_left;
		float my = this.getElement().getAbsoluteTop() - layer_tree.getElement().getAbsoluteTop() + this.margin_top;

		// may 14, width of legend 
		int legend_width = (int) layer_legend.getBBox().getWidth();
		mx += legend_width;

		System.err.println("   layer_legend_width: " + legend_width);

		// may 5, 2013 : get scale xy from screen CTM 
		OMSVGMatrix m = layer_tree.getScreenCTM();
		float sx = m.getA();
		float sy = m.getD();

		mx /= sx;
		my /= sy;

		OMSVGTransform t = svg.createSVGTransform();
		t.setTranslate(mx, my);
		layer_tree.getTransform().getBaseVal().appendItem(t);

		// consolidate all the transforms associated with layer_tree
		layer_tree.getTransform().getBaseVal().consolidate(); // GOOD!!

		/*
		 * save infor to server
		 */
		// update translate xy
		this.updateTranslateXY4layerTree();

		// fit canvas to plot
		this.fitCanvas2Plot();
	}

	private void initiate_defs() {

		/**
		 * ***********************************************
		 * Drop shadows ***********************************************
		 */
		// =================== simple shadow ===================
		String fileterID_simple_shadow = "simpleDropshadow";
		OMSVGFilterElement simpleDropshadow = doc.createSVGFilterElement();
		simpleDropshadow.setId(fileterID_simple_shadow);
		simpleDropshadow.setAttribute("filterUnits", "userSpaceOnUse"); // This is important; otherwise the shadow will be truncated

		// offset
		OMSVGFEOffsetElement feoffset = doc.createSVGFEOffsetElement();
		feoffset.getResult().setBaseVal("offOut");
		feoffset.getIn1().setBaseVal("SourceAlpha");
		feoffset.getDx().setBaseVal(1);
		feoffset.getDy().setBaseVal(1);

		// gaussian blur
		OMSVGFEGaussianBlurElement gaussianBlur = doc.createSVGFEGaussianBlurElement();
		gaussianBlur.getStdDeviationX().setBaseVal(2);
		gaussianBlur.getStdDeviationY().setBaseVal(2);
		gaussianBlur.getIn1().setBaseVal("offOut");
		gaussianBlur.getResult().setBaseVal("blurOut");

		// blend element
		OMSVGFEBlendElement beblend = doc.createSVGFEBlendElement();
		beblend.getIn1().setBaseVal("SourceGraphic");
		beblend.getIn2().setBaseVal("blurOut");
		//beblend.getMode().setBaseVal(SVGFEBlendElement.ELEMENT_NODE);

		// add all to filter 
		simpleDropshadow.appendChild(feoffset);
		simpleDropshadow.appendChild(gaussianBlur);
		simpleDropshadow.appendChild(beblend);

		// add filter to defs
		defs.appendChild(simpleDropshadow);

		// ================ dropshadow2 ================
		String filterID2 = "dropshadow";
		OMSVGFilterElement dropshadow = doc.createSVGFilterElement();
		dropshadow.setAttribute(SVGConstants.XML_ID_ATTRIBUTE, filterID2);
		dropshadow.setAttribute("filterUnits", "userSpaceOnUse"); // This is important; otherwise the shadow will be truncated

		// offset
		OMSVGFEOffsetElement feoffsetDropshadow = doc.createSVGFEOffsetElement();
		feoffsetDropshadow.getResult().setBaseVal("offsetblur");
		feoffsetDropshadow.getDx().setBaseVal(1);
		feoffsetDropshadow.getDy().setBaseVal(1);

		// gaussian blur
		OMSVGFEGaussianBlurElement gaussianBlurDropshadow = doc.createSVGFEGaussianBlurElement();
		gaussianBlurDropshadow.getStdDeviationX().setBaseVal(1);
		gaussianBlurDropshadow.getStdDeviationY().setBaseVal(1);
		gaussianBlurDropshadow.getIn1().setBaseVal("SourceAlpha");

		// merge element
		OMSVGFEMergeElement merge = doc.createSVGFEMergeElement();
		OMSVGFEMergeNodeElement mergenode = doc.createSVGFEMergeNodeElement();
		mergenode.getIn1().setBaseVal("SourceGraphic");

		// note: two mergenodeElements are needed: the first is empty
		merge.appendChild(doc.createSVGFEMergeNodeElement());
		merge.appendChild(mergenode);

		// add all to filter 
		dropshadow.appendChild(feoffsetDropshadow);
		dropshadow.appendChild(gaussianBlurDropshadow);
		dropshadow.appendChild(merge);

		// add filter to defs
		defs.appendChild(dropshadow);
	}

	/**
	 * ***********************************
	 * May 14, 2012: make a new function for plotting gridlines for all plots
	 * ************************************
	 */
	private void drawGridLines(int groupSize, int gridlineCount, float maxvalue, float plotstart, float widthPerValue, float pixPerProtein, float plot_width_per_subpanel,
			OMSVGGElement layer_grid, OMSVGGElement layer_gridlabel) {

		// get leaf information
		String firstleaf_id = phylotree.getFirstLeafNode().getInternalID();
		String lastleaf_id = phylotree.getLastLeafNode().getInternalID();

		APosition firstleafPos = LeafInternalID2NodePosisionts.get(firstleaf_id);
		APosition lastleafPos = LeafInternalID2NodePosisionts.get(lastleaf_id);

		// Feb 12, 2012
		float anglespan_per_leaf_label = angle_span / phylotree.getLeafNodes().size();
		float anglespan_of_leaf_lables = angle_span - anglespan_per_leaf_label;

		// nice axis 
		ArrayList<Double> niceaxis = JAVAFuncs.AxisStartEndStep(0, maxvalue, gridlineCount);

		double step = niceaxis.get(2);
		double aend = niceaxis.get(1) > maxvalue ? (niceaxis.get(1) - step) : niceaxis.get(1);

		float realstart = plotstart; // 0
		float realend = (float) (plotstart + (aend - 0) * widthPerValue);
		float realstep = (float) (step * widthPerValue);

		/*
		 * -- Apr 25, 2012 -- refine grid and axis add numbers for grid lines
		 */
		for (int g = 0; g < groupSize; g++) {
			int grididx = 0;
			for (float s = realstart; s < realend; s += realstep) {
				//String color = grididx == 0 ? "grey" : "grey"; // Apr 25, 2012; make them all grey

				// text label for niceaxis value
				double value = niceaxis.get(0) + grididx * step;
				String sValue = ((int) value == value) ? Integer.toString((int) value) : Double.toString(value);
				float y = lastleafPos.getY() + pixPerProtein;
				OMSVGTextElement txtGridLabel = doc.createSVGTextElement(s, y, OMSVGLength.SVG_LENGTHTYPE_PX, sValue);
				txtGridLabel.getStyle().setFontSize(default_tick_label_font_size, Unit.PX);
				txtGridLabel.setAttribute("text-anchor", "middle"); // April 14, 2011; Align center 

				// if circular mode
				if ((plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM))) {
					float aradius = (float) (s - rootXY.getX());

					OMSVGPoint startpoint = getCoordinatesOnCircle(rootXY, aradius, firstleafPos.getAngle());
					OMSVGPoint endpoint = getCoordinatesOnCircle(rootXY, aradius, lastleafPos.getAngle());

					boolean sweepFlag = circular_tree_plot_in_clockwise_mode;
					boolean large_arc = (anglespan_of_leaf_lables >= 180) ? true : false; // Feb 12, 2011; use global angle_span?

					// boolean largeArcFlag = angle >= 180 ? true : false; 
					OMSVGPathElement path = doc.createSVGPathElement();
					OMSVGPathSegList segs = path.getPathSegList();
					segs.appendItem(path.createSVGPathSegMovetoAbs(startpoint.getX(), startpoint.getY()));
					segs.appendItem(path.createSVGPathSegArcAbs(endpoint.getX(), endpoint.getY(), aradius, aradius, 0, large_arc, sweepFlag));
					layer_grid.appendChild(path);

					// rorate it if necessary
					float angle = circular_tree_plot_in_clockwise_mode ? lastleafPos.getAngle() : firstleafPos.getAngle();
					if (angle != 0) {
						// and then rotate
						OMSVGTransform r = svg.createSVGTransform();
						r.setRotate(180 - angle, rootXY.getX(), rootXY.getY());
						txtGridLabel.getTransform().getBaseVal().appendItem(r);

						// Apr 24, 2012
						//betterReadabilityForSVGTextElem(txtLeafLabel, angle, x, y - fontPxHeight / 4, "end");
					} // circular

				} else {
					OMSVGLineElement line = doc.createSVGLineElement(s, firstleafPos.getY() - pixPerProtein * 0.8f / 2, s, lastleafPos.getY() + pixPerProtein * 0.8f / 2);
					layer_grid.appendChild(line);
				}

				// append it to current layer 
				layer_gridlabel.appendChild(txtGridLabel);
				grididx++;
			} // s
			realstart += (plot_width_per_subpanel + space_between_columns_within_a_dataset);
			realend += (plot_width_per_subpanel + space_between_columns_within_a_dataset);
		}// for 
	}

	/**
	 * **********************************************************
	 * May 13, 2012; add protein domain data to charts
	 * ***********************************************************
	 */
	public void addProteinDomainData(int datasetID, ProteinDomainDataContainerParser domaindata, boolean active) {
		// 
		boolean replot = active;
		if (this.charts.containsKey(datasetID)) {
			ProteinDomains domainplot = (ProteinDomains) charts.get(datasetID);
			domainplot.setProteinDomainDataSet(datasetID, domaindata);
			domainplot.setActive(true);

			replot = true;
		} else {

			// Oct 28, 2011;
			// 1. add dataset
			ProteinDomains domainplot = new ProteinDomains(datasetID, domaindata);
			domainplot.setProteinDomainDataSet(datasetID, domaindata);
			domainplot.setActive(active);
			this.charts.put(datasetID, domainplot);

			if (active) {
				this.activeCharts.add(domainplot);
			}
		}

		if (replot) {
			this.updateAllCharts();
		}

		/*
		 * May 13, 2012; NOTE: here TreeDecoType must be 'CHARTS'; to be fixed
		 */
		this.makeLegendEntryAndUpdateAllLegend(domaindata.isShowLegends(), 
				domaindata.getLegendColors(), 
				domaindata.getLegendTexts(), 
				datasetID,
				domaindata.getTitle(),
				domaindata.getLegendstyle(),
				TreeDecoType.CHARTS,
				replot);

	} // add protein domain data; created on : May 13, 2012 

	/*
	 * >>>>>>>>>>>> private classes >>>>>>>>>>>>>> Here I use several public
	 * classes to manage SVGGElements for piecharts and other types charts
	 */
	/*
	 * April 7, 2011; add opacity March 21, 2011; color strips, can be multiple
	 * column the number of columns is controlled by #type for example, #type
	 * strip,strip will create two columns, both are strips or #type
	 * strip,star,rect will create three columns
	 *
	 * color could be multiple as well; If there are fewer colors than plottypes
	 * they are recycled in the standard fashion
	 *
	 * @ annotation lines @ mode could be prefix, suffix, anywhere and AD
	 * #Colors red,green,yellow #Groups normal,large,small #Type
	 * strip,rect,circle [strip | rect | circle | star | check ]
	 *
	 * A red,green,green mode C,D pink,pink,red mode
	 *
	 * May 17, 2011; fix a bug that plots not showing correctly in circular mode
	 * due to SVG rotation
	 * 
	 * - may 30, 2013 -
	 *   implement the following modifiers 
	 *    - !itemHeightPx
	 *    - !itemHeightPCT
	 *    - !stripHeightPx
	 *    - !stripHeightPCT
	 *    - !plotWidth 
	 * - may 31, 2013 --
	 *   - !checkLineWidth 
	 */
	private final class ColorStrips extends PlotBase {

		private ColorDataContainerParser colorContainer = null;
		// internal ID 2 Array of colors, each color corresponds a plottype
		public HashMap<String, ArrayList<String>> id2color = null; 
		private ArrayList<String> plottypes = null;

		// constructor
		@SuppressWarnings("unused")
		public ColorStrips() {
			this.defaultColor = "white";
		}

		public ColorStrips(int datasetID, ColorDataContainerParser coldatCon) {
			this.defaultColor = "white";
			this.setColorData(datasetID, coldatCon);
		}

		/*
		 * March 22, 2011; only deal with leaf colors
		 */
		public void setColorData(int datasetID, ColorDataContainerParser coldatCon) {
			/*
			 * all old data will be deleted/ discarded
			 */
			this.datasetContentAsString = coldatCon.getOriginal_datastring();
			this.groups = coldatCon.getLegendTexts();
			this.colors = coldatCon.getLegendColors();
			this.id2color = new HashMap<String, ArrayList<String>>();// internal ID 2 colors
			this.radius = coldatCon.getObjectRadius();
			this.plotWidth = coldatCon.getPlotwidth();
			this.plottypes = coldatCon.getPlottypes();
			this.checkLineWidth = coldatCon.getCheckLineWidth(); // May 31, 2013
			this.opacity = coldatCon.getOpacity();
			this.datasetID = datasetID; // may 24, 2011;
			this.linestyle = coldatCon.getLineStyle();

			// May 30, 2013 --
			this.colorContainer = coldatCon;

			// if plottypes is empty, add strip as default; March 22, 2011
			if (this.plottypes.isEmpty()) {
				this.plottypes.add(this.plottype);
			}
			/*
			 * colors
			 */
			for (ColorEx colorset : coldatCon.getColorsets()) {
				ArrayList<String> acolor = colorset.getColors();
				String mode = colorset.getMode();
				String leafID = colorset.getNodeIDs().get(0); // get the first element will be used
				/*
				 * set colors according to modes
				 */
				if (mode.isEmpty()) {
					PhyloNode node = phylotree.getNodeByID(leafID);
					if (node != null && node.isLeaf()) {
						this.id2color.put(node.getInternalID(), acolor);
					}
				} else {
					if (mode.equalsIgnoreCase("prefix")) {
						for (PhyloNode node : phylotree.getLeafNodes()) {
							if (JSFuncs.matchStringStart(node.getID(), leafID) == true) {
								this.id2color.put(node.getInternalID(), acolor);
							}
						} // iterate leaf nodes
					} else if (mode.equalsIgnoreCase("suffix")) {
						for (PhyloNode node : phylotree.getLeafNodes()) {
							if (JSFuncs.matchStringEnd(node.getID(), leafID) == true) {
								this.id2color.put(node.getInternalID(), acolor);
							}
						} // iterate leaf nodes
					} else if (mode.equalsIgnoreCase("anywhere")) {
						for (PhyloNode node : phylotree.getLeafNodes()) {
							if (JSFuncs.matchStringAnywhere(node.getID(), leafID) == true) {
								this.id2color.put(node.getInternalID(), acolor);
							}
						} // iterate leaf nodes

					} else if (mode.equalsIgnoreCase("AD")) {
						PhyloNode node = phylotree.getLCA(colorset.getNodeIDs());
						if (node != null) {
							addColorToAllDescendentLeaf(node, acolor);
						}
					}
				} // different modes
			} // iterate colorsets
			this.setActive(true);
		} // setColorDataSet 

		/*
		 * override; this plot is very similar to leaf-label background,
		 * therefore I copy some codes from there
		 */
		@Override
		public void makeOrUpdatePlot() { // color data
			/*
			 * replot everything; to be changed in the future
			 *
			 */
			clearAllChildNodesFromSVGGElement(layer);
			deleteChildNodeFromLayerTree(layer);

			if (this.active && !disable_all_dataset && chart_plots_enabled) {
				layer_tree.appendChild(layer);
				layer.setAttribute("fill-opacity", Float.toString(this.opacity));
				/*
				 * >>>>>>>>>>>> SOME GLOBAL VARIABLES >>>>>>>>>>>
				 */
				float mostright = currentMostRightPosition + space_between_datasets;

				/**
				 *  -- may 31, 2013 --
				 *  there is something tricky in this calculation,
				 *  in principle, it should be mostright * 2f * Math.PI ????
				 */
				float pixelHeightCircularMode = (float) (mostright * Math.PI * (angle_span / 360f) / (float) LeafInternalID2NodePosisionts.size());

				/**
				 * -- may 30, 2013 --
				 * determine the widths and heights of each shape 
				 * NOTE: current plot width is this.width
				 */
				boolean isCircular = plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM);

				// 
				float widthPerShape = this.plotWidth / this.plottypes.size(); // by default take 80 percent of the width 
				float widthPerShape4plot = widthPerShape * 0.8f;
				float heightNonStripShapes = (isCircular) ? colorContainer.getShapePixelHeight(pixelHeightCircularMode) : colorContainer.getShapePixelHeight(pxPerHeight);

				float widthStrip = widthPerShape * 0.8f;
				float heightStrip = (isCircular) ? colorContainer.getStripPixelHeight(pixelHeightCircularMode) : colorContainer.getStripPixelHeight(pxPerHeight);

				float pixelWidthHeight = widthPerShape4plot < heightNonStripShapes ? widthPerShape4plot : heightNonStripShapes;

				float plwd = this.checkLineWidth > 0 ? this.checkLineWidth : 2f; // line width

				/**
				 * -- may 31, 2013 -- 
				 * for strip in circular mode
				 * this part of the following calculation is newly added :  * ( heightStrip / pixelHeightCircularMode )
				 */
				float angleSpanPerLeafNode = angle_span / phylotree.getLeafNodes().size() * (heightStrip / pixelHeightCircularMode);

				// <<<<<<<<<<<<<<<<< GLOBAL VARIABLES END HERE <<<<<<<<<<<<<<

				// iterate hashPositionOfLeafNodes
				for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
					String leaf_internal_id = entry.getKey();
					APosition startpos = entry.getValue();
					float angle = startpos.getAngle();

					ArrayList<String> colors = this.id2color.containsKey(leaf_internal_id) ? this.id2color.get(leaf_internal_id) : null; // bug fix

					// plot one column for each plottypes
					for (int idx = 0; idx < this.plottypes.size(); idx++) {
						String current_type = this.plottypes.get(idx);
						// March 22, 2011; if array colors is shorter than plottypes, colors are recycled 
						String color = (colors != null) ? (colors.size() > idx) ? colors.get(idx) : colors.get(idx % colors.size()) : this.defaultColor;

						/**
						 * -- May 30, 2013 --
						 * start pos is at the center where the shape shoud be plotted 
						 */
						float start_pos = mostright + idx * (widthPerShape + space_between_columns_within_a_dataset);
						float innerRadius = start_pos - rootXY.getX();
						float outterRadius = innerRadius + widthPerShape;

						// check different plot types
						if (!color.equalsIgnoreCase("white")) {

							/**
							 * --- if NOT strip -- 
							 */
							if (!current_type.equalsIgnoreCase("strip")) {
								/*
								 * May 17, 2011; 
								 * OMSVGTransform is moved from  the beginning of outter for loop otherwise the plot isn't shown correctly in circular mode
								 */
								OMSVGTransform r = null;
								// if circular mode and angle is not zero
								if (angle != 0 || plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
									r = svg.createSVGTransform();
									r.setRotate(180 - angle, rootXY.getX(), rootXY.getY()); // March 22, 2011; this rotation is correct ??? 
								} // May 17, 2011

								if (current_type.equalsIgnoreCase("rect")) {
									/**
									 * 
									 */
									OMSVGRectElement rect = doc.createSVGRectElement(start_pos + (widthPerShape - widthPerShape4plot) / 2, startpos.getY() - pixelWidthHeight / 2,
											pixelWidthHeight, pixelWidthHeight, 0, 0);
									rect.setAttribute("fill", color);
									rect.setAttribute("stroke", "none");

									if (r != null) {
										rect.getTransform().getBaseVal().appendItem(r);
									}

									layer.appendChild(rect);
								} else if (current_type.equalsIgnoreCase("circle")) {
									OMSVGCircleElement circle = doc.createSVGCircleElement(start_pos + widthPerShape / 2, startpos.getY(), pixelWidthHeight / 2);
									circle.setAttribute("fill", color);
									circle.setAttribute("stroke", "none");

									if (r != null) {
										circle.getTransform().getBaseVal().appendItem(r);
									}

									layer.appendChild(circle);
								} else if (current_type.equalsIgnoreCase("star")) {
									OMSVGPolygonElement star = makeStar(svg.createSVGPoint(start_pos + widthPerShape / 2, startpos.getY()), pixelWidthHeight / 2, 0.55f);
									star.setAttribute("fill", color);
									star.setAttribute("stroke", "none");

									if (r != null) {
										star.getTransform().getBaseVal().appendItem(r);
									}

									layer.appendChild(star);
								} else if (current_type.equalsIgnoreCase("check") || this.plottype.equalsIgnoreCase("positivecheck")) {
									OMSVGGElement check = makeCheckSymbol(svg.createSVGPoint(start_pos + (widthPerShape - widthPerShape4plot) / 2, startpos.getY()),
											pixelWidthHeight);
									check.setAttribute("stroke-width", plwd + "px");
									check.setAttribute("stroke", color);
									check.setAttribute("stroke-linecap", "round"); // linecap to round

									if (r != null) {
										check.getTransform().getBaseVal().appendItem(r);
									}
									layer.appendChild(check);
								}
							} else {
								/*
								 * if strips
								 */
								// if circular mode
								if (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
									OMSVGPathElement fan = makeFanPlot(rootXY, innerRadius, outterRadius, angleSpanPerLeafNode);
									fan.setAttribute("stroke", "none");
									fan.setAttribute("fill", color);

									// Feb 12, 2012
									if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
										OMSVGTransform fanr = svg.createSVGTransform();
										fanr.setRotate(-angle, rootXY.getX(), rootXY.getY()); // March 21, 2011; this rotation is correct
										fan.getTransform().getBaseVal().appendItem(fanr);
									}

									layer.appendChild(fan);
								} else {
									OMSVGRectElement rect = doc.createSVGRectElement(start_pos + (widthPerShape - widthStrip) / 2, startpos.getY() - heightStrip / 2, widthStrip,
											heightStrip, 0, 0);
									rect.setAttribute("stoke", "none");
									rect.setAttribute("fill", color);
									layer.appendChild(rect);
								} // if circular mode
							}// different types; rect, circle, check or others
						} // if color isn't default color
					} // plot a column for each plottype
				} // iterate hashPositionOfLeafNodes

				// at the end, update currentMostRightPosition; March 25, 2011;
				currentMostRightPosition += (this.plottypes.size() * widthPerShape + (this.plottypes.size() - 1) * space_between_columns_within_a_dataset) + space_between_datasets;
			} // is current chart is active
		}// makeOrUpdatePlot

		/*
		 * public get/set pairs
		 */
		@SuppressWarnings("unused")
		public ArrayList<String> getColorsByNodeInternalID(String id) {
			return this.id2color.containsKey(id) ? this.id2color.get(id) : null;
		}

		/*
		 * private functions
		 *
		 */
		private void addColorToAllDescendentLeaf(PhyloNode node, ArrayList<String> colors) {
			if (node.isLeaf()) {
				this.id2color.put(node.getInternalID(), colors);
			} else {
				for (PhyloNode dnode : node.getDescendents()) {
					addColorToAllDescendentLeaf(dnode, colors);
				}
			}
		} // addColorToAllDescendentLeaf
	} // ColorStrips

	
	public final class HeatMap extends PlotBase {

		private HeatmapMatrixDataContainer numericDataContainer = null;
		
		public HeatMap(int datasetID, HeatmapMatrixDataContainer mdatCon) {
			this.plotWidth = mdatCon.getPlotwidth();  
			this.defaultColor = "white";
			this.setNumericDataSet(datasetID, mdatCon);
		}

		public void setNumericDataSet(int newID, HeatmapMatrixDataContainer mdatCon) {
			/*
			 * all old data will be deleted/ discarded
			 */
			this.datasetContentAsString = mdatCon.getOriginal_datastring();
			this.groups = mdatCon.getLegendTexts();
			this.colors = mdatCon.getLegendColors();   
			this.datasetID = newID; // may 24, 2011; 

			// May 30, 2013 --
			this.numericDataContainer = mdatCon;
			this.setActive(true);
		} // setNumericDataSet 

		/*
		 * override; this plot is very similar to leaf-label background,
		 * therefore I copy some codes from there
		 */
		@Override
		public void makeOrUpdatePlot() { // color data 
			/*
			 * replot everything; to be changed in the future
			 *
			 */
			clearAllChildNodesFromSVGGElement(layer);
			deleteChildNodeFromLayerTree(layer);
 
			if (this.active && !disable_all_dataset && chart_plots_enabled) {
				layer_tree.appendChild(layer);
				layer.setAttribute("fill-opacity", Float.toString(this.opacity));
				/*
				 * >>>>>>>>>>>> SOME GLOBAL VARIABLES >>>>>>>>>>>
				 */
				float mostright = currentMostRightPosition + space_between_datasets; 
 
				// width for eash shape
				float widthPerShape = this.plotWidth / this.numericDataContainer.getGroupsCount() ; 
				// by default take 80 percent of the width 
				float widthPerShape4plot = widthPerShape * 0.8f;
				float pixelWidthHeight = numericDataContainer.getItemPixelHeight(0);
 
				// <<<<<<<<<<<<<<<<< GLOBAL VARIABLES END HERE <<<<<<<<<<<<<<

				// iterate hashPositionOfLeafNodes
				for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
					final String leaf_internal_id = entry.getKey();
					final APosition startpos = entry.getValue();
					float angle = startpos.getAngle();

					ArrayList<String> colors =  this.numericDataContainer.getRow2color().get(leaf_internal_id);
					ArrayList<Float> data =  this.numericDataContainer.getRow2data().get(leaf_internal_id);
					if(colors == null || colors.isEmpty()) {
						continue;
					}

					// plot one column for each plottypes
					for (int idx = 0; idx < Math.min(colors.size(),data.size()); idx++) {
						String color = colors.get(idx);
						Float dataValue = data.get(idx);

						/**
						 * -- May 30, 2013 --
						 * start pos is at the center where the shape shoud be plotted 
						 */
						float start_pos = mostright + idx * (widthPerShape + space_between_columns_within_a_dataset); 

						OMSVGTransform r = null;
						// if circular mode and angle is not zero
						if (angle != 0 || plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
							r = svg.createSVGTransform();
							r.setRotate(180 - angle, rootXY.getX(), rootXY.getY()); // March 22, 2011; this rotation is correct ??? 
						} // May 17, 2011

						 
						OMSVGRectElement rect = doc.createSVGRectElement(
								start_pos + (widthPerShape - widthPerShape4plot) / 2,
								startpos.getY() - pixelWidthHeight / 2,
								pixelWidthHeight, pixelWidthHeight, 0, 0);
						rect.setAttribute("fill", color);
						rect.setAttribute("stroke", "none"); 
						if (r != null) {
							rect.getTransform().getBaseVal().appendItem(r);
						}
						layer.appendChild(rect);

						OMSVGTextElement item_text = doc.createSVGTextElement(
								start_pos + (widthPerShape - widthPerShape4plot) / 2 + 0.1f*widthPerShape,
								startpos.getY()+ 0.2f*widthPerShape,
								OMSVGLength.SVG_LENGTHTYPE_PX, 
								Integer.toString(dataValue.intValue()));
						// text is 80% of the shape size 
						//item_text.getStyle().setFontSize(widthPerShape*.7, Unit.PX);
						item_text.getStyle().setFontSize(this.numericDataContainer.getLabelHeight(), Unit.PX); 
						if (r != null) {
							item_text.getTransform().getBaseVal().appendItem(r);
						}
						layer.appendChild(item_text);

					} // plot a column for each plottype
				} // iterate hashPositionOfLeafNodes

				// at the end, update currentMostRightPosition; March 25, 2011;
				currentMostRightPosition +=  this.plotWidth  + space_between_datasets;
			} // is current chart is active
		}// makeOrUpdatePlot 
	}
	
	
	
	/*
	 * April 7, 2011; add opacity support March 23, 2011; bars, can be multiple
	 * column the number of columns is controlled by #groups for example,
	 * #groups small,middle,large creates three columns or #groups cat1,cat2
	 * creates two columns
	 *
	 * if #groups is missing, one column is assumed
	 *
	 * color for each column is controlled by #colors, for example #colors
	 * green,red,blue will color the the three columns green, red, blue,
	 * respectively. if there are fewer colors than #groups, the colors are
	 * recycled in the standard fashion;
	 *
	 * if #color is missing, blue (the default color) is assumed
	 *
	 * other modifiers: #alignIndividualColumn or simply #align; if presents,
	 * individual columns will be aligned, otherwise the multiple-column will be
	 * stacked; #fanplot or simply #fan; if presents, bars will be plotted as
	 * fans in circular modes #showvalue or #shownumbers ; if true, values of
	 * each bar will be plotted at the far end of the corresponding bar #axis;
	 * if true, axis will be plotted; #grid; if true, grid (similar to the grid
	 * in R) will be plotted or #grid value;
	 *
	 *
	 * NOTE: this class will use the same parser as piechart
	 *
	 * an example of user-input data
	 *
	 * @ annotation lines will be omitted @ annotation continues ...
	 *
	 * #Groups	group1,group2,group3 #Colors	#95CBE9,#BD2031,#2C5700 #other
	 * modifiers specifies here A	2,2,4 B	2,2,4 C	2,8,4 D	10,8,4
	 *
	 * May 26, 2011; add tooltips for bar plots
	 *
	 */
	public final class Bars extends PlotBase {

		private NumericMatrixDataContainer numericDataContainer = null;
		private float plot_width_per_subpanel = 20f;
		private OMSVGGElement layer_barplot_grid = doc.createSVGGElement();
		private OMSVGGElement layer_barplot_gridlabels = doc.createSVGGElement();
		private ArrayList<Float> maxValuePerColumn = new ArrayList<Float>();
		HashMap<String, ArrayList<Float>> row2data = new HashMap<String, ArrayList<Float>>();
		float fangle = 0f, pwidth_per_value = 0f;

		// constructor
		public Bars() {
			this.defaultColor = "white";
			this.plotWidth = 40;
		}

		public Bars(int datasetID, NumericMatrixDataContainer mdatCon) {
			this.plotWidth = 40f; // shit
			this.defaultColor = "white";
			this.setNumericDataSet(datasetID, mdatCon);
		}

		public void setNumericDataSet(int newID, NumericMatrixDataContainer mdatCon) {
			this.numericDataContainer = mdatCon;
			this.datasetID = newID;
			this.setActive(true);
			this.groups = numericDataContainer.getGroups();
			this.colors = numericDataContainer.getColors();
			this.row2data = numericDataContainer.getTreeNodeID2Data(); // 
			this.datasetContentAsString = mdatCon.getOriginal_datastring(); // get original user input as String ; April 5, 2011;
			this.opacity = numericDataContainer.getOpacity();
			//this.barheightPCT = numericDataContainer.getBarheightPCT();

			HashMap<String, ArrayList<Float>> column2data = numericDataContainer.getColumn2Data();
			for (int sidx = 0; sidx < groups.size(); sidx++) {
				maxValuePerColumn.add(Collections.max(column2data.get(groups.get(sidx))));
			}

			/*
			 * April 7, 2011; allow user to specify plot width
			 */
			this.plotWidth = numericDataContainer.getPlotwidth();
			this.plot_width_per_subpanel = (this.plotWidth - (this.groups.size() - 1) * space_between_columns_within_a_dataset) / this.groups.size();

			// calculate other global parameters 
			pwidth_per_value = (numericDataContainer.isAlignindividualcolumn())
					? (float) this.plot_width_per_subpanel / ((float) (numericDataContainer.getMaxValue() - 0f))
					: (float) this.plotWidth / ((float) (Collections.max(numericDataContainer.getID2Sum().values()) - 0f));
		}

		/*
		 * override the original defination in the base class at the moment,
		 * everytime this function is called, everything will be replotted;
		 * March 24, 2011 
		 * -- May 17, 2011 -- 
		 * fix a bug in barplot/ circular mode, the bar isn't draw correctly somehow 
		 * 
		 * -- Apr 24, 2012 -- 
		 *   - refine grid and axis 
		 *   - add numbers to grid
		 *  
		 * -- May 30, 2013 --
		 *   - add support for !itemHeightPCT and !itemHeightPX 
		 */
		@Override
		public void makeOrUpdatePlot() { // BAR

			if (this.active && !disable_all_dataset && chart_plots_enabled) {
				float mostright = currentMostRightPosition + space_between_datasets;
				/**
				 * -- Apr 28, 2013 --
				 *  recalculate pheight at the begining of the replot
				 * -- May 30, 2013 --
				 *  recalculate itemHeightPixel at the begining of the replot
				 *  two values should be prepared, one for circular mode, the other for normal mode 
				 */
				float itemHeightPixel = (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) ? numericDataContainer
						.getItemPixelHeight((float) (mostright * 2f * Math.PI / 360f * angle_span / (float) LeafInternalID2NodePosisionts.size())) : numericDataContainer
						.getItemPixelHeight(pxPerHeight);

				// a hard limit ??? --
				if (itemHeightPixel > 40) {
					itemHeightPixel = 40;
				}
				// 
				fangle = (float) (Math.atan((itemHeightPixel / 2f) / (mostright - rootXY.getX())) / Math.PI * 180); // if bars are plotted as fans in circular mode

				/*
				 * deal with grid and axis, if available
				 */
				boolean makegrid = false;
				if (numericDataContainer.isPlotgrid()) {
					clearAllChildNodesFromSVGGElement(layer_barplot_grid);

					layer_tree.appendChild(layer_barplot_grid);
					layer_barplot_grid.setAttribute("stroke-width", "0.5");
					layer_barplot_grid.setAttribute("stroke", "grey");
					layer_barplot_grid.setAttribute("stroke-dasharray", "5,5"); // Apr 25, 2012; now all have this dash 
					layer_barplot_grid.setAttribute("fill", "none"); // for path only

					makegrid = true;
				} else {
					deleteChildNodeFromLayerTree(layer_barplot_grid);
				}

				// grid axis; grid labels 
				if (numericDataContainer.isShowGridLabel()) {
					clearAllChildNodesFromSVGGElement(layer_barplot_gridlabels);

					layer_tree.appendChild(layer_barplot_gridlabels);

					makegrid = true;
				} else {
					deleteChildNodeFromLayerTree(layer_barplot_gridlabels);
				}

				if (makegrid) {

					/*
					 * == Apr 25, 2012 == streamline codes here
					 */
					float maxvalue;
					int gridlineCount, groupSize;
					if (numericDataContainer.isAlignindividualcolumn()) {
						maxvalue = numericDataContainer.getMaxValue();
						gridlineCount = 4;
						groupSize = groups.size();
					} else {
						maxvalue = Collections.max(numericDataContainer.getID2Sum().values());
						gridlineCount = 5;
						groupSize = 1;
					}

					drawGridLines(groupSize, gridlineCount, maxvalue, mostright, pwidth_per_value, pxPerHeight, plot_width_per_subpanel, layer_barplot_grid,
							layer_barplot_gridlabels);

				} // make grid
					// <<<<<<<<<<< grid and axis <<<<<<<<<<<

				/*
				 * >>>>>>>>>> the main layer >>>>>>>>>>>>>
				 */
				layer_tree.appendChild(layer);
				clearAllChildNodesFromSVGGElement(layer);
				layer.setAttribute("fill-opacity", Float.toString(this.opacity));

				// iterate hashPositionOfLeafNodes
				for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
					final String leaf_internal_id = entry.getKey();
					final APosition startpos = entry.getValue();
					float angle = startpos.getAngle();
					if (row2data.containsKey(leaf_internal_id)) {
						final ArrayList<Float> cdata = row2data.get(leaf_internal_id);

						float start_pos = mostright; // it'll be changed at the end of the for loop

						/*
						 * May 26, 2011; each row of data now will be added into
						 * a SVGG element -- then create a tooltip for each row
						 * of data
						 */
						final OMSVGGElement rowplot = doc.createSVGGElement();
						layer.appendChild(rowplot);

						// get external id
						final String externalID = (phylotree != null) ? phylotree.getExternalIDbyInternalID(leaf_internal_id) : "";
 
						// mouse event for Leaf selection ...
						//
//						addLeafSelectionHandler(rowplot);
						
						
						rowplot.addMouseOverHandler(new MouseOverHandler() {

							@Override
							public void onMouseOver(final MouseOverEvent event) {
								oeelement_mouse_over = rowplot;
								tooltipx = event.getClientX();
								tooltipy = event.getClientY();

								// >>>> TIMER >>>>
								new Timer() {

									@Override
									public void run() {
										if (oeelement_mouse_over != null && oeelement_mouse_over.equals(rowplot)) { // if mouse is still on the same element as delay_pshow mili sec before
											FlexTable tooltipflex = makeFlextableForToolTip(colors, cdata, groups, externalID);
											tooltipWidget.setWidgetAndShow(tooltipflex, tooltipx, tooltipy);
										}
									}
								}.schedule(delayShowMiliSec);
							}
						});

						rowplot.addMouseOutHandler(new MouseOutHandler() {

							@Override
							public void onMouseOut(MouseOutEvent event) {
								oeelement_mouse_over = null;
								// >>>> TIMER >>>>
								new Timer() {

									@Override
									public void run() {
										if (oeelement_mouse_over == null && !tooltipWidget.getMouseOver()) { // if mouse is not on any evelement and not on popup panel, close the panel
											tooltipWidget.hide();
										}
									}
								}.schedule(delayHideMiliSec);
							}
						});
						// <<<<< may 26, 2011; <<<<<<<

						// iterate data for current node
						for (int idx = 0; idx < groups.size(); idx++) {
							// March 22, 2011; if array colors is shorter than plottypes, colors are recycled 
							String color = (colors != null) ? (colors.size() > idx) ? colors.get(idx) : colors.get(idx % colors.size()) : this.defaultColor;
							float barwidth = (cdata.get(idx) - 0) * pwidth_per_value;
							// check different plot types
							if (!color.equalsIgnoreCase("white")) {

								// if numericDataContainer.isFanplot is true and is circular mode
								if (numericDataContainer.isFanplot() && (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM))) {
									float innerRadius = start_pos - rootXY.getX();
									float outterRadius = innerRadius + barwidth;

									OMSVGPathElement fan = makeFanPlot(rootXY, innerRadius, outterRadius, fangle * 2); // may 30, 2013; 
									fan.setAttribute("stroke", "none");
									fan.setAttribute("fill", color);
									/*
									 * make rotate transform for fan
									 */
									// Feb 12, 2012 
									if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
										OMSVGTransform fanr = svg.createSVGTransform();
										fanr.setRotate(-angle, rootXY.getX(), rootXY.getY()); // March 21, 2011; this rotation is correct
										fan.getTransform().getBaseVal().appendItem(fanr);
									}
									//layer.appendChild(fan);
									rowplot.appendChild(fan); // May 26, 2011
								} else {
									OMSVGRectElement rect = doc.createSVGRectElement(start_pos, startpos.getY() - itemHeightPixel / 2f, barwidth, itemHeightPixel, 0, 0);
									rect.setAttribute("stoke", "none");
									rect.setAttribute("fill", color);

									if (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM) || angle != 0) {
										// if circular mode and angle is not zero; May 17, 2011; this part was moved from the begining of the outter 'for' loop, otherwise the barplots are not shown correctly
										OMSVGTransform r = null;
										if (angle != 0 || plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
											r = svg.createSVGTransform();
											r.setRotate(180 - angle, rootXY.getX(), rootXY.getY()); // March 22, 2011; this rotation is correct ??? 
										}
										rect.getTransform().getBaseVal().appendItem(r);
									}
									// layer.appendChild(rect);
									rowplot.appendChild(rect); // May 26, 2011
								} // if circular mode
							} // if color isn't default color
								// increase start_pos incremetally; March 24, 2011;
							start_pos += (numericDataContainer.isAlignindividualcolumn()) ? (this.plot_width_per_subpanel + space_between_columns_within_a_dataset) : barwidth;
						} // multi column plot 
					} // if data is valid for current leaf node

				} // iterate hashPositionOfLeafNodes

				// at the end, update currentMostRightPosition
				float plotwidth = numericDataContainer.isAlignindividualcolumn() ? numericDataContainer.getGroups().size() * this.plot_width_per_subpanel
						+ (numericDataContainer.getGroups().size() - 1) * space_between_columns_within_a_dataset : this.plotWidth;
				currentMostRightPosition += (plotwidth + space_between_datasets);
			} else {
				deleteChildNodeFromLayerTree(layer);
				deleteChildNodeFromLayerTree(layer_barplot_gridlabels);
				deleteChildNodeFromLayerTree(layer_barplot_grid);
			} // is active
		} // makeorupdate plot

		 
	}

	/**
	 * ***************************************************
	 * May 13, 2012; class for protein domains
	 * 
	 * -- may 31, 2013 --
	 *  
	 * ****************************************************
	 */
	public final class ProteinDomains extends PlotBase {

		private ProteinDomainDataContainerParser dataset = null;
		private OMSVGGElement layer_grid = doc.createSVGGElement();
		private OMSVGGElement layer_axis = doc.createSVGGElement();
		private final int default_plot_width = 100;
		private float pwidth_per_value = 0f;

		// constructor
		public ProteinDomains() {
			this.defaultColor = "white";
			this.plotWidth = default_plot_width;
		}

		public ProteinDomains(int datasetID, ProteinDomainDataContainerParser newdataset) {
			this.plotWidth = default_plot_width; // shit
			this.defaultColor = "white";
			this.setProteinDomainDataSet(datasetID, newdataset);
		}

		public void setProteinDomainDataSet(int newID, ProteinDomainDataContainerParser newdataset) {
			this.dataset = newdataset;
			this.datasetID = newID;
			this.setActive(true);
			this.groups = dataset.getGroups();
			this.colors = dataset.getColors();
			this.datasetContentAsString = newdataset.getOriginal_datastring(); // get original user input as String ; April 5, 2011;
			this.opacity = dataset.getOpacity();

			this.plotWidth = dataset.getPlotwidth();

			// calculate other global parameters 
			pwidth_per_value = (float) this.plotWidth / (float) dataset.getMaxProteinLength();
		}

		/*
		 * override the original defination in the base class at the moment,
		 * everytime this function is called, everything will be replotted;
		 */
		@Override
		public void makeOrUpdatePlot() { // PROTEIN DOMAIN
			if (this.active && !disable_all_dataset && chart_plots_enabled) {
				float mostright = currentMostRightPosition + space_between_datasets;

				/*
				 * deal with grid and axis, if available
				 */
				boolean makegrid = false;
				if (dataset.isPlotgrid()) {
					clearAllChildNodesFromSVGGElement(layer_grid);

					layer_tree.appendChild(layer_grid);
					layer_grid.setAttribute("stroke-width", "0.5");
					layer_grid.setAttribute("stroke", "grey");
					layer_grid.setAttribute("stroke-dasharray", "5,5"); // Apr 25, 2012; now all have this dash 
					layer_grid.setAttribute("fill", "none"); // for path only

					makegrid = true;
				} else {
					deleteChildNodeFromLayerTree(layer_grid);
				}

				// grid axis; grid labels 
				if (dataset.isShowGridLabel()) {
					clearAllChildNodesFromSVGGElement(layer_axis);
					layer_tree.appendChild(layer_axis);
					makegrid = true;
				} else {
					deleteChildNodeFromLayerTree(layer_axis);
				}

				if (makegrid) {
					drawGridLines(1, 5, dataset.getMaxProteinLength(), mostright, pwidth_per_value, pxPerHeight, 0, layer_grid, layer_axis);
				} // make grid
					// <<<<<<<<<<< grid and axis <<<<<<<<<<<

				/*
				 * >>>>>>>>>> the main layer >>>>>>>>>>>>>
				 */
				layer_tree.appendChild(layer);
				clearAllChildNodesFromSVGGElement(layer);
				layer.setAttribute("fill-opacity", Float.toString(this.opacity));

				/*
				 * ********************************************
				 * May 14, 2012: get plot height here height refers to height
				 * per protein element therefore it cannot be higher than
				 * pxPerHeight ********************************************
				 */
				float plotheightPerProtein = (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) ? dataset
						.getItemPixelHeight((float) (mostright * 2f * Math.PI / 360f * angle_span / (float) LeafInternalID2NodePosisionts.size())) : dataset
						.getItemPixelHeight(pxPerHeight);

				// a hard limit ??? --
				if (plotheightPerProtein > 40) {
					plotheightPerProtein = 40;
				}

				boolean showShadow = dataset.isShowShadow();

				/**
				 * ******************************************
				 * Plot domain architecture for all proteins iterate
				 * hashPositionOfLeafNodes
				 ******************************************
				 */
				for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
					String leaf_internal_id = entry.getKey();
					APosition startpos = entry.getValue();
					float angle = startpos.getAngle();
					String leaf_external_id = phylotree.getExternalIDbyInternalID(leaf_internal_id);
					DomainsOfAProtein domains = dataset.getDomainsByGeneID(leaf_external_id);

					/**
					 * *************************************
					 * plot domains for a protein
					 * **************************************
					 */
					//--  each protein will have its own svgg group --
					OMSVGGElement domainchart = doc.createSVGGElement();
					layer.appendChild(domainchart);

					// ==== first of all, plot a horizontal line representing the protein (length) ====
					int proteinlength = domains.getProtein_length();
					if (proteinlength > 0) {
						OMSVGLineElement line = doc.createSVGLineElement(mostright, startpos.getY(), mostright + proteinlength * pwidth_per_value, startpos.getY());
						line.setAttribute("stroke", "black");
						line.setAttribute("stroke-width", "0.5px");
						domainchart.appendChild(line);
					}

					// ==== plot domains ====
					for (final ProteinDomain pd : domains.getDomains()) {
						final OMSVGRectElement rect = doc.createSVGRectElement(mostright + pwidth_per_value * pd.getStart(), startpos.getY() - plotheightPerProtein / 2,
								pwidth_per_value * (pd.getStop() - pd.getStart()), plotheightPerProtein, 0, 0);
						rect.setAttribute("stroke", "none");
						rect.setAttribute("fill", pd.getColor());
						// add drop shadow if datasource is not pfam-B
						if (!pd.isPfamB() && showShadow) {
							rect.getStyle().setSVGProperty(SVGConstants.SVG_FILTER_ATTRIBUTE, DOMHelper.toUrl("dropshadow"));
						}
						domainchart.appendChild(rect);


						// mouse event for Leaf selection ...
						//
//						addLeafSelectionHandler(rect);
						
						
						/**
						 * **********************************************
						 * tooltip for each domain
						 * **********************************************
						 */
						rect.addMouseOverHandler(new MouseOverHandler() {

							@Override
							public void onMouseOver(final MouseOverEvent event) {
								oeelement_mouse_over = rect;
								tooltipx = event.getClientX();
								tooltipy = event.getClientY();

								// >>>> TIMER >>>>
								new Timer() {

									@Override
									public void run() {
										if (oeelement_mouse_over != null && oeelement_mouse_over.equals(rect)) { // if mouse is still on the same element as delay_pshow mili sec before
											FlexTable tooltipflex = makeToolTipFlextableForDomain(pd);
											tooltipWidget.setWidgetAndShow(tooltipflex, tooltipx, tooltipy);
										}
									}
								}.schedule(delayShowMiliSec);
							}
						});

						rect.addMouseOutHandler(new MouseOutHandler() {

							@Override
							public void onMouseOut(MouseOutEvent event) {
								oeelement_mouse_over = null;
								// >>>> TIMER >>>>
								new Timer() {

									@Override
									public void run() {
										if (oeelement_mouse_over == null && !tooltipWidget.getMouseOver()) { // if mouse is not on any evelement and not on popup panel, close the panel
											tooltipWidget.hide();
										}
									}
								}.schedule(delayHideMiliSec);
							}
						});
					}

					// rorate the whole domain it if necessary
					if (angle != 0) {
						// and then rotate
						OMSVGTransform r = svg.createSVGTransform();
						r.setRotate(180 - angle, rootXY.getX(), rootXY.getY());
						domainchart.getTransform().getBaseVal().appendItem(r);
					} // circular
				}// iterate hashPositionOfLeafNodes
					// at the end, update currentMostRightPosition
				currentMostRightPosition += (plotWidth + space_between_datasets);
			} else {
				deleteChildNodeFromLayerTree(layer);
				deleteChildNodeFromLayerTree(layer_axis);
				deleteChildNodeFromLayerTree(layer_grid);
			} // is active
		} // makeorupdate plot

		/**
		 * *****************************************
		 * created on : May 14, 2012 last modified: May 15, 2012
		 * *****************************************
		 */
		private FlexTable makeToolTipFlextableForDomain(ProteinDomain pd) {

			FlexTable f = new FlexTable();
			f.setCellSpacing(flexTableCellSpacing); // cell spacing 

			int idx = 0;
			// from to
			f.setWidget(idx, 0, new HTML("<b>Location:</b>"));
			f.setWidget(idx, 1, new HTML(pd.getStart() + " - " + pd.getStop()));

			// node id:
			idx++;
			f.setWidget(idx, 0, new HTML("<b>Domain Name:</b>"));
			if (pd.isPfamB()) {
				f.setWidget(idx, 1, new HTML("<a title=\"link to PFAM\"  href=\"http://pfam.sanger.ac.uk/pfamb/" + pd.getName() + "\" target=\"_blank\">" + pd.getName() + "</a>"));
			} else {
				f.setWidget(idx, 1, new HTML("<a title=\"link to PFAM\"  href=\"http://pfam.sanger.ac.uk/family/" + pd.getName() + "\" target=\"_blank\">" + pd.getName() + "</a>"));
			}

			// accession; fix a bug here Oct 25, 2013 
			if (!pd.getAccession().isEmpty()) {
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Accession:</b>"));
				if (pd.isPfamB()) {
					f.setWidget(idx, 1, new HTML("<a title=\"link to PFAM\" href=\"http://pfam.sanger.ac.uk/pfamb/" + pd.getAccession() + "\" target=\"_blank\">" + pd.getAccession()
							+ "</a>"));
				} else {
					f.setWidget(idx, 1, new HTML("<a title=\"link to PFAM\"  href=\"http://pfam.sanger.ac.uk/family/" + pd.getAccession() + "\" target=\"_blank\">" + pd.getAccession()
							+ "</a>"));
				}
			}
			
			// source
			if (!pd.getSource().isEmpty()) {
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Source:</b>"));
				f.setWidget(idx, 1, new HTML(pd.getSource()));
			}

			// evalue
			if (!pd.getEvalue().isEmpty()) {
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Evalue:</b>"));
				f.setWidget(idx, 1, new HTML(pd.getEvalue()));
			}

			// bitscore
			if (!pd.getBitscore().isEmpty()) {
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Bitscore:</b>"));
				f.setWidget(idx, 1, new HTML(pd.getBitscore()));
			}
			return f;
		}
	}// protein domains

	/*
	 * April 8, 2011; clean up; done March 17, 2011; make tree skeleton a
	 * seperate class
	 *
	 * this class takes care of tree skeleton + leaf label, colors of branch,
	 * leaf and background of leaf it'll also update positions for plotting leaf
	 * labels, branch lengths and bootstrap scores
	 *
	 */
	public class TreeSkeleton {
		/*
		 * branch / leaf label / leaf backgroud / colors
		 */

		private boolean leafColorEnabled = true, leafBKColorEnabled = true, branchColorEnabled = true, // April 4, 2011; by default they are all true

				alignLeafLabels = false; // Align leaf label
		private int activeLeafColorSetID = -1, activeLeafBKColorSetID = -1, activeBranchColorSetID = -1;
		/*
		 * April 4, 2011; this hash will replace colorsetIDs NOTE: here
		 * datasetID should be type + actualID, for example, a branch color
		 * dataset with name 'set1' should be writen as BRANCHCOLORset1 this way
		 */
		private HashMap<Integer, String> dataset2originalUserinputAsString = new HashMap<Integer, String>();
		private HashMap<Integer, Float> dataset2opacity = new HashMap<Integer, Float>(); // // April 8, 2011; dataset opacity
		/*
		 * use this to store SVG elements such as lines/ texts/ and paths to
		 * modified them later NOTE: all IDs are internal ID
		 */
		private HashMap<String, OMNode> leafID2OMNode = new HashMap<String, OMNode>(), 
				branchID2OMNode = new HashMap<String, OMNode>(),
				bootstrapID2OMNode = new HashMap<String, OMNode>(), 
				leafID2LeafAlignOMNode = new HashMap<String, OMNode>();

		// a constructor
		public TreeSkeleton() {
		}

		/*
		 * public methods
		 */
		/*
		 * make tree skeleton and update positions for plotting branchlength,
		 * bootstrap scores and leaf labels April 8, 2011; cleanup and add
		 * opacity
		 */

		public void makeTreeSkeleton() {
			/**
			 * oct 26, 2013, also clear all hashMap variables to make a fresh start 
			 * TODO: the reason that i made these variables is to make the rending faster, however, if 
			 * i have to clear them everytime, they would not be helpful anymore ...  
			 */
			this.leafID2OMNode.clear();
			this.branchID2OMNode.clear();
			this.bootstrapID2OMNode.clear();
			this.leafID2LeafAlignOMNode.clear();
			
			/*
			 * replot all Jan 23, 2011;
			 * oct 26, 2013: also clear tree_branch length and layer_tree_bootstrap
			 */
			clearAllChildNodesFromSVGGElement(layer_tree_skeleton); // remove all child nodes in a SVGG element
			clearAllChildNodesFromSVGGElement(layer_tree_branchlength);
			clearAllChildNodesFromSVGGElement(layer_tree_bootstrap);
			clearAllChildNodesFromSVGGElement( layer_tree_align_leaf_labels ); // Nov 2, 2013 

			// April 8, 2011; opacity 
			float opacity = this.dataset2opacity.containsKey(this.activeBranchColorSetID) ? dataset2opacity.get(this.activeBranchColorSetID) : 1;
			layer_tree_skeleton.setAttribute("fill-opacity", Float.toString(opacity)); // change it 
			// get branch color for root node; only used when plot-mode is RECT
			String branch_stroke_color = (this.branchColorEnabled && !disable_all_dataset ) ? rootnode
					.getBranchColorByColorsetID(this.activeBranchColorSetID) : "black";
			if (plotmode == treePlotMode.RECT_CLADOGRAM || plotmode == treePlotMode.RECT_PHYLOGRAM) {
				OMSVGLineElement line = doc.createSVGLineElement(rootXY.getX(), rootXY.getY(), plotmode == treePlotMode.RECT_PHYLOGRAM
						? rootXY.getX() - pxPerBranchLength / 10 / 5
						: rootXY.getX() - pxPerWidthCurrentMode / 3, rootXY.getY());
				line.setAttribute("stroke", branch_stroke_color);

				layer_tree_skeleton.appendChild(line);
				this.rect_mode(rootnode, layer_tree_skeleton, rootXY);
			} else if (plotmode == treePlotMode.SLANTED_CLADOGRAM_RECT || plotmode == treePlotMode.SLANTED_CLADOGRAM_MIDDLE || plotmode == treePlotMode.SLANTED_CLADOGRAM_NORMAL) {
				// plotmodes other than slanted_normal are not supported at the moment; jan 20, 2011;
				float x_first_leaf = rootnode.getLevelHorizontal() * pxPerWidthCurrentMode + rootXY.getX();
				//float y_first_leaf = 1 * pxPerHeight + margin_normal * canvas_height;
				float y_first_leaf = 1 * pxPerHeight + margin_y;
				double atan = Math.atan((rootXY.getY() - y_first_leaf) / (x_first_leaf - rootXY.getX()));
				this.slanted_mode(rootnode, layer_tree_skeleton, rootXY, atan, x_first_leaf);
			} else if (plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
				this.circular_mode(rootnode, layer_tree_skeleton, rootXY);
			}
		} // makeTreeSkeleton

		/*
		 * April 8, 2011; cleanup and add opacity including bootstrap scores,
		 * branch length and tree leaf labels == April 23-24, 2012 == change the
		 * orientation of leaf/branch-length/bootstrap score labels for better
		 * readability
		 */
		public void makeLeafLabels() {

			// clear all 
			clearAllChildNodesFromSVGGElement(layer_tree_leaf_labels);
			leafID2OMNode.clear();

			/*
			 * part 1, leaf label; update or make NOTE: all keys in hashes are
			 * internal IDs March 20, 2011; use native javascript to calculate
			 * pixel width and height; much more accurate March 20, 2011; add
			 * align leaf lables; only availale if plotmode = PHYLOGRAM
			 */
			boolean bAlignleaflable = (this.alignLeafLabels == true && (plotmode.equals(treePlotMode.RECT_PHYLOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)))
					? true
					: false;
			// April 8, 2011; opacity 
			float opacity = this.dataset2opacity.containsKey(this.activeLeafColorSetID) ? dataset2opacity.get(this.activeLeafColorSetID) : 1;
			layer_tree_leaf_labels.setAttribute("fill-opacity", Float.toString(opacity)); // change it 
			layer_tree_leaf_labels.getStyle().setFontSize(default_font_size, Unit.PX);// Oct 26, 2011; user default font size at the beginning
			/*
			 * March 20, 2011; keep tracking of the right most leaf label
			 * positions if alignLeafLabels is true, add
			 * layer_tree_align_leaf_labels to layer_tree, else remove it from
			 * layer tree
			 */
			JsArrayInteger stringPixelWidthHeight = JSFuncs.stringPixelWidthHeight("Anystring", default_font_family, default_font_size);
			float fontPxHeight = stringPixelWidthHeight.get(1);

			// if alignLeafLabels, get the right most positions for treeskeleton and leaf labels
			if (bAlignleaflable) {
				layer_tree.appendChild(layer_tree_align_leaf_labels);
			} else {
				//clearAllChildNodesFromSVGGElement(layer_tree_align_leaf_labels);
				deleteChildNodeFromLayerTree(layer_tree_align_leaf_labels);
			} // if alignLeafLabels is true or false

			// 0 = treeskeleton; 1 = leaf
			ArrayList<Float> mostRightPositions = this.getRightMostTreeAndLabelPosisions();
			float mostrightTree = mostRightPositions.get(0);
			// Iterate leaf node positions
			for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
				final String leaf_internal_id = entry.getKey();
				final APosition pos = entry.getValue();

				PhyloNode leafnode = phylotree.getNodeByID(leaf_internal_id);
				String leaf_id = leafnode.getID();

				// if OMNode objects for leaf label not exist; make a SVGtext object and put it at (0,0)
				String leaf_id_without_underline = JSFuncs.JsReplaceUnderlineWithSingleSpaceChar(leaf_id);
				OMSVGTextElement txtLeafLabel = doc.createSVGTextElement(0, 0, OMSVGLength.SVG_LENGTHTYPE_PX, leaf_id_without_underline);
				txtLeafLabel.setId(leaf_id);
				// add it to leafID2OMNode as well as tree_leaf_node layer
				layer_tree_leaf_labels.appendChild(txtLeafLabel);
				leafID2OMNode.put(leaf_internal_id, txtLeafLabel);
				layer_tree_leaf_labels.getTransform().getBaseVal().consolidate();

				//  update position / angle; 
				float x = (bAlignleaflable) ? mostrightTree + space_between_treeskeleton_and_leaflabels : pos.getX() + space_between_treeskeleton_and_leaflabels;
				float y = pos.getY() + fontPxHeight / 4;
				float angle = pos.getAngle();

				// now move the text to intented area --
				txtLeafLabel.setAttribute("x", Float.toString(x));
				txtLeafLabel.setAttribute("y", Float.toString(y));

				if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {

					// and then rotate
					OMSVGTransform r = svg.createSVGTransform();
					r.setRotate(180 - angle, rootXY.getX(), rootXY.getY());
					txtLeafLabel.getTransform().getBaseVal().appendItem(r);

					// Apr 24, 2012
					betterReadabilityForSVGTextElem(txtLeafLabel, angle, x, y - fontPxHeight / 4, "end");
				}

				// update color
				String textfillcolor = (leafColorEnabled && !disable_all_dataset)
						? leafnode.getLeafColorByColorsetID(activeLeafColorSetID)
						: "black";
						txtLeafLabel.setAttribute("fill", textfillcolor); 

				// initLeaf selection ...
				//  
				if( phylotree.getLeafSelectionSet().contains(leaf_id) ) { 
					txtLeafLabel.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE, LEAF_SELECTED_STYLE);
					txtLeafLabel.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE2, LEAF_SELECTED_STYLE2);
					txtLeafLabel.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE3, LEAF_SELECTED_STYLE3);
				}
				addLeafSelectionHandler(txtLeafLabel);
				
				// March 20, 2011; Align leaf node 
				if (bAlignleaflable) {
					// if OMNode objects for leaf label not exist; make a SVGtext object and put it at (0,0)
					if (!leafID2LeafAlignOMNode.containsKey(leaf_internal_id)) {
						OMSVGLineElement line = doc.createSVGLineElement(pos.getX() + space_between_treeskeleton_and_leaflabels / 2, pos.getY(), mostrightTree
								+ space_between_treeskeleton_and_leaflabels / 2, pos.getY());
						// add it to leafID2OMNode as well as tree_leaf_node layer
						leafID2LeafAlignOMNode.put(leaf_internal_id, line);
						layer_tree_align_leaf_labels.appendChild(line);
					}
					/*
					 * update position of aligned lines
					 */
					OMSVGLineElement line = (OMSVGLineElement) leafID2LeafAlignOMNode.get(leaf_internal_id);
					line.setAttribute("x1", Float.toString(pos.getX() + space_between_treeskeleton_and_leaflabels / 2));
					line.setAttribute("y1", Float.toString(pos.getY()));
					line.setAttribute("x2", Float.toString(mostrightTree + space_between_treeskeleton_and_leaflabels / 2));
					line.setAttribute("y2", Float.toString(pos.getY()));

					OMSVGTransform liner = svg.createSVGTransform();
					liner.setRotate(180 - angle, rootXY.getX(), rootXY.getY());

					line.getTransform().getBaseVal().clear();
					if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
						line.getTransform().getBaseVal().appendItem(liner);
					}
				} // if align leaf labels
			} // part 1 leaf label ends here
			/*
			 * part 2, bootstrap scores; update or make
			 */
			if (phylotree.hasBootstrapScores()) {
				for (Map.Entry<String, APosition> entry : BootstrapPositions.entrySet()) {
					String node_internal_id = entry.getKey();
					APosition pos = entry.getValue();
					PhyloNode node = phylotree.getNodeByID(node_internal_id);
					String bootstrapstring = formatterDeci4.format(node.getBootStrap());
					// if OMNode objects for leaf label not exist; make a SVGtext object and put it at (0,0)
					if (!this.bootstrapID2OMNode.containsKey(node_internal_id)) {
						OMSVGTextElement text = doc.createSVGTextElement(0, 0, OMSVGLength.SVG_LENGTHTYPE_PX, bootstrapstring);
						// add it to leafID2OMNode as well as tree_leaf_node layer
						this.bootstrapID2OMNode.put(node_internal_id, text);
						layer_tree_bootstrap.appendChild(text);
					}

					// update position
					OMSVGTextElement txtBootstrapScore = (OMSVGTextElement) this.bootstrapID2OMNode.get(node_internal_id);
					// March 20, 2011;
					JsArrayInteger strPxWidthHeight = JSFuncs.stringPixelWidthHeight(bootstrapstring, "arial", default_bootstrap_font_size);
					float x = pos.getX() + 3;
					float y = pos.getY() + strPxWidthHeight.get(1) / 3;
					float angle = pos.getAngle();

					OMSVGTransform r = svg.createSVGTransform();
					r.setRotate(180 - angle, rootXY.getX(), rootXY.getY());

					txtBootstrapScore.setAttribute("x", Float.toString(x));
					txtBootstrapScore.setAttribute("y", Float.toString(y));

					txtBootstrapScore.getTransform().getBaseVal().clear();
					if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
						txtBootstrapScore.getTransform().getBaseVal().appendItem(r);

						// Apr 24, 2012
						betterReadabilityForSVGTextElem(txtBootstrapScore, angle, x, y - strPxWidthHeight.get(1) / 3, "end");
					}
				} // part 2, bootstrap scores end here; end of FOR
			}// bootstrap scores

			/*
			 * part 3, branch length part
			 */
			if (phylotree.hasBranchlength()) {
				for (Map.Entry<String, APosition> entry : BranchLengthPositions.entrySet()) {
					String node_internal_id = entry.getKey();
					APosition pos = entry.getValue();
					PhyloNode node = phylotree.getNodeByID(node_internal_id);
					// if OMNode objects for leaf label not exist; make a SVGtext object and put it at (0,0)
					if (!this.branchID2OMNode.containsKey(node_internal_id)) {
						OMSVGTextElement text = doc.createSVGTextElement(0, 0, OMSVGLength.SVG_LENGTHTYPE_PX, formatterDeci4.format(node.getBranchLength()));
						text.setAttribute("text-anchor", "middle"); // April 14, 2011; Align center 

						// add it to leafID2OMNode as well as tree_leaf_node layer
						this.branchID2OMNode.put(node_internal_id, text);
						layer_tree_branchlength.appendChild(text);
					}
					// update position
					OMSVGTextElement txtBranchLength = (OMSVGTextElement) branchID2OMNode.get(node_internal_id);

					float x = pos.getX();
					float y = pos.getY() - 3;
					float angle = pos.getAngle();

					OMSVGTransform r = svg.createSVGTransform();
					r.setRotate(180 - angle, rootXY.getX(), rootXY.getY());

					txtBranchLength.setAttribute("x", Float.toString(x));
					txtBranchLength.setAttribute("y", Float.toString(y));

					txtBranchLength.getTransform().getBaseVal().clear();
					if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
						txtBranchLength.getTransform().getBaseVal().appendItem(r);

						betterReadabilityForSVGTextElem(txtBranchLength, angle, x, y + 3, "middle");
					}
				} // part 3, branch length end here; end of FOR
			} // if branch length exist
		} // makeOrUpdate Leaf Lables

		/*
		 * March 21, 2011; April 8, 2011; cleanup and add opacity;
		 */
		public void makeLeafLabelBackground() {
			// first of all, clear its contents
			clearAllChildNodesFromSVGGElement(layer_tree_leaflabel_background);
			boolean bAlignleaflable = (this.alignLeafLabels == true && (plotmode.equals(treePlotMode.RECT_PHYLOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)))
					? true
					: false;
			// April 8, 2011; opacity
			float opacity = this.dataset2opacity.containsKey(this.activeLeafBKColorSetID) ? dataset2opacity.get(this.activeLeafBKColorSetID) : 0.5f; // by default, leaf background opacity is 0.5
			layer_tree_leaflabel_background.setAttribute("fill-opacity", Float.toString(opacity)); // change it 
			// background colors for leaf nodes
			if (this.leafBKColorEnabled && !disable_all_dataset) {
				// 0 = treeskeleton; 1 = leaf
				ArrayList<Float> mostRightPositions = this.getRightMostTreeAndLabelPosisions();
				float mostrightTree = mostRightPositions.get(0);
				float mostrightLeaf = mostRightPositions.get(1);
				// make background; March 21, 2011
				for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
					PhyloNode leafnode = phylotree.getNodeByID(entry.getKey());
					APosition startpos = entry.getValue();
					float angle = startpos.getAngle();

					String bkcolor = leafnode.getLeafBKColorByColorsetID(this.activeLeafBKColorSetID);
					if (!bkcolor.equalsIgnoreCase("white")) {
						// if circular mode 
						if (plotmode.equals(treePlotMode.CIRCULAR_CLADOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
							/*
							 * 1. get four points that mark the fan-shape
							 * locations of the four points are: 1 ---> 2 | |
							 * |arc | arc | v 4 <--- 3
							 */
							float angleSpanPerLeafNode = angle_span / phylotree.getLeafNodes().size();
							float innerRadius = (bAlignleaflable) ? mostrightTree + space_between_treeskeleton_and_leaflabels / 2 - rootXY.getX() : startpos.getX()
									+ space_between_treeskeleton_and_leaflabels / 2 - rootXY.getX();
							float outterRadius = mostrightLeaf - rootXY.getX() + space_between_datasets / 3;

							OMSVGPoint p1 = getCoordinatesOnCircle(rootXY, innerRadius, 360 - angleSpanPerLeafNode / 2);
							OMSVGPoint p2 = getCoordinatesOnCircle(rootXY, outterRadius, 360 - angleSpanPerLeafNode / 2);
							OMSVGPoint p3 = getCoordinatesOnCircle(rootXY, outterRadius, angleSpanPerLeafNode / 2);
							OMSVGPoint p4 = getCoordinatesOnCircle(rootXY, innerRadius, angleSpanPerLeafNode / 2);

							// 2. make fan using path
							OMSVGPathElement fan = doc.createSVGPathElement();
							OMSVGPathSegList segs = fan.getPathSegList();
							segs.appendItem(fan.createSVGPathSegMovetoAbs(p1.getX(), p1.getY())); // move to p1
							segs.appendItem(fan.createSVGPathSegLinetoAbs(p2.getX(), p2.getY())); // line from p1 to p2;
							segs.appendItem(fan.createSVGPathSegArcAbs(p3.getX(), p3.getY(), outterRadius, outterRadius, 0, false, false)); // arc from p2 to p3
							segs.appendItem(fan.createSVGPathSegLinetoAbs(p4.getX(), p4.getY())); // line from p3 to p4;
							segs.appendItem(fan.createSVGPathSegArcAbs(p1.getX(), p1.getY(), innerRadius, innerRadius, 0, false, true)); // arc from p4 to p1
							segs.appendItem(fan.createSVGPathSegClosePath()); // close path
							fan.setAttribute("stroke", "none");
							fan.setAttribute("fill", bkcolor);

							// 3. rotate; Feb 12, 2012 --
							if (angle != 0 || plotmode == treePlotMode.CIRCULAR_CLADOGRAM || plotmode == treePlotMode.CIRCULAR_PHYLOGRAM) {
								OMSVGTransform fanr = svg.createSVGTransform();
								fanr.setRotate(-angle, rootXY.getX(), rootXY.getY()); // March 21, 2011; this rotation is correct
								fan.getTransform().getBaseVal().appendItem(fanr);
							}
							layer_tree_leaflabel_background.appendChild(fan);
						} else { // non-circular mode
							float rectX1 = (bAlignleaflable) ? mostrightTree + space_between_treeskeleton_and_leaflabels / 2 : startpos.getX()
									+ space_between_treeskeleton_and_leaflabels / 2;
							float rectX2 = mostrightLeaf + space_between_datasets / 3;
							float rectWidth = rectX2 - rectX1;
							OMSVGRectElement rect = doc.createSVGRectElement(rectX1, startpos.getY() - pxPerHeight / 2, rectWidth, pxPerHeight, 0, 0);
							rect.setAttribute("stroke", "none");
							rect.setAttribute("fill", bkcolor);
							layer_tree_leaflabel_background.appendChild(rect);
						} // plot mode is circular / non-circular 
					}// if background color isn't white
				} // iterate hashPositionOfLeafNodes one more time
			}// if leafBKColorEnabled 
		}// makeLeafLabelBackground


		/**
		 * Handle Leaf selection ...
		 * 
		 * @param element
		 */
		protected void addLeafSelectionHandler(final OMSVGTextElement element) {  
			element.addMouseUpHandler(new MouseUpHandler() {
				@Override
				public void onMouseUp(MouseUpEvent event) {
					if(!event.isControlKeyDown()) {
						event.stopPropagation();
						return;
					}
					String selectAttributeValue = element.getAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE);
//					MC.alert.setMessageAndShow(" addLeafSelectionHandler " +  element.getId() + " : " + selectAttributeValue);
					if(selectAttributeValue == null || 
							selectAttributeValue.trim().length()==0 ||
							selectAttributeValue.equals(LEAF_NOTSELECTED_STYLE) ) { 
						// select
						MC.rpc.treeLeafSelection(sessionid, getDBserialID(), element.getId(),  new AsyncCallback<Boolean>() {
							@Override
							public void onFailure(Throwable caught) {
								MC.alert.setMessageAndShow("failed to store selected Leaf", AlertWidget.AlertWidgetType.error);
							}
							@Override
							public void onSuccess(Boolean result) { 
								element.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE, LEAF_SELECTED_STYLE);
								element.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE2, LEAF_SELECTED_STYLE2);
								element.getElement().setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE3, LEAF_SELECTED_STYLE3); 
							}
						});
					}
					else {
						// unselect
						MC.rpc.treeLeafUnSelection(sessionid, getDBserialID(), element.getId(),  new AsyncCallback<Boolean>() {
							@Override
							public void onFailure(Throwable caught) {
								MC.alert.setMessageAndShow("failed to store UN-selected Leaf", AlertWidget.AlertWidgetType.error);
							}
							@Override
							public void onSuccess(Boolean result) { 
								element.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE, LEAF_NOTSELECTED_STYLE);
								element.setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE2, LEAF_NOTSELECTED_STYLE2);
								element.getElement().setAttribute( LEAF_SELECTION_CSSS_ATTRIBUTE3, LEAF_NOTSELECTED_STYLE3); 
							}
						});
					}
					DOM.releaseCapture(getElement()); //  
				}
			}); 
		}
		
		/*
		 * this layer is added to layer_tree; therefore it could be moved by
		 * mouse NOTE: only Y-value is changed; April 8, 2011; also change
		 * charts next to the leaf lables Oct 26, 2011; rewrite partial codes
		 *
		 * -- Apr 23, 2012 -- make sure the orientations of the texts of leaf
		 * labels are correct
		 */
		public void setLeafLabelFontSize(int newleaflabelFontSize) {
			if (newleaflabelFontSize > 0 && newleaflabelFontSize != default_font_size) {
				default_font_size = newleaflabelFontSize;

				// remake everything
				this.makeLeafLabels();

				this.makeLeafLabelBackground(); // March 21, 2011;
				updateAllCharts(); // April 8, 2011;
				updateTreeCanvasInfoOnServer("fontSize", default_font_size); // May 14, 2011
			}
		}

		/*
		 * April 8, 2011; TYPE can be : leaf | leafBK | branch This function is
		 * used to add color dataset to Leaf, leaf background and branches Oct
		 * 28, 2011; add param: active
		 */
		public void addColorsetToTreeNodes(int colorID, ColorDataContainerParser coldatcon, TreeDecoType type, boolean active) {
			boolean replot = active;

			// if current dataset exists, delete it
			if (this.dataset2originalUserinputAsString.containsKey(colorID)) {
				for (PhyloNode node : phylotree.getAllNodes()) {
					node.removeColorByDataTypeAndID(type, colorID);
				} // iterate all nodes

				replot = true;
			}
			this.dataset2originalUserinputAsString.put(colorID, coldatcon.getOriginal_datastring());

			// put opacity; NOTE: april 8, 2011; different types of plot have different opacity default values
			if (type.equals(TreeDecoType.LEAFBKCOLOR)) {
				this.dataset2opacity.put(colorID, coldatcon.isIsUserInputOpacity() ? coldatcon.getOpacity() : 0.6f);
			} else {
				this.dataset2opacity.put(colorID, coldatcon.getOpacity());
			}

			/*
			 * add color to tree branch | leaf | leafBK
			 */
			for (ColorEx colorset : coldatcon.getColorsets()) {
				String color = colorset.getColor();
				String mode = colorset.getMode();

				String leafID = colorset.getNodeIDs().get(0); // get the first element
				/*
				 * set colors according to modes
				 */
				if (mode.isEmpty() == true) {
					PhyloNode phylonode = type.equals(TreeDecoType.BRANCHCOLOR) ? phylotree.getLCA(colorset.getNodeIDs()) : phylotree.getNodeByID(leafID);
					if (phylonode != null) {
						phylonode.addColorByDataTypeAndID(type, colorID, color);
					}
				} else {
					for (PhyloNode node : phylotree.getAllNodes()) {
						if ((mode.equalsIgnoreCase("prefix") && JSFuncs.matchStringStart(node.getID(), leafID))
								|| (mode.equalsIgnoreCase("suffix") && JSFuncs.matchStringEnd(node.getID(), leafID))
								|| (mode.equalsIgnoreCase("anywhere") && JSFuncs.matchStringAnywhere(node.getID(), leafID))) {
							node.addColorByDataTypeAndID(type, colorID, color);
						} else if (mode.equalsIgnoreCase("ad")) {
							PhyloNode phylonode = phylotree.getLCA(colorset.getNodeIDs());
							if (phylonode != null) {
								if (type.equals(TreeDecoType.BRANCHCOLOR)) {
									this.colorAllDescendentBranches(phylonode, color, colorID);
								} else if (type.equals(TreeDecoType.LEAFCOLOR)) {
									this.colorAllDescendentLeafLabels(phylonode, color, colorID);
								} else if (type.equals(TreeDecoType.LEAFBKCOLOR)) {
									this.colorAllDescendentLeafLabelBackground(phylonode, color, colorID);
								}
							}
						}// mode is AD
					}// iterate phylonodes 
				} // different modes
			} // iterate colorsets

			/*
			 * at the end, set active datasetID and update plot April 20, 2011;
			 * make legend; delete active ones Oct 28, 2011; add param: active
			 */
			if (replot) {
				if (type.equals(TreeDecoType.BRANCHCOLOR)) {
					//plotLegends.removeLegendEntryByDatasetIDAndType(activeBranchColorSetID, type);
					this.activeBranchColorSetID = colorID;
					this.makeTreeSkeleton();
				} else if (type.equals(TreeDecoType.LEAFCOLOR)) {
					//plotLegends.removeLegendEntryByDatasetIDAndType(activeLeafColorSetID, type);

					this.activeLeafColorSetID = colorID;
					this.makeLeafLabels();
				} else if (type.equals(TreeDecoType.LEAFBKCOLOR)) {
					//plotLegends.removeLegendEntryByDatasetIDAndType(activeLeafBKColorSetID, type);

					this.activeLeafBKColorSetID = colorID;
					this.makeLeafLabelBackground();
				}
			}

			/*
			 * May 24, 2011; make legendentry; delete old ones if exists
			 */
			makeLegendEntryAndUpdateAllLegend(coldatcon.isShowLegends(), coldatcon.getLegendColors(), coldatcon.getLegendTexts(), colorID, coldatcon.getTitle(),
					coldatcon.getLegendstyle(), type, replot);

		}// addColorsetToTreeNodes

		// >>>>>>>>>>> -------------- leaf colors --------------
		public void setActiveLeafColorSetByID(int ccolorSetID) {
			if (ccolorSetID!=this.activeLeafColorSetID && this.dataset2originalUserinputAsString.containsKey(ccolorSetID)) {

				// update leaflable colors
				this.activeLeafColorSetID = ccolorSetID;
				this.updateLeafLabelColors();

				updateAllLegends(); // May 24, 2011;
			}
		}// April 4, 2011

		public boolean setDisableAllLabelColorData(boolean b) {
			if (!this.leafColorEnabled != b) {
				this.leafColorEnabled = !b;
				this.makeLeafLabels();

				updateAllLegends(); // May 24, 2011;
			}
			return this.leafColorEnabled;
		} // April 4, 2011;

		public int getActiveLeafColorDatasetID() {
			return this.activeLeafColorSetID;
		}// April 4, 2011;
			// <<<<<<< --------------- leaf colors ----------------

		// --- >>>>>  leaf BK colors
		public int getActiveLeafBKColorDatasetID() {
			return this.activeLeafBKColorSetID;
		}// April 4, 2011;

		public void setActiveLeafBKcolorByDatasetID(int colorSetID) {
			if ( colorSetID!=this.activeLeafBKColorSetID && this.dataset2originalUserinputAsString.containsKey(colorSetID) ) {

				this.activeLeafBKColorSetID = colorSetID;
				this.makeLeafLabelBackground();

				updateAllLegends(); // May 24, 2011;
			}
		}// April 6, 2011;

		public boolean setDisableAllLabelBKColorData(boolean b) {
			if (!this.leafBKColorEnabled != b) {
				this.leafBKColorEnabled = !b;
				this.makeLeafLabelBackground();

				updateAllLegends(); // May 24, 2011;
			}
			return this.leafBKColorEnabled;
		}// April 4, 2011;
			// <<<<<<< -------- bk colors ------------

		// >>>>>>>>> -------------- branch colors
		public void setActiveBranchColorByDatasetID(int branchColorSetID) {
			if (branchColorSetID != this.activeBranchColorSetID  && dataset2originalUserinputAsString.containsKey(branchColorSetID)  ) {

				this.activeBranchColorSetID = branchColorSetID;
				// at the end, update tree skeleton
				this.makeTreeSkeleton();

				updateAllLegends(); // May 24, 2011;
			}
		}

		public boolean setDisableAllBranchColorData(boolean b) {
			if (!this.branchColorEnabled != b) {
				this.branchColorEnabled = !b;
				this.makeTreeSkeleton();

				updateAllLegends(); // May 24, 2011;
			}
			return this.branchColorEnabled;
		}// April 4, 2011;

		public int getActiveBranchColorDatasetID() {
			return this.activeBranchColorSetID;
		} // April 4, 2011;
			// <<<<<<<<<<< -------------- branch colors -------------

		/*
		 * NOTE: datasetID should be TYPE + actualldatasetID, for example the
		 * name for a branch data named 'bd1' will be BRANCHCOLORbd1 therefore
		 * this color dataset name is unique
		 */
		public String getOriginalUserInputAsStringByID(int datasetid) {
			return this.dataset2originalUserinputAsString.containsKey(datasetid) ? this.dataset2originalUserinputAsString.get(datasetid) : "";
		}// April 4, 2011;

		/*
		 * >>>>>>>>>>>>>>>> align leaf labels >>>>>>>>>>>>>>>
		 */
		public boolean getAlignLeafLabels() {
			return this.alignLeafLabels;
		} // March 20, 2011;

		// March 21, 2011; setAlignLeafLabels and return current status
		// Oct 26, 2011; fix a bug here
		public boolean setAlignLeafLabels(boolean alignleaflabel) {
			if (this.alignLeafLabels != alignleaflabel) {
				this.alignLeafLabels = alignleaflabel;

				// if current mode is phylogram, replot
				if (plotmode.equals(treePlotMode.RECT_PHYLOGRAM) || plotmode.equals(treePlotMode.CIRCULAR_PHYLOGRAM)) {
					this.makeLeafLabels();
					this.makeLeafLabelBackground();
				}
				updateTreeCanvasInfoOnServer("alignLeafLabel", alignLeafLabels ? 1f : 0f); // May 14, 2011
			}
			return alignLeafLabels;
		} // setAlignLeafLabels; 

		// <<<<<<<<<<<<<<<< align leaf labels <<<<<<<<<<<<<<<
		/*
		 * private functions
		 */
		/*
		 * March 21, 2011;
		 * April 11, 2013; maxLenLeafLabel = maxLenLeafLabel + default_font_size
		 */
		private ArrayList<Float> getRightMostTreeAndLabelPosisions() {
			float posLeafLabels = 0f, posTreeSkeleton = 0f, maxLenLeafLabel = 0f;

			// iterate hashPositionOfLeafNodes
			for (Map.Entry<String, APosition> entry : LeafInternalID2NodePosisionts.entrySet()) {
				String leaf_id = phylotree.getNodeByID(entry.getKey()).getID();
				float x = entry.getValue().getX();

				// rightmostPositionsTreeSkeleton
				if (x > posTreeSkeleton) {
					posTreeSkeleton = x;
				} // march 20, 2011

				// get rightmostPositionsLeafLabels
				float pxWidth = JSFuncs.stringPixelWidthHeight(leaf_id, default_font_family, default_font_size).get(0);

				if ((x + space_between_treeskeleton_and_leaflabels + pxWidth) > posLeafLabels) {
					posLeafLabels = x + space_between_treeskeleton_and_leaflabels + pxWidth;
				} // march 21, 2011

				if (pxWidth > maxLenLeafLabel) {
					maxLenLeafLabel = pxWidth;
				}
			} // iterate HashMap : hashPositionOfLeafNodes

			if (this.alignLeafLabels) {
				posLeafLabels = posTreeSkeleton + maxLenLeafLabel + space_between_treeskeleton_and_leaflabels;
			} // March 21, 2011;

			ArrayList<Float> rightmostpos = new ArrayList<Float>(2);
			rightmostpos.add(posTreeSkeleton);
			rightmostpos.add(posLeafLabels + default_font_size); // April 11, 2013

			return rightmostpos;
		} // getRightMostTreeAndLabelPosisions; March 21, 2011

		private void updateLeafLabelColors() {
			for (PhyloNode leafnode : phylotree.getLeafNodes()) {
				OMSVGTextElement text = (OMSVGTextElement) leafID2OMNode.get(leafnode.getInternalID());
				String color = (leafColorEnabled && !disable_all_dataset) ? leafnode.getLeafColorByColorsetID(activeLeafColorSetID) : "black";
				text.setAttribute("fill", color);
			}
			// April 8, 2011; change opacity as well;
			float opacity = this.dataset2opacity.containsKey(this.activeLeafColorSetID) ? dataset2opacity.get(this.activeLeafColorSetID) : 1;
			layer_tree_leaf_labels.setAttribute("fill-opacity", Float.toString(opacity)); // change it 
		}

		/*
		 * >>>>>>>>>> PLOT TREE in various modes >>>>>>>>>>>>>>> Jan 9, 2011;
		 * add support to phylogram mode March 17, 2011; add branch color
		 */
		private void circular_mode(PhyloNode pnode, OMSVGGElement layer_tree, OMSVGPoint parentxy) {
			float px = parentxy.getX();
			float py = parentxy.getY();

			float vlevel = pnode.getLevelVertical();
			//float hlevel = pnode.getLevelHorizontal();

			float pradius = (plotmode == treePlotMode.CIRCULAR_CLADOGRAM) ? (rootnode.getLevelHorizontal() - pnode.getLevelHorizontal()) * pxPerWidthCurrentMode : pnode
					.getBranchLengthToRoot() * pxPerBranchLength;
			// iterate child nodes
			for (PhyloNode dnode : pnode.getDescendents()) {
				// stroke color for current branch; march 17, 2011
				final String branch_stroke_color = (this.branchColorEnabled && !disable_all_dataset) ? dnode
						.getBranchColorByColorsetID(this.activeBranchColorSetID) : "black";
				/*
				 * for each descdent node
				 */
				float vl = dnode.getLevelVertical();
				//float hl = dnode.getLevelHorizontal();
				float dradius = (plotmode == treePlotMode.CIRCULAR_CLADOGRAM) ? (rootnode.getLevelHorizontal() - dnode.getLevelHorizontal()) * pxPerWidthCurrentMode : (dnode
						.getBranchLengthToRoot() * pxPerBranchLength);

				// Dec 22, 2011; fine-tune angle 
				float angle = circular_tree_plot_in_clockwise_mode
						? -(float) (dnode.getLevelVertical() - 1f) * angle_per_vertical_level - 180f
						: (float) (dnode.getLevelVertical() - 1f) * angle_per_vertical_level - 180f; // angle = 360 - angle

				angle += (360f + angle_start);
				angle %= 360f;
				if (angle < 0) {
					angle += 360f;
				}

				// calculate positions of the points on arc
				OMSVGPoint dxy = getCoordinatesOnCircle(rootXY, dradius, angle); // xy of current node at current circle
				OMSVGPoint pxy = getCoordinatesOnCircle(rootXY, pradius, angle); // xy of current node at parent circle

				/*
				 * largeArcFlag: A value of 0 indicates that the arc's measure
				 * is less than 180°. A value of 1 indicates that the arc's
				 * measure is greater than or equal to 180° sweepFlag: A value
				 * of 0 indicates that the arc is to be drawn in the negative
				 * angle direction (counterclockwise). A value of 1 indicates
				 * that the arch is to be drawn in the positive angle direction
				 * (clockwise).
				 *
				 * revised: Feb 28, 2011; add support to
				 * circular_tree_plot_in_clockwise_mode
				 */
				boolean sweepFlag = vl < vlevel ? false : true;
				if (circular_tree_plot_in_clockwise_mode == false) {
					sweepFlag = !sweepFlag;
				}

				// boolean largeArcFlag = angle >= 180 ? true : false; 
				final OMSVGPathElement path = doc.createSVGPathElement();
				OMSVGPathSegList segs = path.getPathSegList();
				segs.appendItem(path.createSVGPathSegMovetoAbs(px, py));
				segs.appendItem(path.createSVGPathSegArcAbs(pxy.getX(), pxy.getY(), pradius, pradius, 0, false, sweepFlag));
				segs.appendItem(path.createSVGPathSegLinetoAbs(dxy.getX(), dxy.getY()));
				path.setAttribute("stroke", branch_stroke_color);
				path.setAttribute("fill", "none");

				final OMSVGCircleElement c = addCircleAndMouseEventToSVEPathElement(path, dxy, branch_stroke_color, dnode);
				// append g to layer tree
				layer_tree.appendChild(path);
				layer_tree.appendChild(c);

				// <<<< =---====  Jan 09, 2012 ----===

				// current position for label, before rotation; jan 23, 2011;
				String node_internal_id = dnode.getInternalID();
				OMSVGPoint current_xy = svg.createSVGPoint(rootXY.getX() + dradius, rootXY.getY());
				if (dnode.isLeaf() == false) {
					// prepare a transform elements; used by bootstrap/ leaf label and branch length
					OMSVGTransform r = svg.createSVGTransform();
					r.setRotate(180f - angle, rootXY.getX(), rootXY.getY());

					BootstrapPositions.put(node_internal_id, new APosition(current_xy, angle));
					circular_mode(dnode, layer_tree, dxy);
				} else {
					LeafInternalID2NodePosisionts.put(node_internal_id, new APosition(current_xy, angle)); 
//					LeafSelectionMap.put(node_internal_id, phylotree.getLeafSelectionSet().contains(node_internal_id));
					 
				}// leaf node VS Internal Node

				// keep trakcing the position for branch length
				BranchLengthPositions.put(node_internal_id, new APosition(svg.createSVGPoint(rootXY.getX() + (dradius + pradius) / 2, rootXY.getY()), angle));
			} // for each descendant

		}// circular_cladogram_normal_plot

		/*
		 * jan 8, 2011; version = 2; slanted MODE March 17, 2011; branch stoke
		 * color
		 */
		private void slanted_mode(PhyloNode pnode, OMSVGGElement layer_tree, OMSVGPoint parentxy, double atan, float x_first_leaf) {
			float px = parentxy.getX();
			float py = parentxy.getY();

			float vlevelS = pnode.getLevelVerticalSlanted();
			float vlevel = pnode.getLevelVertical();
			float hlevel = pnode.getLevelHorizontal();
			// iterate child nodes
			for (PhyloNode dnode : pnode.getDescendents()) {
				// stroke color for current branch; march 17, 2011
				String branch_stroke_color = (this.branchColorEnabled && !disable_all_dataset ) ? dnode
						.getBranchColorByColorsetID(this.activeBranchColorSetID) : "black";
				/*
				 * for each descdent node
				 */
				float vlS = dnode.getLevelVerticalSlanted();
				float vl = dnode.getLevelVertical();
				float hl = dnode.getLevelHorizontal();

				float min_leaf_v = dnode.getMinLeafVerticalLevel();
				float max_leaf_v = dnode.getMaxLeafVerticalLevel();

				//float x_min_leaf = x_first_leaf;
				//float y_min_leaf = min_leaf_v * pxPerHeight + margin_normal * canvas_height;
				float y_min_leaf = min_leaf_v * pxPerHeight + margin_y;

				float dx = 0, dy = 0;
				if (plotmode == treePlotMode.SLANTED_CLADOGRAM_RECT) {
					dx = px + (hlevel - hl) * pxPerWidthCurrentMode;
					dy = py + (vl - vlevel) * pxPerHeight;
				} else if (plotmode == treePlotMode.SLANTED_CLADOGRAM_MIDDLE) {
					dx = px + (hlevel - hl) * pxPerWidthCurrentMode;
					dy = py + (vlS - vlevelS) * pxPerHeight;
				} else if (plotmode == treePlotMode.SLANTED_CLADOGRAM_NORMAL) {
					/*
					 * jan 7, 2011; NOTE: in this mode, the position of internal
					 * node is determined by it's first leaf node and last leaf
					 * node. its XY-coordinates are: y = (Y of its first leaf
					 * node + Y of its last leaf node) / 2; x is a little
					 * complicated; here is howto:
					 *
					 * 1. suppose coordinateXY of root is : x1, y1, and the
					 * first leaf is: x2, y2; then we'll have a global angle for
					 * the whole plot: atan = Math.atan( (y2 - y1) / (x2 - x1)
					 * ); 2. for internal nodes, x = X of leaf node (all leaf
					 * nodes have the same X) - (Y of its last leaf node - Y of
					 * its first leaf node) / 2 / Math.tan(atan);
					 *
					 * the output is similar to SLANTED_CLADOGRAM in Dendroscope
					 */
					dx = dnode.isLeaf() ? x_first_leaf : x_first_leaf - ((max_leaf_v - min_leaf_v) / 2 * pxPerHeight) / (float) Math.tan(atan); // it's very strange here
					dy = y_min_leaf + (max_leaf_v - min_leaf_v) / 2 * pxPerHeight;
				}

				//                OMSVGLineElement sline = doc.createSVGLineElement(px, py, dx, dy);
				//                sline.setAttribute("stroke", branch_stroke_color);
				//                layer_tree.appendChild(sline);
				//                
				// use path instead of two lines to make branches 
				final OMSVGPathElement path = doc.createSVGPathElement();
				OMSVGPathSegList segs = path.getPathSegList();
				segs.appendItem(path.createSVGPathSegMovetoAbs(px, py));
				segs.appendItem(path.createSVGPathSegLinetoAbs(dx, dy));
				path.setAttribute("stroke", branch_stroke_color);
				path.setAttribute("fill", "none");

				final OMSVGCircleElement c = addCircleAndMouseEventToSVEPathElement(path, svg.createSVGPoint(dx, dy), branch_stroke_color, dnode);
				// append g to layer tree
				layer_tree.appendChild(path);
				layer_tree.appendChild(c);
				// <<<< ==== Jan 09, 2012 ==== <<<<

				// check if current node is leaf
				String node_internal_id = dnode.getInternalID();
				OMSVGPoint current_xy = svg.createSVGPoint(dx, dy);
				if (dnode.isLeaf() == false) {
					BootstrapPositions.put(node_internal_id, new APosition(current_xy, 0));
					slanted_mode(dnode, layer_tree, current_xy, atan, x_first_leaf);
				} else {
					LeafInternalID2NodePosisionts.put(node_internal_id, new APosition(current_xy, 0)); 
//					LeafSelectionMap.put(node_internal_id, phylotree.getLeafSelectionSet().contains(node_internal_id));
				} // leaf or internal node
				BranchLengthPositions.put(node_internal_id, new APosition(svg.createSVGPoint((px + dx) / 2, (py + dy) / 2), 0));
			} // for each descendent nodes
		}// similar to rect mode

		/*
		 * jan 8, 2011; version = 2; RECT MODE * March 17, 2011; branch stoke
		 * color April 8, 2011; clean up
		 */
		private void rect_mode(PhyloNode pnode, OMSVGGElement layer_tree, OMSVGPoint parentxy) {
			float px = parentxy.getX();
			float py = parentxy.getY();
			
			System.err.println( "\t\t--> plot " + pnode.getInternalID() + "; " + pnode.getID() );

			float vlevel = pnode.getLevelVertical();
			float hlevel = pnode.getLevelHorizontal();
			// iterate child nodes
			for (PhyloNode dnode : pnode.getDescendents()) {
				// stroke color for current branch; march 17, 2011
				String branch_stroke_color = (this.branchColorEnabled && !disable_all_dataset ) ? dnode
						.getBranchColorByColorsetID(this.activeBranchColorSetID) : "black";
				/*
				 * for each descdent node
				 */
				float vl = dnode.getLevelVertical();
				float hl = dnode.getLevelHorizontal();
				float hb = dnode.getBranchLength();

				float dx = px;
				if (plotmode == treePlotMode.RECT_CLADOGRAM) {
					dx += (hlevel - hl) * pxPerWidthCurrentMode;
				} else {
					dx += hb * pxPerBranchLength;
				}
				float dy = py + (vl - vlevel) * pxPerHeight;

				// use path instead of two lines to make branches 
				final OMSVGPathElement path = doc.createSVGPathElement();
				OMSVGPathSegList segs = path.getPathSegList();
				segs.appendItem(path.createSVGPathSegMovetoAbs(px, py));
				segs.appendItem(path.createSVGPathSegLinetoAbs(px, dy));
				segs.appendItem(path.createSVGPathSegLinetoAbs(dx, dy));
				path.setAttribute("stroke", branch_stroke_color);
				path.setAttribute("fill", "none");

				final OMSVGCircleElement c = addCircleAndMouseEventToSVEPathElement(path, svg.createSVGPoint(dx, dy), branch_stroke_color, dnode);
				// append g to layer tree
				layer_tree.appendChild(path);
				layer_tree.appendChild(c);
				// <<<< ==== Jan 09, 2012 ==== <<<<

				// check if is leaf
				OMSVGPoint current_xy = svg.createSVGPoint(dx, dy);
				String node_internal_id = dnode.getInternalID();
				if (dnode.isLeaf()) {
					/*
					 * keep record of position of leaf label
					 */
					LeafInternalID2NodePosisionts.put(node_internal_id, new APosition(current_xy, 0)); 
//					LeafSelectionMap.put(node_internal_id, phylotree.getLeafSelectionSet().contains(node_internal_id));
				} else {
					/*
					 * if internal label, make text element for branch length
					 * (if any) and bootstrap score
					 */
					BootstrapPositions.put(node_internal_id, new APosition(current_xy, 0));
					rect_mode(dnode, layer_tree, current_xy);
				} // leaf or internal node
				/*
				 * keep traking of positions for showing branch length
				 */
				BranchLengthPositions.put(node_internal_id, new APosition(svg.createSVGPoint((px + dx) / 2, dy), 0));
			}// iterate child nodes
		} // rect cladogram

		// march 17, 2011; a function that calls itself;
		private void colorAllDescendentBranches(PhyloNode phylonode, String color, int colorsetID) {
			phylonode.addColorToBranch(colorsetID, color);
			if (!phylonode.isLeaf()) { // if not leaf
				for (PhyloNode node : phylonode.getDescendents()) {
					colorAllDescendentBranches(node, color, colorsetID);
				}
			}
		}

		private void colorAllDescendentLeafLabels(PhyloNode phylonode, String color, int colorsetID) {
			if (phylonode.isLeaf()) { // if leaf
				phylonode.addColorToLeaf(colorsetID, color);
			} else { // if not
				for (PhyloNode node : phylonode.getDescendents()) {
					colorAllDescendentLeafLabels(node, color, colorsetID);
				}
			}
		} // colorAllDescendentLeafLabels; March 20, 2011;

		private void colorAllDescendentLeafLabelBackground(PhyloNode phylonode, String color, int colorsetID) {
			if (phylonode.isLeaf()) { // if leaf
				phylonode.addColorToLeafBK(colorsetID, color);
			} else { // if not
				for (PhyloNode node : phylonode.getDescendents()) {
					colorAllDescendentLeafLabelBackground(node, color, colorsetID);
				}
			}
		} // colorAllDescendentLeafLabelBackground; March 21, 2011;

		private OMSVGCircleElement addCircleAndMouseEventToSVEPathElement(final OMSVGPathElement path, OMSVGPoint dxy, final String branch_stroke_color, final PhyloNode dnode) {
			final OMSVGCircleElement c = doc.createSVGCircleElement(dxy.getX(), dxy.getY(), 2f);
			c.setAttribute("stroke", "red");
			c.setAttribute("fill", "red");

			c.getStyle().setVisibility(Visibility.HIDDEN);
			c.getStyle().setDisplay(Display.NONE);
			
			/**
			 * Oct 25, 2013: move the make new flextable from inside the mouseInHandler to here
			 */
			final FlexTable tooltipflex = makeToolTipFlextableForDNode(dnode);
			
			/**
			 * >>> oct 25, 2013: make action buttons
			 * for now it's only the action button
			 */
			final AwesomeButton rerootButton = new AwesomeButton( "reroot here", "google", "small");
			rerootButton.addClickHandler( new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					// reroot tree at current node
					phylotree.rerootTree(dnode);
					
					// replot everything 
					drawPhyloTreeUsingSVG( true ); // Nov 5, 2013; use true to recalculate some variables ...
					
					// hide the information panel 
					tooltipWidget.hide();
				}} );
			
			// <<<<< oct 25, 2013 <<<<<
			// add mouse events to c
			/**
			 * Oct 25, 2013; add action buttons
			 * so far only the "reroot tree at here" 
			 */
			path.addMouseOverHandler(new MouseOverHandler() {

				@Override
				public void onMouseOver(MouseOverEvent event) {
					path.setAttribute("stroke", "red");
					c.getStyle().setVisibility(Visibility.VISIBLE);
					c.getStyle().setDisplay(Display.INLINE);
					
					/**
					 * popup information panel 
					 */
					oeelement_mouse_over = path;
					tooltipx = event.getClientX();
					tooltipy = event.getClientY();

					// >>>> TIMER >>>>
					new Timer() {

						@Override
						public void run() {
							if (oeelement_mouse_over != null && oeelement_mouse_over.equals(path)) { // if mouse is still on the same element as delay_pshow mili sec before
								tooltipWidget.setWidgetAndShow(tooltipflex, rerootButton,  tooltipx, tooltipy); // >>>> oct 25, 2013 : also add action buttons
							}
						}
					}.schedule(delayShowMiliSec);
				}
			});

			path.addMouseOutHandler(new MouseOutHandler() {

				@Override
				public void onMouseOut(MouseOutEvent event) {
					path.setAttribute("stroke", branch_stroke_color);
					c.getStyle().setVisibility(Visibility.HIDDEN);
					c.getStyle().setDisplay(Display.NONE);
					
					/**
					 * deal with popup information panel 
					 */
					oeelement_mouse_over = null;
					// >>>> TIMER >>>>
					new Timer() {

						@Override
						public void run() {
							if (oeelement_mouse_over == null && !tooltipWidget.getMouseOver()) { // if mouse is not on any evelement and not on popup panel, close the panel
								tooltipWidget.hide();
							}
						}
					}.schedule(delayHideMiliSec);
				}
			});
			
			return (c);
		}
		
		private FlexTable makeToolTipFlextableForDNode(PhyloNode dnode) {
			FlexTable f = new FlexTable();
			f.setCellSpacing(flexTableCellSpacing); // cell spacing 

			int idx = 0;
			// node id:
			String nodeid = dnode.getID().trim().isEmpty() ? "" : dnode.getID().trim();
			if (!dnode.isLeaf()) {
				nodeid += " (" + dnode.getInternalID() + ")";
			} else {
				nodeid += " (leaf)";
			}
			f.setWidget(idx, 0, new HTML("<b>Name:</b>"));
			f.setWidget(idx, 1, new HTML(nodeid));

			// if branch length is available
			if (getPhyloTree().hasBranchlength()) {
				// branch length
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Branch length:</b>"));
				f.setWidget(idx, 1, new HTML(Float.toString(dnode.getBranchLength())));

				// branch length to root
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Branch length to root:</b>"));
				f.setWidget(idx, 1, new HTML(Float.toString(dnode.getBranchLengthToRoot())));
			}

			// if bootstrap score is avaible 
			if (getPhyloTree().hasBootstrapScores()) {
				// branch length
				idx++;
				f.setWidget(idx, 0, new HTML("<b>Bootstrap:</b>"));
				f.setWidget(idx, 1, new HTML(Float.toString(dnode.getBootStrap())));
			}

			// distance to root
			idx++;
			f.setWidget(idx, 0, new HTML("<b>Distance to root:</b>"));
			f.setWidget(idx, 1, new HTML(Integer.toString(dnode.getDistanceToRoot())));

			// leaf descendents
			idx++;
			f.setWidget(idx, 0, new HTML("<b>Leaf descendents:</b>"));
			f.setWidget(idx, 1, new HTML(Integer.toString(dnode.getNumberOfLeafDescendents())));
			return f;
		} // Jan 09, 2012

		// apr 23, 2012; change the orientation of the text to increase readability
		private void betterReadabilityForSVGTextElem(OMSVGTextElement txtLeafLabel, float angle, float x, float y, String textAnchor) {
			if ((angle >= 270f && angle <= 360f) || (angle >= 0f && angle <= 90f)) {
				txtLeafLabel.setAttribute("text-anchor", textAnchor);

				OMSVGTransform rotateText = svg.createSVGTransform();
				rotateText.setRotate(180, x, y);
				txtLeafLabel.getTransform().getBaseVal().appendItem(rotateText);
			}
		}// -- Apr 24, 2012 --
	}

	/*
	 * April 8, 2011; clean up; add opacity
	 * Nov 5, 2013: after reroot, some pies are not showing correctly, here I am going to figure out why ...
	 */
	public class Pies {
		// hash{pcdataID} = pcdata;

		private HashMap<Integer, NumericMatrixDataContainer> pcDatasets = new HashMap<Integer, NumericMatrixDataContainer>();
		private int activePCDataID = -1;
		private boolean isAllPiechartsDisabled = false;

		// a constructor that does nothing
		public Pies() {
		}

		/*
		 * >>>>>>>>>> public methods >>>>>>>>>>>
		 */
		public void addPCData(int activePCDataID, NumericMatrixDataContainer newpcdata, boolean active) {
			if (newpcdata.isDataValid()) {
				boolean replot = active || pcDatasets.containsKey(activePCDataID);
				// add dataset
				pcDatasets.put(activePCDataID, newpcdata);

				// if active
				if (active) {
					this.activePCDataID = activePCDataID;
					this.makePies();
				}

				/*
				 * May 24, 2011; make legendentry;
				 */
				makeLegendEntryAndUpdateAllLegend(newpcdata.isShowLegends(), 
						newpcdata.getLegendColors(), 
						newpcdata.getLegendTexts(),
						activePCDataID,
						newpcdata.getTitle(),
						newpcdata.getLegendstyle(), TreeDecoType.PIES, replot);
			}
		}

		public void setDisableAllPiechartData(boolean disableAll) {
			this.isAllPiechartsDisabled = disableAll;
			this.makePies();

			updateAllLegends();
		}

		public void setActivePCDataByID(int newid) {
			if (this.activePCDataID != newid) {

				// then update pies
				this.activePCDataID = newid;
				this.makePies();

				updateAllLegends();
			}
		}
		
		public void makePies(){
			makePies( false );
		}

		public void makePies( boolean bReroot ) {
			/*
			 * March 13, 2011 how-to: each piechart is an OMNode object; OMNode
			 * objects will be created and stored in hash: id2nodeid2omnode
			 * piecharts could be added to layer_tree_piecharts; only referneces
			 * of piecharts will be added, meaning if piecharts updated in hash
			 * '', it'll be updated automatically in layer_tree_piecharts
			 *
			 * NOTE: if the activePCDataID has been changed, the min/ max radius
			 * will be reset so that the piecharts will be updated
			 *
			 * March 26, 2011; delte from layer_tree_piecharts and add pies into
			 * the SVGGelement again. May 26, 2011; add tooltip using
			 * TooltipHandlerForOMElements and flextable
			 */
			clearAllChildNodesFromSVGGElement(layer_tree_piecharts);

			// 2. if activePCDataID isn't null, try to add new OMNodes into layer_tree_piecharts
			if (!disable_all_dataset && !this.isAllPiechartsDisabled && this.pcDatasets.containsKey(this.activePCDataID)) {
				NumericMatrixDataContainer activePCData = this.pcDatasets.get(this.activePCDataID);
				
				/**
				 * Nov 5, 2013; if tree is rerooted, remap the IDs to activePCData
				 */
				if( bReroot ){
					activePCData.reMapDataAfterTreeReroot( phylotree );
				}
				
				// April 8, 2011; set opacity
				float opacity = activePCData.isIsUserInputOpacity() ? activePCData.getOpacity() : 0.6f;
				layer_tree_piecharts.setAttribute("fill-opacity", Float.toString(opacity));
				layer_tree_piecharts.setAttribute("stroke-opacity", Float.toString(opacity));
				layer_tree_piecharts.setAttribute("stroke", "#FFFFFF");

				/*
				 * get necessary data
				 */
				//HashMap<String, String> cat2color = activePCData.getGroup2Colors();
				final ArrayList<String> cats = activePCData.getGroups();

				// 2.1.2 make new piecharts
				HashMap<String, ArrayList<Float>> data = activePCData.getTreeNodeID2Data();
				final ArrayList<String> colors = activePCData.getColors();
				for (Map.Entry<String, ArrayList<Float>> entry : data.entrySet()) {

					String node_internalID = entry.getKey();
					final ArrayList<Float> piechartdata = entry.getValue();

					/*
					 * == April 20, 2012 == by default the total number per row
					 * will be the radius, however user can choose to use the
					 * number as area
					 */
					float radius = activePCData.getRadiusByID(node_internalID);// April 8, 2011; fix bug in this
					if (activePCData.isByarea()) {
						radius = activePCData.getAreaByID(node_internalID);
					}//

					float sum = activePCData.getSumByID(node_internalID);

					/*
					 * -- Apr 25, 2012 -- check if radius > 0
					 */
					if (radius > 0 && sum > 0) {
						// making a new pie
						final OMSVGGElement piechart = doc.createSVGGElement();
						float start_angle = 0;
						for (int i = 0; i < piechartdata.size(); i++) {
							float angle_addup = piechartdata.get(i) / sum * 360;

							// Jan 25, 2011; deal with angles of 0 or 360
							if (angle_addup == 0) {
								// do nothing
							} else if (angle_addup == 360) {
								// if it's a whole circle, draw a circle
								OMSVGCircleElement circle = doc.createSVGCircleElement(0, 0, radius);
								circle.setAttribute("fill", colors.get(i));
								piechart.appendChild(circle);
							} else {
								OMSVGPoint start_on_arc = getCoordinatesOnCircle(svg.createSVGPoint(0, 0), radius, start_angle);
								OMSVGPoint stop_on_arc = getCoordinatesOnCircle(svg.createSVGPoint(0, 0), radius, angle_addup + start_angle);

								// plot a pie
								OMSVGPathElement pie = doc.createSVGPathElement();
								OMSVGPathSegList segs = pie.getPathSegList();

								// if the current arc is larger than or equal 180 degree; a value true indicates
								boolean largeArcFlag = angle_addup >= 180 ? true : false;

								segs.appendItem(pie.createSVGPathSegMovetoAbs(0, 0));
								segs.appendItem(pie.createSVGPathSegLinetoAbs(start_on_arc.getX(), start_on_arc.getY()));
								segs.appendItem(pie.createSVGPathSegArcAbs(stop_on_arc.getX(), stop_on_arc.getY(), radius, radius, 0, largeArcFlag, false));
								segs.appendItem(pie.createSVGPathSegClosePath());
								//pie.setAttribute("stoke", "#FFFFFF"); // set stroke to white
								pie.setAttribute("fill", colors.get(i));
								piechart.appendChild(pie);
							}
							start_angle += angle_addup;
						} // a new pie

						/*
						 * get piechart position and move it there
						 */
						if (BranchLengthPositions.containsKey(node_internalID)) {
							System.err.println( "   -> plot pie for internal node: " + node_internalID );
							APosition pos = BranchLengthPositions.get(node_internalID);
							OMSVGTransform t = svg.createSVGTransform();
							if (pos.getAngle() != 0) {
								OMSVGPoint xy = getCoordinatesOnCircle(rootXY, rootXY.distance(pos.getXY()), pos.getAngle());
								t.setTranslate(xy.getX(), xy.getY());
							} else {
								t.setTranslate(pos.getX(), pos.getY());
							}
							piechart.getTransform().getBaseVal().appendItem(t);
						} // get position of current ID

						/**
						 * *******************************************
						 * add tooltip to this piechart may 26, 2011; tooltip
						 * Last modified: May 15, 2012
						 * ********************************************
						 */
						piechart.addMouseOverHandler(new MouseOverHandler() {

							@Override
							public void onMouseOver(final MouseOverEvent event) {
								oeelement_mouse_over = piechart;
								tooltipx = event.getClientX();
								tooltipy = event.getClientY();

								// >>>> TIMER >>>>
								new Timer() {

									@Override
									public void run() {
										if (oeelement_mouse_over != null && oeelement_mouse_over.equals(piechart)) { // if mouse is still on the same element as delay_pshow mili sec before
											FlexTable tooltipflex = makeFlextableForToolTip(colors, piechartdata, cats, null, "circle");
											tooltipWidget.setWidgetAndShow(tooltipflex, tooltipx, tooltipy);
										}
									}
								}.schedule(delayShowMiliSec);
							}
						});

						piechart.addMouseOutHandler(new MouseOutHandler() {

							@Override
							public void onMouseOut(MouseOutEvent event) {
								oeelement_mouse_over = null;
								// >>>> TIMER >>>>
								new Timer() {

									@Override
									public void run() {
										if (oeelement_mouse_over == null && !tooltipWidget.getMouseOver()) { // if mouse is not on any evelement and not on popup panel, close the panel
											tooltipWidget.hide();
										}
									}
								}.schedule(delayHideMiliSec);
							}
						});

						// <<<< tooltip <<<<
						layer_tree_piecharts.appendChild(piechart); // also add new plots into layer_tree_piecharts
					}// if radius > 0

				}// make pies 
			}// if dataset is active 
		} // makePies

		public void removePCDataByID(int pcdataID) {
			if (this.pcDatasets.containsKey(pcdataID)) {
				if (pcdataID==this.activePCDataID) {
					this.activePCDataID = -1;
					this.makePies();
				}
			}
		}

		public int getActivePieID() {
			return this.activePCDataID;
		}

		public String getOriginalUserInputByDatasetID(int pcdataid) {
			return this.pcDatasets.containsKey(pcdataid) ? this.pcDatasets.get(pcdataid).getOriginal_datastring() : "";
		}// April 6, 2011;
	} // make pies

	/*
	 * a private class to hold a position of a text label, including x,y, and
	 * rotate angle (between 0~360) jan 22, 2011
	 */
	public class APosition {

		private OMSVGPoint xy = svg.createSVGPoint();
		private float angle = 0f;

		// constructors
		public APosition() {
		}

		public APosition(OMSVGPoint newxy, float newangle) {
			xy = newxy;
			angle = newangle;
		}

		/*
		 * get-set pairs
		 */
		public OMSVGPoint getXY() {
			return xy;
		}

		public void setXY(OMSVGPoint newxy) {
			xy = newxy;
		}

		public float getAngle() {
			return angle;
		}

		public void setAngle(float newangle) {
			angle = newangle;
		}

		public float getX() {
			return xy.getX();
		}

		public float getY() {
			return xy.getY();
		}

		public void setX(float x) {
			xy.setX(x);
		}

		public void setY(float y) {
			xy.setX(y);
		}
	}

	/*
	 * this ia a base class March 22, 2011
	 */
	public abstract class PlotBase {

		public float plot_start = 0f, plotWidth = 0f, plot_end = 0f;
		public boolean active = false;
		public String defaultColor = "black";
		public int datasetID;
		public String plottype = "rect";// Nov 22, 2011
		protected String datasetContentAsString = "";
		public String linestyle = "";
		// base layer 
		public OMSVGGElement layer = doc.createSVGGElement();
		// for legend
		public ArrayList<String> groups = null, colors = null;
		public float radius = 0f, checkLineWidth = 0f, opacity = 1f, // April 7, 2011;
				barheightPCT = 60f; // Apr 29, 2013; pct space available for the bar

		public PlotBase() {
		}

		/**
		 * to be overiden
		 */
		public abstract void makeOrUpdatePlot() ;
		
		public ArrayList<String> getGroups() {
			return this.groups;
		}

		public String getType() {
			return this.plottype;
		}

		public int getDataSetID() {
			return this.datasetID;
		}

		public String getDefaultColor() {
			return this.defaultColor;
		}

		public boolean getActive() {
			return this.active;
		}

		public void setStripType(String newstriptype) {
			this.plottype = newstriptype;
		}

		public void setDataID(int newid) {
			this.datasetID = newid;
		}

		public void setDefaultColor(String dcolor) {
			this.defaultColor = dcolor;
		}

		public boolean setActive(boolean status) {
			this.active = status;
			return this.active;
		}

		public String getOriginalUserInputAsString() {
			return this.datasetContentAsString;
		}// April 5, 2011;

		public void setDatasetContentAsString(String s) {
			this.datasetContentAsString = s;
		}

		public float setOpacity(float o) {
			this.opacity = o;
			if (this.opacity > 1) {
				this.opacity = 1;
			} else if (this.opacity < 0.5) {
				this.opacity = 0.5f;
			}
			return this.opacity;
		}// April 7, 2011;

		public float getOpacity() {
			return this.opacity;
		}// April 7, 2011;

		public ArrayList<String> getLegendTexts() {
			return this.groups;
		}

		public ArrayList<String> getLegendColors() {
			return this.colors;
		} 
		
	}// PlotBase
	
	
	

	private void makeLegendEntryAndUpdateAllLegend(
			boolean bShowLegend,
			ArrayList<String> colors, 
			ArrayList<String> groups, 
			int datasetID, 
			String legendTitle,
			String legendStyle, TreeDecoType type, 
			boolean datasetactive) {

		// first of all, check if legend entry exists
		LegendEntry pl = null;
		if (colors != null && groups != null) {
			if (colors.size() > 0 && groups.size() > 0 && colors.size() == groups.size()) {
				pl = new LegendEntry();
				pl.setTitle(legendTitle);
				pl.setDataSource(type); // deal with this later
				pl.setLegendStyle(legendStyle);
				pl.setDatasetID(datasetID);
				pl.setShowLegends(bShowLegend);
				for (int cgidx = 0; cgidx < colors.size(); cgidx++) {
					pl.addLegendItem(new LegendItem(colors.get(cgidx), groups.get(cgidx), 15f));
				}
			}// if data is valid
		}

		this.legendEntries.remove(datasetID);
		if (pl != null) {
			this.legendEntries.put(datasetID, pl);
		}

		// Oct 28, 2011; will only update all legend if current (newly added) dataset is actively
		if (datasetactive) {
			updateAllLegends();
		}
	} // May 24, 2011

	/*
	 * May 24, 2011; make legend plots for the following plot types in the
	 * following order -- 1. piechart -- 2. branch color -- 3. leaf color -- 4.
	 * leaf background -- 5. charts
	 * 
	 * May 18, 2013; made public
	 */
	public void updateAllLegends() {
		// clear tree legend
		clearAllChildNodesFromSVGGElement(layer_legend); // this is important

		float current_y = legend_start_pos_y;

		// 1. pie charts
		LegendEntry lepie = this.legendEntries.get( this.pie.getActivePieID());
		if (lepie != null && lepie.getShowLegends() && !this.pie.isAllPiechartsDisabled) {
			current_y = plotLegendEntry(current_y, lepie);
		}

		// 2. branch color
		LegendEntry lebranchcol = this.legendEntries.get(this.treeskeleton.getActiveBranchColorDatasetID());
		if (lebranchcol != null && lebranchcol.getShowLegends() && this.treeskeleton.branchColorEnabled) {
			current_y = plotLegendEntry(current_y, lebranchcol);
		}

		// 3. leaf color
		LegendEntry leleafcol = this.legendEntries.get(this.treeskeleton.getActiveLeafColorDatasetID());
		if (leleafcol != null && leleafcol.getShowLegends() && this.treeskeleton.leafColorEnabled) {
			current_y = plotLegendEntry(current_y, leleafcol);
		}

		// 4. leaf background
		LegendEntry leleafbkcolor = this.legendEntries.get(this.treeskeleton.getActiveLeafBKColorDatasetID());
		if (leleafbkcolor != null && leleafbkcolor.getShowLegends() && this.treeskeleton.leafBKColorEnabled) {
			current_y = plotLegendEntry(current_y, leleafbkcolor);
		}

		/*
		 * 5. charts May 14, 2012; add protein domain
		 */
		for (PlotBase pb : activeCharts) {
			// runtime instances check
			LegendEntry lecharts = null;
			if (pb instanceof ColorStrips) {
				lecharts = this.legendEntries.get( ((ColorStrips) pb).getDataSetID());
			} else if (pb instanceof Bars) {
				lecharts = this.legendEntries.get( ((Bars) pb).getDataSetID());
			}else if (pb instanceof HeatMap) {
				lecharts = this.legendEntries.get( ((HeatMap) pb).getDataSetID());
			} else if (pb instanceof ProteinDomains) {
				lecharts = this.legendEntries.get( ((ProteinDomains) pb).getDataSetID());
			}
			if (pb.getActive() && lecharts != null && lecharts.getShowLegends() && this.chart_plots_enabled) {
				// System.out.println("I'm not null!!!");
				current_y = plotLegendEntry(current_y, lecharts);
			}
		}
	}// 

	private float plotLegendEntry(float current_y, LegendEntry legend) {
		// 1. plot title 
		String title = legend.getTitle();
		int title_font_size = this.default_legend_font_size + 2;
		JsArrayInteger title_height_width = JSFuncs.stringPixelWidthHeight(title, this.default_font_family, title_font_size);

		int title_width = title_height_width.get(0);
		int title_height = title_height_width.get(1);

		String legendstyle = legend.getLegendStyle();

		current_y += title_height;
		OMSVGTextElement text = doc.createSVGTextElement(legend_start_pos_x, current_y, OMSVGLength.SVG_LENGTHTYPE_PX, title);
		text.getStyle().setFontSize(title_font_size, Unit.PX);

		current_y += this.space_between_legend_items * 1 / 2;
		OMSVGLineElement line = doc.createSVGLineElement(legend_start_pos_x, current_y, legend_start_pos_x + title_width + 10, current_y);
		line.setAttribute("stroke", "darkgrey");
		line.setAttribute("stroke-linecap", "round");

		layer_legend.appendChild(text);
		layer_legend.appendChild(line);

		current_y += this.space_between_legend_items * 1 / 2;

		// 2. plot legends
		for (LegendItem item : legend.getLegendItems()) {

			// 2.1 legend [circle/ rect/ star....]
			float item_height = item.getWidth();
			if (legendstyle.equalsIgnoreCase("circle")) {
				OMSVGCircleElement circle = doc.createSVGCircleElement(legend_start_pos_x + item_height / 2, current_y + item_height / 2, item_height / 2);
				circle.setAttribute("fill", item.getColor());
				circle.setAttribute("stroke", "white");
				layer_legend.appendChild(circle);
			} else if (legendstyle.equalsIgnoreCase("star")) {
				OMSVGPolygonElement star = makeStar(svg.createSVGPoint(legend_start_pos_x + item_height / 2, current_y + item_height / 2), item_height / 4, 2);
				star.setAttribute("fill", item.getColor());
				star.setAttribute("stroke", "white");
				layer_legend.appendChild(star);
			} else { // default is rect
				OMSVGRectElement rect = doc.createSVGRectElement(legend_start_pos_x, current_y, item_height, item_height, 2, 2);
				rect.setAttribute("fill", item.getColor());
				rect.setAttribute("stroke", "white");
				layer_legend.appendChild(rect);
			}

			// 2.2 text
			OMSVGTextElement item_text = doc.createSVGTextElement(legend_start_pos_x + item_height + space_between_legend_and_text, current_y + item_height / 2
					+ (float) this.default_font_size * 1 / 3, OMSVGLength.SVG_LENGTHTYPE_PX, item.getName());
			item_text.getStyle().setFontSize(this.default_legend_font_size, Unit.PX);
			layer_legend.appendChild(item_text);

			// 2.3 change current Y
			current_y += item_height + space_between_legend_items;
		}

		// 3. change current Y again
		current_y += (space_between_legends - space_between_legend_items);

		return current_y;
	} // May 24, 2011

	private class LegendEntry {

		private String title = "", legendStyle = "";
		@SuppressWarnings("unused")
		private int datasetID ;
		private ArrayList<LegendItem> items = new ArrayList<LegendItem>();
		private float space_between_legend_item = 3f;
		private float height_per_legenditem = 12f;
		@SuppressWarnings("unused")
		private TreeDecoType tdtype = null;
		private boolean show = false;

		public LegendEntry() {
		}

		public void setTitle(String ti) {
			this.title = ti;
		}

		public void addLegendItem(LegendItem newitem) {
			items.add(newitem);
		}

		@SuppressWarnings("unused")
		public float calcLegendHeight() {
			float height = 0f;
			int items_count = items.size();

			if (items_count > 0) {
				height += items_count * height_per_legenditem + (items_count - 1) * space_between_legend_item;
			}

			return height;
		}

		@SuppressWarnings("unused")
		public void setHeightPerLegendItem(float f) {
			this.height_per_legenditem = f;
		}

		public String getTitle() {
			return title;
		}

		public ArrayList<LegendItem> getLegendItems() {
			return this.items;
		}

		public void setDataSource(TreeDecoType ds) {
			this.tdtype = ds;
		}

		//		public TreeDecoType getDataSource() {
		//			return this.tdtype;
		//		}

		public String getLegendStyle() {
			return this.legendStyle;
		}

		public void setLegendStyle(String st) {
			this.legendStyle = st;
		}

		public void setDatasetID(int id) {
			this.datasetID = id;
		}

		//		public String getDatasetID() {
		//			return this.datasetID;
		//		}

		public void setShowLegends(boolean b) {
			this.show = b;
		}

		public boolean getShowLegends() {
			return this.show;
		}
	}

	private class LegendItem {

		private String color = "", name = "";
		private float width = 0f;

		public LegendItem(String cl, String na, float ra) {
			color = cl;
			name = na;
			width = ra;
		}

		public String getName() {
			return this.name;
		}

		public String getColor() {
			return this.color;
		}

		public float getWidth() {
			return this.width;
		}

		@SuppressWarnings("unused")
		public void setName(String na) {
			this.name = na;
		}

		@SuppressWarnings("unused")
		public void setColor(String col) {
			this.color = col;
		}

		@SuppressWarnings("unused")
		public void setWidth(float wi) {
			this.width = wi;
		}
	}

	/**
	 * Sep 10, 2013 --
	 */
	
	public int getCurrentFontSize4BootStrap() {
		return this.default_bootstrap_font_size;
	}

	public int getCurrentFontSize4BranchLength() {
		return this.default_branchlength_font_size;
	}

	public boolean getBootStrapFontItalic() {
		return this.bootStrapFontItalic;
	}

	public void setBootStrapFontItalic(boolean b) {
		if (b != bootStrapFontItalic) {
			bootStrapFontItalic = b;
			layer_tree_bootstrap.setAttribute("font-style", this.bootStrapFontItalic ? "italic" : "normal");

			this.updateTreeCanvasInfoOnServer("bootStrapFontItalic", bootStrapFontItalic ? 1 : 0);
		}
	}

	public boolean getBranchLengthFontItalic() {
		return this.branchLengthFontItalic;
	}

	public void setBranchLengthFontItalic(boolean b) {
		if (b != branchLengthFontItalic) {
			branchLengthFontItalic = b;
			layer_tree_branchlength.setAttribute("font-style", this.branchLengthFontItalic ? "italic" : "normal");

			this.updateTreeCanvasInfoOnServer("branchLengthFontItalic", bootStrapFontItalic ? 1 : 0);
		}
	}

	public void setBootStrapFontSize(int newFontSize) {
		if (newFontSize > 0 && newFontSize != default_bootstrap_font_size) {
			default_bootstrap_font_size = newFontSize;

			this.layer_tree_bootstrap.getStyle().setFontSize(default_bootstrap_font_size, Unit.PX);
			this.updateTreeCanvasInfoOnServer("bootStrapFontSize", default_bootstrap_font_size);
		}
	}

	public int getBootStrapFontSize() {
		return this.default_bootstrap_font_size;
	}

	public void setBranthLengthFontSize(int newFontSize) {
		if (newFontSize > 0 && newFontSize != default_branchlength_font_size) {
			default_branchlength_font_size = newFontSize;

			this.layer_tree_branchlength.getStyle().setFontSize(default_branchlength_font_size, Unit.PX);
			this.updateTreeCanvasInfoOnServer("branchLengthFontSize", default_branchlength_font_size);
		}
	}

	public int getBranchLengthFontSize() {
		return this.default_branchlength_font_size;
	}
	// <<<<<<< sep 10, 2013 <<<<<<<<<

}