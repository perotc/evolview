package org.evolview.client.phyloUtils;

public enum TreeDecoType {
    PIES,
    STRIPS,
    BARS,
    LEAFCOLOR,
    LEAFBKCOLOR,
    BRANCHCOLOR,
    CHARTS,
    PROTEINDOMAINS
}
