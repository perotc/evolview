package org.evolview.client.phyloUtils;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.xml.client.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.evolview.client.utils.JSFuncs;

/*
 * >>>> bootstrap score support in newick format; normal + itol styles (input)
 * >>>> dec 29, 2010: in newick format, bootstrap scores often take positions of
 * internal node IDs, for example: (A:0.12,B:0.31)90:0.45; in which '90' is
 * suppose to be a ID for internal node, which make things confusing
 *
 * this format is supported by many programs, including Dendroscope, phyml and
 * phylip
 *
 * itol supports this format; however, it has its own implementation /
 * extention, like the following: (A:0.12,B:0.31):0.45[90]; which is better in
 * my oppinoin; however, this format is not supported by Dendroscope
 *
 *
 * >>>> levels (horizontal and vertical); useful in drawing phylo-tree >>>>> dec
 * 29, 2010: levels are assigned to leaf nodes while they are created; level
 * values for internal nodes are calculated using two functions: "" (for
 * horizontal) and "" (for vertical)
 *
 * >>>> add support to output tree in newick format >>>> dec 29, 2010:
 *
 * >>>>> add fail-save to parsing the results >>>>>>
 *
 * >>>> Feb 2, 2012: add support to ncbi exported phylip format check the tree
 * label names before exporting
 * 
 * >>>> April 4, 2013; fix a bug in parsing tree like : ((a:1,b):3,(c:1,(d:1,e:3):1):2);
 * an older version thinks this tree has bootstrap values
 * 
 * >>>> Sep 10, 2013; fix a but in parsing tree like : ((a,b)0.88,(c,d)0.99)0.99;
 * this tree has bootstrap value, but not branch length (i think it's very strange)
 * 
 * >>>> oct 19, 2013: fix a bug in parsing bootstrap value in nexus format like this: ... A:0.1)[&label=99]:0.2
 * 
 * >>>> oct 25, 2013: implement reroot tree function; a tree can be rerooted at a leaf node as well as an internal node
 * 
 * 
 */
public final class PhyloTree implements IsSerializable {

	// NOTE: allNodes DO contain rootnode 
	private ArrayList<PhyloNode> allNodes = new ArrayList<PhyloNode>(), leafNodes = new ArrayList<PhyloNode>(); // March 24, 2011
	private PhyloNode rootNode = null;
	private int treeId;
	private String treeName = "", treeString = "", treeFormat = "", internalID = "", description = "", errormessage = "";
	// leaf selection
	private final Set<String> leafSelectionSet = new HashSet<String>();
	private boolean hasBootStrap = false, hasBranchlength = false, isValidDataset = true, isTreeEndWithSemiColon = false;
	private int maxVerticalLevel = 0, serial_internal_node = 1, serial_leaf_node = 0;
	private HashMap<String, PhyloNode> hashID2Nodes = new HashMap<String, PhyloNode>(); // Jan 22, 2011; 'ID' could be leaf node IDs or internal IDs
	private HashMap<String, String> hsInternalID2externalID = new HashMap<String, String>();
	private float maxRootToTipTotalBranchLength = 0;

	// oct 20, 2013: some regular expression stuff
	RegExp regExpFloat = RegExp.compile("(\\d+\\.?\\d*)");
	RegExp matchLeftBracket = RegExp.compile("\\[");

	/*
	 * constructors
	 */
	public PhyloTree() {
		// do nothing
	}

	// initiate tree using a string; constructor
	/*
	 * bug fix: Feb 28, 2011 fix a bug that trees like :
	 * (A:0.1,B:0.2,(C:0.3,D:0.4)100:0.5)90:0.43;
	 * (A:0.1,B:0.2,(C:0.3,D:0.4)0.5)0.43; are not read correctly;
	 *
	 * there are two changes to make this bug go away: 1. assign "" as default
	 * node ID for internal nodes 2. add the following lines in
	 * calcDistanceToRoot(): this way the branchlength of root will be ignored
	 * (as it should be)
	 *
	 * float branchlength = node.isRoot() ? 0 : node.getBranchLength(); // Feb
	 * 28, 2011; bug fix accumulated_distance += branchlength;
	 *
	 * Feb 28, 2011 last modified: Oct 28, 2011;
	 */
	public PhyloTree(int treeId, String treename, String treestr, String format, String selectedLeaves) {
//		Window.alert("PhyloTree / selectedleaves= " + selectedLeaves);
		this.setTree(treeId, treename, treestr, format, selectedLeaves);
	}

	public void setTree(int treeId, String treename, String treestr, String format, String selectedLeaves) {
//		Window.alert("setTree / selectedleaves= " + selectedLeaves);
		Set<String> lefSelSet = new HashSet<String>();
		if(selectedLeaves!=null) {
			String[] selectedLeavesArray = selectedLeaves.trim().split(";");
			for(int i = 0; i< selectedLeavesArray.length; i++){
				lefSelSet.add(selectedLeavesArray[i]);
			}
		}
		this.setTree(treeId, treename, treestr, format, lefSelSet);
	}

	public void setTree(int treeId, String treename, String treestr, String format, Set<String> lefSelSet) {
		/*
		 * first of all, reset all variables
		 */
		this.allNodes.clear();
		this.leafNodes.clear();
		this.rootNode = null;

		this.leafSelectionSet.clear();
		if(lefSelSet!=null) {
			this.leafSelectionSet.addAll(lefSelSet);
		}

		
		// error messages
		this.errormessage = "";

		// booleans
		this.hasBootStrap = false;
		this.hasBranchlength = false;
		this.isValidDataset = true;
		this.isTreeEndWithSemiColon = false;

		// ints; May 30, 2012: 
		this.maxVerticalLevel = 0;
		this.serial_internal_node = 1;
		this.serial_leaf_node = 0;

		// hashs
		this.hashID2Nodes.clear();
		this.hsInternalID2externalID.clear();

		// floats
		this.maxRootToTipTotalBranchLength = 0;

		/*
		 * then parse the tree string
		 */
		treestr = treestr.trim();

		this.treeId = treeId;
		this.treeName = treename;
		this.treeString = treestr;
		this.treeFormat = format;

		/*
		 * initiate rootnode and add it to allNodes
		 */
		this.rootNode = this.makeNewInternalNode("", "INT" + serial_internal_node, true, null);

		if (format.equals("newick")) { // currently only newick trees are supported
			treestr = JSFuncs.JsRemoveNewLines(treestr);

			/**
			 * first of all, check if the tree is valid
			 */
			int iBrackets = 0;
			for (char c : treestr.trim().toCharArray()) {
				if (c == '(') {
					iBrackets++;
				} else if (c == ')') {
					iBrackets--;
				}
			}//

			if (iBrackets != 0) {
				this.isValidDataset = false;
				this.errormessage = "the numbers of \'(\' and \')\' do not match, please check your tree!!";
			} else {
				newickParser(treestr, null, this.rootNode);
			}
		} else if (format.equalsIgnoreCase("nhx")) {
			treestr = JSFuncs.JsRemoveNewLines(treestr);
			this.nhxParser(treestr);
		} else if (format.equalsIgnoreCase("nexus")) {
			this.nexusParser(treestr);
		} else if (format.equalsIgnoreCase("phyloXML")) {
			this.phyloXMLParser(treestr);
		}

		/*
		 * March 2, 2011; check if current tree data is valid
		 */
		if (this.isValidDataset) { // if it is still true
			if (this.allNodes.size() < 3) {
				this.isValidDataset = false;
				this.errormessage = "at least THREE nodes (two leaves and one internal node) are required";
			} else if (this.isTreeEndWithSemiColon == false) {
				this.isValidDataset = false;
				this.errormessage = "tree doesn't end with a ';', please check";
			} else {

				/*
				 * check intergrety of current tree a tree is valid if only : 1. all
				 * nodes have valid/ non-null ID 2. all nodes have unique ID
				 */
			}
		}//

		if (this.isValidDataset) {
			/*
			 * after successfully loading the tree, calculate height (max
			 * distance to tip) and distance to root
			 */
			hasBranchlength = this.hasBranchlength(); // this goes first!!

			this.reCalcDistanceToRoot();
			this.reCalcMaxDistanceToTip();
			this.reCalcLevels(); //actually this function can be intergrated with reCalcMaxDistanceToTip or reCalcDistanceToRoot; but I don't want the mix thing tegother
			this.reDoID2Node();

		} // if tree is valid
	}

	private void reDoID2Node() {

		hashID2Nodes.clear();

		/*
		 * at the end, go through all nodes and make a ID2Node hash
		 */
		for (PhyloNode node : this.allNodes) {
			String node_id = node.getID(); // NOTE: jan 25, 2011; rootNote usually doesn't have any ID
			String internalnode_id = node.getInternalID();

			if (node_id != null && node_id.trim().length() > 0) {
				this.hashID2Nodes.put(node_id, node);
			}

			// internalID cannot be empty
			if (internalnode_id.trim().length() > 0) {
				this.hashID2Nodes.put(internalnode_id, node);
			}
		}
	}

	/*
	 * >>>>>>>> >>>>>>>>> some important methods; I set them 'protected/private'
	 * so that they are invisible to users
	 */
	private void reCalcLevels() {
		/*
		 * calculate horizontal and vertical levels for internal nodes, vertical
		 * level = (min(levels of all descendents) + max(levels of all
		 * descendents)) / 2; horizontal level = max(levels of all descendents)
		 * + 1; therefore root has the max horizontal level
		 *
		 * jan 7, 2011; add level_vertical_slanted = (min(levels of all
		 * descendents) + (max(levels of all descendents)) - min) / 2;
		 */
		/*
		 * parameters are: node, array ref to hold horizontal levels of all its
		 * descendents array ref to hold vertical levels of all its descendents
		 * array ref to hold horizontal levels of its parent array ref to hold
		 * vertical levels of its parent array ref to hold vertical levels of
		 * all its leaf descendents of the parent node array ref to hold
		 * vertical levels of all its leaf descendents of the current node
		 */
		calcLevels(rootNode, new ArrayList<Float>(), new ArrayList<Float>(), new ArrayList<Float>(), new ArrayList<Float>(), new ArrayList<Float>(), new ArrayList<Float>());

	}

	private void reCalcDistanceToRoot() {
		/*
		 * recalculate distance_to_root for each node; distance to root is the
		 * total branch length from a given node to the root I use a nested
		 * function to do the calculation
		 *
		 * NOTE: this program will continue if only there is valid branchlength
		 */

		if (this.hasBranchlength == false) {
			return;
		}

		calcDistanceToRoot(rootNode, 0);
	}

	private void reCalcMaxDistanceToTip() {
		/*
		 * recalculate height for each node; height is the max branchlength to
		 * get to the tip; start with leaf nodes; calculate accumulative branch
		 * length from it to internal nodes
		 */

		if (this.hasBranchlength == false) {
			return;
		}

		for (PhyloNode pnode : allNodes) {
			if (pnode.isLeaf() == true) { // start with leaf node only
				calcMaxDistanceToTip(pnode, 0);
			}
		}

	}

	/**
	 * 
	 * @param id  real Id
	 * @param branch_length
	 * @param parentnode
	 * @param level_vertical
	 * @return
	 */
	private PhyloNode makeNewLeafNode(String id, float branch_length, PhyloNode parentnode, int level_vertical) {
		PhyloNode leafnode = new PhyloNode();
		leafnode.setLeaf(true);
		leafnode.setRoot(false);
		leafnode.setID(id);
		leafnode.setBranchLength(branch_length);

		/*
		 * level_vertical actually is the serial ID of leaf node; I use
		 * LEF_serial as internal ID of leaf nodes; Jan 14, 2011
		 */
		String internalid = "LEF_" + level_vertical;
		leafnode.setInternalID(internalid);

		/*
		 * horizontal and vertical levels; dec 29, 2010
		 */
		leafnode.setLevelHorizontal(1); // all leaf nodes have a horizontal level of 1
		leafnode.setLevelVertical(level_vertical); // leaf nodes have integer vertical levels starting with 1

		/*
		 * parental and descendental relationships
		 */
		leafnode.setParent(parentnode);
		parentnode.addADescendent(leafnode);

		/*
		 * add new leaf node
		 */
		this.leafNodes.add(leafnode);
		this.allNodes.add(leafnode);
		this.hsInternalID2externalID.put(internalid, id);

		maxVerticalLevel = serial_leaf_node; // dec 30, 2010

		return leafnode;
	}

	// Dec 5, 2011; can be used to make rootnode
	private PhyloNode makeNewInternalNode(String id, String internal_id, boolean isroot, PhyloNode parentnode) {
		PhyloNode newnode = new PhyloNode();
		newnode.setInternalID(internal_id);
		newnode.setRoot(isroot);
		newnode.setLeaf(false);
		newnode.setID(id);

		//System.out.println(id + " = " + internal_id);

		// dec 5, 2011
		if (!isroot && parentnode != null) {
			newnode.setParent(parentnode);
			parentnode.addADescendent(newnode);
		}

		this.allNodes.add(newnode);
		this.hsInternalID2externalID.put(internal_id, id);
		return newnode;
	}

	/*
	 * a function calls itself during runing is quite dangerous ...
	 */
	protected void calcDistanceToRoot(PhyloNode node, float accumulated_distance) {
		float branchlength = node.isRoot() ? 0 : node.getBranchLength(); // Feb 28, 2011; bug fix
		accumulated_distance += branchlength;
		node.setBranchLengthToRoot(accumulated_distance);

		if (node.isLeaf() == true) {
			return;
		}

		for (PhyloNode descendent_node : node.getDescendents()) {
			calcDistanceToRoot(descendent_node, accumulated_distance);
		}
	}

	//
	protected void calcMaxDistanceToTip(PhyloNode pnode, float accumulated_height) {
		if (pnode.getMaxDistanceToTip() < accumulated_height) {
			pnode.setMaxDistanceToTip(accumulated_height);
		}

		if (pnode.isRoot() == true || pnode.getParent() == null) { // quite if only current node is root or it has no parent
			return;
		}

		accumulated_height += pnode.getBranchLength();

		calcMaxDistanceToTip(pnode.getParent(), accumulated_height);

		this.maxRootToTipTotalBranchLength = accumulated_height;
	}

	// created: dec 29, 2010; last modified: dec 29, 2010; version = 1.0;
	// revised jan 7, 2011; ver = 1.1; add level_vertical_slanted (for slanded middle mode);
	// also added min/max leaf vertical level for each internal node, for slanded normal mode;
	protected void calcLevels(PhyloNode node, ArrayList<Float> horiz, ArrayList<Float> verti, ArrayList<Float> horiz_parent, ArrayList<Float> verti_parent,
			ArrayList<Float> verti_leaf_only_parent, ArrayList<Float> verti_leaf_only_current) {
		// do nothing is current node is leaf
		if (node.isLeaf() == true) {
			return; // do nothing
		}

		// if current node is an internal node; go through its descendents
		for (PhyloNode descendent : node.getDescendents()) {
			if (descendent.isLeaf() == true) {
				horiz.add(descendent.getLevelHorizontal());
				verti.add(descendent.getLevelVertical());

				verti_leaf_only_current.add(descendent.getLevelVertical()); // jan 7, 2011
			} else {
				/*
				 * call this function itself, be careful always;
				 */
				calcLevels(descendent, new ArrayList<Float>(), new ArrayList<Float>(), horiz, verti, verti_leaf_only_current, new ArrayList<Float>()); // call this function itself
			}
		}

		/*
		 * now we've obtained horizontal and vertical levels for all
		 * descendents, I'll do: a. calculate and save vertical and horizontal
		 * level for current node b. return the two values to the parent node,
		 * because these values are necessary to calculate corresponding levels
		 * of the parent.
		 */
		// calculate vertical level for current node and add the value to its parent node
		float current_vertical_level = (Collections.min(verti) + Collections.max(verti)) / 2;
		node.setLevelVertical(current_vertical_level);
		verti_parent.add(current_vertical_level);

		// calculate horizontal level for current node and add the value to its parent node
		float current_horizontal_level = Collections.max(horiz) + 1;
		node.setLevelHorizontal(current_horizontal_level);
		horiz_parent.add(current_horizontal_level);

		// jan 7, 2011; level_vertical_slanted; and min/max leaf vertical level for each internal nodes
		float min_leaf_vertial_level = Collections.min(verti_leaf_only_current);
		float max_leaf_vertial_level = Collections.max(verti_leaf_only_current);

		node.setMinLeafVerticalLevel(min_leaf_vertial_level);
		node.setMaxLeafVerticalLevel(max_leaf_vertial_level);

		node.setLevelVerticalSlanted((min_leaf_vertial_level + max_leaf_vertial_level) / 2);

		verti_leaf_only_parent.add(min_leaf_vertial_level);
		verti_leaf_only_parent.add(max_leaf_vertial_level);
	}
	// <<<<<<<< <<<<<<<<<

	/*
	 * >>>>>>> public methods >>>>>>>>> some boring /public methods (get/set
	 * pairs) so I put them at the bottom ... ...
	 *
	 */
	public String getTreeName() {
		return treeName;
	}
 
	public int getTreeId() {
		return treeId;
	}

	public void setTreeId(int treeDBSerial) {
		this.treeId = treeDBSerial;
	}

	public String getInternalID() {
		return internalID;
	}

	public void setInternalID(String newid) {
		internalID = newid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String newdesc) {
		description = newdesc;
	}

	public PhyloNode getRootNode() {
		return rootNode;
	}

	public boolean hasBootstrapScores() {
		return hasBootStrap;
	}

	public int getMaxVerticalLevel() {
		return maxVerticalLevel;
	}

	public float getMaxHorizontalLevel() {
		return rootNode.getLevelHorizontal();
	}

	// Jan 22, 2011;
	public PhyloNode getNodeByID(String node_id) {
		// Note: node_id could be internal AND real IDs
		if (this.hashID2Nodes.containsKey(node_id)) {
			return this.hashID2Nodes.get(node_id);
		} else {
			return null;
		}
	}

	// Jan 22, 2011
	public ArrayList<PhyloNode> getAllAncestors(PhyloNode node) {
		ArrayList<PhyloNode> ancestorNodes = new ArrayList<PhyloNode>();

		while (node.isRoot() == false) {
			ancestorNodes.add(node.getParent());

			node = node.getParent();
		}

		return ancestorNodes;
	}

	/*
	 * has branch length? this is only true if all nodes (except root) have
	 * branch length dec 29, 2010
	 */
	public boolean hasBranchlength() {
		boolean hasbranchlength = false;
		for (PhyloNode node : this.getAllNodes()) {
			if (node.isRoot() == false) {
				if (node.getBranchLength() > 0) {
					hasbranchlength = true;
					break;
				}
			}
		} // end of for

		return hasbranchlength;
	}

	/**
	 * *******************************************************
	 * leaf nodes and leaf node names.
	 * *******************************************************
	 */
	public ArrayList<PhyloNode> getLeafNodes() {
		return this.leafNodes;
	}
	
	/*
	 * May 13, 2012
	 */
	public ArrayList<String> getAllLeafLabels() {
		ArrayList<String> leafnames = new ArrayList<String>();
		for (PhyloNode pn : this.getLeafNodes()) {
			leafnames.add(pn.getID());
		}
		return leafnames;
	} // May 13, 2012

	public PhyloNode getFirstLeafNode() {
		return this.leafNodes.size() > 0 ? this.leafNodes.get(0) : null;
	}

	public PhyloNode getLastLeafNode() {

		return this.leafNodes.size() > 0 ? this.leafNodes.get(this.leafNodes.size() - 1) : null;

	}

	/**
	 * *******************************************************
	 * all nodes *******************************************************
	 */
	public ArrayList<PhyloNode> getAllNodes() {
		return allNodes;
	}

	/*
	 * >>>> some non-get/set public functions that are more interesting
	 * Oct 24, 2013 --
	 * reroot tree at a given node, the latter can be a leaf or internal node
	 * if a leaf node is given, then the leaf node will be used as a outgroup --
	 */
	public void rerootTree(PhyloNode inode) {
		System.out.println(" ----- reroot tree starts ------");
		// first, check if this leaf connects directly to root
		if (inode.isRoot() || inode.getParent().isRoot()) {
			// cannot!!!!
		} else {
			// make a new root
			PhyloNode newroot = new PhyloNode();
			newroot.setRoot(true);
			newroot.setLeaf(false);
			newroot.setID("");
			this.serial_internal_node++;
			String internalID = "INT" + serial_internal_node;
			newroot.setInternalID(internalID);
			System.err.println("\tmake a new root with ID : " + internalID);

			// get the parent node of current leaf
			PhyloNode pnode = inode.removeFromParent();
			
			/**
			 * step 1: ----
			 * set this new root as parent of current node
			 * add current leaf to this new root;
			 */
			newroot.addADescendent(inode); // the parent will be set automatically ...

			/**
			 * make a new internal node, add it to the new root
			 */
			PhyloNode newINode = new PhyloNode();
			this.serial_internal_node++;
			String newInternalNodeID = "INT" + serial_internal_node;
			newINode.setInternalID(newInternalNodeID);
			newINode.setID("");
			newroot.addADescendent(newINode); // the parent will be set automatically ...

			/**
			 *  if this tree has branch length
			 *  two senarios:
			 *  1. if current node is a leaf, then the leaf take 95% of the branch length, and the new internal node take 5%
			 *  2. if current node is an internal node, split it 60% 40%
			 */
			if (this.hasBranchlength) {
				float branchlength = inode.getBranchLength();
				if (inode.isLeaf()) {
					inode.setBranchLength(branchlength * 0.95f);
					newINode.setBranchLength(branchlength * 0.05f);
				} else {
					inode.setBranchLength(branchlength * 0.6f);
					newINode.setBranchLength(branchlength * 0.4f);
				}
			}
			
			/**
			 *  step 3: from the parent node of current node 'pnode', go all the way up to root
			 *  change the parent / descendent relationship 
			 */
			PhyloNode cNode = newINode; // note: here pnode cannot be root, so it has to have a valid parent 
			PhyloNode pNode = pnode; //... 

			while (!pNode.isRoot()) {
				// remove all daughter nodes from current node
				cNode.removeAllDescendents();
				
				// remove pNode from its parent
				PhyloNode ppNode = pNode.removeFromParent();
				
				// make ppNode a daughter node of current node 
				cNode.addADescendent(pNode);
				
				System.err.println("   --> " + cNode.getInternalID() );
				// and also add all the daughter nodes of its parent as its daughters
				for (PhyloNode node : pNode.getDescendents()) {
					cNode.addADescendent(node);
				}

				// now:
				cNode = pNode;
				pNode = ppNode;
			}

			/**
			 * step 4 : now pNode is the root and cNode is the node that connects directly to the old root
			 * this step will delete the old root, and add other descendent nodes 
			 * to 'cNode' 
			 */
			// first of all, remove cNode from its pare
			pNode = cNode.removeFromParent(); // note this will return the new parent of cNote, not the old root
			
			// also note that the old rootNode no longer has cNode ...
			for (PhyloNode node : this.rootNode.getDescendents()) {
				pNode.addADescendent(node);
				if (this.hasBranchlength) {
					node.setBranchLength(node.getBranchLength() + cNode.getBranchLength());
				}
			}
			
			/**
			 * !!! important, this has to be done at the near end of the function, otherwise you will lose all 
			 * nodes at the other side of the root node...
			 */
			rootNode = newroot;

			/**
			 * step 5 : final steps
			 * remake all essential variables ... 
			 */
			this.allNodes.clear();
			this.leafNodes.clear();
			this.hashID2Nodes.clear();
			this.hsInternalID2externalID.clear();
			this.reMakeEssentialVariables(rootNode, 0); // oct 25, 2013 

			// recaclulate some other variables 
			this.reCalcDistanceToRoot();
			this.reCalcMaxDistanceToTip();
			this.reCalcLevels();
			//this.reDoID2Node();

			System.out.println("-------- reroot tree done ---------");
		}//
	}// function root 

	/**
	 * Oct 25, 2013; this is a recursive function
	 * the four global variables will be changed in this function:
	 * allNodes, leafNodes, hashID2Nodes, hsInternalID2externalID
	 * 
	 * also fix the parent and descendent relationships 
	 * 
	 * @param node
	 */
	private int reMakeEssentialVariables(PhyloNode node, int leafVerticalLevel) {
		// reDoID2Node () ...
		String node_id = node.getID(); // NOTE: jan 25, 2011; rootNote usually doesn't have any ID
		String internalnode_id = node.getInternalID();

		if (node_id != null && node_id.trim().length() > 0) {
			this.hashID2Nodes.put(node_id, node);
		}

		// internalID cannot be empty
		if (internalnode_id.trim().length() > 0) {
			this.hashID2Nodes.put(internalnode_id, node);
		}

		if (node.isLeaf()) {
			/**
			 * internal ID to external id; for leaf nodes only
			 */
			this.hsInternalID2externalID.put(internalnode_id, node_id);

			/**
			 * add current node to leafNodes
			 */
			this.leafNodes.add(node);
			leafVerticalLevel++;
			node.setLevelVertical(leafVerticalLevel);
		} else {

			for (PhyloNode dnode : node.getDescendents()) {
				//dnode.setParent( node );
				leafVerticalLevel = reMakeEssentialVariables(dnode, leafVerticalLevel);
			}
		}

		/**
		 * add all nodes to allnodes, including root node
		 */
		this.allNodes.add(node);

		return leafVerticalLevel;
	}

	/*
	 * March 2, 2011;
	 */
	public boolean isTreeDataValid() {
		return this.isValidDataset;
	}

	public String getErrorMessage() {
		return this.errormessage;
	}

	public float getMaxTotalBranchLengthFromRootToAnyTip() {
		return this.maxRootToTipTotalBranchLength;
	}

	/**
	 * get last common ancestor given any leaf-nodes (internal, non-internal Jan
	 * 22, 2011 getLCA works for any nodes, leaf or non-leaf; March 17 2011;
	 */
	public PhyloNode getLCA(ArrayList<String> leaf_ids) {

		/*
		 * first of all, get a list of valid nodes and their ancestors
		 * corresponding to leaf_ids
		 */
		ArrayList<PhyloNode> leaf_nodes = new ArrayList<PhyloNode>();
		ArrayList<ArrayList<PhyloNode>> ancestors = new ArrayList<ArrayList<PhyloNode>>();
		for (String leaf_id : leaf_ids) {
			PhyloNode leaf_node = this.getNodeByID(leaf_id);
			if (leaf_node != null) {
				ancestors.add(this.getAllAncestors(leaf_node));
				leaf_nodes.add(leaf_node);
			}
		}

		//
		PhyloNode lca = null;

		if (leaf_nodes.size() == 1) {
			lca = leaf_nodes.get(0);
		} else if (leaf_nodes.isEmpty()) {
			// do nothing
		} else {

			for (PhyloNode ancestor : ancestors.get(0)) {
				boolean isLCA = true;
				for (int idx = 1; idx < ancestors.size(); idx++) {
					if (ancestors.get(idx).contains(ancestor) == false) {
						isLCA = false;
					}
				}

				if (isLCA == true) {
					lca = ancestor;
					break;
				}

			}// for ancesters
		} // leaf_nodes.size

		return lca;
	} // get lca

	/**
	 * >>>> to string in Newick| nexus | nhx format >>>>> three parameters are
	 * required: 1. show/ hide bootstrap score, if available 2. show/ hide
	 * internal id (if not 3, it's not possible to show both internal ID and
	 * bootstrap score at the same time) 3. itol-compatible style bootstrap
	 * scores
	 *
	 * note: if the tree has branch lengths, they will appear in the tree
	 */
	// created on dec 29, 2010; last modified: dec 29, 2010; version = 1.0; by Weihua Chen
	public String toTreeString(String treeFormat, boolean showBootstrap, boolean showInternalID, boolean writeItolFormat) {
		/*
		 * check the following parameters first showBootstrap showInternalID
		 * showBranchlength writeItolFormat; note writeItolFormat is true only
		 * if showBootstrap == true
		 */
		boolean showBranchlength = this.hasBranchlength; //will show branch length if the tree has bl-values

		if (showBootstrap == true && this.hasBootStrap == false) {
			showBootstrap = false;
		}

		if (showBootstrap == false || showBranchlength == false || treeFormat.equalsIgnoreCase("nexus")) { // itol style requires both bootstrap AND branchlength
			writeItolFormat = false;
		}

		if (showBootstrap == true && writeItolFormat == false) {
			showInternalID = false;
		}

		/*
		 * Dec 5, 2011; prepare translate
		 */
		HashMap<String, String> hashTranslate = null;
		if (treeFormat.equalsIgnoreCase("nexus")) {
			hashTranslate = new HashMap<String, String>();
			int serial = 0;
			for (PhyloNode leafnode : this.leafNodes) {
				serial++;
				hashTranslate.put(leafnode.getID(), Integer.toString(serial));
			}
		}

		/*
		 * >>>> how to write a tree to string? dec 29, 2010 >>>>> basically,
		 * there are three steps: 1. for each internal node, find all its
		 * descendents, join them with ',', and surround the joint string with
		 * () 2. append ID/internalID/boostrap/branchlength of the internal node
		 * to the string 3. repeat 1&2 until the internal node is root
		 */
		// let's start with a simple tree
		String treestr_newick = phyloNodeToString(this.rootNode, new ArrayList<String>(), new ArrayList<String>(), showBootstrap, showInternalID, showBranchlength,
				writeItolFormat, treeFormat, hashTranslate);

		if (treeFormat.equalsIgnoreCase("nexus")) {
			StringBuilder treestr = new StringBuilder();
			treestr.append("#NEXUS\n\n").append("Begin trees; [Treefile created using EViewer]\n").append("\tTranslate\n");
			int serial = 0;
			for (PhyloNode leafnode : this.leafNodes) {
				serial++;
				if (leafnode.getID().contains(" ")) {
					treestr.append("\t\t").append(serial).append(" '").append(leafnode.getID()).append("'");
				} else {
					treestr.append("\t\t").append(serial).append(" ").append(leafnode.getID());
				}

				if (serial < this.leafNodes.size()) {
					treestr.append(",");
				}
				treestr.append("\n");
			}
			treestr.append(";").append("\n");
			treestr.append("\t tree tree_1 = [&R] ").append(treestr_newick).append("\n");
			treestr.append("End;").append("\n");

			treestr_newick = treestr.toString();
		}

		return treestr_newick;
	}

	public String getTreeString() {
		return this.treeString;
	}

	public Set<String> getLeafSelectionSet() {
		return leafSelectionSet;
	}
 

	// Dec 5, 2011; convert phylotree to phyloXML format
	public String toXMLString() {
		Document xml = XMLParser.createDocument();
		Element phyloxml = xml.createElement("phyloxml");
		phyloxml.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		phyloxml.setAttribute("xsi:schemaLocation", "http://www.phyloxml.org http://www.phyloxml.org/1.10/phyloxml.xsd");
		phyloxml.setAttribute("xmlns", "http://www.phyloxml.org");
		xml.appendChild(phyloxml);

		Element phylogeny = xml.createElement("phylogeny");
		phylogeny.setAttribute("rooted", "true");
		phyloxml.appendChild(phylogeny);

		phyloNodeToXML(this.rootNode, phylogeny, xml);
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + xml.toString();
	}

	// Dec 5, 2011; to export to other tree formats, including nhx and nexus
	protected String phyloNodeToString(PhyloNode node, ArrayList<String> for_descendents, ArrayList<String> for_parent, boolean showBootstrap, boolean showInternalID,
			boolean showBranchlength, boolean writeItolFormat, String treeFormat, HashMap<String, String> hashTranslate) {

		// do nothing if current node is leaf
		if (node.isLeaf() == true) {
			return null; // do nothing
		}

		// if current node is an internal node; go through its descendents
		for (PhyloNode descendent : node.getDescendents()) {
			if (descendent.isLeaf() == true) {
				/*
				 * assemble a leaf string: ID:branchlength, in which
				 * ':branchlength' is depending on showBranchlength and add it
				 * to 'for_descendents'
				 */
				// leaf id
				String str_leaf = descendent.getID();
				if (treeFormat.equalsIgnoreCase("nexus") && hashTranslate != null && hashTranslate.containsKey(str_leaf)) {
					str_leaf = hashTranslate.get(str_leaf);
				} else if (str_leaf.contains(" ")) {
					str_leaf = "'" + str_leaf + "'"; // Feb 2, 2012
				}

				// leaf branch length, if available
				if (showBranchlength == true) { // showBranchlength is true means there is branch-length data for all nodes
					str_leaf += ":" + Float.toString(descendent.getBranchLength());
				}

				// nhx string, if available; not implemented

				for_descendents.add(str_leaf);
			} else {
				/*
				 * call this function itself, be careful always;
				 */
				phyloNodeToString(descendent, new ArrayList<String>(), for_descendents, showBootstrap, showInternalID, showBranchlength, writeItolFormat, treeFormat, hashTranslate); // call this function itself
			}
		}

		/*
		 * now we've obtained string representation of all descendents for
		 * current node, I'll a. assemble string representation for this
		 * (internal) node b. return the string to the parent node, it's needed
		 * to assemble string representation for the parent node
		 */
		// assemble string representation for current internal node;
		// normally, one internal node should at least have two+ descendents,
		//   but in itol, internal node without descendents is supported; I'll deal with this latter
		String str_internal_node = "";

		int size_of_for_descendents = for_descendents.size();
		for (int i = 0; i < size_of_for_descendents; i++) {
			str_internal_node += for_descendents.get(i);
			if (i < (size_of_for_descendents - 1)) {
				str_internal_node += ",";
			}
		}
		str_internal_node = "(" + str_internal_node + ")";

		/*
		 * get annotation to current internal node
		 */
		String internalnode_id = node.getID().length() > 0 ? node.getID() : node.getInternalID();
		if (internalnode_id.contains(" ")) {
			internalnode_id = "'" + internalnode_id + "'"; // Feb 2, 2012
		}

		String str_anno = "";

		if (treeFormat.equalsIgnoreCase("nhx")) {
			HashMap<String, String> nhxAttrs = new HashMap<String, String>();
			if (showBootstrap) {
				nhxAttrs.put("B", Float.toString(node.getBootStrap()));
			}
			if (showInternalID) {
				nhxAttrs.put("ND", node.getInternalID());
			}

			// now summarize --
			if (showBranchlength) {
				str_anno += ":" + node.getBranchLength();
			}
			if (!nhxAttrs.isEmpty()) {
				str_anno += "[&&NHX";
				for (Entry<String, String> kv : nhxAttrs.entrySet()) {
					str_anno += ":" + kv.getKey() + "=" + kv.getValue();
				}
				str_anno += "]";
			}
		} else if (treeFormat.equalsIgnoreCase("newick")) {
			if (writeItolFormat == true) {
				str_anno += ":" + node.getBranchLength() + "[" + node.getBootStrap() + "]";
				if (showInternalID == true) {
					str_anno = internalnode_id + str_anno;
				}
			} else {
				if (showBootstrap == true) {
					str_anno = Float.toString(node.getBootStrap());
				}

				if (showInternalID == true) {
					str_anno = internalnode_id;
				}

				if (showBranchlength == true) {
					str_anno += ":" + node.getBranchLength();
				}
			}
		} else if (treeFormat.equalsIgnoreCase("nexus")) {
			if (showBranchlength == true) {
				str_anno += ":" + node.getBranchLength();
			}

			if (showInternalID == true) {
				str_anno += "[" + internalnode_id + "]";
			}
		}

		// append the annotation to the ID of internal node
		str_internal_node += str_anno;

		if (node.isRoot() == true) {
			str_internal_node += ';';
			return str_internal_node;
		} else {
			for_parent.add(str_internal_node);
			return null;
		} // end of node.isRoot
	} // end of function phyloNodeToString

	// Dec 5, 2011;
	protected void phyloNodeToXML(PhyloNode node, Element parentXML, Document xml) {
		// create a new element for current node
		Element nodeXML = xml.createElement("clade");
		// add nodeXML to parent XML
		parentXML.appendChild(nodeXML);

		// if current node is internal node
		if (!node.isLeaf()) {
			for (PhyloNode childnode : node.getDescendents()) {
				phyloNodeToXML(childnode, nodeXML, xml);
			}
		}

		// add branch length to current node
		if (this.hasBranchlength) {
			Text blText = xml.createTextNode("branch_length");
			blText.setNodeValue(Float.toString(node.getBranchLength()));

			Element branchXML = xml.createElement("branch_length");
			branchXML.appendChild(blText);

			nodeXML.appendChild(branchXML);
		}
		// add bootstrap
		if (this.hasBootStrap && !node.isLeaf()) {
			Text bsText = xml.createTextNode("confidence");
			bsText.setNodeValue(Float.toString(node.getBootStrap()));

			Element bootXML = xml.createElement("confidence");
			bootXML.setAttribute("type", "unknow");
			bootXML.appendChild(bsText);

			nodeXML.appendChild(bootXML);
		}

		if (!node.getID().trim().isEmpty()) {
			Element nameXML = xml.createElement("name");
			nameXML.setNodeValue(node.getID());

			Text nameText = xml.createTextNode("name");
			nameText.setNodeValue(node.getID());
			nameXML.appendChild(nameText);

			nodeXML.appendChild(nameXML);
		}
	}

	/**
	 * created: Oct 20, 2013 : a better and easier to maintain parser for newick and nexus trees
	 * NOTE: this is a recursive function
	 * @param treestr : input tree string
	 * @param hashTranslate : aliases for lead nodes (for nexsus format)
	 * @param iNode : current internal node; == rootNode the first time 'newickParser' is called 
	 */
	private void newickParser(String inputstr, HashMap<String, String> hashTranslate, PhyloNode iNode) {
		inputstr = inputstr.trim();

		// NOTE: the input string should look like this: (A,B,(C,D)E)F
		// the first char has to be (
		/**
		 * first, get what's between the first and the last Parentheses, and what's after the last right Parentheses
		 * for example, your tree : (A,B,(C,D)E)F will be split into two parts:
		 *   A,B,(C,D)E = ...
		 *   F = tail string
		 */
		String tailString = "";

		if (!inputstr.isEmpty()) {
			// remove trailing ';'
			while (inputstr.endsWith(";")) {
				this.isTreeEndWithSemiColon = true; // is this really necessary??? 
				inputstr = inputstr.substring(0, inputstr.length() - 1);
			}
			for (int idx = inputstr.length() - 1; idx >= 0; idx--) {
				if (inputstr.charAt(idx) == ')') {
					tailString = inputstr.substring(idx + 1);

					// change input str from (A,B,(C,D)E)F to A,B,(C,D)E
					inputstr = inputstr.substring(1, idx); // !!!!!
					break;
				}
			}
		} // if string is not empty

		/**
		 * parse the tail string and get information for current internal node
		 * possibilities
		 * 1. nothing ... 
		 * 2. bootstrap only: )99, or )0.99
		 * 3. branch length only: ):0.456
		 * 4. both bootstrap and branch length )99:0.456
		 * 5. internal ID : )str
		 * 6. internal ID with branch length : )str:0.456
		 * 7. itol style, internalID, bootstrap and branch length: )internalID:0.2[95]
		 * 8. nexus style bootstrap: )[&label=99]:0.456
		 */
		if (!tailString.isEmpty()) {
			// first of all, split string into two parts by ':'
			String[] parts = tailString.split(":");

			/**
			 *  deal with the first part
			 *  first, check case 8. Square brackets [&label=99]
			 */
			if (parts.length >= 1) {
				String part1 = parts[0];
				if (!part1.isEmpty()) { // if not case 3
					if (matchLeftBracket.exec(part1) != null) { // get the float number from a string like this: [&label=99]
						MatchResult m = regExpFloat.exec(part1);
						if ((m != null)) {
							float bootstrap = Float.parseFloat(m.getGroup(0));
							iNode.setBootStrap(bootstrap);
							this.hasBootStrap = true;
						}
					} else {
						// check if it is a string or numeric 
						try {
							float bootstrap = Float.parseFloat(part1);
							// if success; case 2, 4
							iNode.setBootStrap(bootstrap);
							this.hasBootStrap = true;
						} catch (NumberFormatException nfe) {
							// if fail, i assume the string is internal note name; case 5, 7
							iNode.setInternalID(part1);
						}
					}
				}// if part 1 is not empty
			}

			/**
			 * if there is a part 2 and it's not empty --
			 */
			if (parts.length >= 2) {
				String part2 = parts[1];
				if (!part2.isEmpty()) {
					if (matchLeftBracket.exec(part2) != null) { // if it is the itol style
						/**
						 * split it into two parts by '[', the first part should contain the branch lenth, while the second contains the bootstrap
						 * of cource, both could be empty,
						 * valid inputs are:
						 * :[] - both are empty,
						 * :[99]
						 * :0.456[]
						 * :0.456[99]
						 * NOTE: bootstrap value can also be float number
						 */
						String[] iparts = part2.split("\\[");
						if (iparts.length > 0) {
							// the first part: branch length
							String ipart1 = iparts[0];
							MatchResult m = regExpFloat.exec(ipart1);
							if ((m != null)) {
								float branchlength = Float.parseFloat(m.getGroup(0));
								iNode.setBranchLength(branchlength);
								this.hasBranchlength = true;
							}// branch length

							// the second part, bootstrap
							if (iparts.length >= 2) {
								String ipart2 = iparts[1];
								MatchResult m2 = regExpFloat.exec(ipart2);
								if ((m != null)) {
									float bootstrap = Float.parseFloat(m2.getGroup(0));
									iNode.setBootStrap(bootstrap);
									this.hasBootStrap = true;
								}
							} // bootstrap
						}
					} else {
						// parse branch length value; case 3,4,6,8
						try {
							float branchlength = Float.parseFloat(part2);
							// if success; case 2, 4
							iNode.setBranchLength(branchlength);
						} catch (NumberFormatException nfe) {
							// do nothing
						}
					}// if ... else ...
				} // if part2 is not empty
			}// if part2 is there
		}// if the string4internalNode string is not empty 

		/**
		 * now go through what's between the parentheses and get the leaf nodes
		 *   (A,B,(C,D)E)F = original tree
		 *   A,B,(C,D)E = the part the following codes will deal with
		 */
		if (!inputstr.isEmpty()) {
			/**
			 * split current input string into substrings, each is a daughtor node of current internal node
			 * if your input string is like this: A,B,(C,D)E
			 * it will be split into the following three daughter strings:
			 *  A
			 *  B
			 *  (C,D)E
			 * accordingly, three daughter nodes will be created, two are leaf nodes and one is an internal node 
			 */
			int brackets = 0, leftParenthesis = 0, commas = 0;
			StringBuilder sb = new StringBuilder();
			for (char c : inputstr.toCharArray()) {
				if ((c == ',' || c == ')') && brackets == 0) { // ',' usually indicates the end of an node; is || c == ')' really necessary ??? 
					// make daugher nodes
					String daughter = sb.toString();
					if (leftParenthesis > 0 && commas > 0) {
						serial_internal_node++;
						//System.out.println( serial_internal_node + "  " + daughter );
						newickParser(daughter, hashTranslate, makeNewInternalNode("", "INT" + serial_internal_node, false, iNode));
					} else {
						// a leaf daughter 
						serial_leaf_node++;
						// parse information for current daughter node
						parseInforAndMakeNewLeafNode(daughter, hashTranslate, iNode);
					}

					// reset some variables
					sb = new StringBuilder();
					leftParenthesis = 0;
				} else {
					sb.append(c); // ',' will not be recored

					if (c == ',') {
						commas++;
					}
				}

				/**
				 *  brackets is used to find the contents between a pair of matching ()s
				 *  how does this work???
				 *  
				 *  here is how the value of brackets changes if your input string is like this :
				 *  (A,B,(C,D)E)F
				 *  1    2   1 0 # value of brackets ... 
				 *  +    +   - - # operation
				 *  ^          ^ # contents between these two () will be extracted = A,B,(C,D)E
				 *  
				 *  --- 
				 *  variable 'leftParenthesis' is used to indicate whether current daughter node is likely a internal node; 
				 *  however, this alone cannot garrentee this, because the name of a leaf node may contain Parenthesis
				 *  therefore I use 'leftParenthesis' and 'commas' together to indicate an internal node  
				 */
				if (c == '(') {
					brackets++;
					leftParenthesis++;
				} else if (c == ')') {
					brackets--;
				}
			}// 
				// deal with the last daughter 
			String daughter = sb.toString();
			if (leftParenthesis > 0 && commas > 0) {
				serial_internal_node++;

				//System.out.println( serial_internal_node + "  " + daughter );
				newickParser(daughter, hashTranslate, makeNewInternalNode("", "INT" + serial_internal_node, false, iNode));
			} else {
				// a leaf daughter 
				serial_leaf_node++;
				// parse information for current daughter node
				parseInforAndMakeNewLeafNode(daughter, hashTranslate, iNode);
			}

		} /// new recursive parser 

	}// end of current function

	/**
	 * created on Oct 20, 2013 
	 * input: the leafstr to be parsed, the internal node the leaf node has to be added to 
	 */
	private void parseInforAndMakeNewLeafNode(String leafstr, HashMap<String, String> hashTranslate, PhyloNode iNode) {
		leafstr = leafstr.trim();
		/**
		 * parse a leaf node,
		 * possibilities are:
		 * 1. ,, - leaf node is not named (???)
		 * 2. A  - named leaf node
		 * 3. :0.1 - unamed leaf node with branch length
		 * 4. A:0.1 - named leaf node with branch length
		 */
		if (leafstr.isEmpty()) { // case 1
			makeNewLeafNode("", 0, iNode, serial_leaf_node);
		} else {
			// split it into two parts
			String parts[] = leafstr.split(":");
			float branchlength = 0;
			/**
			 * first do part2, check if it has branch length
			 */
			if (parts.length >= 2) {
				String part2 = parts[1];
				if (!part2.isEmpty()) {
					try {
						branchlength = Float.parseFloat(part2);
						this.hasBranchlength = true;
					} catch (NumberFormatException nfe) {
						// do nothing
					}
				}
			}

			/**
			 * now deal with part 1, two possibilities: named / unamed leaf node
			 */
			String part1 = parts[0];
			if (part1.isEmpty()) {
				makeNewLeafNode("", branchlength, iNode, serial_leaf_node);
			} else {
				String leafNodeName = part1.replaceAll("'", "").replaceAll("\"", "");
				leafNodeName = (hashTranslate != null && hashTranslate.containsKey(leafNodeName)) ? hashTranslate.get(leafNodeName) : leafNodeName;
				makeNewLeafNode(leafNodeName, branchlength, iNode, serial_leaf_node);
			}
		}
	}// end of parseInforAndMakeNewLeafNode

	// moved here on Nov 28, 2011;
	/**
	 * April 4, 2013;
	 * bug fix; tree like this ((a:1,b):3,(c:1,(d:1,e:3):1):2); causes bootstrap value == true 
	 * 
	 * Sep 10, 2013 : bug fix, tree with bootstrap but no branch length : ((a,b)0.88,(c,d)0.99)0.99; 
	 * 
	 * Oct 19, 2013: nexus tree with bootstrap scores like this: 
	 * 
	 */
	@SuppressWarnings("unused")
	private void newickParser(String treestr, HashMap<String, String> hashTranslate) {
		String name_str_backup = "", current_name_str = "";
		PhyloNode current_internal_node = rootNode; // currrent internal node; set to rootnode
		char previous_chr = ' ';

		boolean isBranchLength = false, isFirstLeftParenthesis = true, isRightparenthesis = false;

		for (int idx = 0; idx < treestr.length(); idx++) {
			char c = treestr.charAt(idx);
			if (c == '(') {
				/*
				 * possibilities: a. c is NOT the first '(', create a new
				 * internal node b. c is immediately after a ',', create a new
				 * internal node c. c is the first 'C', do nothing
				 */
				if (isFirstLeftParenthesis == false) { // a or b
					/*
					* if not the first '(', the previous letter MUST BE ',' or
					* '('
					*/
					if (previous_chr != '(' && previous_chr != ',') {
						this.errormessage = " pos:" + (idx - 1) + " '(' or ',' is expected, got '" + previous_chr + "'";
						isValidDataset = false;
					} else {
						/*
						 * make a new internal node NOTE: the ID of the internal
						 * node will be assigned later
						 */
						serial_internal_node++;
						current_internal_node = makeNewInternalNode("", "INT" + serial_internal_node, false, current_internal_node);
					}
				} else {
					isFirstLeftParenthesis = false; // if true, set it to false
				}

				/*
				 * reset variables
				 */
				isRightparenthesis = false;

				current_name_str = "";
				name_str_backup = "";

			} else if (c == '\'') {
				// Feb 2, 2012: add support to phylip format
				while (true) {
					idx++;
					char ch = treestr.charAt(idx);
					if (ch == '\'') { // matching 
						break; // exit while loop
					} else {
						current_name_str += ch; // append it to current_name_str if ch is not '''
					}
				}

			} else if (c == ':') {
				/*
				 * ':' indicates current tree has branch length
				 */
				isBranchLength = true;

				/**
				 * April 4, 2013; 
				 */
				if (isRightparenthesis) {
					float bootstrap = -1;
					/*
					 * >>>> if NOT d or e >>>>> in ')name:0.01', if name is
					 * numeric, it's bootstrap score, otherwise, it's id of
					 * current internal node
					 */

					if (current_name_str.trim().length() > 0) {
						try {
							bootstrap = Float.valueOf(current_name_str.trim()).floatValue();
						} catch (NumberFormatException nfe) {
							bootstrap = -1;
						}
					}

					// System.out.println("  bootstrap is : " + bootstrap + "; current name str is : " + current_name_str);

					if (bootstrap == -1) {
						current_internal_node.setID(current_name_str.trim());
					} else {
						current_internal_node.setBootStrap(bootstrap);
						hasBootStrap = true;
					}
				}

				/*
				 * back up current name string and reset it
				 */
				name_str_backup = current_name_str;
				current_name_str = new String();

			} else if (c == ',' || c == ';' || c == ')' || c == '[') {
				/*
				 * get branch length from current name string
				 */
				float branch_length = 0;
				try {
					branch_length = Float.valueOf(current_name_str.trim()).floatValue();
				} catch (NumberFormatException nfe) {
					branch_length = 0;
				}

				if (isBranchLength == true) {
					/*
					 * copy the value from backup
					 *
					 * *************************************************************
					 * May 11, 2012 bug fix: copy the contents of a string is
					 * only it's not empty
					 */
					if (!name_str_backup.isEmpty()) {
						current_name_str = name_str_backup;
					}
				}

				/*
				 * >>>>> possibilities: >>>>> a. ,name:0.01, or ,name:0.01)
				 * create a new leaf node b. )name:0.01, or )name:0.01) save ID
				 * to current internal node and set parent as current internal
				 * node c. )name:0.01; save ID to internal node and end for loop
				 * >>> dec 29, 2010; itol-style bootstrap >>> d.
				 * )name:0.01[100.00]; in this case, name is the id of current
				 * internal node, '0.01' is branch-length and 100.00 is the
				 * bootstrap-score
				 *
				 * >>> Feb 2, 2012: nexus format with internal IDs, like: e.
				 * ):0.01['internal id']
				 * 
				 * Sep 10, 2013: new possibility:  f )0.01  with bootstrap value, but no branch length 
				 * 
				 */

				// if internal node
				if (isRightparenthesis == true) {

					/*
					 * possibilities b & c means the information is for internal
					 * node; in this case, it will save the infor to current
					 * internal node and change current internal node to its
					 * parent if current internal node isn't the root
					 *
					 * dec 29, 2010; add bootstrap support in cases of b or c,
					 * the 'name' part in 'name:0.01' could be the name of the
					 * internal node or bootstrap score; the only way to
					 * distinguish one from another is: if name is numeric
					 * (integer or floot), it's bootstrap score otherwise, it's
					 * name of the internal node
					 *
					 * NOTE: only internal node should have bootstrap score
					 */

					float bootstrap = -1;

					if (c == '[') {

						// first of all, get the string between []
						// ignore '
						String string_between_parenthesis = "";
						while (true) {
							idx++;
							char ch = treestr.charAt(idx);
							if (ch == ']') {
								idx++;
								c = treestr.charAt(idx); // get the char next to ']' and assign it to c
								break; // exit while loop
							}

							if (ch != '\'') {
								string_between_parenthesis += ch;
							}
						}

						// check if cases d (itol style bootstrap) or e (internal id)
						// Oct 19, 2013: nexus style bootstrap value ... A:0.1)[&label=99]:0.2
						if (string_between_parenthesis.trim().length() > 0) {
							try {
								/*
								 * >>>> if itol style >>>> get the string
								 * between a [ and ]; and assign the char next
								 * to ']' to 'c' in cases like
								 * ')name:0.01[100.00]', name (current_name_str)
								 * is id for internal node 0.01 is branch
								 * length, 100.00 is bootstrap
								 */
								RegExp regExp = RegExp.compile("(\\d+\\.?\\d*)");
								MatchResult m = regExp.exec(string_between_parenthesis);
								if ((m != null)) {
									bootstrap = Float.parseFloat(m.getGroup(0));
									hasBootStrap = true;
								}
								//                            	
								//bootstrap = JSFuncs.extractFloatNumberFromString(string_between_parenthesis);
								//hasBootStrap = true;

								//bootstrap = Float.valueOf(string_between_parenthesis.trim()).floatValue();
								//hasBootStrap = true;

								/*
								 * set values for bootstrap and id for current
								 * internal node
								 */
								current_internal_node.setID(current_name_str.trim());
								current_internal_node.setBootStrap(bootstrap);

							} catch (NumberFormatException nfe) {
								bootstrap = -1;
								// 
								current_internal_node.setID(string_between_parenthesis.trim());
							}
						}
					} else {

						// the following was moved to if( c == ':' );
						//                        /*
						//                         * >>>> if NOT d or e >>>>> in ')name:0.01', if name is
						//                         * numeric, it's bootstrap score, otherwise, it's id of
						//                         * current internal node
						//                         */
						//
						//                        if (current_name_str.trim().length() > 0) {
						//                            try {
						//                                bootstrap = Float.valueOf(current_name_str.trim()).floatValue();
						//                            } catch (NumberFormatException nfe) {
						//                                bootstrap = -1;
						//                            }
						//                        }
						//                        
						//                        System.out.println("  bootstrap is : " + bootstrap + "; current name str is : " + current_name_str);
						//
						//                        if (bootstrap == -1) {
						//                            current_internal_node.setID(current_name_str.trim());
						//                        } else {
						//                            current_internal_node.setBootStrap(bootstrap);
						//                            hasBootStrap = true;
						//                        }

						// Sep 10, 2013 --
						// if branch length does not exist, then the name is 
						if (!isBranchLength & branch_length != 0) {
							current_internal_node.setBootStrap(branch_length);
							this.hasBootStrap = true;
						}
					} // end of 'if c == '['

					/*
					 * set branch length
					 * Sep 10, 2013; make sure isBranchLength is True --
					 */
					if (branch_length > 0 & isBranchLength) {
						current_internal_node.setBranchLength(branch_length);
					}

					/*
					 * if current node is internal, move one level up
					 */
					if (current_internal_node.isRoot() == false) {
						current_internal_node = current_internal_node.getParent(); // change current_internal_node
					}
				} else {
					/*
					 * in case of 'a' -- the first '(',  make a new leaf node and add it to
					 * 'allNodes'
					 */
					serial_leaf_node++;

					// Dec 1, 2011; translate leaf name --
					String leaf_name_str = current_name_str;
					if (hashTranslate != null && hashTranslate.containsKey(current_name_str)) {
						leaf_name_str = hashTranslate.get(current_name_str);
					}
					makeNewLeafNode(leaf_name_str, branch_length, current_internal_node, serial_leaf_node);
				}
				// <<<<< <<<<<<

				// reset string
				current_name_str = "";
				name_str_backup = "";

				// reset values
				//isBranchLength = false;

				if (c == ';') {
					this.isTreeEndWithSemiColon = true;
					break; // exit?
				}

				if (c == ')') {
					isRightparenthesis = true;
				} else {
					isRightparenthesis = false; // important
				}

			} else {
				current_name_str += c; // append c to current string
			}
			previous_chr = c;
		} // for split tree string to char
	}

	/*
	 * Nov 28, 2011; nhx format see here for more details:
	 * http://phylosoft.org/NHX/ please note that using nhx is now discoraged;
	 * use phyloXML instead
	 *
	 * nhx format shares certain similarities with newick, so sode codes were
	 * copied from the newick parser
	 *
	 * a typical nhx tree would look like:
	 *
	 * (((ADH2:0.1[&&NHX:S=human:E=1.1.1.1],
	 * ADH1:0.11[&&NHX:S=human:E=1.1.1.1]):0.05[&&NHX:S=Primates:E=1.1.1.1:D=Y:B=100],
	 * ADHY:0.1[&&NHX:S=nematode:E=1.1.1.1],ADHX:0.12[&&NHX:S=insect:E=1.1.1.1]):0.1[&&NHX:S=Metazoa:E=1.1.1.1:D=N],
	 * (ADH4:0.09[&&NHX:S=yeast:E=1.1.1.1],ADH3:0.13[&&NHX:S=yeast:E=1.1.1.1],
	 * ADH2:0.12[&&NHX:S=yeast:E=1.1.1.1],
	 * ADH1:0.11[&&NHX:S=yeast:E=1.1.1.1]):0.1
	 * [&&NHX:S=Fungi])[&&NHX:E=1.1.1.1:D=N];
	 */
	private void nhxParser(String treestr) {

		String name_str_backup = "", current_name_str = "";
		PhyloNode current_internal_node = rootNode; // currrent internal node; set to rootnode
		char previous_chr = ' ';

		boolean isBranchLength = false, isFirstLeftParenthesis = true, isRightparenthesis = false;

		for (int idx = 0; idx < treestr.length(); idx++) {
			char c = treestr.charAt(idx);
			if (c == '(') {
				/*
				 * possibilities: a. c is NOT the first '(', create a new
				 * internal node b. c is immediately after a ',', create a new
				 * internal node c. c is the first 'C', do nothing
				 */
				if (isFirstLeftParenthesis == false) { // a or b
					/*
					 * if not the first '(', the previous letter MUST BE ',' or
					 * '('
					 */
					if (previous_chr != '(' && previous_chr != ',') {
						this.errormessage = " pos:" + (idx - 1) + " '(' or ',' is expected, got '" + previous_chr + "'";
						isValidDataset = false;
					} else {
						/*
						 * make a new internal node
						 */
						serial_internal_node++;
						current_internal_node = makeNewInternalNode("", "INT" + serial_internal_node, false, current_internal_node);
					}
				} else {
					isFirstLeftParenthesis = false; // if true, set it to false
				}

				/*
				 * reset variables
				 */
				isRightparenthesis = false;

				current_name_str = "";
				name_str_backup = "";
			} else if (c == ':') {
				/*
				 * ':' indicates current tree has branch length
				 */
				isBranchLength = true;

				/*
				 * back up current name string and reset it
				 */
				name_str_backup = current_name_str;
				current_name_str = new String();

			} else if (c == ',' || c == ';' || c == ')' || c == '[') {
				/*
				 * get branch length from current name string
				 */
				float branch_length = 0;

				if (isBranchLength == true) {
					try {
						branch_length = Float.valueOf(current_name_str.trim()).floatValue();
					} catch (NumberFormatException nfe) {
						branch_length = 0;
					}

					/*
					 * copy the value from backup
					 */
					current_name_str = name_str_backup;
				}

				/*
				 * >>>>> possibilities: >>>>> a. ,name:0.01, or ,name:0.01)
				 * create a new leaf node b. )name:0.01, or )name:0.01) save ID
				 * to current internal node and set parent as current internal
				 * node c. )name:0.01; save ID to internal node and end for loop
				 */

				// in case of a: name is a leaf node; make a new leaf node and add it to 'allNodes'
				if (!isRightparenthesis) {
					serial_leaf_node++;
					makeNewLeafNode(isBranchLength ? name_str_backup : current_name_str, branch_length, current_internal_node, serial_leaf_node);
				} else {
					// in cases of b: 
					/*
					 * set branch length
					 */
					current_internal_node.setID(current_name_str.trim());

					if (branch_length > 0) {
						current_internal_node.setBranchLength(branch_length);
					}

					/*
					 * if current node is internal, move one level up
					 */
					if (current_internal_node.isRoot() == false) {
						current_internal_node = current_internal_node.getParent(); // change current_internal_node
					}
				}

				// 
				if (c == '[') {
					/*
					 * nhx style extra information -- just add the information
					 * to the last node, (leaf or internal)
					 */
					boolean first_colon = false;
					int pos_first_colon = 0;
					StringBuilder nhx_string = new StringBuilder();

					while (true) {
						idx++;
						char ch = treestr.charAt(idx);
						if (ch == ':' && !first_colon) {
							first_colon = true;
							pos_first_colon = idx;
						}

						if (ch == ']') {
							idx++;
							c = treestr.charAt(idx); // get the char next to ']' and assign it to c
							break; // exit while loop
						}

						if (first_colon && idx > pos_first_colon) {
							nhx_string.append(treestr.charAt(idx));
						}
					}

					// -- now parse nhx string --
					String nhx = nhx_string.toString();
					if (!nhx.isEmpty()) {
						PhyloNode node = this.allNodes.get(this.allNodes.size() - 1);
						HashMap<String, String> nhxAttributes = new HashMap<String, String>();
						for (String keyvaluepair : JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(nhx, ":"))) {
							if (JSFuncs.matchStringAnywhere(keyvaluepair, "=")) {
								ArrayList<String> keyvalue = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySeperator(keyvaluepair, "="));

								String k = keyvalue.get(0);
								String v = keyvalue.get(1);

								// set bootstrap to current node
								if (k.equalsIgnoreCase("B")) {
									this.hasBootStrap = true;
									float bootstrap = 0;
									try {
										bootstrap = Float.valueOf(current_name_str.trim()).floatValue();
									} catch (NumberFormatException nfe) {
										bootstrap = 0;
									}

									node.setBootStrap(bootstrap);
								} else {
									nhxAttributes.put(k, v);
								}
							}
						}
						node.setAdditionalAttributes(nhxAttributes);
					}// if not empty

					// in cases like: )[&&NHX:S=Eukaryota:D=N:B=0];
					if (c == ';') {
						this.isTreeEndWithSemiColon = true;
						break; // exit?
					}
				}// if c== '['

				// reset string
				current_name_str = "";
				name_str_backup = "";

				// reset values
				//isBranchLength = false;

				if (c == ';') {
					this.isTreeEndWithSemiColon = true;
					break; // exit?
				}

				if (c == ')') {
					isRightparenthesis = true;
				} else {
					isRightparenthesis = false; // important
				}
			} else if (c != ' ') {
				current_name_str += c; // append c to current string
			}
			// Nov 07, 2011; blank letters are now allowed
			//if (c != ' ') {
			// keep track of previous char; blank letters are omitted
			previous_chr = c;
			//}
		} // for split tree string to char
	}

	/*
	 * Dec 1-2, 2011; nexus format; note only the tree part will be processed;
	 * other data will be ignored see :
	 * http://molecularevolution.org/resources/treeformats for more details
	 *
	 * a typical nexsus tree looks like: #nexus ... begin trees; translate 1
	 * Ephedra, 2 Gnetum, 3 Welwitschia, 4 Ginkgo, 5 Pinus ; tree one = [&U]
	 * (1,2,(3,(4,5)); tree two = [&U] (1,3,(5,(2,4)); end;
	 */
	private void nexusParser(String treestr) {
		HashMap<String, String> hashTranslate = new HashMap<String, String>();
		boolean bTranslate = false;
		boolean bBeginTrees = false;

		//System.out.println("======== parsing nexus format =============\nyour input is : " + treestr);

		int line_num = 0;
		for (String line : JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringByNewLine(treestr))) {

			line = line.trim();
			line_num++;
			//System.out.println(line_num + ": " + line);

			if (line_num == 1 && !line.equalsIgnoreCase("#NEXUS")) {
				this.errormessage = "input file doesn't start with #NEXUS!!";
				isValidDataset = false;
				break;
			} else if (JSFuncs.matchStringStart(line.toLowerCase(), "Begin trees".toLowerCase())) {
				bBeginTrees = true;
			} else if (JSFuncs.matchStringStart(line.toLowerCase(), "Begin".toLowerCase()) && bBeginTrees) {
				bBeginTrees = false;
			} else if (line.equalsIgnoreCase("Translate")) {
				bTranslate = true;
			} else if (line.equals(";")) {
				bTranslate = false;
			} else if (bBeginTrees && bTranslate) {
				// split the translate part:
				ArrayList<String> parts = JSFuncs.JsArrayStringToArrayList(JSFuncs.splitStringBySpace(line));
				if (parts.size() >= 2) {
					String trans_str = "";
					for (int i = 1; i < parts.size(); i++) {
						if (i > 1) {
							trans_str += " ";
						}
						trans_str += JSFuncs.JsReplaceStringWithString(JSFuncs.JsReplaceStringWithString(parts.get(i), "'", ""), ",", "");
					}
					hashTranslate.put(parts.get(0), trans_str);
				}
			} else if (bBeginTrees && !bTranslate && JSFuncs.matchStringStart(line.toLowerCase(), "tree".toLowerCase())) {
				String substring_treestr = line.substring(line.indexOf("("));
				//System.out.println(substring_treestr);

				/**
				 * first of all, check if the tree is valid
				 */
				int iBrackets = 0;
				for (char c : substring_treestr.trim().toCharArray()) {
					if (c == '(') {
						iBrackets++;
					} else if (c == ')') {
						iBrackets--;
					}
				}//

				if (iBrackets != 0) {
					this.isValidDataset = false;
					this.errormessage = "the numbers of \'(\' and \')\' do not match, please check your tree!!";
				} else {
					newickParser(substring_treestr, hashTranslate, this.rootNode);
				}
				break; // only the first tree will be taken
			}
		}// for each line
	}// nexusParser

	/*
	 * Dec 2, 2011 -- see
	 * http://code.google.com/p/google-web-toolkit-doc-1-5/wiki/DevGuideXML for
	 * details about how GWT deal with XML string
	 */
	private void phyloXMLParser(String treestrXML) {
		try {
			Document treeDom = XMLParser.parse(treestrXML);
			if (treeDom.getElementsByTagName("phylogeny").getLength() > 0) {
				this.isTreeEndWithSemiColon = true; //?????
				Node phylogeny = treeDom.getElementsByTagName("phylogeny").item(0);

				NodeList childnodes = phylogeny.getChildNodes();
				if (phylogeny.getAttributes().getNamedItem("rooted") != null && phylogeny.getAttributes().getNamedItem("rooted").getNodeValue().equalsIgnoreCase("true")) {
					System.out.println(" =================== rooted tree =================== ");
					for (int i = 0; i < childnodes.getLength(); i++) {
						Node cnode = childnodes.item(i);
						if (cnode.getNodeName().equalsIgnoreCase("clade")) {
							childnodes = cnode.getChildNodes();
							break;
						}
					}
				}

				phyloXMLparserNodesIterator(rootNode, childnodes);
			} else {
				this.errormessage = "no 'phylogeny' tag found in your xml file, check your input";
				this.isValidDataset = false;
			}
		} catch (DOMException e) {
			this.errormessage = e.getMessage();
			this.isValidDataset = false;
		}
	}

	// Dec 2, 2011
	private void phyloXMLparserNodesIterator(PhyloNode parentnode, NodeList nodes) {
		//
		for (int i = 0; i < nodes.getLength(); i++) {
			Node node = nodes.item(i);
			if (node.getNodeName().equalsIgnoreCase("clade")) {
				PhyloNode currentNode = null;

				// check leaf node or not
				if (((Element) node).getElementsByTagName("clade").getLength() > 0) {
					// make a new internal node
					serial_internal_node++;

					// get id for internal node
					String iid = getAttributeValueByItemID(node, "name");
					currentNode = makeNewInternalNode(iid, "INT" + serial_internal_node, false, parentnode);

					// get confidance / bootstrap 
					float bootstrap = getNumericValueForNamedItemFromAnXMLNode(node, "confidence");
					if (bootstrap > -1) {
						this.hasBootStrap = true;
						currentNode.setBootStrap(bootstrap);
					}

					// at the end, call this function itself --
					phyloXMLparserNodesIterator(currentNode, node.getChildNodes());
				} else {
					// make a new leaf node
					serial_leaf_node++;
					currentNode = makeNewLeafNode(((Element) node).getElementsByTagName("name").item(0).getFirstChild().getNodeValue(), 0, parentnode, serial_leaf_node);
				}

				/*
				 * get branch length, if possible case a: branch lenth as inline
				 * attributes
				 *
				 * <clade branch_length="0.06">
				 *
				 * case b: branch length as child nodes
				 *
				 * <clade> <branch_length>0.07466</branch_length> <confidence
				 * type="unknown">32.0</confidence> ... <clade>
				 *
				 * first of all, deal with case a:
				 *
				 * == last updated: Jan 09, 2012 ==
				 */
				float branchlength = getNumericValueForNamedItemFromAnXMLNode(node, "branch_length");
				if (branchlength > -1) {
					this.hasBranchlength = true;
					currentNode.setBranchLength(branchlength);
				} // April 20, 2012

			}// if nodename is 'clade'
		}
	}

	private String getAttributeValueByItemID(Node node, String itemid) {
		String value = "";

		// get confidence; Jan 09, 2012
		if (node.getAttributes().getNamedItem(itemid) != null) {
			value = node.getAttributes().getNamedItem(itemid).getNodeValue();
		} else {
			NodeList dnodes = node.getChildNodes();
			for (int j = 0; j < dnodes.getLength(); j++) {
				Node dnode = dnodes.item(j);
				if (dnode.getNodeName().equalsIgnoreCase(itemid)) {
					value = dnode.getFirstChild().toString();
				}
			}
			// then case b
		} // Jan 09, 2012
		return (value);
	}

	// -- April 20, 2012 --
	private float getNumericValueForNamedItemFromAnXMLNode(Node node, String itemName) {
		float numericvalue = -1;
		if (node.getAttributes().getNamedItem(itemName) != null) {
			try {
				numericvalue = Float.parseFloat(node.getAttributes().getNamedItem(itemName).getNodeValue());

			} catch (NumberFormatException e) {
			}
		} else {
			NodeList dnodes = node.getChildNodes();
			for (int j = 0; j < dnodes.getLength(); j++) {
				Node dnode = dnodes.item(j);
				if (dnode.getNodeName().equalsIgnoreCase(itemName)) {
					try {
						numericvalue = Float.parseFloat(dnode.getFirstChild().toString());
					} catch (NumberFormatException e) {
					}
				}
			}
			// then case b
		} // Jan 09, 2012
		return numericvalue;
	} // April 20, 2012

	// May 13, 2012
	public String getExternalIDbyInternalID(String internal_id) {
		return this.hsInternalID2externalID.containsKey(internal_id) ? this.hsInternalID2externalID.get(internal_id) : "";
	}

	/**
	 * @return the treeFormat
	 */
	public String getTreeFormat() {
		return treeFormat;
	}

}