package org.evolview.client;


import org.evolview.client.pages.HeaderBar;
import org.evolview.client.pages.LoginPage;
import org.evolview.client.pages.MyProjectAndTrees;  
import org.evolview.client.utils.MC;
import org.evolview.shared.SignInStatus;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 * Feb 26, 2013
 * the main player
 *
 */
public class MyHistoryListener implements ValueChangeHandler<String> {

	/**
	 * private variables; Feb 26, 2013
	 */
	// Feb 26, 2013; layouts
	private SimplePanel headerBar = new SimplePanel(), body = new SimplePanel();
	
	// Feb 26, 2013; pages
	private LoginPage loginPage;
	private MyProjectAndTrees mytreesPage;
	private HeaderBar headerbar;

	private String storeRequestTreeId;
	
	/**
	 *  default constructor; Feb 26, 2013
	 *  add two simple panels for layout purpose
	 */
	public MyHistoryListener() {
		// set body width to 100%
		headerBar.setWidth("100%");
		body.setWidth("100%");
		
		// add them 
		RootPanel.get().add(headerBar);
		RootPanel.get().add(body);
		//RootPanel.get().add( new FooterBar() );
	}// class constructor --
	
	/**
	 * deal with history tokens
	 */
	@Override
	public void onValueChange(final ValueChangeEvent<String> event) {
		/**
		 * clear page contents
		 */
		headerBar.clear();
		headerBar.setVisible(false);
		body.clear();
		this.storeRequestTreeId = Window.Location.getParameter("treeId");
		
		// check if browsr is supported
		MC.rpc.isCurrentBrowserSupported(new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				MC.alertFailConnectingServer();
			}

			@Override
			public void onSuccess(Boolean isBrowserSupported) {
					final String token = event.getValue();
					final String sessionID = MC.cookieman.getSessionID();
					if (sessionID.isEmpty()) {
						tokenProcessor(token, sessionID, false, false);
					} else {
						MC.rpc.checkSignInStatus(sessionID, new AsyncCallback<SignInStatus>() {

							@Override
							public void onFailure(Throwable caught) {
								MC.alertFailConnectingServer();
							}

							@Override
							public void onSuccess(SignInStatus s) {
								// -- if sessionID does not exist at the database -- 
								if( !s.isSignedIn() & !sessionID.isEmpty() ){
									MC.cookieman.removeCookie();
									tokenProcessor(token, "", s.isSignedIn(), s.isTemporaryUser());
								} else {
									// -- normal token process --
									tokenProcessor(token, sessionID, s.isSignedIn(), s.isTemporaryUser());
								}
							} // on success 
						});
					} // if session ID is not empty --
			}// check if browser is supported --
		});// -- rpc for browser is supported or not --
	}// on value change 

	/**
	 * Feb 26, 2013 --
	 * deal with history token
	 */
	private void tokenProcessor(String token, String sessionID, boolean isSignedIn, boolean isTempUser) {
		
		System.out.println("\tsessionID is : " + sessionID + ", history token is : " + token + ", is signed in : " + isSignedIn);
		
		// -- if sessionID is null, turn to login page --
		if( sessionID == null || sessionID.isEmpty() ){ 
//			this.storeRequestTreeId = Window.Location.getParameter("treeId");
			/**
			 * by default, load login page 
			 */
			loadLoginPage();
			History.newItem("login", false); 
		} else {
			if( token.isEmpty() ){
				if( !isSignedIn ){
					loadLoginPage();
					History.newItem("login", false);
				} else {
					loadMyTreesPage();
				}
			} else if ( token.equalsIgnoreCase("login") ){
				if( !isSignedIn ){
					loadLoginPage();
				} else {
					loadMyTreesPage();
				}
			}  else if ( token.equalsIgnoreCase("mytrees") ){
				if( !isSignedIn ){
					loadLoginPage();
				} else {
					loadMyTreesPage();
				}
			}  
		}
	}
	 
	
	private void loadLoginPage() {
		if( loginPage == null ){
			loginPage = new LoginPage();
		}
		body.setWidget( loginPage );
		Window.setTitle("EvolView : login");
	}
	
	private void loadMyTreesPage() {
		if( mytreesPage == null ){
			mytreesPage = new MyProjectAndTrees();
		}
		body.setWidget( mytreesPage );
		History.newItem("mytrees", false); 
		Window.setTitle("EvolView for cad4Bio");
		
		// -- April 1, 2013: also load header --
		loadHeaderBar();
		 
		if(this.storeRequestTreeId !=  null) {
			mytreesPage.showTree(Integer.parseInt(this.storeRequestTreeId));
		}
	} 
	
	/**
	 * --helper private functions --
	 */
	// April 1, 2013 
	private void loadHeaderBar() {
		if( this.headerbar == null ){
			headerbar = new HeaderBar();
		}
		this.headerBar.setVisible(true);
		this.headerBar.setWidget( headerbar );
	}
}
