package org.evolview.client.resources;

import com.google.gwt.resources.client.ClientBundle;

public interface MyBundle extends ClientBundle
{
    @Source("MyStylesCss.css")
    public MyStylesCss css();
    
    public MyImages images();
}
