/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package org.evolview.client;

import java.util.ArrayList;
import java.util.HashMap;

import org.evolview.shared.C4BProjectDto;
import org.evolview.shared.SignInStatus;
import org.evolview.shared.UserData;
import org.evolview.shared.UserDataProjectSkeleton;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("GWTService")
public interface IGWTService extends RemoteService {
	public boolean isCurrentBrowserSupported();
//    public Account createAnAccount(Account newuser);
//    public boolean sendEmail(String subject, String content, String emailto);
//    public Integer checkAccountStatus(String email, String password);
//    public boolean activateUserAccount(String email, String pass, String activatecode);
    
    //public Account checkSessionIsStillLegal(String sessionID);
    
    public String processSignIn(String login, String password);
    public String signInAsTemporaryUser();
    
    public boolean updateTreeCanvasInfoOnserver(String sessionid, int dBserialID, String key, float value);
    public boolean changeProjectColor(String sessionID, String projectID, String hexcolor);
    public String getBuildNumber();

    public ArrayList<C4BProjectDto> fetchCad4BioProjects();
    
    public ArrayList<UserDataProjectSkeleton> getProjectSkeletons(String session_id);

//    public Integer addProject(String sessionid, UserData userdata);

    public HashMap<String,String> getTreeStringAndFormatByDBSerialID(String sessionid, int treeDBSerial);

    public HashMap<String, Float> getTreeCanvasInfor(String sessionid, int treeDBSerial);

    public HashMap<String, Boolean> getCatPanelInfo(String sessionid, int treeDBSerial);

    public ArrayList<UserData> getDatasets(String sessionid, int treeDBSerial);

    public Integer addTree(
			String SessionID, 
			int project_dbserial,
			String treeName,
			String treestring, 
			String treeformat,
			String leafType,
			String traitsType,
			boolean active);

    public Integer deleteTree(String sessionid, int dBserialID);

    public Boolean swapDatasetOrdersOnServer(String sessionid, int treeDBSerial,  int dataset1, int dataset2, String catpanalName);

    public Boolean changeCatpanelActiveStatusOnServer(String sessionid, int treeDBSerial, String datasettype, boolean active);
    public Boolean changeDatasetActiveStatusOnServer(String sessionid, int treeDBSerial, String datasetname, String datasettype, Boolean active);
             
    public Boolean deleteDataset(String sessionid, int treeDBSerial,  String datasetID, String datatype);
    
    public String exportTreeToSVG(String svgcontents);

    public String exportTreeImage(String svgfileprefix, String outfileformat, float offsetWidth, float offsetHeight);
    
    public Integer addDataset(String sessionID, int treeDBSerial, String dataSetName, String dataSetContent, String datasettype);
    
//    public Boolean resetPasswordByEmail( String email );
    
//    public Boolean changeAccountPassword( String email, String oldpassword, String newpassword);
    
//    public Boolean deleteSessionID( String sessionid );
    
    public Boolean changeUserSettings( String sessionid, String key, String value );
    
    public HashMap<String, String> getUserSettings( String sessionid );
    
//    public ArrayList<UserDataProjectSkeleton> getProjectSkeletonsForDemos();
    
    public Boolean changeTreeActiveStatus(String sessionid, int treeDBSerial);
    
    public String exporTreeToTextFile( String treestr, String format);
    
//    public ArrayList<VisitData> getVisits();
    
//    public Integer deleteEmptyProject(String sessionID, int dBserialID);
    
    public HashMap<String, ArrayList<String>> getProteinDomainFromPFAM( ArrayList<String> leafnames );
    
    public Boolean changeTreeContents( String sessionid, int treeDBSerial, String treestring );
    
    // check signin status
    public SignInStatus checkSignInStatus( String sessionid );
//	public int createNewAccount(String sessionid, String firstname, String lastname, String institute, String email, String password, boolean bTransferTempData );
	
	/**
	 * May 13, 2013 --
	 * signin or signup with social credentials
	 */
//	public SignInStatus socialSignInSignUp( SocialUser user, String openAuthProvider );
    

	////////////////////////////////////  Leaf selection

	  public Boolean  treeLeafSelection( String sessionid, int treeDBSerial, String leaf );
	  public Boolean  treeLeafUnSelection( String sessionid, int treeDBSerial, String leaf );
	  
	  
}
