package org.evolview.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.evolview.shared.C4BProjectDto;
import org.evolview.shared.SignInStatus;
import org.evolview.shared.UserData;
import org.evolview.shared.UserDataProjectSkeleton;

import com.google.gwt.thirdparty.guava.common.base.Joiner;

/**
 *
 * @author E.T. -- Jan 16, 2012; add extra layers of security --
 * Nov 5, 2013; use 'PreparedStatement' to prepare and execute all SQL commands
 */
public class DB_ops extends DB_Conn {
 
	
	/**
	 * Map of sessions, keyed by session Id, values are user Id
	 * If userId = 0, it is an anonymous user
	 */
	private final static Map<String, Long> sessionsMap = new HashMap<String, Long>();
	private final static Map<Long, String> userNameMap = new HashMap<Long,String>();
	

	private final static String NO_PROJECT_NAME = "other" ;
	 
	
	
	/**
	 * names of CONFIG Properties
	 */
	private final String DATABASE = "database", USERNAME = "username", PASSWORD = "password", SERVERURL = "serverurl";
	//private final String PASSDB = "passdb", PASSTABLE = "passtable";
	private Properties sysconfs = null;
	/**
	 * connections two are required by this class: - con to data database - con
	 * to password database
	 */
	private Connection con = null;
	//private String SERVERLET_REAL_PATH = "";

	public DB_ops(Properties confs, String serverPath) {
		sysconfs = confs;

		System.out.println("connect to data db:" + sysconfs.getProperty(this.DATABASE) + " / " + sysconfs.getProperty(this.USERNAME) + " / " + sysconfs.getProperty(this.PASSWORD)
				+ "/" + sysconfs.getProperty(this.SERVERURL));
		// get data db connection
		this.con = getConn(sysconfs.getProperty(this.DATABASE), sysconfs.getProperty(this.USERNAME), sysconfs.getProperty(this.PASSWORD), sysconfs.getProperty(this.SERVERURL));

		//SERVERLET_REAL_PATH = serverPath;
	}

	private Connection getCon() {
		try {
			if (con == null || con.isClosed() ) {
				con = getConn(sysconfs.getProperty(this.DATABASE), sysconfs.getProperty(this.USERNAME), sysconfs.getProperty(this.PASSWORD), sysconfs.getProperty(this.SERVERURL));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
 

	public String processSignIn(String login, String password) {
		System.out.println("==> processSignIn starts ... ");

		String encryptedpassword = null;
		try {
			PreparedStatement query = getCon().prepareStatement(" SELECT encryptedpassword FROM cad4bio.User WHERE login = ? ");
			query.setString(1, login); 
			ResultSet rs = query.executeQuery(); 
			while (rs.next()) {
				encryptedpassword = rs.getString(1);
			}
			rs.close();
		} catch (SQLException e) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, e);
		}
		if(encryptedpassword == null) {
			return null;
		}
		return createSessionID(login);
	} 

	/**
	 * create session using session table to create unique id
	 *
	 * stores in sessionsMap
	 */
	private String createSessionID(String login) {
		  
		long userId = -1;
		try {
			PreparedStatement query = getCon().prepareStatement(" SELECT iduser FROM cad4bio.User WHERE login = ? ");
			query.setString(1, login); 
			ResultSet rs = query.executeQuery(); 
			while (rs.next()) {
				userId = rs.getLong(1);
			}
			rs.close();
		} catch (SQLException e) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, e);
		}
		if(userId == -1) {
			return null;
		} 
		String SessionID = UUID.randomUUID().toString();
		sessionsMap.put(SessionID, userId); 
		userNameMap.put(userId, login); 
		return SessionID;
	}

	// Oct 26, 2011;
	@SuppressWarnings("unused")
	private Boolean deleteSessionID(String sessionid) {
		System.out.println("==> delete session id"); 
		sessionsMap.remove(sessionid);
		return true;
	}

	// edited: Jan 16, 2012 - check if userid match 
	boolean updateTreeCanvasInforOnServer(String sessionid, int tree_dbserial, String key, float value) {
		System.out.println(" ----------- changeCatpanelActiveStatusOnServer -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				// 1. check if tree_dbserial exists for the tree
				int tree_serial = 0;

				PreparedStatement query = getCon().prepareStatement(" SELECT ID FROM eview.trees WHERE ID = ? and UserID = ? ");
				query.setInt(1, tree_dbserial);
				query.setInt(2, userid);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					tree_serial = rs.getInt(1);
				}
				rs.close();
				query.close();

				// 2. if tree exists
				if (tree_serial > 0) {

					// 2.1 check if the key exists
					int key_count = 0;

					PreparedStatement query2 = getCon().prepareStatement("  SELECT COUNT(1) FROM eview.treecanvasinfo WHERE TreeID = ? AND KEY = ? ");
					query2.setInt(1, tree_serial);
					query2.setString(2, key);

					System.out.println("   exec: " + query2.toString());
					ResultSet rs_key = query2.executeQuery();
					while (rs_key.next()) {
						key_count = rs_key.getInt(1);
					}
					rs_key.close();
					query2.close();

					// 2.2 if the dataset exists , update it; otherwise, insert a new entry
					if (key_count == 0) {

						PreparedStatement insert = getCon().prepareStatement("  INSERT INTO eview.treecanvasinfo (TreeID, key, value) VALUES(?, ?, ?) ");
						insert.setInt(1, tree_serial);
						insert.setString(2, key);
						insert.setFloat(3, value);

						System.out.println("  insert key / value pairs " + insert.toString());
						insert.executeUpdate();
						insert.close();

						success = true;
					} else {

						PreparedStatement update = getCon().prepareStatement("  UPDATE eview.treecanvasinfo SET value = ? WHERE TreeID = ? AND key = ? ");
						update.setFloat(1, value);
						update.setInt(2, tree_dbserial);
						update.setString(3, key);

						System.out.println("  update value for existing key " + update.toString());
						update.executeUpdate();
						update.close();

						success = true;
					}

				}// end of if tree exists ...
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- update treecanvasinfo, status ? " + success + " -------------------- ");
		return success;
	}

	boolean changeProjectColor(String sessionID, String projectID, String hexcolor) {
		// TODO : with Cad4BIo ...
		return true;
	}

	public ArrayList<C4BProjectDto> fetchCad4BioProjects(){

		ArrayList<C4BProjectDto> c4bprojects = new ArrayList<C4BProjectDto>();
		try {
			PreparedStatement query2 = getCon().prepareStatement("  SELECT idproject, acronyme, name FROM cad4bio.project "); 
	
			System.out.println("   exec: " + query2.toString());
			ResultSet rs_key = query2.executeQuery();
			while (rs_key.next()) {
				int idproject = rs_key.getInt(1);
				String acronyme = rs_key.getString(2); 
				String name = rs_key.getString(3);
				c4bprojects.add(new C4BProjectDto(idproject, acronyme, name));
			}
			rs_key.close(); 
		} catch (SQLException ex) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
		}
		return c4bprojects;
	}
	
	// last modified: Dec 4, 2011
	// Jan 16, 2011; 
	public ArrayList<UserDataProjectSkeleton> getProjectSkeletons(String session_id) {
		ArrayList<UserDataProjectSkeleton> projectSkeleton = new ArrayList<UserDataProjectSkeleton>();

		System.out.println(" ----------- getProjectSkeletons starts -------------------- ");
		boolean success = false;

		SignInStatus sin = this.checkSignInSstatus(session_id);
		int userid = sin.getUserID();
		if (userid > -1) {
			projectSkeleton = getProjectSkeletons(userid);
			success = true;
		}
		System.out.println(" ----------- getProjectSkeletons, status ? " + success + " -------------------- ");
		return projectSkeleton;
	}

	
	// Nov 5, 2013;
	private ArrayList<UserDataProjectSkeleton> getProjectSkeletons(int userid) {
		ArrayList<UserDataProjectSkeleton> projectSkeleton = new ArrayList<UserDataProjectSkeleton>();
		try {
			// get data for current user
			PreparedStatement query = getCon().prepareStatement(" SELECT t.ProjectId as projectID, t.ID as treeID, " 
					+  
						"( CASE WHEN t.ProjectId = 0 THEN '"+NO_PROJECT_NAME+"' " +  
						"  ELSE p.name " + 
						"  END)" +
						"   as projectName,"
					+ " t.TreeName as treeName, t.TreeFormat as treeFormat, t.Active as TreeActive, 'red' as color, t.Favorite as fav " 
					+ " from  eview.trees as t left outer join cad4bio.project as p "
					+ " on (t.ProjectID = p.idproject)  "
					+ "   where  t.UserID = ? order by t.ProjectId desc, t.rank");
			query.setInt(1, userid);
			System.out.println("  exec: " + query.toString());
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				UserDataProjectSkeleton ud = new UserDataProjectSkeleton();
				ud.setProjectDBSerial(rs.getInt("projectID"));
				ud.setTreeDBSerial(rs.getInt("treeID"));
				ud.setProjectName(rs.getString("projectName"));
				ud.setTreeName(rs.getString("treeName"));
				ud.setProjectColor(rs.getString("color"));
				ud.setTreeFormat(rs.getString("treeFormat"));
				ud.setProjectFav(rs.getBoolean("fav"));
				ud.setTreeActive(rs.getBoolean("TreeActive")); // Nov 08, 2011;

				projectSkeleton.add(ud);
			}
			rs.close();
			query.close();
		} catch (SQLException ex) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
		}
		return projectSkeleton;
	}
 

	// Jan 16, 2012; also check userID
	// Feb 2, 2012; also activate this tree
	// March 29, 2013 : use checkSignInSstatus instead of 'checkSessionIsStillLegal'
	HashMap<String, String> getTreeStringAndFormatByDBSerialID(String sessionid, int treeDBSerial) {
		HashMap<String, String> treeAndFormat = new HashMap<String, String>();

		System.out.println(" ----------- getTreeStringByDBSerialID starts -------------------- ");
		boolean success = false;

		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userid = sin.getUserID();
		// if userid is valid ; March 29, 2013 --
		if (userid > -1) {
			try {
				// get tree content
				int retrived_user_id = 0;

				PreparedStatement query = getCon().prepareStatement(
						" SELECT TreeContent as tree, TreeFormat as format, selectedleaves, UserID FROM eview.trees " +
						"  WHERE ID = ?  ");
				query.setInt(1, treeDBSerial); 

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					treeAndFormat.put("tree", rs.getString("tree"));
					treeAndFormat.put("format", rs.getString("format"));
					treeAndFormat.put("selectedleaves", rs.getString("selectedleaves"));
					retrived_user_id = rs.getInt("UserID");
				}
				rs.close();
				query.close();
				success = true;

				// Feb 2, 2012; activate current tree
				// Apr 27, 2012; only if current user is the owner of the tree
				if (retrived_user_id == userid) {
					System.out.println("\tset active tree");

					// set all other trees inactive --
					PreparedStatement active1 = getCon().prepareStatement(" update eview.trees set Active = FALSE WHERE UserID = ?");
					active1.setInt(1, userid);
					active1.executeUpdate();
					active1.close();

					// set current tree active
					PreparedStatement active2 = getCon().prepareStatement(" update eview.trees set Active = TRUE WHERE ID = ? "); 
					active2.setInt(1, treeDBSerial);
					active2.executeUpdate();
					active2.close();

				}// Apr 27, 2012 
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- getTreeStringByDBSerialID, status ? " + success + " -------------------- ");
		return treeAndFormat;
	}

	// Jan 16, 2012; also check userID
	HashMap<String, Float> getTreeCanvasInfo(String session_id, int tree_dbserial) {
		System.out.println(" ----------- get tree canvas infor -------------------- ");
		HashMap<String, Float> keyvaluepair = new HashMap<String, Float>();
 
		SignInStatus sin = this.checkSignInSstatus(session_id);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {   
				PreparedStatement query2 = getCon().prepareStatement(
						" SELECT key, value FROM eview.treecanvasinfo WHERE TreeID = ?");
				query2.setInt(1, tree_dbserial);

				System.out.println("   exec: " + query2.toString());
				ResultSet canvasinfor = query2.executeQuery();
				while (canvasinfor.next()) {
					keyvaluepair.put(canvasinfor.getString(1), canvasinfor.getFloat(2));
				}
				canvasinfor.close();
				query2.close(); 
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- get treecanvas information, for tree " + tree_dbserial + " -------------------- ");
		return keyvaluepair;
	}

	// Jan 16, 2012; userid will not be checked!!
	HashMap<String, Boolean> getCatPanelInfo(String session_id, int treeid) {
		HashMap<String, Boolean> cat2status = new HashMap<String, Boolean>();

		System.out.println(" ----------- getCatPanelInfo -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(session_id);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {

				PreparedStatement query = getCon().prepareStatement(" SELECT DatasetType, Active FROM eview.catpanels WHERE TreeID = ? ");
				query.setInt(1, treeid);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					cat2status.put(rs.getString(1), rs.getBoolean(2));
				}
				rs.close();
				query.close();
				success = true;
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}// session id isn't null
		System.out.println(" -----------getCatPanelInfo, status ? " + success + " -------------------- ");
		return cat2status;
	}

	ArrayList<UserData> getDatasets( int treeID) {
		ArrayList<UserData> userdata = new ArrayList<UserData>();

		try {
			PreparedStatement query = getCon().prepareStatement(
					"SELECT ID, DatasetName, Description, Active, DatasetContent, DatasetType FROM eview.datasets" +
					"   WHERE TreeID = ?  ORDER BY rank ");
			query.setInt(1, treeID);

			System.out.println("   exec: " + query.toString());
			ResultSet result = query.executeQuery();
			while (result.next()) {
				UserData ud = new UserData();
				ud.setDBSerial(result.getInt(1));
				ud.setName(result.getString(2));
				ud.setDesc(result.getString(3));
				ud.setActive(result.getBoolean(4));
				ud.setContent(result.getString(5));
				ud.setTypeOrFormat(result.getString(6));
				userdata.add(ud); // add it to userdata arraylist
			}

			result.close();
			query.close();
		} catch (SQLException ex) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
		}
		return userdata;
	}

	/**
	 *  last modified: Dec 4, 2011;
	 *  April 4, 2013; when add a new tree, also activate it 
	 */
	Integer addTree(
			final String SessionID, 
			final int project_dbserial,
			final String treeName,
			final String treestring, 
			final String treeformat,
			final String leafType,
			final String traitsType,
			final boolean active) {
		int tree_dbserial = 0;
		
		boolean added = false;
		boolean exists = false;

		if (project_dbserial < 0) {
			return null;
		}
		System.out.println(" ----------- add tree starts -------------------- " + "\n\ttreeformat is : " + treeformat);

		/*
		 * 1. check if session is valid 2. check tree exists 3. if not, insert a
		 * new tree and 4. insert entries in table datasetinfo
		 */
		// 1
		SignInStatus sin = this.checkSignInSstatus(SessionID);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {

				// 2. check if tree exists
				PreparedStatement query = getCon().prepareStatement(
						" SELECT ID FROM eview.trees WHERE ProjectID = ? AND TreeName = ? AND UserID = ? ");
				query.setInt(1, project_dbserial);
				query.setString(2, treeName);
				query.setInt(3, userid);
				
				System.out.println("   exec: " + query.toString());
				ResultSet result = query.executeQuery();
				while (result.next()) {
					exists = true;
					tree_dbserial = result.getInt(1);
				}
				result.close();
				query.close();

				System.out.println(" current tree exists? " + exists);

				// 3. if not exists
				if (!exists) {

					// 3.1 get projectID and max tree serial in this project
					int maxOrder = 0;  

					// get max tree serial in this project
					PreparedStatement query3 = getCon().prepareStatement(" SELECT MAX( rank ) FROM eview.trees WHERE ProjectID =  ? ");
					query3.setInt(1, project_dbserial);

					System.out.println("   exec: " + query3.toString());
					ResultSet mo = query3.executeQuery();
					while (mo.next()) {
						maxOrder = mo.getInt(1);
					}
					mo.close();
					query3.close();

					// increase ...
					maxOrder++;

					// 3.2 if projectID is valid, insert a new tree

						// Feb 2, 2012; in case tree_string contains some unacceptable strings
						PreparedStatement addtree = getCon().prepareStatement(
								" INSERT INTO eview.trees (UserID, ProjectID, TreeName, Description, DateCreated, LastModified, Color, Active, Favorite, rank, TreeContent, TreeFormat, leaftype, traitstype) "
							    + " VALUES ( ?, ?, ?, ?, current_timestamp, current_timestamp, 'red', ?, FALSE, ?, ?, ?, ?, ? ) ");

						addtree.setInt(1, userid);
						addtree.setInt(2, project_dbserial);
						addtree.setString(3, treeName);
						addtree.setString(4, " ");
						addtree.setBoolean(5, true); // always make this active 
						addtree.setInt(6, maxOrder);
						addtree.setString(7, treestring);
						addtree.setString(8, treeformat);
						query.setString(9, leafType);
						query.setString(10, traitsType);

						System.out.println(" create new tree with order : " + maxOrder + " with sql command : " + addtree.toString());
						addtree.executeUpdate();

						// 3.3 also insert new entries for datasetinfor
						PreparedStatement query4 = getCon().prepareStatement("  SELECT ID FROM eview.trees WHERE ProjectID = ? AND TreeName = ?");
						query4.setInt(1, project_dbserial);
						query4.setString(2, treeName);

						System.out.println("   exec: " + query4.toString());
						ResultSet rs = query4.executeQuery();
						while (rs.next()) {
							tree_dbserial = rs.getInt(1);
						}
						rs.close();
						query4.close();

						if (tree_dbserial > 0) {

							PreparedStatement insert_catpanels = getCon().prepareStatement(
									" INSERT INTO eview.catpanels (TreeID, DatasetType, Active, DateCreated, LastModified) "
									+ "VALUES ( ?, 'SavedViews', TRUE, current_timestamp, current_timestamp ), "
									+ "       ( ?,  'Pies', TRUE,   current_timestamp, current_timestamp ), "
									+ "       ( ?, 'Branch Colors',  TRUE, current_timestamp, current_timestamp ), "
									+ "       ( ?,  'Leaf Colors',  TRUE,  current_timestamp, current_timestamp ), "
									+ "       ( ?,  'Leaf BK Colors', TRUE, current_timestamp, current_timestamp ), "
									+ "       ( ?,  'Charts',  TRUE,  current_timestamp, current_timestamp ) ");

							insert_catpanels.setInt(1, tree_dbserial);
							insert_catpanels.setInt(2, tree_dbserial);
							insert_catpanels.setInt(3, tree_dbserial);
							insert_catpanels.setInt(4, tree_dbserial);
							insert_catpanels.setInt(5, tree_dbserial);
							insert_catpanels.setInt(6, tree_dbserial);

							System.out.println(" add category panels : " + insert_catpanels.toString());
							insert_catpanels.executeUpdate();
							insert_catpanels.close();
							added = true; // 

							/**
							 * April 4, 2013; also inactivate all other trees;
							 */
							PreparedStatement inactivateOtherTrees = getCon().prepareStatement(
									" update eview.trees set Active = False WHERE UserID = ? AND ID != ? ");
							inactivateOtherTrees.setInt(1, userid);
							inactivateOtherTrees.setInt(2, tree_dbserial);

							System.out.println(" exec : " + inactivateOtherTrees.toString());
							inactivateOtherTrees.executeUpdate();
							inactivateOtherTrees.close();
						}// treeid exists 
				}// tree not exists 
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" --------------- addtree ends, added is : " + added + " -------------");
		return tree_dbserial;
	}

	// Jan 16, 2012; check user id as well
	int deleteTree(String SessionID, int tree_dbserial) {
		System.out.println(" ----------- delete tree and related datasets -------------------- ");
		int treeSerial = 0;
		SignInStatus sin = this.checkSignInSstatus(SessionID);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {

				// 1. get treeID from project and tree tables
				PreparedStatement query = getCon().prepareStatement("  SELECT ID FROM eview.trees WHERE ID = ? and UserID = ? ");
				query.setInt(1, tree_dbserial);
				query.setInt(2, userid);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					treeSerial = rs.getInt(1);
				}
				rs.close();
				query.close();

				// 2. if treeSerial != 0, delete records from tables: eview.trees, eview.datasets, eview.catpanels and datasetinfo
				if (treeSerial > 0) {

					// 2.1 eview.trees
					PreparedStatement delete = getCon().prepareStatement("  DELETE FROM eview.trees WHERE ID = ? ");
					delete.setInt(1, treeSerial);

					System.out.println("   exec: " + delete.toString());
					delete.executeUpdate();
					delete.close();

					// 2.2 eview.datasets
					PreparedStatement deleteDatasets = getCon().prepareStatement("  DELETE FROM eview.datasets WHERE TreeID =  ? ");
					deleteDatasets.setInt(1, treeSerial);

					System.out.println("   exec: " + deleteDatasets.toString());
					deleteDatasets.executeUpdate();
					deleteDatasets.close();

					// 2.3 eview.catpanels
					PreparedStatement deleteCatpanels = getCon().prepareStatement("   DELETE FROM eview.catpanels WHERE TreeID =  ? ");
					deleteCatpanels.setInt(1, treeSerial);

					System.out.println("   exec: " + deleteCatpanels.toString());
					deleteCatpanels.executeUpdate();
					deleteCatpanels.close();

					// 2.5 eview.treecanvasinfo
					PreparedStatement deleteCanvasInfo = getCon().prepareStatement("   DELETE FROM eview.treecanvasinfo WHERE TreeID = ? ");
					deleteCanvasInfo.setInt(1, treeSerial);

					System.out.println("   exec: " + deleteCanvasInfo.toString());
					deleteCanvasInfo.executeUpdate();
					deleteCanvasInfo.close();
				}
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- delete tree and related datasets, for tree " + treeSerial + " -------------------- ");
		return treeSerial;
	}

	// Jan 16, 2012; check user id as well
	boolean swapDatasetOrdersOnServer(String sessionID, int treeDBSerial, int dataset1, int dataset2, String datasettype) {
		System.out.println(" ----------- swap orders of datasets -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionID);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				// 1. get indexes of the two datasets
				int index_dataset1 = 0, order1 = 0, index_dataset2 = 0, order2 = 0;

				// April 14, 2011;
				if (datasettype.equals("Charts")) {
					datasettype = " 'Bars', 'Strips' ";
				} else {
					datasettype = "'" + datasettype + "'";
				}
  
				PreparedStatement query = getCon().prepareStatement(
						" SELECT dat.ID, dat.rank FROM eview.datasets AS dat  WHERE  dat.ID in (?,?)");
				query.setInt(1, dataset1);
				query.setInt(2, dataset2);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					if (index_dataset1 == 0) {
						index_dataset1 = rs.getInt(1);
						order1 = rs.getInt(2);
					} else {
						index_dataset2 = rs.getInt(1);
						order2 = rs.getInt(2);
					}
				}
				rs.close();
				query.close();

				// 2. update the orders
				if (index_dataset1 > 0 && index_dataset2 > 0 && index_dataset1 != index_dataset2 && order1 != order2) {

					PreparedStatement update = getCon().prepareStatement("    UPDATE eview.datasets SET rank = ? WHERE ID =  ? ");
					// update 1 --
					update.setInt(1, order2);
					update.setInt(2, index_dataset1);

					System.out.println("   exec: " + update.toString());
					update.executeUpdate();

					// update 2 --
					update.setInt(1, order1);
					update.setInt(2, index_dataset2);

					System.out.println("   exec: " + update.toString());
					update.executeUpdate();

					// close 
					update.close();

					success = true;
				}
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- swap orders of datasets, status ? " + success + " -------------------- ");
		return success;
	}

	// doubled checked; Oct 28, 2011;
	// Jan 16, 2012; check user id as well
	Boolean changeCatpanelActiveStatusOnServer(String sessionid, int treeDBSerial, String datasettype, boolean active) {
		System.out.println(" ----------- changeCatpanelActiveStatusOnServer -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				// 1. check if datasettype exists for the tree
				int catpanelID = 0; 
				PreparedStatement query = getCon().prepareStatement(
						" SELECT cat.ID FROM eview.catpanels AS cat, eview.trees AS tre "
						+ "   WHERE tre.ID = ? "
						+ " AND cat.TreeID = tre.ID AND cat.DatasetType = ? ");
				query.setInt(1, treeDBSerial); 
				query.setString(2, datasettype);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					catpanelID = rs.getInt(1);
				}
				rs.close();
				query.close();

				// 2. update the status
				if (catpanelID > 0) {

					PreparedStatement update = getCon().prepareStatement(
							" UPDATE eview.catpanels SET Active = ?, LastModified = current_timestamp WHERE ID = ? ");
					// update --
					update.setBoolean(1, active ? Boolean.TRUE : Boolean.FALSE);
					update.setInt(2, catpanelID);

					System.out.println("   exec: " + update.toString());
					update.executeUpdate();
					update.close();

					//
					success = true;
				}
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- changeCatpanelActiveStatusOnServer, status ? " + success + " -------------------- ");
		return success;
	}

	// Oct 28, 2011; 
	// Jan 16, 2012; check user id as well
	Boolean changeDatasetActiveStatusOnServer(String sessionid, int treeDBSerial, String datasetname, String datasettype, Boolean active) {
		System.out.println(" ----------- change Dataset ActiveStatus On Server starts -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				// 1. check if datasettype exists for the tree
				int datasetid = 0 ;
				PreparedStatement query = getCon().prepareStatement(
						" SELECT dat.ID, dat.TreeID  FROM eview.datasets AS dat, eview.trees AS tre "
						+ "     WHERE tre.ID = ?   "
						+ "           AND dat.TreeID = tre.ID AND dat.DatasetType = ? AND dat.DatasetName = ?");
				query.setInt(1, treeDBSerial);
				query.setString(2, datasettype);
				query.setString(3, datasetname);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					datasetid = rs.getInt("ID"); 
				}
				rs.close();
				query.close();

				System.out.println("  >>> datasetID is : " + datasetid);

				// 2. update the status
				if (datasetid > 0) {

					// 2.1 ; if dataset is Strips or Bars; if protein domains 
					if (datasettype.equals("Strips") || datasettype.equals("Bars") || datasettype.equalsIgnoreCase("Protein Domains")) {

						PreparedStatement update = getCon().prepareStatement(
								"    UPDATE eview.datasets SET Active = ?, LastModified = current_timestamp WHERE ID = ? ");
						// update --
						update.setBoolean(1, active ? Boolean.TRUE : Boolean.FALSE);
						update.setInt(2, datasetid);

						System.out.println("   exec: " + update.toString());
						update.executeUpdate();
						update.close();

						// 2.2 or other types 
					} else if (active) {

						// set everything else to false;
						PreparedStatement update = getCon().prepareStatement(
								"   UPDATE eview.datasets SET Active = FALSE, LastModified = current_timestamp WHERE TreeID = ? AND DatasetType = ? ");
						update.setInt(1, treeDBSerial);
						update.setString(2, datasettype);

						System.out.println("   exec: " + update.toString());
						update.executeUpdate();
						update.close();

						// and set target to true;
						PreparedStatement update2 = getCon().prepareStatement("   UPDATE eview.datasets SET Active = TRUE, LastModified = current_timestamp WHERE ID =  ? ");
						update2.setInt(1, datasetid);

						System.out.println("   exec: " + update2.toString());
						update2.executeUpdate();
						update2.close();
					}

					success = true;
				}
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
				success = false;
			}
		}
		System.out.println(" ----------- change Dataset ActiveStatus On Server; updated ? " + success + " -------------------- ");
		return success;
	}

	// Jan 16, 2012; check user id as well
	// Apr 25, 2012; fix a bug here
	boolean deleteDataset(String SessionID, int treeDBSerial, String datasetname, String datasettype) {
		System.out.println(" ----------- delete dataset -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(SessionID);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				PreparedStatement delete = getCon().prepareStatement(
						" DELETE FROM eview.datasets WHERE DatasetType = ? AND DatasetName = ? AND TreeID   = ?   ");
				delete.setString(1, datasettype);
				delete.setString(2, datasetname);
				delete.setInt(3,treeDBSerial);

				System.out.println("   exec: " + delete.toString());
				delete.executeUpdate();
				delete.close();

				//
				success = true;

			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		System.out.println(" ----------- delete dataset, status ? " + success + " -------------------- ");
		return success;
	}
	
//
//	private int getProjectId(String projName) {
//
//		if(projName.equals(NO_PROJECT_NAME)) {
//			return NO_PROJECT_ID;
//		}
//		int projId = -1;
//		try {
//			PreparedStatement query = getCon().prepareStatement(
//					" SELECT pro.idProject as ID FROM  cad4bio.project AS pro WHERE pro.name =  ?   ");
//			query.setString(1, projName);
//			
//			System.out.println("   exec: " + query.toString());
//			ResultSet rs = query.executeQuery();
//			if (rs.next()) {
//				projId = rs.getInt("ID"); 
//			}
//			rs.close();
//			query.close();
//	
//		} catch (SQLException ex) {
//			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
//		}
//		return projId;
//	}

	/*
	 * Jan 16, 2012; user id not checked! May 13, 2012; add new datatype:
	 * protein domain
	 */
	Integer addDataset(int userID, int treeDBSerial, String datasetname, String datasetstring, String datasettype) {
		Integer dataset_id = null; 
		System.out.println(" --------------- addDataset starts -------------");
		try {
			/**
			 * ********************************************
			 * check if dataset already exists
			 * *********************************************
			 */
			PreparedStatement query = getCon().prepareStatement(
					" SELECT dat.ID FROM eview.trees AS tre, eview.datasets AS dat "
					+ " WHERE   tre.ID = ? "
					+ " AND dat.TreeID = tre.ID AND dat.DatasetName = ? AND dat.DatasetType = ? ");
			query.setInt(1, treeDBSerial);
			query.setString(2, datasetname);
			query.setString(3, datasettype);

			System.out.println("   exec: " + query.toString());
			ResultSet rs = query.executeQuery();
			while (rs.next()) {
				dataset_id = rs.getInt(1);
			} 
			rs.close();
			query.close();

			/**
			 * ********************************************
			 * if exists *********************************************
			 */
			if (dataset_id != null) {

				PreparedStatement updateDataset = getCon().prepareStatement(
						" UPDATE eview.datasets SET DatasetContent = ?, LastModified = current_timestamp WHERE ID = ? ");
				updateDataset.setString(1, datasetstring);
				updateDataset.setInt(2, dataset_id);

				System.out.println(" -- dataset exists; exec update: " + updateDataset.toString());
				updateDataset.executeUpdate();
				updateDataset.close();

				/**
				* ********************************************
				* if not *********************************************
				*/
			} else {
			    
				int maxorder = 0;

				PreparedStatement query3 = getCon().prepareStatement(" SELECT MAX(rank) FROM eview.datasets WHERE TreeID = ?");
				query3.setInt(1, treeDBSerial);

				System.out.println("   exec: " + query3.toString());
				ResultSet serial = query3.executeQuery();
				while (serial.next()) {
					maxorder = serial.getInt(1);
				}
				serial.close();
				query3.close();

				// insert into datasets
				maxorder++;

				PreparedStatement update = getCon().prepareStatement(
						" INSERT INTO eview.datasets ( TreeID, DatasetName, DateCreated, LastModified, rank, DatasetContent, DatasetType ) "
								+ "VALUES(?,  ?,            current_timestamp, current_timestamp, ?,                   ?,            ?)");

				update.setInt(1, treeDBSerial);
				update.setString(2, datasetname);
				update.setInt(3, maxorder);
				update.setString(4, datasetstring);
				update.setString(5, datasettype);

				System.out.println("    add new dataset; exec: " + update.toString());
				update.executeUpdate();
				update.close();
 
			}
		} catch (SQLException ex) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex); 
		}
		System.out.println(" ----------- end of addDataset, datasetID is " + dataset_id + " ------------------------ ");
		return dataset_id;
	}


	/**
	 * Nov 6, 2011;
	 * April 1, 2013; check first if the key exists 
	 */
	Boolean changeUserSettings(String sessionid, String key, String value) {
		System.out.println("======== change user settings ============");

		Boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userid = sin.getUserID();
		if (userid > -1) {

			try {
				// check if the key exists
				PreparedStatement query = getCon().prepareStatement(
						"select count(*) as count from eview.settings where UserID = ? and key = ? ");
				query.setInt(1, userid);
				query.setString(2, key);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				int count = 0;
				while (rs.next()) {
					count = rs.getInt("count");
				}
				rs.close();
				query.close();

				// if not, insert
				if (count == 0) {
					PreparedStatement insert = getCon().prepareStatement(
							"insert into eview.settings (UserID, key, value) VALUES (?,?,?)");
					insert.setInt(1, userid);
					insert.setString(2, key);
					insert.setString(3, value);

					System.out.println("    add new dataset; exec: " + insert.toString());
					insert.executeUpdate();
					insert.close();
				} else {
					// if yes, update
					PreparedStatement update = getCon().prepareStatement(
							"update eview.settings set value = ? where UserID = ? and key = ?");
					update.setString(1, value);
					update.setInt(2, userid);
					update.setString(3, key);

					System.out.println("    add new dataset; exec: " + update.toString());
					update.executeUpdate();
					update.close();
				}

				success = true;
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		System.out.println("======== change user settings done =============");
		return success;
	}

	// March 9, 2012: check if target tree belong to current user
	Boolean changeTreeActiveStatus(String sessionid, int treeDBSerial) {
		System.out.println("======== changeTreeActiveStatus =============");

		Boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userID = sin.getUserID();
		if (userID > -1) {
			try {
				// March 9, 2012: first of all check if the target tree (the tree to be activated) belong to current user --
				PreparedStatement query = getCon().prepareStatement("select UserID, Active from eview.trees WHERE ID = ? ");
				query.setInt(1, treeDBSerial);

				System.out.println("   exec: " + query.toString());
				ResultSet userSerial = query.executeQuery();

				int retrievedUserID = 0;
				boolean active = false;
				while (userSerial.next()) {
					retrievedUserID = userSerial.getInt("UserID");
					active = userSerial.getBoolean("Active");
				}
				userSerial.close();
				query.close();

				System.out.println("   retrived userID: " + retrievedUserID + " your user ID: " + userID);
				// if tree is not active and current user is the owner of the tree
				if (!active && retrievedUserID == userID) {
					// 2. set all trees inactive
					PreparedStatement update = getCon().prepareStatement("update eview.trees set Active = FALSE WHERE UserID = ?");
					update.setInt(1, userID);

					System.out.println("    exec: " + update.toString());
					update.executeUpdate();
					update.close();

					// 3. set the target tree active
					PreparedStatement update2 = getCon().prepareStatement("update eview.trees set Active = TRUE WHERE UserID = ? AND ID = ?");
					update2.setInt(1, userID);
					update2.setInt(2, treeDBSerial);

					System.out.println("    exec: " + update2.toString());
					update2.executeUpdate();
					update2.close();

					// set success true
					success = true;
				}
			} catch (Exception e) { //debug out output this way
				System.err.println("Mysql Statement Error in function: changeTreeActiveStatus");
			}
		}

		System.out.println("======== changeTreeActiveStatus done =============");
		return success;
	}// Nov 08, 2011;

	/**
	 * last modified: April 1, 2013; use sessionid instead of email --
	 * 
	 */
	HashMap<String, String> getUserSettings(String sessionid) {
		HashMap<String, String> usersettings = new HashMap<String, String>();

		System.out.println("======== get user settings =============");
		SignInStatus sin = this.checkSignInSstatus(sessionid);

		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				PreparedStatement query = getCon().prepareStatement("select key,value from eview.settings where UserID =  ? ");
				query.setInt(1, userid);

				System.out.println("   exec: " + query.toString());
				ResultSet rs = query.executeQuery();
				while (rs.next()) {
					usersettings.put(rs.getString("key"), rs.getString("value"));
				}
				rs.close();
				query.close();
			} catch (Exception e) { //debug out output this way
			}
		}

		System.out.println("======== get user settings done =============");
		return usersettings;
	}
 

	Boolean changeTreeContents(String sessionid, int treeDBSerial, String treeString) {
		System.out.println(" ----------- change/ revise tree string/ contents -------------------- ");
		boolean success = false;
		SignInStatus sin = this.checkSignInSstatus(sessionid);
		int userid = sin.getUserID();
		if (userid > -1) {
			try {
				// Feb 2, 2012; in case tree_string contains some unacceptable strings
				// if only current userID is owner of the tree !! March 29, 2013 --
				PreparedStatement updateTree = getCon().prepareStatement(" UPDATE eview.trees set TreeContent = ? WHERE ID = ? ");
				updateTree.setString(1, treeString);
				updateTree.setInt(2, treeDBSerial);

				System.out.println(" -- exec: " + updateTree.toString());
				updateTree.executeUpdate();
				updateTree.close();

				success = true;
			} catch (SQLException ex) {
				Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
				success = false;
			}
		}
		System.out.println(" ----------- change/ revise tree string/ contents ends here -------------- ");
		return success;
	}

	/**
	 * a liter version for checkSessionIsStillLegal
	 * Feb 18, 2013 
	 * March 28, 2013; if useID is -1 
	 * May 14, 2013 : also get social type
	 */
	public SignInStatus checkSignInSstatus(String sessionID) {
		SignInStatus newsigninStatus = new SignInStatus();
		System.out.println("  ==> check signed in status ");

		if (sessionID != null && !sessionID.isEmpty()) {
			Long userID = getUserIDBySessionID(sessionID);
			if(userID == null) {
				return newsigninStatus;
			}
			else if (userID == 0) {
				newsigninStatus.setUserID(0);
				newsigninStatus.setUsername("");
				newsigninStatus.setSignedIn(true);
				newsigninStatus.setTemporaryUser(true);
			}
			else {
				newsigninStatus.setUserID(userID.intValue());
				newsigninStatus.setUsername(userNameMap.get(userID));
				newsigninStatus.setSignedIn(true);
				newsigninStatus.setTemporaryUser(false);
			} 
		}
		return newsigninStatus;
	} // checkSignInSstatus

	/**
	 * some other private functions --
	 */
 
	/**
	 * 
	 * @param sessionID
	 * @return
	 */
	private Long getUserIDBySessionID(String sessionID) {
		return sessionsMap.get(sessionID);
	} // getUserIDBySessionID

	/**
	 * Feb 19, 2013 --
	 * @return
	 */
	public String signInAsTemporaryUser() {
		System.out.println("==> sign in as a temporary user : starts ... "); 
		// 0 is the key 
		long userId = 0; 
		String SessionID = UUID.randomUUID().toString();
		sessionsMap.put(SessionID, userId); 
		userNameMap.put(userId, "anonymous");  
		return SessionID;
	}
 
	
	


	////////////////////////////////////  Leaf selection

	
	public Boolean  treeLeafSelection( String sessionid, int treeId, String leafName ){
		return this.treeLeafSelection(sessionid, treeId, leafName, true);
	}

	
	public Boolean  treeLeafUnSelection( String sessionid, int treeId, String leafName ){
		return this.treeLeafSelection(sessionid, treeId, leafName, false);
	}
	

	private Boolean  treeLeafSelection( String sessionid, int treeId, String leafName, boolean select ){
		HashMap<String, String> treeContent = this.getTreeStringAndFormatByDBSerialID(sessionid, treeId);
		if(treeContent == null) { 
			return false;
		}
		 
		String selectedLeaves = treeContent.get("selectedleaves");
		Set<String> lefSelSet = new HashSet<String>();
		if(selectedLeaves != null && selectedLeaves.trim().length()>0) {
			String[] selectedLeavesArray = selectedLeaves.trim().split(";");
			for(int i = 0; i< selectedLeavesArray.length; i++){
				if(selectedLeavesArray[i].trim().length()>0) {
					lefSelSet.add(selectedLeavesArray[i].trim());
				}
			}
		}
		if(select) {
			lefSelSet.add(leafName);
		}
		else {
			lefSelSet.remove(leafName);
		}
			
		// new value for selectedLeaves
		selectedLeaves = Joiner.on(";").join(lefSelSet); 
		
		// save in database 
		try {
			// Feb 2, 2012; in case tree_string contains some unacceptable strings
			// if only current userID is owner of the tree !! March 29, 2013 --
			PreparedStatement updateTree = getCon().prepareStatement(" UPDATE eview.trees set selectedleaves = ? WHERE ID = ?  ");
			updateTree.setString(1, selectedLeaves);
			updateTree.setInt(2, treeId); 

			System.out.println(" -- exec: " + updateTree.toString());
			updateTree.executeUpdate();
			updateTree.close(); 
		} catch (SQLException ex) {
			Logger.getLogger(DB_ops.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
		return true;
	}

}
