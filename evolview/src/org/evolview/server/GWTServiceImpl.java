/*******************************************************************************
 * Copyright 2011 Google Inc. All Rights Reserved.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package org.evolview.server;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.XMLAbstractTranscoder;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.transcoder.image.TIFFTranscoder;
import org.apache.fop.render.ps.EPSTranscoder;
import org.apache.fop.render.ps.PSTranscoder;
import org.apache.fop.svg.PDFTranscoder;
import org.evolview.client.IGWTService;
import org.evolview.shared.C4BProjectDto;
import org.evolview.shared.SignInStatus;
import org.evolview.shared.UserData;
import org.evolview.shared.UserDataProjectSkeleton; 
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class GWTServiceImpl extends RemoteServiceServlet implements IGWTService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private final int dpi = 900;

	/*
	 * May 15, 2011; load db and system configurations from a text file
	 */
	private String SERVERLET_REAL_PATH = "";
	private DB_ops db = null;
	private static final String PROPERTIES_FILE_SYSCONFS = "/WEB-INF/confs/db.conf"; // be careful
	private static final String PROPERTIES_FILE_SUPPORTED_BROWSERS = "/WEB-INF/confs/supported_browsers.conf";
	private static final String PROPERTIES_FILE_VERSION = "/WEB-INF/confs/version.properties";
	private Properties properties_sysconfs, properties_browsers;

	/**
	 * Feb 18, 2013 use java.properties to read conf files
	 */
	@Override
	public void init() throws ServletException {
		SERVERLET_REAL_PATH = this.getServletContext().getRealPath("/");

		// read system confs
		properties_sysconfs = new Properties();
		try {
			properties_sysconfs.load(new FileInputStream(SERVERLET_REAL_PATH + PROPERTIES_FILE_SYSCONFS));
		} catch (IOException ioe) {
			throw new ServletException(ioe);
		}

		// read supported browsers
		properties_browsers = new Properties();
		try {
			properties_browsers.load(new FileInputStream(SERVERLET_REAL_PATH + PROPERTIES_FILE_SUPPORTED_BROWSERS));
		} catch (IOException ioe) {
			throw new ServletException(ioe);
		}

		/**
		 * if only successful ...
		 */
		// Oct 14, 2011;
		db = new DB_ops(properties_sysconfs, SERVERLET_REAL_PATH);

		/**
		 * -- May 12, 2013 -- update 'user' table to
		 */
//		db.updateUserTable4SocialLogin();
	}

	/*
	 * == Oct 19, 2011 ==
	 * oct 27, 2011; load supported browsers from conf file
	 * Feb 11, 2013: use properties 
	 */
	public boolean isCurrentBrowserSupported() {
		// check if browser is supported
		boolean b = false;

		String userAgent = getThreadLocalRequest().getHeader("User-Agent");
		userAgent = escapeHtml(userAgent);

		// get current browser
		String browser = getBrowser(userAgent);
		float current_version = getBrowserVersion(userAgent, browser);

		// check if current browser is supported --
		if (properties_browsers.containsKey(browser)) {
			float supported_version = Float.parseFloat(properties_browsers.getProperty(browser));
			if (current_version >= supported_version) {
				b = true;
			}
		}

		// only do this if the user browser is supported
		if (b) {
			// Oct 28, 2011; keep tracking user ips
			// note: only keep tracking visits from independent ips; only one visit will be track within a day
			// june 1, 2013; stop tracking visits; will use google analytics instead ...
			// db.keepTrackingUserIPs(this.getThreadLocalRequest().getRemoteHost());
		}

		return b;
	}

	/*
	 * May 20, 2011; everytime a new build is done, the build number will be increased by 1
	 * Feb 18, 2013: get build number
	 */
	@Override
	public String getBuildNumber() {

		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(SERVERLET_REAL_PATH + PROPERTIES_FILE_VERSION));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties.contains("BUILD") ? properties.getProperty("BUILD") : "NA";
	} // getBuildNumber

	/*
	 * ==================== private funcdtions ===================
	 */
	/**
	 * Escape an html string. Escaping data received from the client helps to
	 * prevent cross-site script vulnerabilities.
	 * 
	 * @param html the html string to escape
	 * @return the escaped string
	 */
	private String escapeHtml(String html) {
		if (html == null) {
			return null;
		}
		return html.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}

	private String getBrowser(String ua) {
		String browser = "Other";
		if (ua.contains("MSIE")) {
			browser = "MSIE";
		} else if (ua.contains("Chrome")) {
			browser = "Chrome";
		} else if (ua.contains("Firefox")) {
			browser = "Firefox";
		} else if (ua.contains("Safari")) {
			browser = "Safari";
		} else if (ua.contains("Opera")) {
			browser = "Opera";
		}

		return browser;
	}

	// ver 1.1: Oct 25, 2011; 
	// ver 1.2; Oct 27, 2011; add support to MSIE 9
	private float getBrowserVersion(String ua, String browser) {
		float version = 0;

		if (browser.equalsIgnoreCase("opera") || browser.equalsIgnoreCase("safari")) {
			browser = "Version/";
		} else if (browser.equalsIgnoreCase("MSIE")) {
			browser = "MSIE ";
		} else {
			browser = browser + "/";
		}

		String matchedstring = "";
		Pattern p = Pattern.compile(browser + "([0-9]+\\.[0-9]+)");
		Matcher m = p.matcher(ua);
		if (m.find()) {
			matchedstring = m.group();
		}

		if (!matchedstring.isEmpty()) {
			matchedstring = matchedstring.replaceAll(browser, "");

			try {
				version = Float.parseFloat(matchedstring);
			} catch (NumberFormatException ex) {
				version = 0;
			}
		}
		return version;
	}

	// == Oct 20, 2011 ==
	@Override
	public String processSignIn(String email, String password) {
		return db.processSignIn(email, password);
	}

	@Override
	public boolean updateTreeCanvasInfoOnserver(String sessionid, int dBserialID, String key, float value) {
		return db.updateTreeCanvasInforOnServer(sessionid, dBserialID, key, value);
	}

	@Override
	public boolean changeProjectColor(String sessionID, String projectID, String hexcolor) {
		return db.changeProjectColor(sessionID, projectID, hexcolor);
	}

	@Override
	public ArrayList<C4BProjectDto> fetchCad4BioProjects() {
		return db.fetchCad4BioProjects();
	}
	
	/*
	 * May 16, 2011;
	 */
	@Override
	public ArrayList<UserDataProjectSkeleton> getProjectSkeletons(String session_id) {
		return db.getProjectSkeletons(session_id);
	}
 
	@Override
	public HashMap<String, String> getTreeStringAndFormatByDBSerialID(String sessionid, int treeDBSerial) {
		return db.getTreeStringAndFormatByDBSerialID(sessionid, treeDBSerial);
	}

	/*
	 * May 13, 2011
	 */
	@Override
	public HashMap<String, Float> getTreeCanvasInfor(String session_id, int tree_dbserial) {
		return db.getTreeCanvasInfo(session_id, tree_dbserial);
	}

	@Override
	public HashMap<String, Boolean> getCatPanelInfo(String session_id, int treeid) {
		return db.getCatPanelInfo(session_id, treeid);
	}

	@Override
	public ArrayList<UserData> getDatasets(String SessionID, int treeID) {
		ArrayList<UserData> datasets = null;
		// check if current session id is still valid
		System.out.println("get trees... Check Session Still Legal first");
		try {
			SignInStatus sin = db.checkSignInSstatus(SessionID);
			int userid = sin.getUserID();
			if (userid > -1) {
				System.out.println("Session is Still Legal, proceed to get datasets");
				datasets = db.getDatasets( treeID);
			}
		} catch (Exception e) {
			System.out.println("SignInStatus checkSessionIsStillLegal Error: ");
		}

		System.out.println("getdatasets: everything seems fine, return project data");
		return datasets;
	}

	@Override
	public Integer addTree(
			final String SessionID, 
			final int project_dbserial,
			final String treeName,
			final String treestring, 
			final String treeformat,
			final String leafType,
			final String traitsType,
			final boolean active)  {
		return db.addTree(SessionID, project_dbserial, treeName, treestring, treeformat,leafType, traitsType, active);
	}

	@Override
	public Integer deleteTree(String SessionID, int tree_dbserial) {
		return db.deleteTree(SessionID, tree_dbserial);
	}

	@Override
	public Boolean swapDatasetOrdersOnServer(String sessionID, int treeDBSerial, int dataset1, int dataset2, String datasettype) {
		return db.swapDatasetOrdersOnServer(sessionID, treeDBSerial, dataset1, dataset2, datasettype);
	}

	@Override
	public Boolean changeCatpanelActiveStatusOnServer(String sessionid, int treeDBSerial, String datasettype, boolean active) {
		return db.changeCatpanelActiveStatusOnServer(sessionid, treeDBSerial, datasettype, active);
	}

	@Override
	public Boolean changeDatasetActiveStatusOnServer(String sessionid, int treeDBSerial, String datasetname, String datasettype, Boolean active) {
		return db.changeDatasetActiveStatusOnServer(sessionid, treeDBSerial, datasetname, datasettype, active);
	}

	@Override
	public Boolean deleteDataset(String SessionID, int treeDBSerial, String datasetname, String datasettype) {
		return db.deleteDataset(SessionID,treeDBSerial, datasetname, datasettype);
	}

	@Override
	public Integer addDataset(String SessionID, int treeDBSerial, String datasetname, String datasetstring, String datasettype) {
		Integer idDataset = null;
		// check if current session id is still valid
		System.out.println("add dataset ... Check Session Still Legal first");
		try {
			SignInStatus sin = db.checkSignInSstatus(SessionID);
			int userid = sin.getUserID();
			if (userid > -1) {
				System.out.println("Session is Still Legal, proceed to get datasets");

				// then add/ update current dataset
				idDataset = db.addDataset(userid, treeDBSerial, datasetname, datasetstring, datasettype);

			}
		} catch (Exception e) {
			System.out.println("SignInStatus checkSessionIsStillLegal Error: ");
		}

		System.out.println("getdatasets: everything seems fine, return project data");
		return idDataset;
	}

	/*
	 * this is the necessary step 1;
	 */
	@Override
	public String exportTreeToSVG(String svgcontents) {
		/*
		 * first of all, write to SVG
		 */
		String temp_dir = this.SERVERLET_REAL_PATH + "/userfiles/";
		String prefix = Long.toHexString(Double.doubleToLongBits(Math.random()));
		String svgfile = temp_dir + prefix + ".svg"; // add directory information
		//String svgPretty = prettyPrinterXML(svgcontents);

		svgcontents = this.prettyPrintXML(svgcontents);

		// write svg file 
		try {
			FileWriter outfile = new FileWriter(svgfile);
			PrintWriter out = new PrintWriter(outfile);

			out.print(svgcontents);
			out.close();

		} catch (IOException e) {
		}

		// return 
		return prefix;
	}

	/*
	 * May 3, 2011; use batik as backend to do the exporting tree plots
	 * May 13, 2011; export image in the same size as SVG by using JPEGTranscoder.KEY_WIDTH/ KEY_HEIGHT
	 * -- April 20, 2012: now the size is set to be 1.25x the size of svg ( originally was 4x ) --
	 */
	@Override
	public String exportTreeImage(String svgfileprefix, String fileformat, float width, float height) {
		fileformat = fileformat.toLowerCase();
		String outdir = this.SERVERLET_REAL_PATH + "/userfiles/";

		String infile = outdir + svgfileprefix + ".svg";
		String outfile = outdir + svgfileprefix + "." + fileformat;

		Transcoder t = new PNGTranscoder();
		System.out.println(" -- svg transcoder --");

		// == April 20, 2012 ==
		width *= 1.25f;
		height *= 1.25f;

		if (fileformat.equalsIgnoreCase("pdf")) {
			t = new PDFTranscoder();
			t.addTranscodingHint(PDFTranscoder.KEY_WIDTH, width);
			t.addTranscodingHint(PDFTranscoder.KEY_HEIGHT, height);

			// Sep 10, 2013; draw text as text instead of linestoke --
			t.addTranscodingHint(XMLAbstractTranscoder.KEY_XML_PARSER_VALIDATING, Boolean.FALSE);
			t.addTranscodingHint(PDFTranscoder.KEY_STROKE_TEXT, Boolean.FALSE);

		} else if (fileformat.equalsIgnoreCase("ps")) { // not supported anymore
			t = new PSTranscoder();
			t.addTranscodingHint(PSTranscoder.KEY_WIDTH, width);
			t.addTranscodingHint(PSTranscoder.KEY_HEIGHT, height);
		} else if (fileformat.equalsIgnoreCase("eps")) { // nore supported anymore
			t = new EPSTranscoder();
			t.addTranscodingHint(EPSTranscoder.KEY_WIDTH, width);
			t.addTranscodingHint(EPSTranscoder.KEY_HEIGHT, height);
		} else if (fileformat.equalsIgnoreCase("tiff")) {
			t = new TIFFTranscoder();
			t.addTranscodingHint(TIFFTranscoder.KEY_BACKGROUND_COLOR, Color.white);
			t.addTranscodingHint(TIFFTranscoder.KEY_WIDTH, width);
			t.addTranscodingHint(TIFFTranscoder.KEY_HEIGHT, height);
		} else if (fileformat.equalsIgnoreCase("png")) {
			t = new PNGTranscoder();
			t.addTranscodingHint(PNGTranscoder.KEY_BACKGROUND_COLOR, Color.white);
			t.addTranscodingHint(PNGTranscoder.KEY_WIDTH, width);
			t.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, height);
		} else if (fileformat.equalsIgnoreCase("jpeg")) {
			t = new JPEGTranscoder();
			t.addTranscodingHint(JPEGTranscoder.KEY_BACKGROUND_COLOR, Color.white);
			t.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, width);
			t.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, height);
		}

		try {

			TranscoderInput input = new TranscoderInput(new File(infile).toURI().toString());
			System.out.println(" -- good 1 --");

			OutputStream ostream = new FileOutputStream(outfile);
			TranscoderOutput output = new TranscoderOutput(ostream);

			System.out.println(" -- xml string --");
			try {
				t.transcode(input, output);
			} catch (TranscoderException ex) {
				System.out.println(" -- fail 1 --");
				Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
			ostream.flush();
			ostream.close();

		} catch (IOException e) {
			System.out.println(" -- fail 4 --");
		}

		return "userfiles/" + svgfileprefix + "." + fileformat;
	}
	

	@Override
	public Boolean changeUserSettings(String sessionid, String key, String value) {
		return db.changeUserSettings(sessionid, key, value);
	}

	@Override
	public HashMap<String, String> getUserSettings(String sessionid) {
		return db.getUserSettings(sessionid);
	}
 
	@Override
	public Boolean changeTreeActiveStatus(String sessionid, int treeDBSerial) {
		return db.changeTreeActiveStatus(sessionid, treeDBSerial);
	}// Nov 08, 2011;

	@Override
	public String exporTreeToTextFile(String treestr, String fileformat) {
		String outfile_relpath = "userfiles/" + Long.toHexString(Double.doubleToLongBits(Math.random())) + "_" + fileformat + ".txt";
		String outfile_fullpath = this.SERVERLET_REAL_PATH + outfile_relpath;

		// pretty print
		if (fileformat.equalsIgnoreCase("xml") || fileformat.equalsIgnoreCase("phyloxml")) {
			treestr = this.prettyPrintXML(treestr);
		}

		try {
			FileWriter outfile = new FileWriter(outfile_fullpath);
			PrintWriter out = new PrintWriter(outfile);

			out.print(treestr);
			out.close();

		} catch (IOException e) {
		}

		return outfile_relpath;
	}

	private String prettyPrintXML(String xmlString) {
		String xmlPretty = xmlString;
		try {
			Source xmlInput = new StreamSource(new StringReader(xmlString));
			StreamResult xmlOutput = new StreamResult(new StringWriter());
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformer.transform(xmlInput, xmlOutput);
			xmlPretty = xmlOutput.getWriter().toString();
		} catch (TransformerException ex) {
			Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
		}
		return xmlPretty;
	} // prettyPrintXML
//
//	@Override
//	public ArrayList<VisitData> getVisits() {
//		return db.getVisits();
//	}
//
//	@Override
//	public Integer deleteEmptyProject(String sessionID, int project_dBserialID) {
//		return db.deleteEmptyProject(sessionID, project_dBserialID);
//	}

	/*
	 * *****************************************************
	 *   May 11, 2012
	 *   the following codes are used to retrieve pfam domains 
	 *   using protein names
	 * *****************************************************
	 */
	@Override
	public HashMap<String, ArrayList<String>> getProteinDomainFromPFAM(ArrayList<String> proteins) {

		// -- get protein domains from pfam --
		System.out.println(" ===== get protein domains from pfam ===== ");
		System.out.println("\t in total we have " + proteins.size() + " proteins ");
		HashMap<String, ArrayList<String>> pro2domains = new HashMap<String, ArrayList<String>>();
		for (String protein : proteins) {
			String al = getXMLfromURL("http://pfam.sanger.ac.uk/protein/" + protein + "?output=xml");
			if (!al.isEmpty()) {
				ArrayList<String> domains = parsePfamXML(al);
				pro2domains.put(protein, domains);
			}
		}

		System.out.println(" ===== get protein domains from pfam done !! ===== ");
		return pro2domains;
	}

	private String getXMLfromURL(String url) {
		StringBuilder al = new StringBuilder();
		try {
			URL u = new URL(url);
			BufferedReader in;
			try {
				in = new BufferedReader(new InputStreamReader(u.openStream()));

				String inputLine;
				while ((inputLine = in.readLine()) != null) {
					al.append(inputLine);
				}
				in.close();
			} catch (IOException ex) {
				Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		} catch (MalformedURLException ex) {
			Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
		}

		// a very simple validator for retrieved XML
		String xmlstring = al.subSequence(0, 5).toString();
		if (xmlstring.equalsIgnoreCase("<?xml")) {
			return al.toString();
		} else {
			return "";
		}
	}

	private ArrayList<String> parsePfamXML(String al) {
		ArrayList<String> domains = new ArrayList<String>();
		if (!al.isEmpty()) {
			try {
				String protein_length = "0";
				ArrayList<String> ds = new ArrayList<String>();

				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(new InputSource(new java.io.StringReader(al)));
				doc.getDocumentElement().normalize();

				NodeList nList = doc.getElementsByTagName("entry");
				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);
					NodeList nodes = nNode.getChildNodes();

					for (int i = 0; i < nodes.getLength(); i++) {
						Node node = nodes.item(i);

						/**
						 * ************************************************
						 * 'matches' - domains
						 * ************************************************
						 */
						String nodename = node.getNodeName().toLowerCase();
						if (nodename.equals("matches")) {
							NodeList matchNodes = node.getChildNodes();

							for (int j = 0; j < matchNodes.getLength(); j++) {
								Node matchNode = matchNodes.item(j);
								if (matchNode.getNodeName().equalsIgnoreCase("match")) {

									String accession = getTagValue(matchNode, "accession");
									String name = getTagValue(matchNode, "id");
									String source = getTagValue(matchNode, "type"); // type = pfam-A/ pfam-B
									String start = "";
									String end = "";
									String evalue = "";
									String bitscore = "";

									NodeList locNodes = matchNode.getChildNodes();
									for (int k = 0; k < locNodes.getLength(); k++) {
										Node loc = locNodes.item(k);
										if (loc.getNodeName().equalsIgnoreCase("location")) {
											start = getTagValue(loc, "start");
											end = getTagValue(loc, "end");
											evalue = getTagValue(loc, "evalue");
											bitscore = getTagValue(loc, "bitscore");
										}
									}

									// add to domains
									if (!start.isEmpty() && !end.isEmpty()) {
										//start,end,name,source,shape,color,accession,evalue,bitscore
										ds.add(start + "," + end + "," + name + "," + source + ",,," + accession + "," + evalue + "," + bitscore);
									}
								}
							}
						} else if (nodename.equals("sequence")) {
							/**
							 * ************************************************
							 * 'sequence'
							 * ************************************************
							 */
							protein_length = getTagValue(node, "length");
							//System.out.println("seq: " + getTagValue(node, "length") + " ; md5 " + getTagValue(node, "md5") + "; crc64" + getTagValue(node, "crc64") + "; version " + getTagValue(node, "version") + node.getFirstChild().getNodeValue());
						} else if (nodename.equals("taxonomy")) {
							//System.out.println("taxid: " + getTagValue(node, "tax_id") + " ; species_name " + getTagValue(node, "species_name"));
						}
					}
				}

				/*
				 * *****************************************************
				 *   at the end, assembl domains
				 * *****************************************************
				 */
				domains.add(protein_length);
				domains.addAll(ds);

			} catch (SAXException ex) {
				Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			} catch (ParserConfigurationException ex) {
				Logger.getLogger(GWTServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			System.out.println("the retrived string is unfortunately empty");
		}

		return domains;
	}

	// <<<<<<<<<<<<<<<<<<<<<<<<< get pfam domains by using protein names <<<<<<<<<<<<<<<<<<<<<

	/*
	 * ************************************************************
	 *  private functions used by other functions
	 * ************************************************************
	 */
	private static String getTagValue(Node node, String itemid) {
		String value = "";

		// get confidence; Jan 09, 2012
		if (node.getAttributes().getNamedItem(itemid) != null) {
			value = node.getAttributes().getNamedItem(itemid).getNodeValue();
		} else {
			NodeList dnodes = node.getChildNodes();
			for (int j = 0; j < dnodes.getLength(); j++) {
				Node dnode = dnodes.item(j);
				if (dnode.getNodeName().equalsIgnoreCase(itemid)) {
					value = dnode.getFirstChild().getNodeValue();
				}
			}
			// then case b
		} // Jan 09, 2012
		return (value);
	}

	@Override
	public Boolean changeTreeContents(String sessionid, int treeDBSerial, String treeString) {
		return db.changeTreeContents(sessionid, treeDBSerial, treeString);
	}

	@Override
	public SignInStatus checkSignInStatus(String sessionid) {
		return db.checkSignInSstatus(sessionid);
	}

	@Override
	public String signInAsTemporaryUser() {
		return db.signInAsTemporaryUser();
	}


	////////////////////////////////////  Leaf selection

	@Override
	public Boolean  treeLeafSelection( String sessionid, int treeId, String leafName ){
		return db.treeLeafSelection(sessionid, treeId,leafName );
	}

	@Override
	public Boolean  treeLeafUnSelection( String sessionid, int treeId, String leafName ){
		return db.treeLeafUnSelection(sessionid, treeId, leafName);
	}

}
