package org.evolview.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
    
public class DB_Conn {

    /**
     * Constructor
     * 
     */
    public DB_Conn() {
    }

    /**
     * @return Connection
     */
    protected static Connection getConn(String db, String user, String pass, String url) {
        Connection conn = null;
        
        // we use postgrsql at BGene
        // String driver = "com.mysql.jdbc.Driver";
        String driver = "org.postgresql.Driver";
        
        try {
            Class.forName(driver).newInstance();
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (ClassNotFoundException e) {
            System.err.println("Can't find driver:");
            System.err.println(e); 
        }
        try {
            conn = DriverManager.getConnection(url + db, user, pass);
        } catch (SQLException e) {
            System.err.println("Database Connection Error:");
            System.err.println(e); 
        }
        
        System.out.println(" ==> con is " + conn == null );
        
        return conn;
    }

    /**
     * Get row count from resultset
     * @param Resultset
     * @return size
     */
    protected static int getResultSetSize(ResultSet resultSet) {
        int size = -1;
        try {
            resultSet.last();
            size = resultSet.getRow();
            resultSet.beforeFirst();
        } catch (SQLException e) {
            return size;
        }
        return size;
    }

    /**
     * get a unix time stamp for current day
     */
    protected static int getUnixTimeStamp() {
        Date date = new Date();
        int TimeStamp = (int) (date.getTime() * .001);
        return TimeStamp;
    }
}