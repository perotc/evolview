package org.evolview.server.OAuthServerUtils.util;

import java.util.UUID;

import argo.format.JsonFormatter;
import argo.format.PrettyJsonFormatter;
import argo.jdom.JdomParser;
import argo.jdom.JsonRootNode;

public class OAuthKeySecretsEGAmazon {
	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * Sep 7, 2013 Now the redirect URL is : http://www.evolgenius.info/evolview/
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	
	/**
	 * Chinese / eastern media, to be supported 
	 */
	/**
	 * SINAWEIBO
	 */
	public static final String SINAWEIBO_APP_NAME = "evolviewTest";
	public static final String SINAWEIBO_APP_ID = "149819808536998";
	public static final String SINAWEIBO_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	/**
	 * MEETUP
	 */
	public static final String MEETUP_APP_NAME = "evolviewTest";
	public static final String MEETUP_APP_ID = "149819808536998";
	public static final String MEETUP_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	/**
	 * SOHUWEIBO
	 */
	public static final String SOHUWEIBO_APP_NAME = "evolviewTest";
	public static final String SOHUWEIBO_APP_ID = "149819808536998";
	public static final String SOHUWEIBO_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	
	/**
	 * QQWEIBO
	 */
	public static final String QQWEIBO_APP_NAME = "evolviewTest";
	public static final String QQWEIBO_APP_ID = "149819808536998";
	public static final String QQWEIBO_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	/**
	 * NETEASEWEIBO
	 */
	public static final String NETEASEWEIBO_APP_NAME = "evolviewTest";
	public static final String NETEASEWEIBO_APP_ID = "149819808536998";
	public static final String NETEASEWEIBO_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	/**
	 * KAIXIN001
	 */
	public static final String KAIXIN001_APP_NAME = "evolviewTest";
	public static final String KAIXIN001_APP_ID = "149819808536998";
	public static final String KAIXIN001_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	
	/**
	 * RENREN
	 */
	public static final String RENREN_APP_NAME = "evolviewTest";
	public static final String RENREN_APP_ID = "149819808536998";
	public static final String RENREN_APP_SECRET = "c57b02877370ba0dd4d71d27473bcfc3";
	
	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * western meida 
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	/**
	* ## /** Facebook June 1, 2013 ##
	* http://developers.facebook.com/docs/facebook-login/getting-started-web/
	* https://developers.facebook.com/apps/
	* ## chenwh550@gmail.com / evolgenius.team@googlemail.com ## ## --
	* Sep 7, 2013 --
	*/
	public static final String FACEBOOK_APP_NAME = "EvolviewEGTomcat"; 
	public static final String FACEBOOK_APP_ID = "520923831310638";
	public static final String FACEBOOK_APP_SECRET = "34763a1000d1c751ed82a9628e99974d";
	/**
	 * Facebook ends --
	 */
	/**
	 * #	* -- Google starts May 31, 2013 -- #	*
	 * https://code.google.com/apis/console #	* using
	 * evolgenius.team@googlemail.com ## june 1, 2013 :
	 * http://www.evolgenius.info/evolview/
	 * Sep 7, 2013 --
	 */
	public static final String GOOGLE_APP_NAME = "evolview";
	public final static String GOOGLE_APP_ID = "520682346365-aqav7b27cere0lvfmiuvmrocvnuqlblc.apps.googleusercontent.com";
	public final static String GOOGLE_APP_SECRET = "vDCTBiqYYKqGVedu_b9FheXG";
	public final static String GOOGLE_SCOPE = "https://www.googleapis.com/auth/userinfo.profile";
	/**
	 * Google ends --
	 */
	/**
	 * #/** Twitter : may 31, 2013 -- #	* registered using email :
	 * evolgenius.team@googlemail.com #	* http://dev.twitter.com/apps/new
	 * Sep 7, 2013 --
	 */
		public static final String TWITTER_APP_NAME = "EvolviewEGTomcat";
		public final static String TWITTER_APP_ID = "O4GKGLTAu0PRJY4xujNqag";
		public final static String TWITTER_APP_SECRET = "RhSpYf5R04pfINfJyd7paZSeZFSnk6loGQAcYJjkvLc";
		
//	public static final String TWITTER_APP_NAME = "evolviewLocalHost";
//	public final static String TWITTER_APP_ID = "P3JXUjfriPP9ERyNLRcB1g";
//	public final static String TWITTER_APP_SECRET = "RZVges6Ma4irVrABhu06Q6PQqO2WjaegLR21ft82as";

	/**
	 * Twitter ends --
	 */
	/**
	 * # Linkedin starts may 31, 2013 # evolgenius.team@googlemail.com #
	 * https://www.linkedin.com/secure/developer
	 * Sep 7, 2013 --
	 */
	public static final String LINKEDIN_APP_NAME = "EvolviewEGTomcat";
	public final static String LINKEDIN_APP_ID = "ylk7v73z0hsx";
	public final static String LINKEDIN_APP_SECRET = "jLZylbNYNlP18iMr";
	public final static String LINKEDIN_USER_TOKEN = "99dc3e45-0885-4934-b696-27b8e7c0dcc3";
	public final static String LINKEDIN_USER_SECRET = "187552bd-d6de-47a7-858d-a3e2d744ca6c";
	/**
	 * Linkedin ends --
	 */
	/**
	 * #/** github starts may 31, 2013 #	* username : evolgenius-team #	* email
	 * : evolgenius.team@googlemail.com #	*
	 * http://developer.github.com/v3/oauth/
	 * https://github.com/settings/applications
	 * Sep 3, 2013 --
	 */
	public static final String GITHUB_APP_NAME = "EvolviewEGTomcat";
	public final static String GITHUB_APP_ID = "dcde899df89fbf523ec0";
	public final static String GITHUB_APP_SECRET = "f5a74fadc17e6ac94fc9cc6f99669d3bfd3defb4";
	/**
	 * github starts --
	 */
	/**
	 * #/** flickr may 31, 2013 # http://www.flickr.com/services/apps/create/
	 * email: evolgenius.team@yahoo.com
	 * Sep 7, 2013 --
	 * 
	 */
	public static final String FLICKR_APP_NAME = "EvolviewEGTomcat";
	public final static String FLICKR_APP_ID = "33508b9bf4d9e5f1e84f782ac327888d";
	public final static String FLICKR_APP_SECRET = "59c0e26098c7aa59";
	
//	public static final String FLICKR_APP_NAME = "evolviewTest";
//	public final static String FLICKR_APP_ID = "d44dc68354fdbab62d9c1ae9651beb3b";
//	public final static String FLICKR_APP_SECRET = "a623819b4d6d52a5";
//	
	/**
	 * #/** vimeo may 31, 2013 #	* username : evolgenius team #	* email :
	 * evolgenius.team@googlemail.com #	*
	 * https://developer.vimeo.com/apis/advanced
	 * Sep 7, 2013 
	 */
	public static final String VIMEO_APP_NAME = "EvolviewEGTomcat";
	public final static String VIMEO_APP_ID = "1a27ea24db9bdb4a4fb304bb0813692ba3feed61";
	public final static String VIMEO_APP_SECRET = "9e7fe2143e85d131a0515b66d967eae7f549a65a";
	
//	public static final String VIMEO_APP_NAME = "evolviewTest";
//	public final static String VIMEO_APP_ID = "e648aa03aeb458fcfa8ddb2a50fbbdb6bc50bd2c";
//	public final static String VIMEO_APP_SECRET = "a4e14ecaba606fba7175f75250a476895eae947b";
//	
	
	/**
	 * Windows Live starts Dec-23-2012
	 */
	/**
	 * #	* Windows Live may 31, 2013 -- #	* evolgenius.team@live.com #	*
	 * https://account.live.com/developers/applications
	 * 
	 * Sep 2013
	 *
	 */
	public static final String WINDOWS_LIVE_APP_NAME = "evolview";
	public final static String WINDOWS_LIVE_APP_ID = "00000000400FADB9";
	public final static String WINDOWS_LIVE_APP_SECRET = "2gDiddew0bT3bITtAGmuYieWYgjkwvG2";
	
//	public static final String WINDOWS_LIVE_APP_NAME = "evolviewTest";
//	public final static String WINDOWS_LIVE_APP_ID = "00000000480FA51E";
//	public final static String WINDOWS_LIVE_APP_SECRET = "Wbf6mOrYpAvg4SMet9nRofVAGNXRR-6-";
//	
	/* #/* foursquare starts june 1, 2013 
	 # * https://developer.foursquare.com/overview/auth
	 # * email : evolgenius.team@googlemail.com 
	 
	 Sep 7, 2013 --
	 
	 */
	public static final String FOURSQUARE_APP_NAME = "EvolviewEGTomcat";
	public final static String FOURSQUARE_APP_ID = "Q51BR3MLW35FAQ5SPWSCO5JQJUN5F0PT203QPSJ0O5WDJRED";
	public final static String FOURSQUARE_APP_SECRET = "04YTS0USGJPWLNHLDRIVFAWX1KNRYWCTIMBPO0QGWAXEKWHI";

//	public static final String FOURSQUARE_APP_NAME = "evolviewTest";
//	public final static String FOURSQUARE_APP_ID = "QMLPYEGWXOKUXQ2PQTRWOEGQISNFEKGVIXBI1LYC1EQEMRVE";
//	public final static String FOURSQUARE_APP_SECRET = "JDKUBIAGZMYJ4X115KSM0Y23ZONLRTN1L4APIVL1BGXKSVER";

	/**
	 * Yahoo! starts June 13, 2013 --
	 */
	/**http://developer.yahoo.com/oauth/; email: evolgenius.team@yahoo.com
	 * NOTE about stupid Yahoo: when creating the app, at least one api must be
	 * selected *
	 */
	public static final String YAHOO_APP_NAME = "EvolviewEGTomcat";
	public final static String YAHOO_APP_ID = "dj0yJmk9aTVkTUgxUG9YZ1hxJmQ9WVdrOVFtZDVaWE14Tm04bWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD03OQ--";
	public final static String YAHOO_APP_SECRET = "cd63a79e7bc821455761f744572c85d3e01a3c82";

	/**
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 * June 12, 2013 : the following are either not supported or yet to
	 * implement --
	 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	 */
	/**
	 * Imagur starts Dec-08-2012 - their cert is invalid -
	 */
	public static final String IMGUR_APP_NAME = "evolview";
	public final static String IMGUR_APP_ID = "5de89977fe51b0497fd485f656f2215c050c3be63";
	public final static String IMGUR_APP_SECRET = "105e221a68f6c7fd1a740b158e7c6ab3";
	/**
	 * Evernote starts Dec-23-2012 Not supported due to their sandbox
	 * restriction
	 */
	public static final String EVERNOTE_APP_NAME = "evolview";
	public final static String EVERNOTE_APP_ID = "oauthdemo2012";
	public final static String EVERNOTE_APP_SECRET = "05aae97837ab74ee";
	/**
	 * Evernote ends Dec-23-2012
	 */
	public static final String INSTAGRAM_APP_NAME = "evolview";
	public final static String INSTAGRAM_APP_ID = "a72e67f10c334edc95d21f5624490526";
	public final static String INSTAGRAM_APP_SECRET = "4d29065ebb6542f592a7957f6d0686cb";
	/**
	 * Instagram ends --
	 */
	/**
	 * tumblr starts Dec-23-2012
	 */
	public static final String TUMBLR_LIVE_APP_NAME = "evolview";
	public final static String TUMBLR_LIVE_APP_ID = "cmoxXXFqWm9wwkxF1MRSLW8IUseVjZndAj3Q0CkZJfJ5eZvZVa";
	public final static String TUMBLR_LIVE_APP_SECRET = "ZB40SOPKLZbVLSxwpK7cqvt3QlTXJlkL2l7P52ayXhAuoNu5Xl";
	/**
	 * tumblr ends Dec-23-2012
	 */

	/**
	 * functions ...
	 * @return 
	 */
	public static String makeRandomString() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	/**
	 * Pretty print json string.
	 *
	 * @param uglyJson
	 * @return prettyfied json if possible, ugly one otherwise I found Argo is
	 * the only one which can format a json string the way I want. Argo:
	 * http://argo.sourceforge.net/documentation.html
	 */
	public static String prettyPrintJsonString(String uglyJson) {
		try {
			JsonRootNode jsonRootNode = new JdomParser().parse(uglyJson);
			JsonFormatter jsonFormatter = new PrettyJsonFormatter();
			String prettyJson = jsonFormatter.format(jsonRootNode);
			return prettyJson;
		} catch (Exception e) {
			return uglyJson;
		}
	}
}
